# UniMI Master CS

TODO: add a repo description

## Contributing rules

- The official language is English
- The markup language to use universally throughout the repo is [Markdown](https://daringfireball.net/projects/markdown). [Here's a handy guide](https://docs.gitlab.com/ee/user/markdown.html).
  - To add emphasis you can also use Markdown inside code comments
- Each subject has its own folder
  - For each subject there must be two folders: `notes` and `exercises`
  - Notes are shared per subject, no own version allowed (except in separate branches, but they are not authoritative). Notes can be edited by everyone to improve them. Refrain on deleting big chunks of text, if unsure open an issue.
  - There must be **one** authoritative source code per exercise (in a folder called `common`). Each participant can have his own unadulterated code in a separate folder called after their gitlab username.
- Errors, TODOs, incomplete stuff or other kind of reports must be handled through the [issue tracker](https://gitlab.com/gabmus/unimi-master-cs/issues) referencing the file and line (when applicable) where the problem lies
- **Effing up the repo will revoke your push rights until further notice**
- No binary files allowed (including, but not limited to: PDFs, compiled binaries, various bytecode)
- No links to untrustworthy websites, download sites, pirate sites, adult content, or otherwise inappropriate stuff
- Use **meaningful** commit messages. Commits with meaningless or unintelligible messages will be reverted without warning.
- **Don't make huge commits!** Each commit should be a granular change to one isolated area of the repo. IE: adding info to an existing note and adding new notes for new lessons **cannot** be in the same commit
