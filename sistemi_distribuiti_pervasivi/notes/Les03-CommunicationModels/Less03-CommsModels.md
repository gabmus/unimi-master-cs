# Communication Models

There are various communication models distributed systems implement to let its components communicate. 

Particularly we're gonna see

- Types of communication, distinguishing between persistent/transient and synchronous/asynchronous:
  - Transient message-oriented communications: Sockets;
  - Persistent message-oriented communications: Queuing system.
- Remote Procedure Calls (RPC) and Remote Method Invocation (RMI);
- Communication of data streams.

## Middleware Protocols

As we know, communication is essential to coordinate the hosts and make the distributed systems work correctly. 

To do so, we need to work with the usual network communication stack (TCP/IP) and **add another abstraction layer** to implement our **middleware protocol**. This level is just below the application level of the standard ISO-OSI model.

Middleware must take some load off the application layer, while ensuring transparency properties to hide all the complexities. Because of this, the **middleware layer** is located between the application and the transport protocol. 

All the communication and message protocols we're gonna see are based on the **transport protocol** (TCP mostly, but some UDP) in order to work and deliver messages through the distributed system.

<img src="img/midprot.png" style="zoom:67%;" />

## Communication with Middleware

We can see the middleware as an intermediate (distributed) service in application-level communication:

<img src="img/midsw.png" style="zoom:67%;" />

Let's see how client and server act in this environment

- **Client request**
  - The client makes a request to the server, but there is some latency, because communication isn't instantaneous (that's why the arrow is oblique).
- **Server handling request**
  - The server, which was sleeping (dashed line) now is active (bold continuous line): he received a request and is dispatching resources in order to fulfill it.
- **Server replying**
  - The server replies to the client, giving him what requested (or not if not possible).

There is some synchronization done between client and middleware: This is useful because it gives information in real time about the request that has been made.

- **Request submission sync**
  - As soon as client send the request, he gets notified that his submission has been received by the middleware.
- **Request delivery sync**
  - As soon as the middleware forwards the request to the server, the client gets notified about this.
- **Reply sync**
  - Middleware syncs with the client when the request is completed by the server.

All of these intermediate steps are cached by the middleware in **storage facilities**, in order to keep a copy and retry the sync if not successful.

## Persistence and Synchronization

Back in the days, the **Pony Express mail service** was a model where a user wrote a letter and deposited it in a post office, from that office a raider carried that mail to another post office from where another raider took care of the mail to carry away and so on until it reaches its destination.

<img src="img/pony.png" style="zoom:67%;" />

In our terms, we can see that each message is "cached" through each intermediate node between the sender and the receiver. This means that the post-office is a **persistent communication system**, because each mail is memorized in each post office it passes through, until the receiver is met.

If our message has to be directly delivered to a user and it comes back to the sender if the target user is not available and can't take it, we're having a **transient system**.

So let's define these terms properly:

- **Persistent**: A communication system is called persistent if there is caching and memorization of messages passing through the intermediate nodes (those between sender and receiver). This way if the receiver can't be met (temporarily), the message can wait in the closer "post-office" and retry later;
- **Transient**: A communication system is called transient if there is NO caching and memorization of messages in between the sender and receiver. If the receiver can't be met, the message returns to the sender.

We also have to distinguish between synchronous and asynchronous communication:

- **Synchronous**: The sender awaits for the response of the receiver in order to continue its work (maybe because he needs it for further computation or reach his goal);
- **Asynchronous**: The sender sends the message, but doesn't await a response from the receiver.

Let's see now how these terms combine.

### Persistent Asynchronous Communication

<img src="img/pasync.png" style="zoom:67%;" />

In this model the sender **A doesn't care about knowing that B received the message** or not: he delivers the message but then he continues to process its things until he finishes. 

B is not running right now but since this system is also persistent there is no problem: the message can be stored in an intermediate middleware node until B wakes up and receives it.

### Persistent Synchronous Communication

<img src="img/psync.png" style="zoom:67%;" />

In this case, instead the **sender halts its execution** until he knows that the receiver obtained it correctly.

B's location has a persistent system that can hold the message, so even if the process is not running he will store it until the process B executes. The persistent system also has to notify the sender A that he received the message correctly

### Transient Asynchronous Communication

<img src="img/trasync.png" style="zoom:67%;" />

Sender **A sends the message without waiting** to know that B received it. However if B wasn't active, there wouldn't have been any memorization system to hold it, because this is a transient system.

### Transient Synchronous Communication

For synchronous transient communication we'll see **three methods** to achieve it, depending on the type of feedback the sender A expects from receiver B:

- Receipt-based;
- Delivery-based;
- Response-based.

### Receipt-Based Transient Synchronous Communication

<img src="img/trsync.png" style="zoom:67%;" />

In this method, sender A just needs to **know that receiver B acquired the message**: because of this A waits for an ACK (acknowledgment) from B. B sends the ACK as soon as he receives the message, even if he's doing something else.

### Delivery-Based Transient Synchronous Communication

<img src="img/tdsync.png" style="zoom:67%;" />

In this method, sender A needs to know that receiver **B acquired the message and that he's going to work on it**: because of this A waits for an ACK (acknowledgment) from B. B sends the ACK as soon as he processes the request.

### Response-Based Transient Synchronous Communication

<img src="img/trpsync.png" style="zoom:67%;" />

In this method, sender A needs to know that receiver **B acquired the message and has worked on his request**: because of this A waits for an ACK (acknowledgment) from B. B sends the ACK together with the result of the request.

## Communication Models: Berkley Sockets

Sockets are the low-level, **message-oriented transient communication with two endpoints**: The client and the server (two hosts). Intuitively, we can imagine the socket communication model as two electrical sockets, one on host A, one on host B, connected by a cable: through that cable and those sockets, the hosts can communicate through messages.

Sockets are based on the **transport protocol** and ports have an unique identifier in each host (because each process has an unique PID and each process has its own port). If we have to communicate with host B, it's sufficient to open a socket in both hosts processes and read/write data on the socket just as it was on a file or on standard output.

We're going to see the base Berkeley Socket model and their primitives, which have their own implementation various languages

- **Socket**: Create a new communication endpoint on a host. We need two of those, one for each end point;
- **Bind**: Attach a local address on the host to a socket;
- **Listen**: Announce the willingness to accept connections. If a queue is prepared, all requests waiting to be accepted will be placed there;
- **Accept**: Block caller until a connection request arrives. If there are already requests in the queue, accepts the first one:
- **Connect**: Actively attempt to establish a connection with the other host;
- **Send** (Write): Send some data over the connection established;
- **Receive** (Read): Receive some data over the connection established;
- **Close**: Release and close the connection with the other host.

*Example of typical connection-oriented communication pattern with sockets*

<img src="img/socket.png" style="zoom:67%;" />

Remember that this kind of communication is **transient**, which means that both processes must be running at the same time on both machines (client and server) in order to be able to communicate.

## Communication Models: Queuing Systems

<img src="img/qsys.png" style="zoom:67%;" />

Queuing systems are **message-oriented persistent communications**. In many use cases, persistence is a preferable quality to have, so, in order to satisfy this need, **queuing systems lets messages pass through a queuing layer in which they're stored if the receiver is not reachable**. 

Physical mails and e-mails follow this model: the mails are stored in a safe space waiting for us to reach for them and read them. 

*General organization of a message-queuing system with routers.*

<img src="img/qsysrout.png" style="zoom: 67%;" />

This is implemented **putting queues also in the intermediate nodes** (routers) between the sender and the receiver, to grant consistence if a host goes down. Other than guaranteeing the persistence, it offers other bonuses such as better management of traffic and congestion.

In queuing systems, general primitive functions are:

- **Put**: Append a message to a specified queue;
- **Get**: Block until the specified queue is nonempty, and remove the first message;
- **Poll**: Check a specified queue for messages, and remove the first. Doesn't block;
- **Notify**: Install a handler to be called when a message is put into the specified queue;

Remember: `read`, `get` and similar operations are blocking, which means that the server can only do them until he finished executing them.

## Message Brokers

What if the hosts have different OSes, different architectures and other discrepancies which make the communication impracticable? 

I could implement, in an intermediate node, a **message broker**, which is a **middleware program which acts like a storage for messages between hosts**. It handles multiples queues and sorts messages between them following a given algorithm and set of rules, "translating" them for the relative receiving host.

<img src="img/messbrok.png" style="zoom:67%;" />

For example, we could see a Source client sending a message (in Italian) to the Destination (which is Chinese). The message broker has a broken program which can translate the message for the receiver (e.g. Google Translate), other than sorting it in the right queue.

### When Queues Are Worth Being Considered? 

Message queuing systems are considered worth using when:

- Asynchronous communication is preferable (ex. e-mails) when I don't have the temporal need of a quick answer by the server (e.g. Website request);
- Scalability can be increased by admitting delays in requests and responses, and since it is an asynchronous environment, delays are not a big hassle;
- The producer is faster than the consumer. When the data producer is faster than the consumer, a queuing system is logical to save the overflowing messages in a buffer(queue) to be read later;
- A publish-subscribe communication pattern is present. Like RSS, publish-subscribe model lets creators of content reach its readers by publishing it. To access it and getting it from his own queue, a host need to subscribe to the service.

## Java Messaging Service (JMS)

An implementation of this system is the **Java Messaging Service JMS**, which is an API introduced in Java framework, that supports point-to-point as well as publish-subscribe asynchronous communication models, and has a reliable delivery (ACK mechanism).

JMS is notoriously useful for enterprise application integration. In this environment, usually all of its components are developed separately, with various languages and on various types of machines, and so on... 

<img src="img/jmsent.png" style="zoom:67%;" />

For this reason, JMS acts as an uniform API to exchange data between application elements through messages, adapting and translating data in a common format understandable by all the hosts in an heterogeneous network.

## Communication Models: Common Protocols

Other messaging protocols are the following:

- **XMPP** (Extensible Messaging and Presence Protocol): Free and open, mostly used for instant messaging (used by Facebook till 2014 and many others);
- **MQTT** (Message Queue Telemetry Transport): Lightweight messaging protocol with publish/subscribe model for constrained devices (commonly used by IOT and M2M);
- **AMQP** (Advanced Message Queuing Protocol): General-purpose messaging protocol suitable for a broad range of messaging-middleware infrastructures, and also for peer-to-peer data transfer.

## Protocol Example: RabbitMQ

RabbitMQ is a multi-language open-source implementation of AMQP (and other protocols). It is well regarded in his environment, even by Google.

It consists of
- A Broker (server written in Erlang) usually hosted in cloud;
- Clients, available in many languages;
-  Clients communicate with the broker through AMQP

<img src="img/rabbmq.png" style="zoom:50%;" />

**N.B.** For more info, read the paper given by the professor.

# Remote Procedure Call

**Sending messages** in a distributed system is useful, of course, but what if I want to use it **for computations**? In this case our distributed system (DS) is considered as a DCE (Distributed Computing Environment).

For example, let's say that a machine in our DCE has an efficient implementation of matrix multiplication and I want to use it in my program, instead of the local implementation.

As we saw before, we could send a message to that machine and ask for the calculation and then retrieve it from her. However this doesn't respect the transparency standards we would expect from a distributed system. We would prefer to **call and use that efficient method LIKE it was on our machine** (even if it isn't). 

For this purpose, remote procedure calls were created.

## Conventional Procedure Call

Conventionally, when a method, procedure or function is called, all the information about it (parameters, local variables and values) are saved on the stack. However, since we're now working with a remote machine, how the parameter passing and return value should be handled? Should they be passed for reference or value?

<img src="img/rpcconv.png" style="zoom:50%;" />

Of course, since we're talking about a remote machine, **memory addresses and their content are surely different from the local machine**, so passing by reference is not an option: we're left only with passing by value.

For this reason, we should use an efficient serialization mechanism in order to marshalling/unmarshalling the values and send them through the network.

## Client and Server Stubs

To implement the RPC, there are two entities called client and server stubs, which are portions of code that serve as a middleware to make this happen. 

RPC generally follow a transient synchronized communication model:

<img src="img/rpcstub.png" style="zoom:67%;" />

Steps of a remote procedure call:

1. Client procedure calls client stub in normal way;
2. Client stub builds message, calls local OS;
3. Client's OS sends message to remote OS;
4. Remote OS gives message to server stub;
5. Server stub unpacks parameters, calls server;
6. Server does work, returns result to the stub;
7. Server stub packs it in message, calls local OS;
8. Server's OS sends message to client's OS;
9. Client's OS gives message to client stub;
10. Stub unpacks result, returns to client.

Let's see this more in detail:

![](img/rpcsteps.png)

As we can see the packet going through the network with the procedure name and the parameter values is the product of a marshalling operation:

- **Parameter marshalling**: Procedure to format RPC parameters in a message;
- **Parameter unmarshalling**: Inverse procedure to extract RPC parameters from a message.

As we said marshalling requires a serialization of the objects, either in binary or in strings (XML, JSON).

<img src="img/valconv.png" style="zoom:67%;" />

We always need to keep in mind that we're working with an heterogeneous network, so when marshalling we have to consider how each machine handles values (big endian, little endian) and translate them with the appropriate representation.

## Synchronous vs Asynchronous RPC

While the RPC standard uses a synchronous communication model, other frameworks also offer an asynchronous one, available to use. 

For example, when we need to execute a method remotely but don't need the result right in that moment but in the future.

<img src="img/rpcasync.png" style="zoom:67%;" />

The client is not blocked until the result of the procedure is received, but only until he knows that the server received the request. When the result is ready, the server sends the return value of the procedure to the client, blocking it for a while in order to let him unmarshal the data for later use. Lastly, the client sends an ACK to the server to let him know that everything was received.

This model is implemented with two asynchronous RPCs:

- The first, made by the client, passing the name of the function and the parameters;
- The second, made by the server, passing the result of the procedure once it is executed.

So RPC is both synchronous and asynchronous basing off its use.

## Writing a Client and a Server

The steps in writing a client and a server in DCE RPC are the following:

<img src="img/clientserver.png" style="zoom:120%;" />

Three files output by the IDL compiler:

- A header file (ex. `interface.h`, in C terms);
- The client stub;
- The server stub.

After writing a client and a server it's important a binding operation between them, that's why there is **the registration of a server** that makes it possible for a client to locate the server and bind to it.

Servers which offer procedures need to be registered in some way to be **easily localized and bound by the clients** needing them.

This localization is done in two steps:

- Locate the server's machine;
- Locate the server process on that machine.

Let's see the process of localization of a procedure by a client:

<img src="img/findrpc.png" style="zoom:67%;" />

1. The server process register its endpoint (port) with the help of a DCE daemon, holding in a table all the endpoints of that machine;
2. The server process register the service it offers in a directory server, which holds all the services offered in that network;
3. When a client needs a particular service (a procedure, for example) he looks up for it in the directory server;
4. Once found the server's machine that holds the needed service, the client process asks for an endpoint for it to the DCE daemon on that same machine. The daemon checks if the requested process exists in the endpoint table and if it is running: if it's not running then it creates a new instance of it;
5. Once the endpoint is bound, the client process is able to communicate with the server process and is able to use RPCs;

### A Modern RPC Example: gRPC

- Open source remote procedure call (RPC) framework;
- Largely used to build low latency, scalable distributed systems;
- Supporting many languages (C/C++,Java, Go, Python, ...);
- Originally developed by Google;
- https://grpc.io/

# Remote Object Invocation

Many systems take advantage of the client-server model to call and use object that are not in our current machine but lays on a remote server, those are called **remote objects**. Common organization of a remote object with a client-side proxy:

<img src="img/proxy.png" style="zoom:120%;" />

## An Example: Java RMI

An example of remote object invocation mechanism is given by **Java RMI (Remote Method Invocation)**, an old implementation of the RPC model into Java but with a twist: Instead of binding a client to a remote procedure, it is bound instead to an entire Object. This way, all of his methods can be accessed remotely.

## Data Stream Communication

**A data stream is a sequence of coherent data packets being transmitted from a source**. Examples of data stream composing are audio/video streams or, more simply, constant information coming from a physical sensor (accelerometer, motion sensor and so on...).

The **temporal distance between the data packets in a stream is usually relevant** (ex. a long delay between two video frames would degrade the user experience in watching a video).

The goal is to transfer the data packets satisfying given temporal constraints, but not all the streams have the same temporal constraints!

Typical time constraints in data streams that need to be satisfied, although it depends on the type of streaming:

- **Asynchronous transmission**: Data packets are transmitted in sequence without particular time constraints (ex. a still image);
- **Synchronous transmission**: Data packets are transmitted in sequence with a maximum end-to-end delay (ex. data from a sensor);
- **Isochronous transmission**: Data packets are transmitted in sequence with both a maximum and a minimum end-to-end delay (to ensure an equal and constant packet communication for audio/video streaming).

## Quality of Service (QoS)

Even if we are working on a middleware level, up just before the transport level, we can still enforce QoS (Quality of Service) in our data streaming by different means, for example, we can use a buffer to reduce packet jitter and packet loss, but also some interpolation techniques between audio/video frames for entirely missing packets. This is just some band-aid but it's better than nothing.

Properties to establish a Quality of Service:

- Required bit rate at which data should be transported;
- Maximum delay until a session has been set up;
- Maximum end-to-end delay;
- Maximum delay variance, or jitter;
- Maximum round-trip delay.

*A general architecture for streaming stored multimedia data over a network.*

<img src="img/qosservice.png" style="zoom:110%;" />

## Enforcing QoS

Using a buffer to reduce jitter

<img src="img/qosbuffer.png" style="zoom:120%;" />

The effect of packet loss in:

- (a) Non interleaved transmission;
- (b) Interleaved transmission.

<img src="img/qospackloss.png" style="zoom:100%;" />

Instead of just passing the data to the video and audio devices raw, we hold it in our middleware, process it until it is usable (following the quality standards we set) and only then we forward it to the devices.

*The principle of explicit synchronization on the level data units.*

<img src="img/streammid2.png" style="zoom:100%;" />

*The principle of synchronization as supported by high-level interfaces.*

![](img/streammid.png)

