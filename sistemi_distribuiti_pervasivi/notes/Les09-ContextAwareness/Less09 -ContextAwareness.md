# Context Awareness

## Understanding Context

Traditional software applications cannot understand the "context" of a request. User must make explicit all the request parameters (ex. “What’s the best route to go from X to Y, by car, considering that it’s rush hour and that I hate highways?” ). 

Communicating with a computer is hard because we have limited interfaces (especially from mobile devices). For this reason, we need automatic mechanisms to acquire and transmit the context of our request. 

## Defining Context

We have **different interpretations** in psychology, philosophy and computer science for "context". 
The vocabulary defines "context" as "The set of circumstances or facts that surround a particular event, situation.". In our case the event is a mobile service request. 

Many statements make **sense only within their context**. For example in the question “how do I get there?” from the context it may be clear:

1. Where the user is;
2. Where he wants to go;
3. How (on foot, car or public transportation or anything else).

Many definition have been proposed for computer science application:

1. Location, time, surrounding people, season, temperature;
2. Location, environment, identity and time;
3. Mood, level of attention, location, time, surrounding objects and people.

A common definition was given by Anind Dey (Carnegie Mellow University):

> “Context is any information that can be used to characterize the situation of an entity. An entity is a person, place, or object that is considered relevant to the interaction between a user and an application, including the user and applications themselves.”

That can be simplified as "**All data useful to adapt a service**". That is general enough for our applications. 

## Classification of Context

A **Taxonomy** was given for classifying the context based on primary and secondary dimensions. Here is a simplified list: 

- U = user;
- S = sensors;
- D = device;
- O = other.

![](img/taxonomy.jpg)   

Moreover, the temporal context doesn't refer only at the time of day, month or season, but it also refer to **context history** that plays an important role. Keeping track of context data enables *deriving new context*  (ex. A sequence of position can give us a trajectory) or *predicting context* (ex. Observing recurrent trajectory at a given time we can predict current destination). 

## Adaptiveness

Adaptiveness is the property of a system to **adapt to a given context** for providing a better service/experience.

A context-aware system acquires context data to automatically adapt its behaviour. This is very important for mobile computing application because of:

- Changes in network connectivity;
- Changes in energy availability (battery);
- Limited interfaces to specify parameters;
- Changes in user situation (location, time, activity, ...);
- Changes in environment (light, temperature, crowd, ...).

Example: Adaptive video streaming is based on estimated network bandwidth and device battery level, video quality is continuously changed without interrupting the service. 

There are two different types of adaptiveness:

- **Adapting functionality**: The system may change the data flow, hide or expose functionalities or decrease precision.

  *Examples*: Increase caching, move more computation server-side, change interface mode, hide security sensitive functionalities.

-  **Adapting data**: The system may provide less or more precise position data, use higher or
  lower quality images, change the sampling rate from sensors.

## Obtaining Context Data

The obtainment of context data can be distinguished in: 

- **Low-level context**: 
  
- **Directly acquired** from sensors or other sources.
  
    *Example*: Raw data from sensors, explicit user preferences from profiles, mobile device capabilities).
  
  - Obtained by simple **processing and/or fusion** of raw data (ex. Taking bandwidth estimation by averaging over a set of samples in a time window).
  
- **High-level context**:

  - Derived by applying (possibly complex) **inference methods on low level context**.

    *Example*: The activity "cooking a meal" inferred by domotics sensors data and positioning, by a machine learning algorithm, or user's mood derived by combining data from the activities performed in the day, Galvanic Skin Response sensed at his wrist, and face recognition if available.

  - Inference can also be obtained by **reasoning on high-level context and common knowledge**.

    We can classify inference in:
  - **Symbolic approaches**: Uses subset of **first-order logic** (ex. Horn logic programs,  general logic programs, description logics, description logics + rules, ...).
  
    ![](./img/inferencemethods.jpg)
  
  - **Statistical approaches**: Mostly **supervised by machine learning** such as *classification* (divide into classes) and *clustering* (generating classes by aggregating common situations)
  
  - **Hybrid approaches**: That try to take the best from both methods (the most used one).

## High-Level Inference: Activity Recognition

An example of inference high-level context is Activity recognition. This can consist in:

- **Preprocessing of data:** When applying filters to raw data from sensors in order to noise. Then subdivide signal in temporal windows and aggregate signals from different sensor in the same temporal window;
  ![](./img/activityrecognition.jpg)

- **Extracting features:** Extracting statistical characteristics (min, max, avg, ...) from each temporal window;

- **Predictions:** Association of the activity with a vector of features obtaining probability distribution among classes of our statistical model;

  ![](./img/activityrecognition2.jpg)

- **Hybrid reasoning:** We can refine predictions from our statistical reasoning with a symbolic reasoning using formal rules.

  *Example*: If the user is in the semantic location of work, he is probably not running.
  ![](./img/activityrecognition3.jpg)

## Context Representation

Data are obtained from **heterogeneous sources**, by using different languages and a shared semantics of context is missing. The same context data may be used by multiple applications and even shared, so a formal representation of the data is needed to automatically process them. 

Context models, the middleware for shared semantics, should support:

- *Heterogeneity* of context data;
- *Relationships* among context data;
- Context *history*;
- *Uncertainty* (including imprecision) of context data;
- *Reasoning* on context data;
- *Usability*;
- *Efficiency*.

## Context Models

There are three types of context models:

- **Flat Models**: Generally tailored to single applications, it is based on a **common vocabulary** obtained by key/value pairs (no structure and used by commercial applications servers) or Mark-up languages (XML) (with hierarchies of attributes, supported by well-established technologies). 

- **DB-based Models**: The main objective is to use **formal models for querying and reasoning** in order to **facilitate modeling** by abstractions (classes, properties, relations...) and graphical tools. This is an extension of the classic ER-model where entities represent class of objects and relations represent relations between them. 
  
*Example*: **Context Modeling Language** (CML), derived from Object-Role Modeling, is a graphical notation to simplify the specification of context requirements by applications. 
  
- **Ontological Models**: Ontology means *"A formal specification of a shared conceptualization"*. In this case the domain of interest is described in terms of classes, relations, individuals and properties. Ontology context is usually **multi-layer**. We use **first-order logic subsets** for this model, that allow to modelling very **complex domains**, formal semantics and **automatic consistency checking and reasoning**. We can distinguish reasoning in:

  - *subsumption:* is this concept a specialization of this other concept?
  - *realization:* which concept captures this set of observations?
  - *consistency:* is there any contradiction in our definitions of concepts?

  An example is OWL2 where complex descriptions can be obtained by composing simpler ones

  ![](./img/onthologicalmodels.jpg)

  We can model classes and subclasses using **is-a relation**.

  ![](./img/onthologicalmodels2.jpg)

  

- *Hybrid Models:* Integrate different models to take the best from the previous approaches.

Here is a summary of the supported features that the descripted models supports:

![featuresontology](./img/featuresontology.jpg)



## Uncertainty

Our perception of the world, as well as measurements we take, are **prone to uncertainty** (eg. “It’s hot” / “it’s cold” , different sensors may provide different values for the same data ). For this reason it is important to manage uncertainty and conflicts among context data. We can have different types of uncertainty:

- *Incompleteness:* the **data we need is missing** or is difficult to handle. They do not use the value, or use default values instead.
- *Inaccuracy:* the **data is imprecise**. We can use data aggregation to improve precision ( if multiple source are available ) or Probabilistic logics ( eg. “if the probability that the user has fallen is more than 30%, call the medical center” ).
- *Temporal uncertainty:* **“old” data may not be valid now.** We can use a time-to-live mechanism,  data prediction, or progressively lower the confidence in the value that we took in consideration.
- *Conflicts:* different sources **provide contradictory values for the same data** ( eg. the user is in two different places at the same time). We can choose the most reliable value (and/or consider majority)
- *Inconsistency:* Some data are **not compatible with others** ( eg. the user is in a car and he is walking at the same time ). We can use automatic consistency check tools for this problematic (eg. ontology-based).

## Support for Uncertainty

As we saw before, different context models support uncertainty in different ways:

- *Key/value models:* they support uncertainty only for **particular data** ( eg. location )
-  *CML:* they have **quality metadata** ( timestamp, accuracy, probability, ... ) or they can handle inconsistencies through **three-valued logic** ( true, possibly true, false ).
- *Ontological models:* they support **automatic consistency checking** ( some languages support probability and fuzziness, but sometimes they are less expressive than OWL2 )

## Middleware Support

As we saw in this lecture context is the key to offer a better solution to the users of our application. We can think of a **software layer below applications that offers common context services** to any ( or at least more ) type of applications such as:

- *Context acquisition* from different sources
- *Context fusion* and conflict resolution
- *Context reasoning*
- *Privacy preservation*

An example is **CARE (Context Aggregation and REasoning)** : a context middleware developed at EWLab which enables an integrated representation and **handling of profile information** ( for delivering personalized content to users ) , including user, device, network, and context descriptions as well as a mechanism to resolve conflicts in static and dynamic descriptions.

![featuresontology](./img/CARE.jpg)