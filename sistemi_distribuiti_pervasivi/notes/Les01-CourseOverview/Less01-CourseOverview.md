# Distributed and Pervasive Systems - Introduction

As we know from the definition

> A distributed system (DS) is a collection of independent computers that appears to its users as a a single coherent system

![](img/distsys.png)

A distributed system is organized as **middleware**, a medium layer through which the distributed system appears as one to the users. 

In the example, we have four different computers, each one with his own operative system and software running on them. We add **a new layer between the operative system and the application level whose purpose is to link those machines**. That is the middleware.

Some distributed systems:

- A computer with a shared file system, where folders are seen as part of the pc we are interacting with, but in reality those folders are located in another computer. You can also launch applications on such devices and it is not known on which machine the application is running on.
- Massive multi-player online games with cooperating components distributed over different devices/computers/OSes are the perfect example of how a convoluted, distributed and complex system can appear so simple to the users, who doesn't know how everything in the background is managed.
- The Internet (web) grants access to documents whose location is often not quite defined.

As Leslie Lamport ironically says:

> "You know you have one (distributed system) when the crash of a computer you've never heard of stops you from getting any work done".

This is what happens from the eyes of the user if something critical in the distributed system fails.

## Goals of a Distributed System

The goals of a distributed system are mainly the following:

- Making resources accessible;
- Distribution transparency;
- Openness;
- Scalability;

Let's see these in detail

## Accessible Resources

Whatever kind of **resources** we are treating in our distributed system (computational, multi-medial, ...) we need to make them **accessible in an efficient and effective way**.

## Transparency

Transparency is **the quality of a distributed system which determines how much it hides its internal functioning from the user**. There are different forms of transparency that a distributed system should and could meet in his implementation. However, since it's difficult (if not impossible) to ensure them all, each system should achieve at least the transparencies that are the most needed in that application and domain.

| Transparency | Description                                                  |
| ------------ | ------------------------------------------------------------ |
| Access       | Hide differences in data representation and how a resource is accessed. |
| Location     | Hide where a resource is located. The user doesn't know where a resource is. |
| Migration    | Hide that a resource may move to another location.           |
| Relocation   | Hide that a resource may be moved to another location while used. |
| Replication  | Hide that a resource is replicated.                          |
| Concurrency  | Hide that a resource may be shared by several competitive users. |
| Failure      | Hide the failure and recovery of a resource.                 |

- **Access**
  - Since in a distributed system we could have different architectures, each computer can represent data in a different way (little/big endian), so the physical data representation is hidden, but not it's abstraction;
  - A solution would be creating a middleware API in order to provide a common language between the machines.
- **Location**, **Migration**, **Relocation**
  - When we use a service in a distributed system, we usually don't know were each resource is exactly located or when and if it gets moved to another location.
- **Replication**
  - We could hide from the user the fact that we could parallelize a computation (replicating it effectively), but giving to the user only the result of one of them (the first that finishes maybe).
- **Concurrency**
  - It's difficult to implement, however using smart locking techniques (reducing critical sections as much as possible), concurrency transparency could be achieved and users could not notice that the resource they're using is shared between others.
- **Failure**
  - We should implement some fallback mechanism that activate when a something in the DS fails: resource is not found, machine isn't responding and so on. For example, if a resource is not found, maybe there's a backup that has a previous version of it: so instead of showing a mere error to the user, and old version of the resource is shown.

As we will see later, distributed systems are similar to operating systems except for a fact: **they DON'T have a shared memory**. The only way the components of a distributed system can communicate is via messages, which we will also see later. 

## Openness

An open distributed system shouldn't be and shouldn't use proprietary solutions but follow public open standards in order  be available not only to it's creator, but also to everyone, by achieving:

- **Interoperability** with other systems. An external process, with opportune API, can access to the distributed system in order to obtain a service without knowing how that service is implemented;
- **Portability** of this system in other domains;
- **Extensibility** of the system to other areas of application (through APIs or standard protocols).

## Scalability

There are different scalability problems to face in a distributed system.

| Concept                | Example                                                      |
| ---------------------- | ------------------------------------------------------------ |
| Centralized services   | A single server for all users. Problem: If we have a spike in the number of users, the server could be overloaded, thus failing to deliver the service (single point of failure). |
| Centralized data       | A single on-line telephone book (single file available for everyone). Same problem as before: If many users need the file at the same time, I'd have an heavy overload (since it's centralized and there's only one copy) |
| Centralized algorithms | Doing routing based on complete information. This solution is easier than a decentralized algorithm, however usually is not so scalable for a series of reasons (malfunctioning of a node ruins algorithm, difficult to obtain complete info on the network) |

For decentralized algorithms we have:

- No machine has complete information about the system state. The system state is the sum of all the states of the different nodes;
- Machines make decisions based only on local information. Distributed systems have the problem of having *unsynchronized clock*, so we have a coordination problem solved by leaving each machine decision separated;
- Failure of one machine does not ruin the algorithm;
- There is no implicit assumption that a global clock exists.

### Scalability Techniques

We can achieve some scalability using various scaling techniques that **takes some load off the systems' machines**.

![](img/scalelog.png)

For example, in a form application, instead of validating each form on the server, operation that weighs a lot if we have a big number of clients, we can validate forms on the client: this way we distribute this kind of operation with all the downfalls of this solution (client-side validation is reliable and/or safe? probably not).

Sometimes data have to be divided and organized, like the division of the DNS namespace into zones.

![](img/dns.png)

## Pitfalls of Distributed System Development

The following are all FALSE assumptions made by first time developers:

- Network is reliable;
- Network is secure;
- Network is homogeneous;
- Topology doesn't change;
- Latency is zero;
- Bandwidth is infinite;
- Transport cost is zero;
- There is one administrator;
- Debugging distributed applications is analogous to standard applications;
  - This is because it's difficult to simulate the interleaving, which is the message flow between the machines with its latency.

# Types of Distributed Systems

- Distributed Computing Systems:
  - Clusters;
  - Cloud Computing;
  - Edge Computing;
- Distributed Information Systems:
  - Distributed Databases;
  - Distributed Transactions;
- Distributed Pervasive Systems.

## Pervasive Computing

As Mark Weiser says:

> "The most profound technologies are those that disappear. They weave themselves into the fabric of everyday life until they are indistinguishable from it"

The way these system blend in so well is the reason of their success. So in order to become **pervasive**, a distributed system should have the following features:

- **Include unconventional nodes**.
  - Originally only the computer had computing capabilities, while the phone was a dumb object which only transmitted and received voice. Today, however, our smartphones became computing-capable, so the phone is an unconventional node (and need to be considered during the pervasive system development);
  - Another unconventional node could be a table (another dumb object). If we ever create a smart table with computing capabilities and if our distributed system includes this (as an unconventional node), then the system can be considered pervasive.
- **Adaptivity**.
  - A system adapt his logic basing on the current context, in order to optimize itself and follow its goal;
  - For example, automatic doors opens when a person is coming by: however, if while they're closing back another person comes by to enter, the system reads the context and adapts to it by re-opening the doors instead of completing the closure (this is not a distributed system but an example of adaptivity);
  - Another example is the smart usage of GPS in our phones: an application can offer different ads or offers basing off our GPS location, instead of static ones (it adapts to our position).

![](img/pervchall.png)

## Cluster Computing Systems

A **cluster computing system** (CCS) is a collection of equal or similar workstations/servers closely connected by high-speed LANs usually running the same operating system. This means all the **nodes in a CCS are physically close** and the latencies are usually very low.

The goal they pursue is **high performance computing** tasks or providing **high availability** for a service.

There are two types of cluster approach:

- **Asymmetric approach**, where a master node coordinates slave nodes;
- **Symmetric approach**, where nodes cooperates at the same level.

### Asymmetric Approach

A master node distributes tasks on a set of computing nodes. The master has his own set of managing software, while the other slave nodes share the same software. The master is also connected to a remote access network in order to communicate with the rest of the Internet and provide the service. 

![](img/asymccs.png)

Examples: 

- Beowulf for Linux clusters;
- Google Borg (large scale cluster management used by Google internally).

### Symmetric Approach

There is no master, all nodes have the same software installed. 

Example:

- MOSIX;

## Grid Computing Systems

The **grid computing system is the idea that inspired the cloud computing**. In the same way the electric power network (grid) is composed by distributed power generators and giving the users access to electric power as needed, the **cloud computing renders computation an on-demand commodity**.

> Cloud computing is a model for enabling ubiquitous, convenient, on-demand network access to a shared pool of configurable computing resources that can be rapidly provisioned and released with minimal management effort or service provider interaction.

This model is the evolution of grid computing and of the Software as a Service (SaaS) paradigm and its final goal is to **offer anytime, anywhere and unilaterally computing capabilities**.

For example, lets go back to the electric power network example: if we need to power some lights, my 1KW contract suffice largely. However if I turn on the oven, a dishwasher and other stuff, I need more than that. So the user should be able to upgrade its contract to the wattage he needs (3, 4 KW or more) with the least effort possible.

### Grid Nodes

The nodes and resources of a grid are not limited to servers. They can be:

- Supercomputers;
- Network storage systems;
- Databases;
- Special devices (telescopes, sensors, ...).

Each node may belong to, and reside in a different organization. The grid is perceived as a single system of a virtual organization.

### Grid Examples

- Bio-medical informatics network providing researchers with access to experiments and visualizations of results (nbcr.sdsc.edu);
- Analysis of data from the CMS high energy particle detector at CERN by physicists world-wide over 15 years (www.uscms.org);
- Use of the (Sun/Oracle) Grid Engine to enhance aerial photographs by using spare capacity on a cluster of web servers (www.globexplorer.com).

## Cloud Computing

Cloud computing is **an evolution of grid computing and SaaS paradigm**, by using web resources if and when needed. His main characteristics:

- **On-demand self-service**
  - A consumer can unilaterally provision computing capabilities and access to them in the easiest way possible. This way the consumer can himself upgrade its contract basing off his needs.
- **Broad network access**
  - Capabilities should be available over the network and should be accessible through standard mechanism;
- **Resource pooling**
  - The provider's computing resources are pooled in order to serve multiple consumers rapidly;
- **Rapid elasticity**
  - Capabilities can be elastically and automatically provisioned and released, according to the consumer needs;
- **Measured service**
  - Cloud systems should have some kind of measuring system of the resources used by each of his consumer for various reasons: control and optimization of the resources use, metering system for the consumers so that they can check their usage quotas and so on;

### Cloud Service Models

- **Software as a Service (SaaS)**
  - The consumer **uses the provider's applications** running on the provider's cloud infrastructure;
  - Example: Google Suite is a software that offers office software like Docs, Spreadsheet and others in a cloud environment, usable without installing any software on the machine (except the browser of course). The entry price for consumers is little, since there is no need to maintain machines or licenses; this way, they can use the service on-demand and pay only what is consumed;
  - Google Docs, Microsoft Office 365.
- **Platform as a Service (PaaS)**
  - The consumer **deploys his own application** onto the provider's cloud infrastructure using supported tools;
  - Example: It's possible to borrow some of the provider tools (es. DBMS) and integrate them into the deployed application (because they're more optimized for example);
  - Amazon AWS, Google App Engine, Microsoft Azure.
- **Infrastructure as a Service (IaaS)**
  - The consumer **deploys his own arbitrary software** onto the provider's cloud infrastructure (including operating systems, applications and alike);
  - Example: the provider provides only the infrastructure (virtual or physical machine) while the software on it is completely managed by the consumer;
  - Amazon EC2, Google Compute Engine, Microsoft Azure.

### Cloud Deployment Models

- **Private cloud**
  - Exclusive use by a single organization comprising multiple consumers (cloud hosted by a private organization locally). It needs a locally managed server farm and an IT department to maintain it.
- **Community cloud**
  - Exclusive use by a specific community of consumers from organizations that have shared concerns.
- **Public cloud**
  - Open use by the general public (Amazon AWS, Azure, Google...),
- **Hybrid cloud**
  - It's a composition of different types of cloud (private, community or public);
  - Example: Zoom is a scalable distributed system which uses an hybrid cloud. In our lesson streaming we're using a public cloud, because the data transmission passes through public server, however if we wanted we could avoid this by enabling a private cloud, always ensuring the transparency needed for the user not to worry about.

### Cloud computing infrastructure

This kind of infrastructure, in order to respond to the scalability and elasticity needs, has to provide

- A special switching and load balancing technology;
- Each data center should have one or more clusters of thousands of computing nodes.

![](img/google.png)

## Edge / Fog Computing

Instead of the big cloud, fog is an intermediate-low level of computing. This is the popular model of cloud for IoT devices, which are sensorized connected devices that produce massive amount of data that usually needs to be processed in real time.

**Edge computing expands cloud computing reaching the "edge" of the infrastructure**, reaching single sensors, so that latency between those components is minimized, making the web more scalable.

For this reason, there is a stringent **need for reliability and low latency**; an automatic car needs to obtain data from local environment (traffic lights, other cars) in less than 1 ms in order to take quick decisions. Cloud isn't suited to respond to these needs since it doesn't meet latency requirements.

![](img/fog.png)

### Fog vs Edge

The term “fog” is often used as a synonym of “edge”, but  “fog” is sometimes used differently, to refer to an intermediate
level of decentralization between cloud and edge using more powerful nodes.

