# Project - Gateway

Il *gateway* ha il compito di:

- Gestire inserimento e rimozione nodi;
- Ricevere le statistiche relative all'inquinamento;
- Permette agli analisti di essere interrogato.

I servizi vengono erogati in modo REST, quindi risulta necessario un meccanismo di sincronizzazione per gestire l'accesso alle risorse condivise.

## Gestione Risorse

Individuare le risorse da modellare e le operazioni CRUD effettuabili sulle risorse e il relativo mapping con HTTP.

I nodi e le statistiche sono risorse.

Mapping CRUD:

- GET → Ottenere le informazioni relative ai nodi del quartiere;
- POST → Inserire un nuovo nodo nel quartiere;
- PUT → Modificare le informazioni relative ad un nodo (ci serve?);
- DELETE → Rimuovere un nodo dal quartiere.

## Gestione Nodi

Quando un *nodo* fa richiesta di entrare nella rete il *gateway* deve:

- Cerca di inserirlo nel quartiere;
- Se esiste già un nodo con lo stesso id viene restituito un messaggio di errore;
- Se tutto funziona correttamente il nodo viene aggiunto all'elenco dei nodi del quartiere e viene restituita la lista dei nodi presenti nel quartiere.

L'uscita di un nodo avviene semplicemente rimuovendolo dall'elenco. 

Sincronizzazione sull'accesso alla lista.

## Gestione Statistiche

Lato nodo:

- Il quartiere invia periodicamente le statistiche globali;
- Queste statistiche vanno salvate internamente al server in una struttura dati per permettere successivamente l'analisi.

Lato analista:

- Necessità di interfacce per analizzare i dati come da specifiche.

Server REST il più leggero possibile, occhio ai synchronized.

Cercare di realizzare una sincronizzazione più a grana fine possibile, non ha senso bloccare ogni operazione mentre vengono calcolate le statistiche, alcune possono essere comunque eseguite. Lock diversi per risorse diverse.

## Reminder Jersey

Ogni classe annotata con @Path viene istanziata ogni volta che viene effettuata una richiesta HTTP, quindi è necessario gestire adeguatamente la memoria condivisa, ovvero, usando singleton o in alternativa campi statici. Sincronizzare i metodi di una classe annotata con @Path non serve a nulla.

Viene automaticamente gestito il multithreading: Chiamate concorrenti eseguono in concorrenza il codice di diverse istanze della classe che gestisce la risorsa. Attenzione ai problemi di concorrenza.

## Sincronizzazione

All'interno dello sviluppo del server sono presenti svariati problemi di sincronizzazione:

- L'elenco dei nodi viene modificato e letto in maniera concorrente;
- Le statistiche vengono aggiunte e lette in maniera concorrente.

Gestire la sincronizzazione in maniera snella. Non usare synchronized a caso.

## Testing

Il primo passo è quello di 

- Testare ogni singolo metodo REST con tool comodi (ad esempio Advanced REST Client);
- Testare problemi di concorrenza tramite sleep();
- Test automatizzati con JUNIT;
- Utilizzo API Client di Jersey, col vantaggio di sfruttare nuovamente marshalling e unmarshalling offerto da JAXB.

























