# Progettazione Nodo

Realizzare una rete P2P ad anello, ogni nodo fungerà sia da client che da server allo stesso momento. Ogni nodo è un processo a sè stante.

Ogni nodo ha lo scopo di aggregare i dati provenienti da un sensore.

I nodi si devono sincronizzare per calcolare una statistica globale da inviare al gateway.

- Comunicazione tra nodi avviene tramite grpc;
- Comunicazione con gateway tramite librerie client REST;
- Formati JSON per scambio dati;.

L'ID dei nodi è scelto dal nodo stesso.

## Struttura del Nodo

- Metodi grpc per la gestione dei messaggi;
- Un thread per I/O da standard input, per permettere al nodo di uscire dalla rete in modo controllato;
- Un modulo per gestire la topologia della rete, capire come strutturare l'anello;
- Un modulo per gestire il token;
- Un modulo per la generazione di statistiche dei dati di sensore.

## La Rete

Inizialmente il quartiere è vuoto e il gateway è pronto a ricevere richieste di inserimento. 

Ogni volta che facciamo partire un nodo, tenterà automaticamente di entrare nel quartiere (evitare di far mandare il token a se stesso se un nodo è l'unico in rete).

Se la registrazione va a buon fine, il nodo riceve la lista degli altri nodi nel quartiere, deve quindi comunicare con gli altri nodi per entrare nella rete.

...

## Token Ring

Quando un processo acquisisce il token controlla se deve entrare nella sezione critica:

- Se è così ci accede e poi manda il token;
- Se non è così manda avanti il token.

La sezione critica è la comunicazione col gateway, ma potrebbe non essere l'unica cosa.

Il token è un messaggio, il token viene trattenuto deliberatamente solo se necessario, gestire l'attesa del token evitando busy waiting (wait() e notify()).

## Problemi

Il fatto che i nodi possono entrare e uscire in qualsiasi momento, bisogna pensare a tenere la rete consistente, evitando token duplicati o token perso.

Casi limite da gestire:

- Due nodi che vogliono entrare contemporaneamente nell'anello;
- Un nodo entra mentre un altro nodo esce;
- Due nodi escono contemporaneamente;
- ...

I puntatori di un nodo al successivo vengono aggiornati in concorrenza, rendendo la rete inconsistente.

Importante considerare tutti i casi limite, saranno richiesti all'orale. Testare con sleep().

## Gestione Problemi

Soluzione che garantisca che la rete abbia sempre uno stato consistente.

1. Gestire la modifica della topologia come un ulteriore problema di mutua esclusione distribuita.
2. Ogni nodo può sfruttare gli identificatori dei vari processi (che sono univoci) per avere una struttura implicita della topologia (ad esempio, ordinando gli id).
3. Altre soluzioni sono accettate pur mantenendo la consistenza.

## Misurazioni di PM10

Ogni misurazione di sensore è caratterizzata da:

- ID sensore (non del nostro progetto);
- Tipologia di sensore (non del nostro progetto);
- Valore letto;
- Timestamp.

Ogni sensore riversa le misurazioni in un buffer, usato per calcolare periodicamente una statistica locale.

## Sliding Window

Tecnica standard per analisi o compressioni real-time di stream di sensori che consiste nel calcolare statistiche considerando finestre temporali di dimensione w. Ogni volta che vengono accumulate w misurazioni calcoliamo le statistiche.

Utilizzo di una percentuale di overlap o per catturare le transazioni tra finestre temporali, ogni 6 minurazioni faccio la media su 12. 

La finestra temporale wi ha una percentuale o delle misurazioni più recenti della finestra w(i-1)

## Statistiche Locali e Globali

Ogni nodo calcola periodicamente statistiche locali, ovvero la media su sliding window.

Ogni statistica locale deve essere aggiunta al token quando disponibile.

Il primo nodo che si accorge di avere tutte le statistiche della rete, trattiene il token per comunicare la statistica globale al server.

Problemi:

- Chi sono i nodi dai quali mi aspetto una statistica locale per calcolare quella globale?
- Cosa succede quando un nuovo nodo entra nella rete? Deve essere considerato da subito per il calcolo della statistica globale?
- Cosa succede quando un nodo esce mentre si aspetta una sua statistica locale?

## Gestione Messaggi

All'arrivo di un qualsiasi messaggio, un nodo deve esaminarlo.

Siccome i servizi erogati tramite grpc sono multi-thread, la ricezione dei messaggi è concorrente.

Una coda condivisa e un pool di thread possono rappresentare una buona soluzione.

N.B. Gestire un solo messaggio alla volta non va bene, è un collo di bottiglia, si aboliscono i vantaggi del multi-threading.

Una buona stesura del protocollo di comunicazione può semplificare il codice che esamina i messaggi in ingresso:

- `header content [parameters] timestamp`

Può essere conveniente considerare i diversi stati dell'applicazione durante l'interpretazione dei messaggi.

## Pattern Singleton

In diversi problemi di sincronizzazione è necessario un oggetto di coordinazione.

In un sistema con un dispatcher e molti thread, il problema è riferirsi alla stessa istanza.

...

```java
public class Singleton {
    private static Singleton instance = null;
    
    public synchronized static Singleton getInstance() {
        if (instance==null)
            instance = new Singleton();
        return instance;
    }
    
    // The private constructor impedisce la creazione di istanze da parte 	  // di classi esterne
    private Singleton() {
        /*...constructor code...*/
    }
    ...
}
```

Con diversi processi, non tutti accedono alla stessa istanza del singleton. Due processi hanno due singleton diversi. Il singleton è valido a livello di processo.







































