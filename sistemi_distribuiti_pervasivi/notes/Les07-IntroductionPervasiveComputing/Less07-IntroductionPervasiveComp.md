# Introduction to Pervasive Computing

As Mark Weiser said about Pervasive Computing:

> "The most profound technologies are those that disappear. They weave themselves into the fabric of everyday life until they are indistinguishable from it."

In order to become **pervasive**, a distributed system should have the following main features:

- **Include unconventional nodes**. Possibly mobile objects with computing and communication capabilities (like smartphones and sensor networks);
- **Adaptivity**. A system adapt his logic basing on the current context, in order to optimize itself and follow its goal.

However, they are also **volatile systems** whose main features are:

- Failures of devices and communication;
- Changes in the characteristics of communication such as bandwidth;
- Creation and destruction of associations between software components resident on the device.

![](./img/pervchall.png)

## Mobile Computing

Mobile Computing is an evolution of Distributed Systems which the main **issues** are:

- Limited resources of the nodes (energy, CPU, memory, ...);
- Different types of interfaces among the nodes;
- High variance in connectivity, because they can communicate only if they are in reciprocal radio range;
- Variable location.

The main **research topics** in mobile computing are:

- Networking (Mobile IP, Mobile networks, ad hoc networks, ...);
- Mobile information access (disconnected operation, proxy architectures, bandwidth adaptivity access);
- Mobile data management (spatio-temporal data management, LBS, context-awareness, privacy and security, mobile cloud services);
- Positioning (indoor and outdoor localization, proximity, tracking);
- Software (App and mobile services design, development and testing, scalability).

## Pervasive Computing

Mobile nodes, which are sensors, actuators or smart devices, are connected through Internet in order to listen signals from the physical world and react to it.

![sensorsandactuators](./img/sensorsandactuators.jpg)

The main **issues** in Pervasive computing are:

- Smart spaces need to be effectively used (adaptiveness, context-awareness, anticipation of needs);
- Invisibility, where the interaction with users should be minimized;
- Resources in pervasive computing environment should be discoverable and dynamic association/dissociation should be enabled.

**Examples** of Pervasive Systems are: 

- IoT / Smart Environments, where to integrate sensors and devices for sensing in Internet to build smart environments systems such as:
  - Smart Home services;
  - Smart energy management;
  - Smart transportation (ex. using crowdsensing through smartphones and sensors).
- Industry 4.0, by using sensors and actuators in production phase to improve production.
- e-Health systems for
  - Tele-healthcare;
  - Independent living and aging well;
  - Accessibility.

The current **research topics** in Pervasive computing are:

- Context and Activity Modelling and Recognition;
  - How to represent rich context and how to reason with it;
  - How to recognize human activities from streams of sensor data; 
  - ...
- Crowd Sensing;
  - Distributed acquisition of data from users, taken from their smartphone, their car, their home;
- Energy Analytics;
  - Analysis of smart-meter data and enabled services;
- Pervasive Health, with wearable devices and environment sensors enable continuous monitoring goals such as: 

  - Early detection of health problem;
  - Monitoring health conditions in chronic diseases;
  - Facilitating rehabilitation at home;
  - Extended independent living (reminders, alarms, intervention, ...).

- Pervasive Transportation;
  - Road sensors, car sensors, driver smartphones coupled with inter-vehicle communication enable new services;
  - Goal: mobility optimization, emission reduction, hazard detection.
- Security and privacy.

## Why "smart"?

This objects and applications are considered "smart" because they are:

- Connected to Internet, which guarantees remote access, invocation of services, cooperation and more;
- Run algorithms (locally or remotely) to interpret data and understand "context";
- Offer *personalized context-aware* services.

![](./img/allsmart.png)

## Sensor Networks

Sensor networks are important components of pervasive systems.

Sensors can be also part of smart objects, like in smartphones and cars, but also wearable objects especially in e-Health, like airflow sensor, electrocardiogram sensor and patient position sensor.

## "SECURE": A eHealth Project

SECURE is an intelligent system for preemptive diagnosis and home follow-up. It was founded by MIUR and Region Lombardy.

4 partners:

- Health Telematics Network;
- FatebeneFratelli IRCCS;
- University of Milan;
- 3 Caravels.

## The Hard Part: Data Analysis

The hard part of a sensor network remain **data analysis** (understand from low level context what we should do) which is usually done with statistical models and other solutions.

![dataanalysis](./img/dataanalysis.jpg)