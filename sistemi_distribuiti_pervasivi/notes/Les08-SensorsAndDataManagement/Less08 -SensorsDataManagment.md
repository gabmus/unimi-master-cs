# Sensors Data Acquisition and Data Management

## Outline

- Introduction to sensors;
- Sensor-based devices;
- Managing and querying sensor data;
- Sensor-based systems and apps.

## Introduction to Sensors and Sensor Devices

A **transducer** is a device that transform one form of energy into another. 

They are two kinds of transducers:

- Input transducers (sensors);

- Output transducer (actuators).

  ![transuderexample](img/transuderexample.jpg) 	

## Sensors

Sensors can also be divided into:

- **Physical sensors** are devices that **measure a physical quantity** and transform it into a signal;
- **Virtual sensors** are Services and applications that **provides context data** to remote clients (ex. Weather web service based on its physical sensors)

How do sensors work? The sensing process consist of the detection of a physical changes and the production of an electrical signal as data stream. 

![](img/senswork.png)

For example, a particular sensors called Micro Electro - Mechanical Systems (MEMS) are accelerometers (which measures acceleration by measuring changes in capacitance using mass, springs and fixed plates) and gyroscope (which measures angular rate using Coriolis effect, measured also with changes in capacitance). 

More info: https://www.youtube.com/watch?v=eqZgxR6eRjo

## Actuators

Actuators are particular types of transducers, usually powered, that **converts energy into actions** (motion, switch, ...) upon receiving commands from a control systems 

Examples: Dim lights, open a door, steer a wheel, open a radiator valve, ....

A network with sensors and actuators has many applications, like ZigBee, Z-Wave or Thread, with more main application in Home automation and IOT. 

## Sensing Devices

Sensing devices are particular devices with sensing and processing capabilities. 

![](img/geigsensor.png)

They main components are:

- *Sensing subsystem* for data acquisition from the environment;
- *Processing subsystem* for local data processing;
- *Wireless Communication systems*; 
- *Power Source*, either battery or solar power;
- Local Data Storage for caching data;
- Wired Interfaces like USB or mini USB;
- Mobilizer, to change location or configuration.

**N.B.** The first 4 components are needed in every sensing device.

## Base Stations

Base Stations, also called border routers, controllers or Sink, **collect data** from distributed sensing and actuating devices.

They may act as a gateway between networks and usually they come with an embedded database while **transmit data to processing units** like cloud, servers or locals. 

## Example of Sensor/Sensing Devices/Applications

Some examples of sensors, sensing devices and applications are:

- *Environmental sensors*, which measures light, temperature, humidity, magnetic fields, pollution, pressure, electrical fields, sound, chemical elements, ...
- *Smart home sensor devices* like movement detector, video door entry, natural gas detector, temperature extremes detector, bed/chair occupancy sensor, ...
- *Biosensors*, which measures biological signals such as electrocardiogram, body temperature, blood pressure, oxygen saturation, skin conductivity (for mood detection), ...
- *Wearables sensors* like smart clothes, watches, glasses, ring, ...
- *Bluetooth (BLE) beacon sensor* used for micro localization of objects;
- *Smartphone-based sensing* to sense data through phones;
  - CenceMe, developed by Dartmouth College in 2008, is an application to infer: activity, disposition, habits, surroundings.

# Managing and Querying Sensor Data

Smart devices can **frequently appear/disappear in smart spaces**. For this reason they must be discovered and paired:

- **Network bootstrapping**: Register the devices to the network assigning a unique addresses (DHCP-like) to him
- **Association**: Associate the device to the application based on smart space *boundary* and device *services*

## Data Acquisition

There are several problems in comparison with traditional databases:

- **Continuous query** are required due to continuous data stream arriving from sensor network.
- Strong **spatio-temporal characterization** of data, because where and when an event occurred is strongly correlated.

## Basic Methods

There are three basic methods for data acquisition:

- **Batch processing**: Raw data stream arrives from sensors to the base station (or remote server), that then process data offline. 

  A precise answers is often needed to particular sink requests. In this case no processing on the sensor is done, but with high communication costs. The base station receives all the data, but with a delay.

- **Sampling**: Not all sensed signal is used from the sensor, for example he can read data only at a given frequency. 
  
  Statistics obtained through sensor readings extracted according to a given probability distribution to have representative and useful data. This provides formal guarantees bound about the introduction of errors.
  
  This method is the one used by common sensors.
  
- **Sliding Windows**: An approximate answer based on a group of consecutive reads, by using temporal windows. 
  
  A device does this computation in real-time in order to send more accurate data to the sink. Incoming data are aggregated by using an aggregation functions (ex. min, max, avg, ...).
  
  The base station receives a continuous stream of approximated sensor reading, with a small delay.
  
  There are also **overlapping sliding windows**, that do not create a partition of the stream, to have more accurate and frequent data. 

![overlappingwindow](./img/overlappingwindow.jpg)



## Advanced Methods

Advanced methods have more specific goals such as:

- **Save energy** of the device;
- **Improve data quality**.

Transmitting data is much more power consuming than processing it: Transmitting 1 bit is worth 1000 CPU operations in power cost. The cost of sensing depends also on the sensed phenomena. 

There are several advanced methods for data acquisition:

- **In-network query processing**: Opposed to centralized networks and built as an overlay network, this method performs **data aggregation in intermediate nodes** to reduce transmission of data to the sink. 

  We can use an overlay (ring, spanning tree, ...) and follow it through the base station doing aggregation in between. 

  ![innetworkprocessing](./img/innetworkprocessing.jpg)

- **Duty cycling**: The radio of our device is put on sleep mode most of the time using coordinated **sleep/wakeup scheduling** algorithms to prevent unnecessary battery usage.

- **Mobility-based**: The base station may be mobile or nodes may be carried away (by people, cars, animals, and similar stuff).

  We can **exploit that mobility** to better distribute transmission cost between nodes, when available.

- **Model-based approaches** (data-driven): Sampled data may have strong spatio-temporal correlation (ex. The temperature goes down during night). 
  
  The main goal here is to reduce the number of samples (read or transmitted) while keeping good data quality. This is done by **exploiting the model of underlying phenomena** (it can be constructed by computational nodes and distributed among device sensors).
  
  ![model based](./img/model based.jpg)
  
  
  There are different approaches:
  
  - **Data cleaning:** Identify and remove noisy readings (according to our models).
    
    Noisy readings are found if there is some data really far from the rest of the model. This can be done **online**, while reading the data stream, or **offline**, before saving it to clean-up data. 
    
    Some risks are introduced when throwing data, this must be considered only if, for example, 1 of 100 data is not following the model.
    **N.B.** The sink does this cleaning.
    
    ![data cleaning](./img/data cleaning.jpg)
    
    **Noisy readings** can be introduced because sensor data are uncertain and erroneous (discharged battery, network failures, low-quality sensors, freezing/heating of the casing device, accumulation of dirt, mechanical failure or vandalism). For this reason, the **most probably sensor values are inferred** using statistical model by comparing raw data with the corresponded inferred value. 
    
    **Regression models** can capture both continuity and correlation. It is mainly used a polynomial regression, but many other models can be used.  
  
    ![datacleaning2](./img/datacleaning2.jpg)
    
    
    
  - **Data acquisition:** The main goal here is to acquire efficiently samples from sensors. Sensors doesn't transmit anything if data follow the model (with some delta error), however, sensors should transmit some bit that indicates "I'm alive", to let the sink distinguish between the two situation when he's not transmitting, because he can be dead or the data are following the model. We can distinguish two different approaches:
  
    - **Push-based data acquisition**: The sink and the sensors agree on an expected behaviour of the sensor values: the model (ex. The usual temperature fluctuation during the day). If sensed values deviate from the model, the sensor communicate only the unexpected values. 
  
      Push-based data acquisition can follow **dynamic probabilistic models**, that can evolve over time (ex. Linear prediction model) considering only temporal features and treating each sensor independently, or **Markovian models**, considering complex and spatial dependencies among sensors, using initial state and transition probability.
  
      Keep in mind that we need to **keep the model in sync** with sensors and sink in order to achieve our goal.
    
    **N.B.**: The difference with data cleaning is that this is done sensor side. 
    
  - **Pull-based data acquisition:** In this case the user defines the interval and frequency of data acquisition. Values from sensors are **requested explicitly.** 
    
    ![queryofdataacquisition](./img/queryofdataacquisition.jpg)
    
    
    
  - **Query processing:** The main goal is processing query by accessing or generating minimal amount of data.
  
    When querying a database for the value at time `t`, in a range between `t1-t2` (saved in the database), i calculate `f(t)` following the model. This way the database size is reduced, but I lose in accuracy of the data, because you can only estimate `f(t)` according to the model, but it's not his real value. 
  
  - **Data compression:** Sensors may produce high volumes of data, so it could be useful to **eliminate redundancy** and save space for storing my data. 
  
    The main goal here is to approximate a sensor data stream by a set of functions. In order to do that, it is possible to use **regression, transformation and filtering** to approximate those values exploiting spatio-temporal correlation. Also, **orthogonal transformation methods** (Fourier or Wavelet transform) can reduce data dimensionality. 
  
    Here we have some examples:
  
    - **Poor Man's Compression - MidRange** (PMC-MR): Approximate a data segment with a **constant value** (ex. `(Max-Min)/2` or mean) introducing a **bonded error** epsilon, storing both in local database for the considered interval.
  
       The "function" here is replaced with constant values. Note that time intervals have not the same length (it depends on the actual measurements) but the width of the segment is the same (`2*epsilon`).
       
     ![pmc-mr model](./img/pmc-mr model.jpg)
    
    
    
    - **Multi-model approximation**: This is the more complex one, where I **use multiple models in different time interval** to approximate my data (storing always the function and interval to approximate the values).
      
    **N.B.** Materialization of data is a view of them in a table from a graphical visualization. 
      
    ![model table](./img/model table.jpg)
      
      ​                         ![segment table](./img/segment table.jpg)

## Cloud Platforms for IoT and Data Stream Processing

- Amazon AWS IOT, a suite of modules and Kinesis Streams for  processing;
- Google Cloud IOT make by Cloud IoT core + Cloud Dataflow;
- Apache Flink, an open source framework for data stream processing.

# Sensor-Based Systems and Applications

## Case 1: Health Monitoring

![](img/health.png)

Requirements for a good health monitoring systems are:

- Reliability and fault tolerance;
- Unobtrusiveness, usability and aesthetic issues;
- Security and privacy;
- Operational lifetime.

## Case 2: Environmental Monitoring

![](img/env.png)

Typical deployment in smart cities, with smart parking, structural health, traffic congestion,
smart lighting, intelligent transportation systems and much more. The most important are:

- Smart environment: Air pollution, forest fire detection, landslide and avalanche prediction, river floods, and so on;
- Agriculture and farming;
- Security and emergencies: Radiation levels, gad detection, and more;
- Supply chain control, logistics, industrial control, and more.