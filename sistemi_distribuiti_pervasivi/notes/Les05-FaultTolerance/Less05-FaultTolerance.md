# Fault Tolerance and Consensus

The higher the number of nodes, the higher is the chance of failure. *Fault tolerance* is a quality that makes systems dependable. It implies having the following properties:

- **Availability** - The probability that the system operates correctly at any given moment: a user expects the system to be up when trying to connect to it;
- **Reliability** - The ability to run correctly for a long time. You can have high availability but low reliability if your system crashes once a day;
- **Safety** - Failure to operate correctly does not lead to catastrophic failures, easy to recover;
- **Maintainability** - The ability to "easily" repair a failed system through its lifetime.

## The CAP Theorem for Distributed DBMS

The CAP theorem says that distributed DBMS **can only have at best only two of these three quality** at the same time:

<img src="img/cap.png" style="zoom: 100%;" />

- **Consistency** - Every read receives the most recent write or an error.
  - That means that all nodes see the same data, even if databases are replicated (like the multi-cast example we saw before).
- **Availability** - Every request receives a response. 
  - That means that the system is operational at any given time.
- **Partition Tolerance** - The system tolerates an arbitrary number of node crashes and messages lost or delayed.
  - That means that we have replicated resources that can helps replace the crashed nodes or resend lost messages.

There is an important trade-off to choose from, based on our DBMS needs and what is deemed important in our system. Generally, **partition tolerance** is very useful in distributed systems, so usually that's the **preferred quality** we want for our system.

After that, we need to **choose between consistency and availability**. Of course, there is no black and white, we can always assure some consistency while we focus on availability and vice versa. For example, I could focus 80% on consistency and 20% availability because I'm making a transaction system, since consistency is king in this scope.

## Failure Models

There are different types of failures that can occur in distributed systems:

| Type of failure                                           | Description                                                  |
| --------------------------------------------------------- | ------------------------------------------------------------ |
| ***Crash failure***                                       | A server halts, but it was working correctly until it halts. |
| ***Omission failure*** (Receive / Send omission)          | A server fails to respond to incoming requests. Either the server fails to receive incoming messages or fails to send them. |
| ***Timing failure***                                      | A server's response lies outside the specified time interval. |
| ***Response failure*** (Value / State transition failure) | A server's response is incorrect. Either the value is wrong or the server deviates from the correct flow of control. |
| ***Arbitrary failure***                                   | A server may produce arbitrary responses at arbitrary times. It is a combination of timing and response failure, including malevolent nodes. |

## Failure Masking by Redundancy

Since these failures can't always be fixed, the best way to deal with them is hide them by redundancy:

- **Information redundancy** - If the message contains noise, some extra bits can be added to it while trying to reconstruct it.
  - *Example*: If some frames are missing in a video streaming, some interpolation techniques between the received frames can be applied to mask the failure.
- **Time redundancy** - If a transaction fails, it is repeated after some time.
  - *Example*: When I try to connect to a website and Internet connection is failing, the browser will wait for a while before retrying to connect to it, hoping that the connection is re-established by then;
  - **N.B.** Careful when dealing with non-idempotent transactions, because dangerous side effects could happen (especially in RPCs).
- **Physical redundancy** - Extra hardware or software (processes) is deployed to recover from failure of some component.
  - *Example*: If I suspect a sensor is failing and sending wrong values, I could deploy more sensors to mitigate the issue by obtaining more data from them and giving less value to the failing sensor.

### Redundancy Application

There is a system of three processes that communicate in sequence: A produces some output and passes it to B, which creates some other output and sends it to C and so on.

Let's **implement redundancy** to try to avoid the failure of one of these and, consequently, of the whole system.

<img src="img/voter.png" style="zoom:70%;" />

To achieve process resilience we **create a group of identical processes**, creating redundancy (for example A1, A2, A3 belong to group A). 

Each of these processes receives the messages sent to that group (through a inner multi-cast system). Since we created a distributed subsystem, we also need a communication protocol and a mechanism for nodes to join and leave a group. 

We also add some **voters, which are middle agents who inspects the output given by the group of processes**: through a consensus function, each voter check the data received and: 

- If consensus is achieved, the data shared by the majority of process is outputted;
- Otherwise, an unknown value is outputted.

For example, let's suppose that A1, A2 and A3 are temperature sensors. Each of them outputs temperature values to each voter: let's say that A1 = 9.2°C, A2 = 8.4°C and A3 = 8.8°C

V1 receives the data, rounds it so that the array of values (A1, A2, A3) is (9, 8, 9) and then applies the consensus function: the majority of sensors say that the temperature is 9°C, so the voter V1 output will be 9°C. Of course, it is possible to have different types of consensus function (weighted average, distance and so on...) but the mechanism would be similar.

### How Much Redundancy?

The real question, however, is: how much redundancy is needed to achieve the wanted resilience? **A system is k-fault tolerant if it still works correctly with, at most, k faulty processes**.

- If faulty processes just stop working (crash failure), k+1 processes provide k-fault tolerance. This is because, until I have at least one working process, my system would function correctly;

- If faulty processes reply with wrong values, 2k+1 processes are needed. In this case, the processes don't care about the value they output, the one doing the checking is the "voter", which could be a client as well as another process or component.
  
  For this reason, we need at least the majority of values to get k-fault tolerance, because we are supposing that some processes give wrong values (like in the temperature example from before);
  
- If instead the internal process group consensus is needed (like to decide transaction commitment, election, and so on) then the problem is more complicated.

## Consensus in Faulty Systems

The consensus between processes in faulty systems is a hard problem, whose solution is not unique and depends on the properties of the system:

- Synchronous vs asynchronous systems;
- Communications delay is bounded or not;
- Message delivery is ordered or not;
- Message transmission is done with unicast or multicast protocol.

## Byzantine Agreement

Agreement and consensus problems were studied by Lamport, who brought us a solution for the interactive consistency problem. 

To make the interactive consistency problem easier to understand, Lamport devised a colorful allegory in which a group of army generals formulate a plan for attacking a city. 

In its simplest form, the **generals must decide only whether to attack or retreat**. Some generals may prefer to attack, while others prefer to retreat. The important thing is that **every general agrees on a common decision**, for a halfhearted attack by a few generals would become a rout, and would be worse than either a coordinated attack or a coordinated retreat.

The problem is complicated by the **presence of treacherous generals** (with "Byzantine behavior") who may not only cast a vote for a suboptimal strategy, they may do so selectively. For instance, if nine generals are voting, four of whom support attacking while four others are in favor of retreat, the ninth general may send a vote of retreat to those generals in favor of retreat, and a vote of attack to the rest. Those who received a retreat vote from the ninth general will retreat, while the rest will attack (which may not go well for the attackers). The problem is complicated further by the generals being physically separated and having to send their votes via messengers who may fail to deliver votes or may forge false votes.

Now from the general problem, let's see how this works in our distributed system. It's a specific problem, the **interactive consistency**, which Lamport solved by theorizing a system with:

- N processes with k faulty processes with "Byzantine behaviour";
- Each process i produces a value `V[i]`;
- Goal: the `(N-k)` non-faulty processes need to reach agreement on a result vector of values (one for each process) isolating faulty processes.

Lamport formulated the Byzantine agreement algorithm, assuming that the system has the following properties:

- Synchronous processes;
- Unicast communication;
- There is a bounded communication delay;
- The order of the messages is preserved.

### Example: The Algorithm Application

Let's see an example of how it works with N = 4 processes and k = 1 faulty process:

1. Each process sends (with reliable unicast) its value to the others. The faulty process may send different values, so we can't trust it (a);

   ![](img/byz1.png)

2. Each process assembles a vector from the received values, which represents his overview of the system (b);

3. Each process sends its vectors to the others. The faulty process, again, can send different arbitrary vectors. The vectors that each process receives are shown in (c);

   ![](img/byz2.png)

4. Each process then examines the i-th element of each of the received vectors, and then:
   - If any value has a majority, it is put in the i-th element of the result vector;
   - Otherwise, *UNKNOWN* is put in the i-th element of the result vector.

5. The consensus is achieved on the v[1], v[2] and v[4] values of the result vector among the 3 non-faulty processes (we can't tell anything about v[3]).

It has been proved that **at least 3k+1 processes are needed** to solve this problem with k faulty processes.

## Impossibility of Agreement in Asynchronous Systems

Let's see an example of N = 3 and k = 1 in which consensus cannot be reached.

![](img/byz3.png)

We can clearly see that the two non-fault processes cannot create a valid result vector since they don't have enough reliable data to have the majority.

Remind that this only works under a determined set of assumptions. Agreement in asynchronous systems is impossible because of the arbitrary delays that come with it.

When delays in answering messages are arbitrary there is no guaranteed solution to Byzantine agreement (neither to totally ordered multicast) even if a single node fails. There are solutions for partially synchronous systems that can be used to model practical systems.

## Others

- Reliable client-server communication.
  - *Example*: RPC in presence of failures.
- Reliable group communication.
  - *Example*: Atomic multicast
- Distributed commit.
  - Generalization of atomic multicast to arbitrary operations performed by members of a group (all-or-none semantics).