# Distributed Systems Architectures

A distributed systems' architecture defines as:

- **The main entities of the system**.
  - Processes/threads, sensor nodes, objects/components, services.
- **Pattern of communications**.
  - Publish-subscribe, message-oriented ... (it depends on the architecture).
- **How the entities communicate**.
  - The technology used to apply the pattern: RPC, Sockets and so on.
- **The role of those entities** (and how it evolves in time).
  - clients, servers, masters, slaves, and so on.
- **How the entities are mapped to physical infrastructure**.
  - caching, replication, clustering in containers (for example of server entity).

We're gonna see different types of architectures based on the roles of its entities:

- **Centralized**, the classic client-server model;
- **Decentralized**, the peer-to-peer model;
- **Hybrid**, a mix of centralized and decentralized.

# Centralized Architectures

The classic centralized architecture is the client-server model, where **many client refers to a server** who can refer to other servers as well.

![](img/csmodel.png)

Generally, the expected behavior of the entities in this model is the request-reply, in which the client asks the server for something and the server replies with the content requested (or an error if it's unavailable).

![](img/csbhvr.png)

<div align=center><i>General interaction between a client and a server</i></div>

## Multi-Tier Client-Server

Architectures can get complicated: **multi-tier client-server**, where a client refers to an applicative server that contains the application logic which refers to a database server (three level system):

- Client devices + Application server + Database server;

When implemented, the client-server architecture is usually multi-tier. We can distinguish between front-end and back-end.

- The **front-end** is typically the part of the application present in the client-side computer which gives an UI for the user to;
- The **back-end**, instead, is the part of the application which resides on the server machine and handles client requests and hold their data.

However, we'll see that usually there's not a fine line between what features are considered for the front-end and which for the back-end. For this reasons, each client-server distributed system follows the most fitting model for its purpose.

![](img/multics.png)

<div align=center><i>Tier 2 shrinks application logic and data into one server, while Tier 3 keeps them as separated entities.</i></div>

![](img/multitier.png)

## Alternative Organizations

Many different organizations are possible depending on the **task division between client and server**, by deciding which task should be handled by who, creating many possible combination.

![](img/mixarch.png)

*From a dumb terminal in model (a) to a heavily client-side application (e), we have all kinds of possible models.*

So there is not a better model than the other: we must make the right choice for our distributed system, while also following our constraints of transparency, scalability and more.

## Caching in Client-Server

Scalability-wise, we have to consider how to **reduce the load of requests from multiple clients** to our servers. Considering the web-page example, usually all clients requests would be kinda the same (give me `page.html`): it would be wise to cache those frequent requests in order to have them already available to anyone requesting them.

![](img/prox.png)

To achieve this, a **proxy server** acts as a man-in-the-middle between clients and servers: it holds a cache of highly requested pages and gives them to the clients. If the request is not satisfiable by it, then it forwards the request to the web server. This technique is called **caching**.

With proxy servers we avoid overloading server with the same requests, returning "fresh" data, stored in a cache memory.

## Architecture Distribution

When designing some loading balance mechanism, it is imperative to take into consideration how functionalities are distributed among the servers in our distributed system.

There are generally two types of distribution:

- **Vertical distribution**: A distinct level for each functionality, each one managed by a server;
  - *Example*: In the 3-tier model we saw before a server handled the application logic while another handled the database.
- **Horizontal distribution**: Same functionality distributed on multiple servers;
  - *Example*: In a resilient service, I could host the same web-page or service on different servers to balance the load and offer service even with a high number of users. This is what happens, in the web-page and caching model we saw before: i have multiple web-servers with the same functionality, ready to replace one another if anyone fails.

Those distribution types can coexist in a distributed system, creating a **hybrid distribution**: I can have a 3-tier model (vertical distribution) in which I have multiple application logic servers to handle connections with clients (horizontal distribution); this way, we would have a horizontal distribution in a tier of a vertical distribution.

## Event-Based Communication Pattern

Client-server is not the only pattern in the centralized architectures: completely opposed to the request-response there's the **publish-subscribe pattern**.

![](img/eventcomm.png)

There is a single component (typically a service) that produces tagged data, named **publisher**, and other components that are subscribed to it, known as **subscribers**, that receives that data (or only those fragments of it in which they're interested in).

# Decentralized Architectures

## The Peer-To-Peer Model

A common decentralized architecture model is **peer-to-peer**. In this model **every entity has the same functional capability of any other** (this typically means that they run the same code).

These architectures are designed so that 

- Each user can contribute resources to the network (objects, data and so on...);
- Every operation doesn't depend on any centrally administered system (because they're all peers).

![](img/p2p.png)

## P2P Example: Napster

Napster is a well known peer-to-peer system: it's original function was **file and music sharing over the web**. However it wasn't a pure P2P system: it involved a central server index.

In Napster **centralized addresses are replicated on directory servers**, but **files are provided by peers**, users. When a user/peer requests an index for a certain file, the server replies by finding that index and returning a peer list of those peers that have that file, then the user peer choose one of them (closest one, more reliable, ...) and make the file request to them, if the machine is available.

So peers could:

- Manifest the availability of a file in their system; 
- Request for the peers who had a particular file to download it from one of them.

![](img/nap.png)

In the end, we could say **it's an hybrid system**, but more P2P-oriented.

## P2P: A Key Problem

One common problem of peer-to-peer systems was finding an efficient way on how to place these shared files and objects across many hosts (in other words, distributing them uniformly). Generally, the goals were:

- **Achieving load balancing while accessing data**, to avoid that a single node is overloaded with requests, adapting the load to the node capabilities and distributing it to others that have the same files;
- **Ensuring availability avoiding overheads**, in order to optimize the localization and distribution of the resources to avoid that a resource is only available in a certain region and high latencies for those far from that location.

## Three Generations of P2P Systems

In time, three generations of P2P systems were identified:

- **First file system sharing**, the ancestors. 
  - Napster is one of the first sharing systems: they usually relied on a central database to co-ordinate look ups on the network.
- **Protocols are refined** to improve scalability, fault tolerance, anonymity.
  - Gnutella, improves scalability by implementing a full P2P system, instead of an hybrid one like Napster, by using the flooding mechanism to locate files and nodes. No one is different, peers can get the same data from each other peer, and they only knows some of system peers, not all of them;
  - FreeNet, improves anonymity and privacy of the users;
  - bitTorrent, improves scalability by fragmenting files and distributing it through peers this way. His main goal was to improve scalability with file shattered in pieces, so that a peer can take those pieces from different peers and rebuild the original file.
- P2P with a **middleware**. Instead of just being specialized software for file sharing, these are multi-purpose middleware usable in any distributed system. They usually use DHT (Distributed Hash Tables) to look up files in the network: they store resource locations throughout the network, with the intention of locating the desired nodes quickly.
  - Pastry, Tapestry, Chord, Kademlia;

Middleware P2P are the latest evolution and their goal is to:

-  Locate and communicate with any resource;
- Add and remove resources;
- Add and remove peers.

## Overlay Network

Efficiency is the key, so there are **multiple optimization criteria** to achieve it such as: global scalability, load balancing, locality of interaction, support of encryption and authentication, anonymity.

For this reason, P2P systems are often organized in a **overlay network**, a virtual network in which nodes are formed by processes and links represent possible communication channels. 

Basically, it is an abstraction, **a simplified logical network over an existing lower level network** (the Internet). It's a logical scheme, not physical, of communication between system peers, with optimization criteria based on internet.

An example of overlay network is Chord, where peers are organized as a big ring where each peer sees only one ahead and one before of himself, knowing only their addresses (single o double linked list).

### Routing Overlay

Once the logical scheme is established, a good routing algorithm is needed to find what is required. It has to be a **distributed algorithm**, without a coordinator, that can find nodes/objects/resources in an overlay network.

This **routing overlay algorithm** should be able to:

- Route requests from clients to hosts holding objects at application level instead of the network level (IP);
- Routing is based on global identifiers (IP/port? URI? URL? depends on the naming convention) and should direct to one of the replicas of the requested object;
- Routing overlay protocols also deal with insertion/removal of objects but also nodes.

### Two Types of Overlay Networks

There are two main types of overlay networks:

- **Structured**.
  - Overlay network built deterministically (often via hash tables) in order to obtain efficient routing towards the node containing the required data. These are the most common implementation of overlay networks, however they could have some problems when the network is ever-changing (peers added, going off, ...).
- **Unstructured**.
  - The network is built using randomized algorithms, in which each peer only knows its neighbors (Gnutella). It is scalable, but it has its own problems: the structure is highly dynamic, and in some cases, there's the risk of having a disconnected network, with two or more separated regions;
  - For these reasons, sometimes they implement super-peer nodes, which introduces a hierarchical structure to keep a stable backbone for the network (losing the full P2P structure, though);
  - Other mechanisms to avoid infinite searches along the network (neighbor to neighbor) are to limit the number of hops or the time to live of the search.

## A Structured P2P Overlay Network: Chord

Chord is an example of structured overlay network involving **a ring shaped network made of nodes and data**, that implements an algorithm based on a DHT (Distributed Hash Table).

![](img/chord1.png)

The idea is to have a fixed addressing space, with unique identifiers, each node has been given an address in that space (generated in many ways like hashing on IP address).

This algorithm has an **addressing system** based on the number of bits assigned for each global identifier in the network: In this example we're using 4 bit for addressing, therefore we have 2<sup>4</sup> = 16 possible identifiers (in this case 5 nodes + 11 objects). Usually however they are 128 to 160 in real implementations of it.

Each node entering the network gets an **id** in that addressing space, usually calculated by hashing its IP or other info, while each object (data item) gets a **key** in the same space.

As we can see in our example, each node is holding a bunch of data, but how is it determined who holds what? 

The algorithm states that the object with key `k` must be held by the node which id `i` is 

$ i = \min{\{id \in ids \ | \ id >= k \}}$.

That intuitively means that the node which holds that object is the one with the smallest id bigger than the object key : in our example the object 6 is held by the node 7 because `min{7,12,15} = 7`

**N.B.** if 15 was an object instead of a node, 1 would take all the objects behind him (13,14,15,0,1). This is because we have to consider the modulo since it's a ring structure.

The function that returns the id of the holder of the object `k` is called `lookup(k)` or `succ(k)`. The challenge here is to make `lookup(k)` as much efficient as possible: in Chord it is done via DHT.

If DHT wasn't implemented the lookup would take O(n), since in the worst case, all nodes should be interrogated on a single resource: this is because the ring is a linear structure. However, since we have DHT, we take a lot less time since we need to just lookup on the distributed hash table for it.

![](img/chord2.png)

Each node holds an hash-table (called **finger table**) of n-bits length (in this example 5 since we have 2<sup>5</sup> nodes) containing key-value pairs:

- as key they have an index `i`;
- as value they have a number that is the result of *lookup(p + 2<sup>i-1</sup>)*, with `p` id of the node that holds that finger table.

This way, I can take fewer steps in searching for a node: let's see an example to clear our mind

Let's suppose we want to resolve k = 12 starting from node 28. Which node holds it?

1. Let's look at the finger table: we don't know if the node 14 holds effectively 12 (because it could be possible that nodes 13 or 12 has it), so let's ask at the bigger node with id < k, which is node 4;
2. Now, node 4 looks at his finger table: for the same reason, we ask node 9 since it's the bigger node with id < k in the finger table;
3. Again, node 9 looks at his finger table and asks to node 11;
4. Now, node 11 does the same: however he notices that the smaller node he knows of has id > k. This means that 14 holds the object with k = 12, so 11 tells to node 14 that object 12 is needed.

In just 4 steps, the resource was found: we saved time at the little cost of holding a finger table for each node.

## A Bi-Dimensional Overlay: CAN

The CAN overlay network, instead of a linear ring, uses a **bi-dimensional address space**, where each node has an assigned area of space in which all objects and data are handled. 

If a new node connects, one of the areas split in two to accommodate it. In the same way, if a node is removed, then the orphan area is split and handled by the remaining nodes.

- Assignment of data to nodes in CAN;
- CAN uses a bi-dimensional address space;
- Each data key is a point;
- Each node manages all the point in its region.

![](img/can.png)

## Limitations of Structured Overlays

The use of DHT allows a search in logarithmic computational time, so we have good advantage in using it.

Maintaining complex overlay structures can be difficult, it is needed a **self-organization between nodes**, that they can be naturally resistant from failure. 

For now, it has been explained how to make the lookup function efficient for Chord and similar systems, however, the insertion/deletion of a node needs to be efficient. As expected, structured overlays **require more maintenance and management** to make everything work towards the goal. 

One could implement self-organizing nodes to make the network more resilient to failures (abrupt disconnection of nodes, management of orphan resources, and so on...), however this still imposes a **computational cost on each node**.

Unstructured overlays don't have these kind of problems because they don't follow deterministic algorithms to create and manage the network, making them naturally scalable and resilient.

## Unstructured Overlays: Peer Sampling

Unstructured overlays **follow the peer-sampling technique**, it mimics the social paradigm of "person that knows other persons": In a network each peer knows a bunch of neighbor peers, which then know another bunch of neighbors and so on...

This way **nodes periodically exchange their view** and update their personal random views of the network creating their own random sample. 

When a node wants to find a resource, the lookup function goes searching for it in his neighborhood first, then asks for neighbors to search for it in their neighborhood and so on: This method is called **query flooding**.

This lookup method is not very efficient and can effectively flood the network of requests, so usually **there's a limited number of "hops"** between neighborhoods before stopping the research.

The node insertion/deletion as we said is almost free, since we just need to add the new node to another node neighborhood or remove the node from it.

# Hybrid Architectures

Napster was an hybrid architecture, in which nodes had a client-server relationship with the server index while they had a peer-to-peer relationship with the rest of the nodes. 

Another type of hybrid architecture is **the super-peer network**: In this case we can identify sub-networks of peers, each administered by a super-peer. These sub-networks can only talk together through the super-peers, which also act as a proxy for the sub-network.

<img src="img/super.png" style="zoom:67%;" />

Yet, another hybrid architecture is **the BitTorrent network**: similar to Napster, a node communicates with a web server in order to obtain a `.torrent` file. This file contains all the meta-informations necessary to connect to a tracker which knows which other nodes have either the requested full file or a portion of it (fragments).

<img src="img/bitt.png" style="zoom:67%;" />

# Micro-services, Containers, and Cloud services
## Micro-services Architectures

In the last years, there is a technology that is taking off quite well: the **micro-service architecture**.

Micro-services pushes forward the vertical distribution, **running each functionality of the application logic in a separate process**, possibly on a separate machine.

This way micro-services have the excellent quality of being **independently replaceable and upgradeable**, while also being packaged with all they need to be deployed. The granularity also allow extremely **focused scalability**, enhancing only the pieces of functionality that need to be scaled.

They communicate and coordinate themselves using lightweight mechanisms (like the REST API or through RPC). 

They can even be written by different teams in **different languages**, provided that they **share a common and understandable API** in order to understand each other.

<img src="img/micros.png" style="zoom: 80%;" />



## Containers

Containers are an abstraction at the application layer that **packages code and dependencies together**. This way I can achieve **virtualization at process level in user space**, instead of emulating all the hardware as a virtual machine.

Containers are super useful for micro-services since now I can put an entire component inside a container, with all the dependencies and libraries needed, and replicate only that container, enhancing granularity to achieve scalability.

Main goals for containers are:

- **Offering efficient support for the micro-services architecture**. As we said, I can create an appropriate container for each micro-service, with all the dependencies needed and replicate it with little cost;
- **Increasing the portability, enabling easy migration**. Since everything is contained, migration is just a matter of moving a container file;
- **Reducing problems at deployment time**.

*Container Vs VM*:

![](img/vmvsctr.png)

There are multiple virtualized services running as an application on a real machine, instead of services on virtualized machine on a real machine: removing a layer of virtualization (the OS) reduces the computations cost and making it a lot more portable.

## Cloud Services for Distributed Systems

**Micro-services and containers** are working so well that a lot of the big techs use this combo themselves to **offer cloud platform services** on a distributed system.

A set of services are built on distributed system, for example:

- **Communicating**.
  - *Examples*: Google PUB/SUB that offers message-oriented middleware on cloud and also many-to-many asynchronous messaging;
- **Computing**. 
  - *Examples*: Google DataFlow;
- **Storage & DBMS**.
  - *Examples*: Google Distributed File System (GFS), Distributed RDBMS/NoSQL DBMS;
- **Identity & Security**.
- **Management**.

They offer several types of transparency. The most famous services are: 

- Amazon AWS;
- Google Cloud Platform;
- Microsoft Azure.

## Extensibility and Portability in Cloud Platforms

Extensibility and portability are currently a problem for complex solutions running on a specific cloud platform (AWS, Google, Azure, ...), but the introduction of cloud containers and micro-services help portability, thanks to granularization.

They had to face another challenge though: they needed **tools for orchestration** of the micro-services and for their **distribution** over multiple machines.

### Kubernetes

Kubernetes is a portable, extensible, **open-source platform for managing containerized workloads and services over clusters**, facilitating declarative configuration and automation.

Each application component is called **pod**, and many of them are hosted in **multiple worker nodes** in the clusters. All of the pods and nodes can be managed through a **Control Plane**.

![](img/k8s.png)

### Google File System (old)

Google needed a distributed file system which could answer to these needs:

- Storage of very large files;
- Use of large clusters of “commodity" computers (high failure rate);
- Files are mostly read or appended to;
- High data throughput desired.

The main idea was to **divide file into chunks** with unique labels and distribute them over several **chunk-servers**. This way they would be handled by a **master server**, keeping the meta-data needed to map the chunk labels over the chunk-servers.

What we've got here is a simple client-server architecture, in which the GFS client could contact the master server in order to obtain the information of which chunk-server to interrogate to retrieve the file.
