# Distributed Ledger Technologies and Blockchain

Distributed register, a blockchain is an example of DLT. 

Blockchain is a buzzword, considered by market research among the “essential eight technologies” with many related applications.

![](img/buzz.png)

## Blockchain: The Story

Bitcoin. This is the first term in a 2008 white-paper "Bitcoin : A Peer to Peer Electronic Cash System", which author, under the fictional name of Satoshi Nakamoto, remains unknown.

Its first open-source implementation and the first client is released in 2009, just a year later. In the following years, many more other DLT-based crypto-currencies are created: XRP, LiteCoin, Ethereum and many many more. 

The blockchain technology is in wide expansion and it has been implemented for other reasons other than Bitcoin and money, like Ethereum, which was created to go beyond simple crypto-currencies by handling tokens and "smart" contracts.

## The DLT System Model

The **DLT model** is a distributed system which has:

- **Decentralized control**, handled by decentralized algorithms;
- **No trust and byzantine behaviour** because nodes run by different entities that do not trust each other and/or may even be malicious;
- **Distributed and consistent data**, where a copy of data records (financial transactions, medical records, sensed data ...) is stored at each node consistently.

This is a complex distributed system to work with because of the **big consensus problem**: how can two nodes agree on something if they don't trust each other? They need to **agree on a history of data** (a ledger with a history of financial transactions, for example). They will need to agree on the transactions and on the order in which they were executed.

## Data in Blockchain

A **blockchain is a ledger of transactions**, where each **transaction is a data record**. This data structure is exactly what needs to emulate an history of transaction.

<div align = "center">
    <img src="img/transaction.png"  />
    <i>We can see, in this example, a Bitcoin transaction: Alice wants to transfer 0.15 BTC to Bob. The Bitcoin transaction forces the payer to use all their money on the wallet because the amount of input BTC must equal the amount of output BTC. This means that Alice must input 0.2 BTC, however, since she has to pay Bob only 0.15 BTC, the remaining 0.05 BTC will go back to Alice as "change".</i>
</div>

In the usual centralized transaction model, there is a third "person" which is a central authority into which everyone puts his trust (the bank). Its job is to register and witness transactions between people that don't trust each other. Here instead, in the distributed world, the **trust is based on the blockchain**.

The blockchain approach consists in **having a distributed algorithm to handle those transactions**, in a world where nodes don't trust each others. The validation of a transaction is made by each node independently, the characteristic of a register is to store something and **it stays in that document history forever**. Database append-only.

## The Blockchain Approach

![](img/block.png)

Each transaction is digitally signed (with the private key of the sender) and propagated to all participant nodes.

In a blockchain, **transactions are grouped in timestamped blocks** and the whole history is stored at each node as a chain of blocks (therefore blockchain). A block is made of:

- A **set of transactions** (a tree);
- A **timestamp**, that says when that block has been added;
- The **hash of the previous block** in the blockchain;
- A **Nonce**, a simple number, used in hashing.

The complete block is then propagated to all participating nodes in the network.

![](img/blockchain.png)

Transactions are stored in the `Tx_Root` structure, where **each transaction is stored in an hashed Merkle Tree**, an efficient way to store and find our transactions in our blocks.

When a node receives a transaction, it validates it (according to some network-wide rule). For example, in a financial transaction it is possible to check if the sender actually has the money that he's spending. However, even if the transaction is validated, it is still considered as "pending" until the block in which it is contained belong to the blockchain.

Unfortunately, validating a transaction isn't enough for it to gain the consensus of the whole network.

## Problems

Since there is an unpredictable latency, imprecise clock synchronization and faulty (or even malevolent) nodes:

- There could be a **different order of arrival** of transactions from node to node;
- Some transactions may be **contradicting each other**:
  
  - The *double-spending problem* is a great example: The issue with it is a problem when someone wants to spend cash that he does not have. When paying for a sandwich with a 10 dollar bill, after giving that bill to the maker of the sandwich, you cannot turn around and spend that same 10 dollar bill elsewhere. 
  
    A transaction using a digital currency like Bitcoin, however, occurs entirely digitally. This means that it is possible to copy the transaction details and rebroadcast it such that the same bitcoin could be spent multiple times by a single owner. It can happen both for technical issues or because of malevolent nodes.
- Different nodes may **build different blocks**. Since some nodes receive the transactions in a different order because of latency, the blocks will be different from the others, even if equivalent and validated;
- Different nodes may end up with **different chains** (no consensus). For the same reason, nodes with different but equivalent chains can't reach consensus.

## Consensus in Blockchain

The principal challenge to solve these problems is to have a **consensus on the blocks and on the sequence of blocks** (each node should eventually have the same copy of the blockchain).

The main idea that came in mind in the Bitcoin paper and was implemented to fix this problem is the **Proof of Work**:

- Compute the hash of each transaction and of the block;
- Include, in the hash of the block, the hash of the previous block in the chain;
- Include a trick to make the computation of the block's hash expensive but very easy to verify;
- Make the nodes compete on this computation with a reward, and have the winner propagate its computed block to the others (it is like "electing" a node to impose its block).

The core of this idea is the trick to make the computation "waste time" on the nodes, needing them to make it worth the time spent computing. Each block in the blockchain should be verified before adding more blocks to it: this is because the authenticity of a block also depends on the previous ones.

The implementation of this consensus idea is the **Proof-Of-Work algorithm** (POW): The trick here is a computational puzzle, needed to verify a block. The idea is to **have a difficult problem to solve, but very easy to verify** its correctness (ex. Rubik's cube).

## Hashing

Let' remember what hashing means. A cryptographic hash function `f` (ex. SHA-256) should:

- Have `f(A)` with **fixed length**, which means that should be the same length (ex. 256 bits) independently of `A`'s length;
- Be **collision resistant**, which means that if `A ≠ B` then `f(A) ≠ f(b)`;
- Be very **difficult to find `A`** from `f(A)`;
- Be very **easy to obtain `f(A)`** from `A` and verify that `B = f(A)` or not.

## Blockchain Proof of Work Algorithm (PoW)

To enforce Proof of Work, we need to add a computational puzzle. Let's see an example of how a block can be validated through this PoW.

![](img/redblock.png)

Let's look at the typical structure of a block (except the tree structure for transactions and other details): the hash of the entire block is determined by all of its contents, which are in this case:

- The block number;
- The Nonce, which is a key integer for our puzzle;
- The data, which can be our transactions.

This means that **changing anyone of them will change the hash of the block**. The main idea here is to put a constraint to the hash in a way that if it is satisfied, then the block is valid.

To satisfy this constraint we can modify the data or the block number but it wouldn't make sense in the block semantic: we will instead **find a particular integer**, with which fill the **nonce** field, **which can satisfy this constraint**.

*Example*: Let's suppose that a block is valid if its hash starts with four 0s (hash: 0000xxxxxx...), then, i need to find a certain value for my nonce in order for my hash to satisfy that constraint. Since this is not an easy operation (remember, we needed a difficult trick), the only thing left to do is to "mine" for it, which means find it via brute force and trying all the possible combinations of numbers.

After a quite long period of time, we could find the correct nonce of 37174 in order to obtain a hash starting with four 0s.

![](img/greenblock.png)

Now, **the block "turned green" and is validated**: this block is now part of the local blockchain and is ready to be sent to the other nodes, hoping to be part of the others blockchain too.

![](img/blkchainred.png)

However, we have to remind ourselves that in a blockchain each block should point to the previous one and this field must be used in the hashing function; for the block to be able to enter the blockchain, we need to recalculate the nonce accordingly.

![](img/blkchaingreen.png)

Now the block is correct and verified in the local chain and is sent to the network.

When each node receives a block, it checks the puzzle solution and if the nonce is correct and the block is valid, then adds it to its local copy of the chain (linked to the one whose hash is in the block).

![](img/blockpow.png)

We can see that, **at some point, a tree is created instead of a chain**: This can happen for various reasons like latency, transaction order, or malicious nodes, however, by assuming most nodes are working on the same chain, the one growing fastest will be the longest and most trustworthy. 

If a malicious node wants to change a transaction in an intermediate block, it has to re-compute all the subsequent block hashes in his chain and prevail over other nodes in the network, and since we said that recomputing the hash by finding the correct nonce takes a long time, the whole re-computation of the chain will take even longer. By the time the malicious node has finished recomputing, the other nodes would have completed several blocks already, overthrowing the malicious node plans.

Even if the malicious node is faster than the other nodes, the blockchain system is safe through chain-consensus as long as more than 50% of the work being put in by miners is honest, because **if the majority of the nodes are honest and share the same blockchain, they can have consensus on their chain**, not trusting the malicious chain.

However, if this quota isn't met, the system would be in chaos, with most nodes acting maliciously or faultily. In these situations, with this system, consensus would be impossible to reach.

## Other Consensus Algorithms

Other than the Proof of Work (PoW), many more other consensus algorithms exists, based on different premises or evaluating other features of the nodes.

![](img/conswheel.png)

BITCOIN EXPLAINED: https://www.youtube.com/watch?v=Lx9zgZCMqXE

## Smart Contracts: Beyond Bitcoins

The Blockchain is useful to store more than financial transactions, since blocks can contain any data inside of them. Beyond Bitcoin, **Ethereum** was specifically **designed for smart contracts** and more.

A **smart contract is a piece of software code defining a self-executing "contract"**. While originally proposed as digital version of a legal contract, it can be a general software program.

The program is usually stored on the Blockchain (becoming "immutable", just like a contract) and any node can verify if the conditions of the contract are satisfied and perform the consequent actions (the output is "distributively validated"). In the end all nodes should agree on the resulting "state".

An application example is a **crowdfunding platform without any central authority**.

![](img/kick.png)

In a centralized crowd-funding platform (like Kickstarter), it is the platform that holds the money as a third-party, between the contractors (funders and funded). 

If the goal is met, the product team gets his money from the platform, otherwise the supporters get the money back. This means that both sides need to trust the third-party.

![](img/smartc.png)

In a decentralized one, instead, each node can transfer each money on the smart contract. This means that the money lives in a **distributed and immutable blockchain**:

- The fact that is distributed makes it easy for every person to **verify if the contract has been satisfied** or not (thus knowing if the money should go to product team or back to the supporters). The trust is gained through consensus;
- The fact that is immutable makes it so that **nobody can tamper on the premises of the contract** or modify the code backwards.

As this can be useful, it is also difficult to work with, because the fact that code is immutable makes it difficult to fix it if any bug appear in the smart contract. For this reason, some new languages are growing ad-hoc for this kind of development.

## Blockchain Sustainability

> "In order to secure a blockchain it's estimated that both Bitcoin and Ethereum burn over $1 billion worth of electricity and hardware cost per day as part of their consensus mechanism".

This is what will put the nail in the coffin of these blockchain mechanism: They are **unsustainable now and in the long run**, since the actual consensus mechanisms (such as the PoW we saw before) waste too much energy on making those heavy calculations for the nonce values and solve similar puzzles. Moreover, the mining reward, decreasing after successful block, will become so little that mining it will be a cost instead of a profitable activity.

Ethereum, for this reason, now uses **Proof of Stake** as consensus mechanism to mitigate this waste, but this is a pressing issue and a lot of research is being done in this area.

## Permissionless Distributed Ledgers

The blockchains we talked about until now are all **permission-less distributed ledgers** because: 

- They're decentralized and track all transactions;
- Don't have a trusted third party;
- There's an unconditioned access to the ledger (each node has its own);
- Transactions validation and new "coins" generations are performed by miners (everyone can do it;)
- There's a pseudo-anonymity of participants;
- Transactions are immutable.

This is very similar to a peer-to-peer network. However, an hybrid of a centralized and decentralized approach is given by **Permissioned Distributed Ledgers**, characterized by additional trusted third parties which offer a conditioned access to the ledger and different permissions to his users, acting as super-nodes.

| Permission-less                                              | Permissioned                                                 |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| They're decentralized and track all transactions             | They're decentralized and track all transactions             |
| Transactions are immutable                                   | Transactions are immutable                                   |
| *Don't have a trusted third-party*                           | *One or more trusted third-parties*                          |
| *There's an unconditioned access to the ledger (each node has its own)* | *There's an conditioned access to the ledger (only elected nodes can access it)* |
| *Transactions validation and new "coins" generations are performed by miners (everyone can do it)* | *Transactions validation and new "coins" generations are performed by miners (only elected miners can do it)* |
| *There's a pseudo-anonymity of participants*                 | *Participants have a well-known identity*                    |



