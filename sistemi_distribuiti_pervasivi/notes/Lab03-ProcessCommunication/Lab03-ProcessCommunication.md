# Process Communication

In a distributed system, processes of different machines, connected through the web, needs to communicate, and the only way they can do that is by **exchanging messages**.

The cooperation between processes on the web consists in two main operations:

- Transfer messages;
- Receiving messages.

Those messages gets exchanged via socket communication by using more structured protocols than simple strings: on an applicative level, data are stored in some structure, which is send to a process, and on web level data are communicated as a stream of bytes.

Problem: Processes can be heterogeneous, that means they can store data in different ways that may not be directly compatible:

- Big endian VS Little endian;
- Different floating point representation;
- ASCII VS Unicode;
- ...

There needs be a mutual agreement on how to represent data. Each data goes through two main operation in order to prepare them to be transmitted:

- **Marshalling** - Carried on by the transmitter, where data got assembled in a suitable format ready to be sent;
- **Unmarshalling** - Carried on by the receiver, where data get translated in an opportune readable data structure.

The use of an external data format is arranged to grand interoperability. The main formats are:

- XML;
- JSON;
- Protocol Buffers.

## XML - eXtensible Markup Language

XML is a standard for information storage, a generalized markup language that got used to create many dialects (ex. HTML).

The main advantage of XML is that it can be read both by humans and machines ("""easily""") and it has many instrument to validate documents, some tools through with it is possible to know if a document is false.

```xml
<researcher>
	<name>Gabriele</name>
	<surname>Civitarese</surname>
	<publishedPapers>
		<paper>
			<title>activity recognition in smart homes</title>
			<year>2014</year>
		</paper>
		<paper>
			<title>activity recognition again</title>
			<year>2015</year>
		</paper>
	</publishedPapers>
</researcher>

```

## JSON - JavaScript Object Notation

JSON  is a more recent standard that took the place of XML. It was born for Javascript but it's completely independent from it and gets used almost everywhere.

It is sill human and machine readable, but is more compact and readable than XML, and it offers also the possibility of array description.

Those features makes the transmission quite faster for the same information content

```json
{
	name : Gabriele,
	surname : Civitarese,
	publishedPapers : [
		{title : activity recognition in smart homes , year : 2014},
		{title : activity recognition again , year : 2015}
	]
}
```

For a specific definition of JSON objects see http://json.org/

*JSON encoding example in Java*:

```java
public class Researcher {
	//...
	public String toJSONString() throws JSONException {
		JSONObject researcher = new JSONObject();
        researcher.put( " name ", this.name);
        researcher.put( " surname ", this.surname);
        JSONArray publications = new JSONArray()
        for (Paper p : this.publishedPapers) {
            JSONObject publication = new JSONObject();
            publication.put( " title ", p.getTitle());
            publication.put( " year ", p.getYear());
            publications.put(publication);
        }
        researcher.put( " publishedPapers ", publications);
        return researcher.toString()
    }
	//...
}
```

*JSON decoding example in Java*:

```java
public class Researcher {
	//...
    public Researcher(String jsonString) throws JSONException {
        JSONObject input = new JSONObject(jsonString);
        this.setName(input.getString( " name " ));
        this.setSurname(input.getString( " surname "));
        JSONArray array = input.getJSONArray(" publishedPapers ");
        for (int i=0; i<array.length(); i++){
            JSONObject current = array.getJSONObject(i);
            String title = current.getString(" title ");
            int year = current.getInt( " year ");
            Paper p = new Paper(title, year);
            this.publishedPapers.add(p);
		}
    }
	//...
}
```

## Gson

Gson is a Google library used to convert Java objects into a JSON representation and vice versa. 

This is a strong and useful tool that allows each pre-existing Java object type, even without knowing it's source code, but i does not support annotation and generic data types. It saves a lot of implementation time.

The library contains two main methods:

- Method `toJson()` used to pass from an object to a JSON string;
- Method `fromJson()` used to pass from a JSON string to an object instance.

More info at: https://code.google.com/p/google-gson/

## Real Data Transfer

All data are represented in their textual codification (no problem for string, numbers, booleans). 

To send these data, there must also be a specific encoding for binary data: **Base64**

- Used to transform a bit string into an ASCII char string;
- Its main idea is to divide a bit string in 6 bit blocks (= 64 values) where each value is represented as a letter.

# Protocol Buffers

Mechanism to serialize data structure proposed by Google, a more compact and efficient than XML and JSON.

In protocol buffers, data are represented in a binary format, so they lose human readability, but it becomes more efficient on the web. There is a language completely independent from the programming language that is used to define a schema for data structures.

When the data structure scheme is defined, it gets automatically generated the marshalling and unmarshalling code, based on current language, then those data can be read and written on different streams.

Highly interoperability and there are libraries to support many languages. 

## Using Protocol Buffers

Define on file with a `.proto` extension the message format that will be serialized:

- It is necessary to define the expected fields and their type;
- Files `.proto` must be shared among all programs that wants to communicate.

Run the Protocol Buffer compiler to automatically generate marshalling and unmarshalling code:

- From command line: `protoc –proto path=src –java out=build/gen src/foo.proto`

After the compilation, it is possible to write code that is able to send and receive messages.

Each field of a message must be defined with one of the following keywords:

- **Required** - An obligatory field;
- **Optional** - A field that can be omitted;
- **Repeated** - A field that can be repeated `n` times, like a dynamic array.

*Scalar types:*

<img src="img/scalartypes.png" style="zoom:120%;" />

## Tags

A message is a record with different fields, where each field has a type and is associated with a **unique tag**, a unique number that is used to identify each field in the binary format.

Tags are used to match fields in marshalling and unmarshalling operations:

- Tags from 1 to 15 are represented with a byte and are used for the most frequent fields;
- Tags from 16 to 2047 are represented with two bytes.

Supposing that there is a long field name, for efficiency in the binary representation it is not memorized the full field name, but it gets substituted by its tag, used for both marshalling and unmarshalling operations.

Tags help back-compatibility on message definitions: supposing to have a 4 field protocol, after some times a new field is added and this new field is not used on all code version. Systems that doesn't not have an updated version of the code can ignore the new fields.

**N.B.** Tags must not me changed once the message is effectively used in the system!

*A `.proto` file example*:

```protobuf
message Researcher {
    required string name = 1;
    required string surname = 2;
    
    enum ResearcherType {
        PHDSTUDENT = 0;
        POSTDOC = 1;
        ASSISTANTPROFESSOR = 2;
    }
    
    message Paper {
        required string title = 1;
        required int32 year = 2;
    }
    
    repeated Paper paper = 3;
    optional ResearcherType type = 4;
}
```

*Build data structure from a compiled `.proto` file in Java*:

```java
// ...
Researcher researcher =
    Researcher.newBuilder()
        .setName( " G a b r i e l e " )
        .setSurname( " C i v i t a r e s e " )
        .setType(Researcher.ResearcherType.POSTDOC)
        .addPaper(Researcher.Paper.newBuilder()
            .setTitle( " A c t i v i t y R e c o g n i t i o n ")
            .setYear(2014))
        .addPaper(Researcher.Paper.newBuilder()
            .setTitle( " A c t i v i t y R e c o g n i t i o n A g a i n " )
            .setYear(2015))
        .build();
// ...
```



## Getters

Besides setters, Protocol Buffer provides automatic methods to access each field, for example:

- `researcher.getName()` returns a string;
- `researcher.getPaperList()` returns a list of `Paper` objects.

## Socket Communication

As seen before, the code automatically generated handles marshalling and unmarshalling for data communication.

On **client side** it is sufficient the use of method `writeTo()` to wrap the output stream socket through the server

- `researcher.writeTo(socket.getOutputStream());`

On **server side** it is necessary to call method `parseFrom()` to wrap the input stream

- `researcher.parseFrom(socket.getInputStream());`

# RPC - Remote Procedure Call

RPC is a paradigm for web service where instead of sending messages explicitly, the client invokes a remote service through a call to a local procedure.

The local procedure (**stub**) hides all web communication details.

Internally, when a client requests a procedure, the RPC library makes the marshalling, transmitting to the server all parameters of method stub. The server then does the unmarshalling, invokes the requested method and then sends the output back to the client. The client does not notice a web request, but it sees all this as a local procedure.

## The gRPC Framework

gRPC is an RPC system developed and used widely by Google (and other bigs like Netflix).

It defines services through Protocol Buffer: method names, arguments, returned value, ...

On server side it is necessary to implement the interface and run a gRPC server to handle all client's requests. On client side there are simply called the stub methods that are exactly those provided by the server.

![](img/grpc.png)

gRPC has a pretty high interoperability

![](img/grpcinter.png)

Benefits:

- Communication efficiency granted by Protocol Buffer (binary transfer);
- Communication channel based on HTTP2:
  - Takes advantage of *multiplexing*: Multiple requests and response can be received asynchronously with a single TCP connection;
  - Bi-directional data streams: Once the connection gets established, it is possible to send more messages in both directions.
- Highly typed;
- More sophisticated than REST, which is based only on HTTP verbs.

```protobuf
// The greeter service definition.
service Greeter {
    // Sends a greeting
    rpc Greeting (HelloRequest) returns (HelloResponse) {}
}
// The request message containing the user ’s name.
message HelloRequest {
    string name = 1;
}
// The response message containing the greetings
message HelloResponse {
	string message = 1;
}
```

## Synchronous VS Asynchronous

It is possible to exploit RPC methods both in a synchronous and asynchronous way, depending on application.

Synchronous calls are blocking: The client makes a call to a stub method, getting the lock on that and releasing it only when he receives an answer from the server. A more abstract approximation than an RPC procedure.

Distributed systems are asynchronous by default, and gRPC allows them to have asynchronous stubs: The client calls the stub method without blocking the current thread, and the answer is received from the server in an asynchronous way.

## RPC Types

RPC comes in with different types:

- **Unary RPC**: The client sends a single requests and receives a single response;
  - `rpc Greeter (HelloRequest) returns (HelloResponse) {}`
- **Server Streaming RPC**: The client sends a single request and the server answers with a stream of responses. The client concludes when receives all the answers;
  - `rpc LotsOfReplies (HelloRequest) returns (stream HelloResponse) {}`
- **Client Streaming RPC**: The client sends a stream of requests and the server responds with a single message, but it does not wait necessary for all the requests to answer;
  - `rpc LotsOfGreetings (stream HelloRequest) returns (HelloResponse) {}`
- **Bidirectional Streaming RPC**: What happens depends on the application. Client and server can both send messages in any order, streams are completely independent.
  - `rpc BidiHello (stream HelloRequest) returns (stream HelloResponse) {}`

To compile the service definition and generate the client and server's stub code the following command is needed:

- `protoc –plugin=protoc-gen-grpc- java=build/exe/java plugin/protoc-gen-grpc-java –grpc-java out=”$OUTPUT FILE” –proto path=”$DIR OF PROTO FILE” ”$PROTO FILE”`

On server side, for each service defined in the `.proto` file it's logic needs to be implemented. On client side, it's enough to specify the address of the service and make local calls to stub methods.

## Example: Greeter

### Server Side

*GreeterImpl.java*

```java
private class GreeterImpl extends GreeterImplBase {
    @Override
    public void greeting(HelloRequest req, StreamObserver<HelloResponse> responseObserver) {
        HelloResponse reply = HelloResponse.newBuilder()
            .setMessage( " Hello " + req.getName()).build();
        responseObserver.onNext(reply);
        responseObserver.onCompleted();
    }
}
```

*ServerBuilder.java*

```java
Server server = ServerBuilder.forPort(8080).addService(new GreetingServiceImpl()).build();
server.start();
System.out.println( " Server started ! ");
server.awaitTermination();
```

On default, the `ServerBuilder` class is implemented with a thread pool that handles requests, that makes each gRPC call asynchronous. This means that there will be concurrency problems that has to be dealt with as in a traditional multi-thread server.

### Client Side

A blocking RPC call

```java
// plaintext channel on the address which offers the GreetingService service
final ManagedChannel channel = ManagedChannelBuilder.forTarget(" localhost :8080 ")
                                .usePlaintext(true)
                                .build();
// creating a blocking stub on the channel
GreetingServiceBlockingStub stub = GreetingServiceGrpc.newBlockingStub(channel);
// creating the HelloResponse object (argument for RPC call)
HelloRequest request = HelloRequest.newBuilder().setName(" Gianni ").build();
// calling the method. it returns an instance of HelloResponse
HelloResponse response = stub.greeting(request);
// printing the answer
System.out.println(response.getGreeting());
// closing the channel
fig/lochannel.shutdown();

```

In order to communicate in an asynchronous way on streams the class `StreamObserver` is used.

This class receives asynchronous notification from a message stream. It is used both from client stub and from service implementation to communicate on streams.

The class provides three method handlers:

- `onNext(V value)`: Receives a value from stream;
- `onError(Throwable t):` Receives an error that happens on the stream;
- `on Completed()`: Receives the communication that stream has been successfully completed.

Who receives the stream **must implement those methods**. Who sends the stream must call the stub of those methods, they are RPC methods.

## Example: Greeting Stream

### Server

*`.proto` file*

```protobuf
rpc StreamGreeting (HelloRequest) returns (stream HelloResponse) {}
```

*Server implementation*:

```java
public void streamGreeting(HelloRequest request, 
                           StreamObserver<HelloResponse> ResponseObserver){
    HelloResponse response = HelloResponse.newBuilder()
            .setGreeting( "Hello there, " +request.getName())
            .build();
    responseObserver.onNext(response);
    responseObserver.onNext(response);
    responseObserver.onNext(response);
    responseObserver.onCompleted();
}
```

### Client

```java
final ManagedChannel channel = ManagedChannelBuilder
		.forTarget( "localhost:8080" ).usePlaintext(true).build();
GreetingServiceStub stub = GreetingServiceGrpc.newStub(channel);
HelloRequest request = HelloRequest.newBuilder()
		.setName( "Gianni").build();
stub.streamGreeting(request, new StreamObserver<HelloResponse>() {
    // this hanlder takes care of each item received in the stream
    public void onNext(HelloResponse helloResponse) {
        System.out.println(helloResponse.getGreeting());
    }
    // if there are some errors , this method will be called
    public void onError(Throwable throwable) {
        // handle error
    }
    // when the stream is completed just close the channel
    public void onCompleted() {
    	channel.shutdownNow();
    }
});
```

