# Synchronization Problems in Distributed Systems

This section talks about how nodes in distributed systems synchronize themselves, which algorithms are better and which problems arise from them. In particular:

- **Physical clock synchronization** and how accurate synchronization with physical clocks is an harsh and not recommendable task;
- **Logical clocks** and how to reorganize events chronologically in a distributed system without the need of a global physical clock;
- **Mutual exclusion algorithms** in a distributed environment by looking at some algorithms to handle shared resources in a mutual exclusive way between the machines through messages;
- **Election algorithms**, some algorithms that help choosing coordinator nodes on the fly if the predecessor fails or is unreachable. This is to guarantee fault tolerance.

## Clock Synchronization

When each machine has its own clock, an event that occurred after another may be nevertheless be assigned an earlier time: this is because there is no synchronization.

A "vertical cut" on the image represent the **absolute time**, which cross different times on different machines. Having such time diversity could be problematic.

![](img/csync.png)

For example, suppose there is a node on which a user uses a editor to create a `output.c` file. He, then, sends it to another node for compilation, this node executes `make` and creates a `output.o` file at 2144 of his local clock. Then, the user finds a bug and modifies the `output.c` file at 2143 of his local clock. 

When a `make` instruction is called, all `.o` files are re-compiled only if their timestamps are older than the .c files.

At this point, when the compiler node calls `make` again, he will see that the `output.o` file (2144) is more recent than the modified `output.c` file, so he will not recompile it, **leaving the bug where it stands**. 

Therefore clock desynchronization and distributed systems transparency can get devilish, forcing the user to find a non-existent error, only to be revealed later to be a compilation problem.

To avoid these kind of problems, some kind of clock synchronization between local clocks of the machines is needed.

## Physical Clocks

Computes do time measures based on concrete time, but how do you calculate the time?

Time is based on astronomical considerations on the medium solar day. Time is the perception of the distance between two events, so the primitive way to measure time was based on the sun-cycle, from sunrise to sunset. This is the solar reference system, and is subject to many variations (in summer day is longer, while in winter is shorter, and so on...)

![](img/solartime.png)

Nowadays, to measure time accurately, we use atomic clocks from the **TAI** (International Atomic Time), which gives us seconds of constant length, unlike solar seconds, these atomic clocks can scan events through natural measures.

However, to keep in phase with the sun, while keeping the constant length seconds, we need to add some leap seconds to the sum, because solar time is not always constant, so we have to add or remove time artificially to keep us in phase. 

The result is the **UTC reference time** on which every digital system is based on: `UTC = TAI + Leap Seconds`.

![](img/utctai.png)

In a distributed system, the main goal is to make the local clocks of our machines as much as synchronized to the UTC, so that each one has the same clock and reference system. 

Needless to say that, even if they are synchronized on the start, clocks on the nodes will **drift away** from each other because we'll have faster clocks and slow clocks, each machine is different.

<img src="img/utcds.png" style="zoom: 80%;" />

Different solutions are given to keep our nodes synchronized.

## External Synchronization: GNSS

A way to keep clocks in sync with UTC is the use of the GNSS (Global Navigation Satellite System), the most used implementation of it is the GPS.

Each satellite of these kind of systems need to have an internal atomic clock. They act as beacons, sending broadcasts messages that include timing informations based on it.

GNSS receivers listen to multiple satellites and use triangulation to determine their own position and UTC time deviation, obtaining a time with an accuracy in the order of microseconds.

### Global Positioning System (GPS)

System created to **compute a position in a two-dimensional space**.

Real world facts that complicates GPS:

- It takes a while before data on a satellite's position reaches the receiver;
- The receiver's clock is generally not in sync with tat of a satellite.

![](img/gps.png)

GPS receivers take advantage of GNSS: They never transmit anything, they only capture broadcast messages from satellites that has time information. This way the **device's position can be triangulated by calculating its displacement between the time-frame of two satellite broadcasts**.

If the nodes of a distributed system has an incorporated GPS, a way to make temporal synchronization is to check the signal given by the satellite (that's how mobile phones do that).

## External Synchronization: NTP / Christian's Algorithm

**Perfect synchronization in a distributed system is an unachievable task**. There could never be a global time because there is always some latency when trying to reach each distributed node and its impossible to estimate this latency to counterbalance it in some way.

Let's suppose that `B` is a time server and has the perfect UTC clock while  `A` wants to synchronize his local clock to it. We can clearly see that the server sends his time at T<sub>3</sub>, however `A` receives it at T<sub>4</sub>, so it's not accurate anymore since some time passed during the communication and latency dT<sub>res</sub> occurred.

It is too difficult to foresee this latency, since we have to consider too many factors, especially if the communication happens through Internet, instead of a LAN. Just asking the time to a server doesn't seem the solution here.

<img src="img/ntp.png" style="zoom:80%;" />

The protocol **NTP (network time protocol) derives from the Christian algorithm**: It repeats the "asking" process in order to gather T<sub>1</sub>, T<sub>2</sub>, T<sub>3</sub> and T<sub>4</sub> as many times as possible (usually 8 times). This way, it's possible to round the estimate as close as possible to the real latency value. This will not be perfect but it will be accurate enough.

This method is also applied on the same time servers with other time servers, creating hierarchies of servers, in order to have all the servers updated, even if they are spread across countries.

## Internal Synchronization: The Berkeley Algorithm

Since now, it is given to our distributed system the same external time as we live in, synchronized with the UTC (external sync). While this can be useful in some scenarios, this is not strictly necessary!

The **Berkeley algorithm synchronizes the local clocks** not on a UTC reference but **on a time daemon present in one of the distributed nodes**.

![](img/berkf.png)

1. The time daemon asks all the other machines for their clock values, it does that by sending to all its own clock value in broadcast (even to himself);
2. All the other machines will answer to him with the difference between their local clock and the time daemon clock;
3. The time daemon now will calculate the average of the displacement between the local clocks of the nodes in the network and will tell to each of them how much to bring forward or backward their clock to synchronize them. 

**N.B** Moving the clock backwards can create some problems in the file-system (inconsistencies in files timestamps). So instead of moving it backwards, the clock is slowed down such that, after some time, it gets synchronized with the others.

Berkeley algorithm is usually used in tightly coupled networks, in which latency is there but is negligible since the algorithm is optimized for that and machines are close.

## Logical Clocks

In many cases it is not necessary the alignment with an internal or external clock, it may be sufficient a partial order and just care about the **order of events** that followed one another in the history of the distributed system.

In the previous `make` example, we don't need the time on which the `.o` was assembled or the `.c` was edited but just the chains of events to know if the re-compilation is needed or not.

Sometimes a partial order is sufficient, while in other situations the total order is required.

## Lamport's Algorithm

If `a` and `b` are events, the expression `a->b` means that `a` happened before `b`. We don't know when they happened but we know their **relative order** in time.

Goal: Assign a clock value (shared by all nodes) `C(a)` for the event `a` in a way that if `a->b` then `C(a) < C(b)`.

Let's suppose that the events we are tracking are sending/receiving of a message:

- Each message should have a timestamp from the sender;
- If at arrival the receiver clock is less or equal than the message time-stamp then the clock is set to the value of time-stamp + 1. Then all the following events of that process should be updated.

Let's see it in action: Three processes, each with its own clock and each with its own rate (faster / slower):

![](img/lampex1.png)

1. **P<sub>1</sub> sends the message m<sub>1</sub> to P<sub>2</sub>**:

   - We can identify the sent event `s` on P<sub>1</sub> with `C(s) = 6` and the received event `r` on P<sub>2</sub> with `C(r) = 16`;
   - Since `C(s) < C(r)`, then we can say that `s->r`;
   - We are perfectly fine, the event is already ordered correctly.

2. **P<sub>2</sub> sends the message m<sub>2</sub> to P<sub>3</sub>**:

   - As before `C(s) < C(r)`, with `C(s) = 24` and `C(r) = 40`;
   - The event is already ordered correctly.

3. **P<sub>3</sub> sends the message m<sub>3</sub> to P<sub>2</sub>**:

   - We can see that the time of the sent event is later than the received event, thus `C(s) > C(r)` (60 > 56);
   - This is because our clocks run at different rates and are not synchronized (as expected);
   - For this reason, we have to fix the receiver clock, sending it forward until `C(r) = C(s) + 1`;

   ![](img/lampex2.png)

   - In our example, we have to shift the clock so that `C(r)` will be `C(s) + 1`, this means that `C(r)` needs to be 61 and to achieve that we shift P<sub>2</sub> clock by 5 units (56 to 61);
   - Remind that we need to shift all the subsequent timestamps (64 to 69, 72 to 77 and so on...);
   - **N.B.** It's the clock that shifts, not the event!

4. **P<sub>2</sub> sends the message m<sub>4</sub> to P<sub>1</sub>**

   - As we just did for m<sub>3</sub>, `C(s) > C(r)` (69 > 54); Note that it is 69, not 64, since we just shifted it in the previous step;
   - This means that `C(r)` needs to be 70 and to achieve that we shift P<sub>1</sub> clock by 16 units (54 to 70);
   - Remind that we need to shift all the subsequent timestamps (60 to 76 and so on...). 

The implementation of this algorithm should live **in the middleware layer**, between application and network, because the application layer shouldn't know and shouldn't care about synchronizing the clocks and this transparency should be ensured.

![](img/lamp.png)

## Enforcing Total Order

The Lamport's algorithm can be modified by **attaching a process number to an event** to enforce a total order, assuming that each process in the system has an unique identifying number.

Each process P<sub>i</sub> timestamps event `e` with C<sub>i</sub>(e) where `i` is the process number: this way we can say that C<sub>i</sub>(a) happens before C<sub>j</sub>(b) (`a->b`) if and only if:

- `Ci(a) < Cj(b)` or
- `Ci(a) = Cj(b)` with `i < j` (in this way I enforce a total order)

It can be visualized it this way, assuming each process' logical clock starts from 0.

![](img/totord.png)

The notation used is `<clock>.<process_num>`, so, for example, the `d` event happens at 4.1, which means at time 4 on the process 1. This way we can potentially compare every event in the system.

## Application: Totally-Ordered Multicasting

Let's see an application of the algorithm: let's suppose we have a replicated database for various reasons. The principal problem of replicated databases is the consistency of data if multiple requests happens at the same time.

![](img/totmul.png)

In a replicated database at a certain time can happen operation simultaneously like:

- Update 1 is generated by a user: Because it is closer, this update is performed on database 1 and, later on, gets replicated on database 2 because of latency;
- Update 2 is generated by another user: It is performed first on database 2 and, later, on database 1 because of latency.

In such a scenario, the **consistency is not preserved** because in both databases there is the same data but in swapped order.

The main idea is to apply Lamport's algorithm to give an order to the transactions but that alone is not enough. We also need and ACK system and some buffered queues to **keep transactions in stasis** (and not perform them as soon as they are received).

This way I'll be able to:

- Send and obtain all transactions correctly (with ACK system);
- Hold them until all transactions are received (with buffered queues);
- Perform them in the correct order (with total-order Lamport).

### Implementation

Going deep, there are some assumption to be made:

- No messages are lost (we're using TCP as transport protocol underneath);
- Messages from the same sender are received in the order they were sent.

Each process P<sub>i</sub> sends timestamped message m<sub>i</sub> to all the others (in multi-cast, of course). The message itself is put in a local queue i. Any incoming message at P<sub>j</sub> is queued in queue j, according to its time-stamp, and an ACK is sent to every other process.

P<sub>j</sub> process the message m<sub>i</sub> and passes it to the application if both:

- m<sub>i</sub> is at the head of the queue j;
- m<sub>i</sub> has been ACKed by each of the other processes.

This way, all processes will eventually have the same copy of the local queue (while being sure of it!) and are going to be able to pass the messages to the application in the same order everywhere.

# Mutual Exclusion

When a set of processes needs to concurrently access a shared resource, it's important to **avoid inconsistencies**.

In a single node, this problem can be solved by:

- **Identifying one or more critical regions**, which essentially are sets of instructions that need to access a resource in exclusive mode in order to preserve consistency;
- **Use of semaphores or monitors** to ensure that no other process can access the resource when the process is in its critical region.

Those solutions need to be extended for consistency among a set of nodes, not only one.

## The Centralized Solution

A centralized algorithm is the standard way to face the problem, by having a central coordinator that holds the resource and handle the accesses to it.

![](img/mutcent.png)

1. Process 1 asks the coordinator to access the resource, and the access is granted;
2. Process 2, then, asks for the resource: The coordinator doesn't answer so he keeps waiting. However the coordinator silently puts him in the waiting queue, so he knows who asked for the resource;
3. When Process 1 is done, it notifies the coordinator, which then proceeds to answer process 2, granting him access.

This solution, though, has some problems:

- **Single point of failure**: If the coordinator goes down for any reason, nobody can access the resource anymore;
- **Low scalability**: As the networks increase in size, a single coordinator will hardly manage to satisfy all requests in a reasonable time (performance bottleneck);
- **No feedback**: Difficult to distinguish if the coordinator is ignoring a node because the resource is already in use or because he didn't receive the message at all.

## A Distributed Solution: Ricart and Agrawala's Algorithm

A distributed approach following Ricart and Agrawala algorithm, starting with assuming that there is a total order of events (based on Lamport's algorithm) and a reliable message delivery (an ACK system).

Since there isn't a centralized coordinator, **all the nodes are peers and need to coordinate themselves** to share the resource in an orderly way.

Let's see how it works:

1. If a process P wants to use a resource R, he builds a message containing:
   - The name of R;
   - The ID of P;
   - The current time-stamp.
2. After building it, he sends the message in broadcast to all the processes (including itself);
3. When a process Q receives a message, we have 3 cases:
   - If Q is not using R and does not require it, it answers OK to P (using the ACK system);
   - If Q is using R then it doesn't answer to P, queuing his request;
   - If Q is not using R but he wants to, he compares his request time-stamp Qr with the one he receives Pr: the earliest wins, so 
     - If his Qr < Pr, he wins and doesn't answer, since he has a lower time-stamp; he also queues the received message for later;
     - If his Qr > Pr, he loses and sends OK to P, since he has a higher time-stamp.
4. After sending out its messages, process P waits for the OK from all the other processes before accessing R;
5. When P is done with R, he sends OK to all processes in its queue and empties it.

### Example

Let's see the algorithm in action:

![](img/mutdist.png)

1. Processes 0 and 2 want to access R at the same time: 
   - P<sub>0</sub> broadcasts a message with time-stamp 8;
   - P<sub>2</sub> broadcasts a message with time-stamp 12.
2. Now there is the ACK phase:
   - P<sub>0</sub> has the earliest time-stamp, so it wins the access to the resource R; he adds P<sub>2</sub> to his local queue and doesn't answer;
   - P<sub>2</sub>, comparing time-stamps, sees that his own is lower, so he sends OK to P<sub>0</sub>;
   - P<sub>1</sub> is not interested in R, so he sends OK to both.
3. P<sub>0</sub> accesses the resource since he received OK from all the other nodes; when he's done with it, he sends OK to all the nodes in his queue (only P<sub>2</sub> at the moment);
4. At that point, P<sub>2</sub> has the OK from both P<sub>0</sub> and P<sub>1</sub>, so he can access R.

### Problems

Even if this algorithm is really scalable, I still have some problems:

- **No answer from a process may also be due to its crash**, there is a the need to wait an ACK from all the other nodes to obtain access to a region (low fault tolerance);
- Involving all processes of a distributed system may be a **waste of resources**. There is flooding of messages if many nodes want to access a resource, It would be useful to limit access to the resource to a restricted group, reducing the number of messages and ACKs on the whole network.

We'll also see that a node crashing is one of the least of our problems, because we usually can detect it and react to it faster. Instead, nodes giving wrong informations (maliciously or erroneously) is one of the worst case scenario, but we'll talk about this later in the fault tolerance analysis.

## A Distributed Solution: A Ring Algorithm

This solution is based on an **unordered group of processes** whose logic is defined by a specific software.

![](img/distring.png)

The **group of processes is connected as a ring-shaped linked list**, this model also share similarities with the "token ring" network model, in which the host in possession of the token could communicate on the network, taking turns through it.

Let's see how the algorithm works:

1. At the start, the token is given to process 0;
2. When a process `i` wants to talk he must have the token. Once used to communicate, he must give it to its successor (`i+1 MOD n`, with `n` total number of processes);
3. If a process is interested in the resource and has the token, he can use it. When done, the process sends the token to the next process in the ring. It cannot use twice the same token.

This same algorithm can be used to handle a shared resource: Only the holder of the token can use it and access its critical regions.

We can identify some downfalls here too:

- **The token could be loss in some way**. When that happens, usually it is just regenerated a random node and go on. However, we have to check that the loss is effective though, otherwise we could have two tokens at the same time, letting two processes access the critical region of the resource and disrupting its consistency;
- **One of the nodes could crash or not respond**. This can be handled by adding to each node a pointer to the next-next one, making the network a bit more resilient;
- **Long time waiting for the token**. In the worst case, a node has to wait that all the other nodes use or pass the token to be able to access the resource again.

## Algorithms Comparison

| Algorithm           | Messages to acquire or release resource                      | Delay to acquire resource (n of messages) | Problems                                       |
| ------------------- | ------------------------------------------------------------ | ----------------------------------------- | ---------------------------------------------- |
| Centralized         | 3                                                            | 2                                         | Crash of coordinator (single point of failure) |
| Distributed         | 2 (n - 1)                                                    | 2 (n - 1)                                 | Crash of any process                           |
| Distributed as ring | 1 to ∞ (means that the token keep circling because every node passes) | 0 to n - 1                                | Token loss, crash of any process               |

# Election Algorithms

Usually, a distributed solution is preferred, but electing a coordinator process may be useful in some instances, for example to implement the centralized solution for mutual exclusion we saw before. There, the crash of the coordinator would be a single point of failure: If we're able to make the election generalized and automatized through an algorithm, we could get the advantages of the centralized approach with an improved fault tolerance.

**Election algorithms usually aim at electing the active process with the highest identifier**, without losing generality though. The identifier, for each process `i`, is defined by the couple `ID(Pi) = <1/load(Pi), i >`, where the unique index `i` acts as the secondary ordering criteria when two processes have the same load.

In this case, the processes with high identifier are the ones with the lowest load, so that they can effectively be good and dedicated coordinators.

Generally, processes don't know the identifier of all the other processes, so they can't discern who is the one with highest identifier. However, there are some algorithms that assume that each process knows the identifiers and can communicate with any other process.

In any case, they cannot know if a process is currently active or it crashed before trying to communicate with them.

## Bully Algorithm

This algorithm works **on a sparse network of unordered processes** (it's not necessarily ring-shaped like the example). It is assumed that each process knows the identifiers of the others or, at least, the identifiers of the processes which are higher than his.

![](img/bully.png)

We can see that each process has their own ID based on their `1/load(Pi)` ratio. 

In the example, process 7 is the one with the higher identifier, so he's the coordinator. Unfortunately, we can see that the coordinator 7 has crashed: however, neither of the processes knows that, so they'll just try to communicate to him.

1. Process 4 tries to contact the old coordinator, and only then he realizes that 7 is not active anymore. For this reason he starts and election by sending a message to all processes with an ID greater that its own (5,6,7). 7 is included to re-check if the old coordinator came back or not;

2. Processes 5 and 6 answer telling 4 that they will take care. Since 7 didn't answer, it probably crashed for good;

3. Processes 5 and 6 start an election, just as 4 did. They will send a message to the processes with higher ID than theirs, including 7 since they don't know it crashed (only 4 knows now):

   - Process 5 sends message to 6 and 7;
   - Process 6 sends message only to 7.

   <img src="img/bully2.png" style="zoom:80%;" />

4. Since 7 doesn't respond, process 6 will answer to 5 that he will take care of it;

5. Now, since process 6 has the highest identifier in the network, he will auto-elect himself as the coordinator, letting it know to all the other processes.

## Ring-Based Election

Let's suppose that the assumption that everyone knows the identifiers of all the others is too strong or difficult to realize. The Chang-Roberts' algorithm is a ring-based election in which we, instead, assume that:

- N processes are logically ordered in a ring. Process P<sub>k</sub> has a communication channel to P<sub>(k+1) MOD n</sub> (his successor);
- Messages circulate clockwise in the ring without failures;
- Processes have unique identifiers.

The goal is to elect the active process with the highest identifier. The choice must be unique, even if multiple elections are concurrently held.

Let's see the algorithm:

1. Initially, all processes are marked as non-participant (not candidate for election);
2. When a process P<sub>i</sub> understands that the coordinator isn't answering, it starts an election by marking itself as participant and sending to his successor a message with his ID: <Election, ID(P<sub>i</sub>)> ;
3. When a process P<sub>m</sub> receives the election message <Election, ID(P<sub>i</sub>)> :
   - if ID(P<sub>i</sub>) > ID(P<sub>m</sub>), P<sub>m</sub> forwards the message in the ring, marking itself as participant;
   - if ID(P<sub>i</sub>) < ID(P<sub>m</sub>):
     - If it was non-participant, the node becomes participant and forwards a message with its own ID <Election, ID(P<sub>m</sub>)> ;
     - If it was already participant, it doesn't forward the message.
   - if i == m (he received his own message), then P<sub>m</sub> is the new coordinator, since there is no one with higher ID: it marks itself as non-participant and sends <Elected, ID(P<sub>m</sub>)> to the next process.
4. When a process P<sub>k</sub> receives <Elected, ID(P<sub>m</sub>)>:
   - It marks itself as non-participant;
   - Stores the ID of the coordinator for later uses;
   - Forwards the message to the next process (unless k == m, the coordinator received his own Elected message).

### Example

Let's see this example, where the election was started by process 17. The highest process identifier encountered so far is 24. Participant processes are shown in a darker color.

<img src="img/ringelec.png" style="zoom:67%;" />

1. Process 17 starts the election, since the coordinator isn't answering anymore. it becomes participant and sends <Election, 17>;

2. Process 24 receives the election message: since 24 > 17, it becomes participant and sends <Election, 24>;

3. Process 1 receives the election message: since 1 < 24, it becomes participant and forwards <Election, 24>;

4. Process 28 receives the election message: since 28 > 24, it becomes participant and sends <Election, 28>;

   ...

5. Since, no process has higher identifier than 28, process 28 receives its own message <Election, 28> : that means that he's becoming the new coordinator;

6. Process 28 sends <Elected, 28> so that everyone knows who is the new coordinator, clearing their participant state.

The use of the participant/non-participant state helps extinguishing as soon as possible unneeded messages in concurrent elections, avoiding the extra traffic caused by that.

**Handling failures**: Failures due to crash of nodes in the ring are handled by each process by storing, not only the address of the next process, but also a few others following it in the ring. If the communication with the next process fails, the message is sent to the first among the ones following it that is active.

**Concurrent elections**: The use of the participant/non-participant state helps extinguishing as soon as possible unneeded messages in concurrent elections.

With this algorithm, in the worst case, excluding failures, I would need `3N-1` messages to elect a new coordinator: this is because the last node before the election starter could be the one with the highest identifier. This way, I'd need N-1 messages to reach the highest process, then N messages to conclude election, then N messages to announce the coordinator.

