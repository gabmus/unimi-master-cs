package main.java;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

public class ServerSum
{
    public static void main( String[] args )
    {
        try {

            Server server = ServerBuilder.forPort(9999).addService(new SumServiceImpl()).build();
            server.start();
            server.awaitTermination();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}