package com.company;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class WaitingRoom {
    public ArrayList<Class> queue;

    public WaitingRoom(){
        queue = new ArrayList<Class>();
    }
    synchronized void enterRoom(Animal a){
        boolean entered = false;
        while(!entered){
            if(a.getClass()==Cat.class && !queue.isEmpty()){
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else if(a.getClass()==Dog.class && ( queue.contains(Cat.class) || Collections.frequency(queue, Dog.class) == 4)) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }else{
                queue.add(a.getClass());
                entered = true;
                System.out.println(Arrays.toString(queue.toArray()));
            }
        }
    }
    synchronized void exitRoom(Animal a){
        queue.remove(a.getClass());
        System.out.println(Arrays.toString(queue.toArray()));
        notifyAll();
    }
}
