import java.io.*;
import java.net.*;

class TCPMultiServer {
    private static Prenotazioni p;

    public static void main(String argv[]) throws Exception
    {
        p = new Prenotazioni(5);
        ServerSocket welcomeSocket = new ServerSocket(6789);

        while(true) {
            Socket connectionSocket = welcomeSocket.accept();
            /* Creazione di un thread e passaggio della established socket */
            TCPServerThread theThread = new TCPServerThread(connectionSocket,p);
            /* Avvio del thread */
            theThread.start();
        }
    }
}