import java.io.IOException;

public class Consumer extends Thread {

    private final Queue<TCPServerThread> threadQueue;
    private final Queue<String> buffer;

    public Consumer(Queue<TCPServerThread> q, Queue<String> b) {
        threadQueue = q;
        buffer = b;
    }

    public void run() {
        while (true) {

            consume(buffer.take());
        }
    }

    public void consume(String message) {
        if(message.startsWith("/exit")){
            int indexToRemove = Integer.parseInt(message.substring(message.length() - 2,message.length() - 1));
            int otherindex = -1;
            for(int i=0;i<threadQueue.size();i++){
                if(threadQueue.get(i).iden == indexToRemove){
                    threadQueue.get(i).sendMessagetoClient("/exit\n");
                    otherindex = i;
                }
            }
            threadQueue.remove(otherindex);
            message = "**Client <"+ indexToRemove + "> quit the Chat**\n";
            System.out.println(message);
        }
        else if(message.startsWith("/enter")){
            int indexOfnewEntered = Integer.parseInt(message.substring(message.length() - 2,message.length() - 1));
            message = "**Client <"+ indexOfnewEntered + "> entered the Chat**\n";
            System.out.println(message);
        }
        for(int i=0;i<threadQueue.size();i++){
            threadQueue.get(i).sendMessagetoClient(message);
        }
    }
}
