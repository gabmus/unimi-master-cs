import java.io.*;
import java.net.*;

public class TCPServerThread extends Thread {
    private     Socket connectionSocket = null;
    private     BufferedReader inFromClient;
    private     DataOutputStream  outToClient;
    private     Queue<String> buffer;
    public     int iden;

    public TCPServerThread(int id, Socket s, Queue<String> b) {
        this.iden = id;
        this.buffer = b;
        connectionSocket = s;
        try{
            inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
            outToClient = new DataOutputStream(connectionSocket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        String clientSentence="";
        while(!clientSentence.equals("/exit\n")){
            try {
                clientSentence = inFromClient.readLine()+"\n";
                if(clientSentence.equals("/exit\n")){
                    String newclientSentence = "/exit " + Integer.toString(iden) +"\n";
                    buffer.put(newclientSentence);
                }else {
                    clientSentence = "Client <" + Integer.toString(iden) + "> says: " + clientSentence;
                    buffer.put(clientSentence);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendMessagetoClient(String message){
        try {
            outToClient.writeBytes(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}