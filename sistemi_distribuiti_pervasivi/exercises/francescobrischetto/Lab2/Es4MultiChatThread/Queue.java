import java.util.ArrayList;

public class Queue<T> {

    public ArrayList<T> buffer = new ArrayList<T>();

    public synchronized void put(T message) {
        buffer.add(message);
        notify();
    }

    public synchronized T take() {
        T message = null;

        while(buffer.size() == 0) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if(buffer.size()>0){
            message = buffer.get(0);
            buffer.remove(0);
        }


        return message;
    }

    public synchronized int size(){
        return buffer.size();
    }
    public synchronized T get(int i){
        return buffer.get(i);
    }
    public synchronized void remove(int i){
        buffer.remove(i);
    }

}
