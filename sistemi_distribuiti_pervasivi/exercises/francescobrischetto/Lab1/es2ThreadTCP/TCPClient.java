import java.io.*;
import java.net.*;

class TCPClient {
    public static void main(String argv[]) throws Exception {
        final int port = 6789;
        final String server = "localhost";
        String message, result;
        boolean FinishOperation = false;
        int argument = 1;

        try {
            BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
            Socket clientSocket = null;
            try{
                clientSocket = new Socket(server, port);
            }catch(IOException e){
                System.err.println("An I/O error occured while opening the socket!\n");
                System.exit(-1);
            }catch(IllegalArgumentException e){
                System.err.println("The port parameter is outside the specified range of valid ports!\n");
                System.exit(-1);
            }catch(Exception e){
                System.err.println(e.getMessage());
                System.exit(-1);
            }
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            while(!FinishOperation){
                System.out.println("Insert "+argument+" arg: ");
                message = inFromUser.readLine()+"\n";
                outToServer.writeBytes(message);
                result = inFromServer.readLine();
                if(result.equals("ACK")){
                    System.out.println("Parameter accepted correctly!");
                    argument++;
                    if(argument > 3 )   FinishOperation=true;

                }else{
                    System.err.println("Parameter not accepted! It's format is not correct. Please re-send the correct parameter.");
                }
            }
            result = inFromServer.readLine();
            System.out.println("Final Result of the operation is: " + result);
            clientSocket.close();

        }catch(IOException e){
            System.err.println(e.getMessage());
            System.exit(-1);
        }
    }
}