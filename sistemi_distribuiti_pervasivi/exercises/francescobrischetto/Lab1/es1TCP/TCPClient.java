package com.company;
import java.io.*;
import java.net.*;

class TCPClient {
    public static void main(String argv[]) throws Exception {
        int num1,num2,port=0;
        String address="",message, result;

       /* indirizzo e porta dagli argomenti della linea di comando */
        try {
            address = argv[0];
            port = Integer.valueOf(argv[1]).intValue();
        }catch(ArrayIndexOutOfBoundsException e){
            System.err.println("Insert Server Address and Port! \nCorrect syntax: <programName> <server_address> <server_port>");
            System.exit(-1);
        }

        try {
            /* Inizializza l'input stream (da tastiera) */
            BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
            Socket clientSocket = null;
            try{
                /* Inizializza una socket client, connessa al server */
                clientSocket = new Socket(address, port);
            }catch(IOException e){
                System.err.println("An I/O error occured while opening the socket!\n");
                System.exit(-1);
            }catch(IllegalArgumentException e){
                System.err.println("The port parameter is outside the specified range of valid ports!\n");
                System.exit(-1);
            }catch(Exception e){
                System.err.println(e.getMessage());
                System.exit(-1);
            }
            /* Inizializza lo stream di output verso la socket */
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());

            /* Inizializza lo stream di input dalla socket */
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            /* Leggo da tastiera i due numeri */
            System.out.print("Insert First Number: ");
            num1 = Integer.parseInt(inFromUser.readLine());
            System.out.print("Insert Second Number: ");
            num2 = Integer.parseInt(inFromUser.readLine());

            /* Costruisco il messaggio */
            message = String.format("%d %d\n", num1, num2);

            /* Invia la linea al server */
            outToServer.writeBytes(message);

            /* Legge la risposta inviata dal server (linea terminata da \n) */
            result = inFromServer.readLine();

            /* Stampo i risultati */
            System.out.println("FROM SERVER: " + result);

            /* Chiudo la socket */
            clientSocket.close();
        }catch(IOException e){
            System.err.println(e.getMessage());
            System.exit(-1);
        }
    }
}
