package com.company;
import java.io.*;
import java.net.*;

class TCPServer {

    public static void main(String argv[])
    {
        String clientSentence;
        String resultString;
        int num1,num2,port=0;
        ServerSocket welcomeSocket = null;
        Socket connectionSocket = null;

        //porta dagli argomenti della linea di comando
        try{
            port = Integer.valueOf(argv[0]).intValue();
        }catch(ArrayIndexOutOfBoundsException e){
            System.err.println("Insert Server Port! \nCorrect syntax: <programName> <server_port>");
            System.exit(-1);
        }


        /* Crea una "listening socket" sulla porta specificata */
        try {
            welcomeSocket = new ServerSocket(port);
        }catch(IOException e){
            System.err.println("An I/O error occured while opening the socket!\n");
            System.exit(-1);
        }catch(IllegalArgumentException e){
            System.err.println("The port parameter is outside the specified range of valid ports!\n");
            System.exit(-1);
        }catch(Exception e){
            System.err.println(e.getMessage());
            System.exit(-1);
        }

        while(true) {

            BufferedReader inFromClient = null;
            DataOutputStream outToClient = null;
            try {
                /*
                 * Viene chiamata accept (bloccante).
                 * All'arrivo di una nuova connessione crea una nuova
                 * "established socket"
                 */
                connectionSocket = welcomeSocket.accept();
            }catch(IllegalArgumentException e) {
                System.err.println("The proxy is of an invalid type or null");
                System.exit(-1);
            }catch(Exception e) {
                System.err.println(e.getMessage());
                System.exit(-1);
            }

            try{
                /* Inizializza lo stream di input dalla socket */
                inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));

                /* Inizializza lo stream di output verso la socket */
                outToClient = new DataOutputStream(connectionSocket.getOutputStream());

                /* Stampo le info del Client */
                InetAddress clientAddress = connectionSocket.getInetAddress();
                int clientPort = connectionSocket.getPort();
                System.out.println(String.format("\nNew Client Connected! %s %d\n",clientAddress.toString(), clientPort));

                /* Legge una linea (terminata da \n) dal client */
                clientSentence = inFromClient.readLine();

                /* Leggo i numeri dalla linea*/
                String[] splitted = clientSentence.split(" ");
                num1 = Integer.valueOf(splitted[0]).intValue();
                num2 = Integer.valueOf(splitted[1]).intValue();

                /*eseguo il calcolo*/
                int resultint = num1+num2;

                /* Creo la stringa risultante*/
                resultString = String.valueOf(resultint).toString() + '\n';

                /* Invia la risposta al client */
                outToClient.writeBytes(resultString);

            }catch(IOException e){
                System.err.println(e.getMessage());
                System.exit(-1);
            }
        }
    }
}
