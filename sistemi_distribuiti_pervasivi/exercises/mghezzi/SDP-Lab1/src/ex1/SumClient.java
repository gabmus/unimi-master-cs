package ex1;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class SumClient {
    public static void main(String argv[]) throws Exception {
        String num1;
        String num2;
        String sum;

        /* Inizializza l'input stream (da tastiera) */
        BufferedReader inFromUser =
                new BufferedReader(new InputStreamReader(System.in));

        /* Inizializza una socket client, connessa al server */
        Socket clientSocket = new Socket("localhost", 6789);
        DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        /* Legge una linea da tastiera */
        num1 = inFromUser.readLine();
        num2 = inFromUser.readLine();

        /* Invia la linea al server */
        outToServer.writeBytes(num1 + '\n');
        outToServer.writeBytes(num2 + "\n");

        /* Legge la risposta inviata dal server (linea terminata da \n) */
        sum = inFromServer.readLine();

        System.out.println("FROM SERVER: " + sum);

        clientSocket.close();
    }
}