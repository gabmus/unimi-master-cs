package ex2;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class POpClient {
    public static void main(String argv[]) throws Exception {
        String result;
        int argument = 1;

        try {
            // Initiating server connection
            Socket clientSocket = new Socket("localhost", 6789);
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            // Initiating input generation
            BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));

            while(true){
                System.out.println("Insert arg " + argument + ":");
                outToServer.writeBytes(inFromUser.readLine()+"\n");

                if(inFromServer.readLine().equals("ACK")){
                    System.out.println("Input accepted correctly!");
                    argument++;
                    if(argument > 3)
                        break;
                }else{
                    System.err.println("Wrong input, please retry.");
                }
            }
            result = inFromServer.readLine();
            System.out.println("Final Result of the operation is: " + result);
            clientSocket.close();

        }catch(IOException e){
            System.err.println(e.getMessage());
            System.exit(-1);
        }
    }
}
