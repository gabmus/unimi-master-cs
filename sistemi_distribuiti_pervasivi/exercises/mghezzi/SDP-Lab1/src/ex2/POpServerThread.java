package ex2;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class POpServerThread extends Thread{
    private Socket connectionSocket = null;
    private BufferedReader inFromClient;
    private DataOutputStream  outToClient;

    public POpServerThread(Socket s) {
        connectionSocket = s;
        try{
            inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
            outToClient = new DataOutputStream(connectionSocket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isOperation(char operation){
        char tmp = operation;
        if((tmp=='+') || (tmp=='-') || (tmp=='*') || (tmp=='/'))
            return true;
        else
            return false;
    }

    public void run() {
        String clientIn;
        String result;
        int param = 0;
        String[] clientReceived = new String[]{"","",""};

        try {
            while(param < 3){
                clientIn = inFromClient.readLine();
                if(param < 2){
                    try {
                        Float.parseFloat(clientIn);
                        clientReceived[param] = clientIn;
                        param++;
                        outToClient.writeBytes("ACK\n");
                    } catch (NumberFormatException e) {
                        outToClient.writeBytes("NACK\n");
                    }
                } else {
                    if(clientIn.length() == 1 && isOperation(clientIn.charAt(0))){
                        clientReceived[param] = clientIn;
                        param++;
                        outToClient.writeBytes("ACK\n");
                    } else {
                        outToClient.writeBytes("NACK\n");
                    }
                }
            }

            // This implements the evaluation of the string using javascript
            clientIn = clientReceived[0] + clientReceived[2] + clientReceived[1];
            ScriptEngineManager mgr = new ScriptEngineManager();
            ScriptEngine engine = mgr.getEngineByName("JavaScript");
            try {
                result = String.valueOf(engine.eval(clientIn));
            }catch (ScriptException e){
                System.err.println(e.getMessage());
                result="Error in server evaluation!";
            }
            outToClient.writeBytes(result);
            connectionSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
