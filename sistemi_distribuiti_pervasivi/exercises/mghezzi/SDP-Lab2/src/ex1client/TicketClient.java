package ex1client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class TicketClient {
    public static void main(String argv[]) throws Exception {
        // Inserting request to server
        BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));

        // Initiating server socket
        Socket clientSocket = new Socket("localhost", 6789);
        DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        // Requesting a ticket
        String request = inFromUser.readLine();
        outToServer.writeBytes(request + '\n');
        String response = inFromServer.readLine();

        // Output my ticket
        if (response == null)
            System.out.println("Invalid operation requested");
        else if (response.equals("0"))
            System.out.println("No tickets available ");
        else
            System.out.println("Received ticket: " + response);
        clientSocket.close();
    }
}