package ex1server;

import java.net.ServerSocket;
import java.net.Socket;

class TicketMultiServer {

	public static void main(String argv[]) throws Exception 
	{
		Reservation ticket = new Reservation();

		ServerSocket welcomeSocket = new ServerSocket(6789);

		while(true) {   
			Socket connectionSocket = welcomeSocket.accept();

			// Thread creation with a Reservation object to allow shared data
			ServerThread theThread = new ServerThread(connectionSocket, ticket);

			theThread.start();
		} 
	} 
}