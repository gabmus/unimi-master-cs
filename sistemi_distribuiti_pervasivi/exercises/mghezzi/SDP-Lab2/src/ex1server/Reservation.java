package ex1server;

public class Reservation {
    private int seats = 5;

    public Reservation() {}

    public Reservation(int customNum) {
        seats = customNum;
    }

    public synchronized int getTicket() {
        if (seats > 0) {
            seats--;
            return seats + 1;
        }
        else
            return 0;
    }
}
