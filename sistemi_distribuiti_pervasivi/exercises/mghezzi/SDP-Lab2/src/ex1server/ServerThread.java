package ex1server;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ServerThread extends Thread {
	private Socket connectionSocket = null;
	private Reservation reservation = null;
	private DataOutputStream outToClient;
	private BufferedReader inFromClient;

	// Taking an enstablished socket and the Reservation object
	public ServerThread(Socket s, Reservation reservation) {
		connectionSocket = s;
		this.reservation = reservation;

		try{
			inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
			outToClient = new DataOutputStream(connectionSocket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		try {
			String operation = inFromClient.readLine();
			if(operation.equals("pick")) {
				int ticket = reservation.getTicket();
				outToClient.writeBytes(""+ticket);
			}
			connectionSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
