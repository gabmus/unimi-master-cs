package ex2;

import java.util.ArrayList;

public class VetRoom {
    ArrayList<Thread> waitRoom;

    public VetRoom(){
        this.waitRoom = new ArrayList<Thread>();
    }

    private boolean catCond(){
        return waitRoom.size() <= 0;
    }

    private boolean dogCond() {
        for(int i = 0; i < waitRoom.size(); i++) {
            if(waitRoom.get(i) instanceof Cat) {
                return false;
            }
        }
        return true;
    }

    public void printState() {
        System.out.println("Current room:");
        if (waitRoom.size() <= 0) {
            System.out.println("    Empty");
        } else {
            for(int i = 0; i < waitRoom.size(); i++) {
                System.out.println("    " + waitRoom.get(i).getClass());
            }
        }
    }

    public synchronized void enterRoom(Thread animal) throws InterruptedException {
        while(true) {
            if(animal instanceof Cat) {
                if(catCond()) {
                    waitRoom.add(animal);
                    break;
                } else {
                    wait();
                }
            } else if (animal instanceof Dog) {
                if(dogCond()) {
                    waitRoom.add(animal);
                    break;
                } else {
                    wait();
                }
            } else {
                System.out.println("Animal not allowed here");
                break;
            }
        }
    }

    public synchronized void exitRoom(Thread animal) {
        waitRoom.remove(animal);
        notify();
    }
}
