package ex2;

import com.example.grpc.AdderGrpc;
import com.example.grpc.AdderGrpc.*;
import com.example.grpc.AdderOuterClass.*;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

public class AdderClient {
    public static void main(String[] args) throws InterruptedException, IOException {
        System.out.println("Calling a simpleAdd method:");
        simpleAdd("6", "4");
        System.out.println("\nCalling a repeatedAdd method:");
        repeatedAdd("5", "10");
        System.out.println("\nCalling a streamSum method:");
        streamSum();
    }

    public static void simpleAdd(String num1, String num2) throws InterruptedException {

        // Synchronous call
        final ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8080").usePlaintext(true).build();
        AdderBlockingStub stub = AdderGrpc.newBlockingStub(channel);
        AddRequest request = AddRequest.newBuilder().setNum1(num1).setNum2(num2).build();
        AddResponse response = stub.simpleAdd(request);
        System.out.println(response.getSum());
        channel.shutdown();

        System.out.println("--------------");

        // Asynchronous call
        final ManagedChannel channel2 = ManagedChannelBuilder.forTarget("localhost:8080").usePlaintext(true).build();
        AdderStub stub2 = AdderGrpc.newStub(channel2);
        AddRequest request2 = AddRequest.newBuilder().setNum1(num1).setNum2(num2).build();
        stub2.streamSimpleAdd(request2, new StreamObserver<AddResponse>() {
            public void onNext(AddResponse helloResponse) {
                System.out.println(helloResponse.getSum());
            }
            public void onError(Throwable throwable) {
                System.out.println("Error! " + throwable.getMessage());
            }
            public void onCompleted() {
                channel.shutdownNow();
            }
        });
        channel2.awaitTermination(2, TimeUnit.SECONDS);
    }

    public static void repeatedAdd(String n, String t) throws InterruptedException {
        final ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8080").usePlaintext(true).build();
        AdderStub stub = AdderGrpc.newStub(channel);
        AddRequest request = AddRequest.newBuilder().setNum1(n).setNum2(t).build();
        stub.repeatedAdd(request, new StreamObserver<AddResponse>() {
            public void onNext(AddResponse helloResponse) {
                System.out.println(helloResponse.getSum());
            }
            public void onError(Throwable throwable) {
                System.out.println("Error! " + throwable.getMessage());
            }
            public void onCompleted() {
                channel.shutdownNow();
            }
        });
        channel.awaitTermination(2, TimeUnit.SECONDS);
    }

    public static void streamSum() throws IOException {
        final ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8080").usePlaintext(true).build();
        AdderStub stub = AdderGrpc.newStub(channel);
        StreamObserver<AddRequest> observer = stub.streamAdd(new StreamObserver<AddResponse>() {
            @Override
            public void onNext(AddResponse value) {
                System.out.println("onNext from client: " + value.getSum());
            }

            @Override
            public void onError(Throwable t) {
                System.out.println("on error");
                t.printStackTrace();
            }

            @Override
            public void onCompleted() {
                System.out.println("on completed");
            }
        });

        while(true){
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            String num1 = br.readLine();
            if(num1.equals("quit")) {
                observer.onCompleted();
                break;
            }

            String num2 = br.readLine();
            if(num2.equals("quit")) {
                observer.onCompleted();
                break;
            }

            AddRequest request = AddRequest.newBuilder().setNum1(num1).setNum2(num2).build();
            observer.onNext(request);
        }
        channel.shutdown();
    }
}
