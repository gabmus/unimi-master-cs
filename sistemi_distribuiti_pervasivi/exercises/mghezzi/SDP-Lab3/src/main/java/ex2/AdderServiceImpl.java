package ex2;

import com.example.grpc.AdderGrpc.*;
import com.example.grpc.AdderOuterClass.*;
import io.grpc.stub.StreamObserver;

public class AdderServiceImpl extends AdderImplBase{

    @Override
    public void simpleAdd(AddRequest request, StreamObserver<AddResponse> responseObserver) {
        System.out.println(request);

        // Calculating sum
        int num1 = Integer.parseInt(request.getNum1());
        int num2 = Integer.parseInt(request.getNum2());
        int sum = num1 + num2;

        // Building the AddResponse message
        AddResponse response = AddResponse.newBuilder().setSum(""+sum).build();

        // Sending the AddResponse message to the client
        responseObserver.onNext(response);

        // Completing the communication
        responseObserver.onCompleted();
    }

    @Override
    public void streamSimpleAdd(AddRequest request, StreamObserver<AddResponse> responseObserver) {
        System.out.println(request);

        // Calculating sum
        int num1 = Integer.parseInt(request.getNum1());
        int num2 = Integer.parseInt(request.getNum2());
        int sum = num1 + num2;

        // Building the AddResponse message
        AddResponse response = AddResponse.newBuilder().setSum(""+sum).build();

        // Sending the AddResponse message to the client
        responseObserver.onNext(response);
        responseObserver.onNext(response);
        responseObserver.onNext(response);
        responseObserver.onNext(response);

        // Completing the communication
        responseObserver.onCompleted();
    }

    @Override
    public void repeatedAdd(AddRequest request, StreamObserver<AddResponse> responseObserver) {
        System.out.println(request);

        // Calculating sum
        int n = Integer.parseInt(request.getNum1());
        int t = Integer.parseInt(request.getNum2());
        int sum = 0;

        for(int i=0; i<t; i++) {
            sum += n;
            // Building the AddResponse message
            AddResponse response = AddResponse.newBuilder().setSum("" + sum).build();

            // Sending the AddResponse message to the client
            responseObserver.onNext(response);
        }

        // Completing the communication
        responseObserver.onCompleted();
    }

    @Override
    public StreamObserver<AddRequest> streamAdd(final StreamObserver<AddResponse> responseObserver){

        return new StreamObserver<AddRequest>() {

            @Override
            public void onNext(AddRequest request) {
                System.out.println(request);
                int num1 = Integer.parseInt(request.getNum1());
                int num2 = Integer.parseInt(request.getNum2());
                int sum = num1 + num2;

                // Building the AddResponse message
                AddResponse response = AddResponse.newBuilder().setSum(""+sum).build();

                // Sending the AddResponse message to the client
                responseObserver.onNext(response);
            }

            @Override
            public void onError(Throwable t) {
                responseObserver.onError(t);
            }

            @Override
            public void onCompleted() {
                System.out.println("on complete");
            }
        };
    }
}
