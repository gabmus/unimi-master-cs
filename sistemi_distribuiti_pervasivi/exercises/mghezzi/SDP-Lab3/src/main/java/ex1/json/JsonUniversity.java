package ex1.json;

import com.google.gson.Gson;
import ex1.impl.Student;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class JsonUniversity {
    public static void main(String[] args) throws IOException {

        // Connection stuff
        ServerSocket serverSocket = new ServerSocket(9999);

        Socket s = serverSocket.accept();
        BufferedReader inFromClient =
                new BufferedReader(new InputStreamReader(s.getInputStream()));

        String jsonString = inFromClient.readLine();

        // Json conversion
        Gson gson = new Gson();
        Student student = gson.fromJson(jsonString, Student.class);

        System.out.println(student);
    }
}
