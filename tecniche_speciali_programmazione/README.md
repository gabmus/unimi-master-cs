# Credentials for the study materials

These are the credentials to log into Cazzola's webpage to download the study materials:

*NOTE: these credentials are case-sensitive*

- Website Link: <https://cazzola.di.unimi.it/>
- ID: `tsp`
- Password: `TSP-MI2009`

# AspectJ tutorial

0. I will assume `aspectj` has been installed in the directory `/opt/aspectj`
1. make sure `ajc` is in your `PATH`: `export PATH=$PATH:/opt/aspectj/bin/`
2. make sure `aspectjrt.jar` is in your `CLASSPATH`: `export CLASSPATH=$CLASSPATH:/opt/aspectj/lib/aspectjrt.jar`
3. compile the aspectj project: `ajc *.java *.aj` (note, in case you're using a package you need to run `ajc package/path/here/*.java package/path/here/*.aj`)
4. run the project as a normal java program: `java MainClassName`
