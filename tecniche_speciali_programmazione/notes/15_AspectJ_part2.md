# AspectJ (Part 2)

Join points can be matched by a common property: public/private, return type, common prefixes, and so on.

*Example: to log all the exceptions raised by any method*

```java
aspect ExceptionLogging {
    // neatly captures any possible method execution
	pointcut excpt(): execution(* *..*.*(..));
	after() throwing(Throwable e): excpt() {
		System.out.println("EXCT: "+e);
		for (StackTraceElement f: e.getStackTrace())
			System.out.println(" · "+f.getClassName()+"\n . "+f.getMethodName());
	}
}
```

Consider code maintenance:

- If another programmer adds a method that raises an exception, this code will still work;
- If another programmer reads this code, what is going on is explicit.

## Wildcarding in Pointcuts

- "*" is wildcard;
- ".." is multi-part wildcard.

```java
target(Point)
target(graphics.geom.Point)
target(graphics.geom.*)				// any type in graphics geom
target(graphics..*)					// any type in any sub-package of graphics

call(void Point.setX(int))
call(public * Point.*(..))			// any public method on Point class
call(public * *(..))				// any puclic method on any type
call(void Point.getX())
call(void Point.getY())
call(void Point.get*())
call(void get*())					// any getter method
    
call(Point.new(int, int))
call(new(..))						// any constructor
```

*Example: Testing preconditions*

```java
package it.unimi.di.adapt.figures;

public class TestPreConditions {
	public static void main(String[] args) {
		Point p = new Point();
		try {
			p.setX(10);
		} catch(Throwable t) {
            ...
        }
	}
}
```

```
>>> ajc -1.9 ExceptionLogging.aj PointBoundsPreCondition.aj
			it/unimi/di/adapt/figures/*.java
>>> aj it.unimi.di.adapt.figures.TestPreConditions
EXCT: java.lang.RuntimeException
  [CLASS] PointBoundsPreCondition [METHOD] the_assert
  [CLASS] PointBoundsPreCondition [METHOD]  													ajc$before$PointBoundsPreCondition$1$3b981aae
  [CLASS] it.unimi.di.adapt.figures.Point [METHOD] setX
  [CLASS] it.unimi.di.adapt.figures.TestPreConditions [METHOD] main
```

Let's see the compiled bytecode for `TestPreConditions.java`

```java
public class it.unimi.di.adapt.figures.TestPreConditions {
	public it.unimi.di.adapt.figures.TestPreConditions();
		Code:
			0: aload_0
			1: invokespecial #8			// Method java/lang/Object."<init>":()V
			4: return
	public static void main(java.lang.String[]);
		Code:
			0: new			  	#17 	// class it/unimi/di/adapt/figures/Point
			3: dup
            // Method it/unimi/di/adapt/figures/Point."<init>":()V
			4: invokespecial  	#19 
			7: astore_1
			8: aload_1
			9: bipush		  	10
            // Method it/unimi/di/adapt/figures/Point.setX:(I)V
			11: invokevirtual	#20 
			14: goto			18
			17: pop
			18: return
			19: astore_2
            // Method ExceptionLogging.aspectOf:()LExceptionLogging;
			20: invokestatic 	#39
			23: aload_2
// Method ExceptionLogging.ajc$afterThrowing$ExceptionLogging$1$b142a415:(Ljava/lang/Throwable;)V
			24: invokevirtual 	#43
			27: aload_2
			28: athrow
		Exception table:
			from	to	 target	 type
				8	14		17		Class java/lang/Throwable
				0	19		19		Class java/lang/Throwable
}
```

Other primitive pointcuts:

- `this(<type_name>)`: Any join point at which currently executing object is an instance of <type_name>;
- `within(<type_name>)`: Any join point at which currently executing code is contained within <type_name>;
- `withincode(<method/constructor signature>)`: Any join point at which currently executing code is in the specified method or constructor;
- `get(int Point.x)` and `set(int Point.x)`: Field reference or assignment join points.

## Fine-Grained Protection

### Run-Time Errors

We want to ensure at run-time that any creation of figure elements goes through the factory methods.

```java
package it.unimi.di.adapt.figures;

public class FigureFactory {
	public Line makeLine(Point p1, Point p2) { 
        return new Line(p1, p2);
    }
	public Point makePoint(int x, int y) { 
        return new Point(x, y);
    }
}
```

```java
import it.unimi.di.adapt.figures.* ;

aspect FactoryEnforcement {
	pointcut illegalNewFigElt(): (call(*..Point.new(..)) || call(*..Line.new(..))) &&
		 						  !withincode(* FigureFactory.make*(..));

	before(): illegalNewFigElt() {
		throw new Error("Use factory method instead.");
	}
}
```

```
>>> ajc -1.9 FactoryEnforcement.aj it/unimi/di/adapt/figures/*.java
>>> aj it.unimi.di.adapt.figures.TestPreConditions
Exception in thread "main" java.lang.Error: Use factory method instead.
 at 		FactoryEnforcement.ajc$before$FactoryEnforcement$1$3ba7d934(FactoryEnforcement.aj:8)
 at it.unimi.di.adapt.figures.TestPreConditions.main(TestPreConditions.java)
```

### Compile-Time Errors

We want to ensure at compile-time that any creation of figure element goes through the factory methods.

```java
package it.unimi.di.adapt.figures;

public class FigureFactory {
	public Line makeLine(Point p1, Point p2) {
        return new Line(p1, p2);
    }
	public Point makePoint(int x, int y) {
        return new Point(x, y);
    }
}
```

```java
import it.unimi.di.adapt.figures.*;

aspect CompileTimeFactoryEnforcement {
	pointcut illegalNewFigElt(): 
    	(call(*..Point.new(..)) || call(*..Line.new(..))) &&
            !withincode(* FigureFactory.make*(..));

    declare error: illegalNewFigElt(): "Use factory method instead.";
}
```

```

[17:57]cazzola@hymir:~/> ajc -1.9 CompileTimeFactoryEnforcement.aj
								it/unimi/di/adapt/figures/TestPreConditions.java
TestPreConditions.java:5 [error] Use factory method instead.
Point p = new Point();
^^^^^^^^^^^^^^^^^^^^^
	constructor-call(void it.unimi.di.adapt.figures.Point.<init>())
	see also: CompileTimeFactoryEnforcement.aj:8::0
1 error
```

We want to ensure at compile-time that any change to the state of a line goes through the setter methods.

```java
package it.unimi.di.adapt.figures;

public class Line implements FigureElement {
	private Point p1;
    private Point p2;

	public Line(Point p1, Point p2) {
        this.p1 = p1;
        this.p2 = p2;
    }
	public Point getP1() {
        return p1;
    }
	public Point getP2() {
        return p2;
    }
	public void setP1(Point p1) {
        this.p1 = p1;
    }
	public void setP2(Point p2) {
        this.p2 = p2;
    }
	public void moveBy(int dx, int dy) {
        p1.moveBy(dx, dy);
        p2.moveBy(dx, dy);
    }

	public String toString() { 
        return "Line("+this.p1+", "+this.p2+")";
    }
}
```

```java
import it.unimi.di.adapt.figures.* ;

aspect SetterEnforcement {
	declare error:
		set(Point *..Line.*) && !withincode(void *..Line.set*(Point)):
    		"Use setter method.";
}
```

## Reflecting on Join Point

A set of statements (exploitable by the advices) to introspect the join points consistently with Java

- `thisJoinPoint`
  - `Signature getSignature()`
  - `Object[] getArgs()`

```java
import it.unimi.di.adapt.figures.*;

aspect PointCoordinateTracing {
	before(int newVal):
		set(int Point.*) && args(newVal) {
			System.out.println("At " + thisJoinPoint.getSignature() +
								" field is set to " + newVal + ".");
		}
}
```

```java
>>> aj it.unimi.di.adapt.figures.TestPreConditions
At int it.unimi.di.adapt.figures.Point.x field is set to 0.
At int it.unimi.di.adapt.figures.Point.y field is set to 0.
At int it.unimi.di.adapt.figures.Point.x field is set to 10.
```

`thisJoinPoint` makes it possible for the advice to recover information about where it is running.

## Inner-Type Declarations

```java
package it.unimi.di.adapt.figures;

public class TestFigures {
	public static void main(String[] args) {
		FigureElement[] fe = {new Point(5,3), new Point(-1,2),
							  new Line(new Point(-5,3), new Point(-1,-2))
                             };
		for (FigureElement f:fe) {
			System.out.println(f);
			f.moveBy(3,-5);
		}
		for (FigureElement f:fe) 
            System.out.println(f);

		Point p = new Point(0,0);
		p.setX(10);
		p.setX(0);
	}
}
```

```java
import it.unimi.di.adapt.figures.*;
import java.util.Hashtable;

public aspect CountingCalls {
	public Hashtable<FigureElement, Integer> counters = new Hashtable();
	pointcut calls(FigureElement fe) : target(fe) && call(* *..*.*(..)) ;
	pointcut ends() : execution(public static void *..*.main(String[])) ;

	before(FigureElement fe) : calls(fe) {
		Integer old = counters.get(fe) ; counters.put(fe, (old!=null)?old+1:1) ;
	}

	after() : ends() {
		for(FigureElement fe : counters.ketSet())
            System.out.println("### Figure "+fe+" has called "+counters.get(fe)+" 									methods!");
    }
}
```

```
>>> ajc -1.9 CountingCalls.aj it/unimi/di/adapt/figures/*.java
>>> aj it.unimi.di.adapt.figures.TestFigures
Point(5, 3)
Point(-1, 2)
Line(Point(-5, 3), Point(-1, -2))
Point(8, -2)
Point(2, -3)
Line(Point(-2, -2), Point(2, -7))
### Figure Point(0, 0) has called 2 methods!
### Figure Line(Point(-2, -2), Point(2, -7)) has called 1 methods!
### Figure Point(2, -7) has called 1 methods!
### Figure Point(-2, -2) has called 1 methods!
### Figure Point(8, -2) has called 1 methods!
### Figure Point(2, -3) has called 1 methods!
```

- `cflow(<pointcut_designator>)`;
- `cflowbelow(<pointcut_designator>)`.

All join points within the dynamic control flow of (below) any join point in pointcut designator

```java
import it.unimi.di.adapt.figures.* ;
import it.unimi.di.adapt.display.* ;

public aspect DisplayUpdating {
	pointcut move(FigureElement figElt):
		target(figElt) && (
			call(void FigureElement.moveBy(int, int)) 	||
			call(void Line.setP1(Point))				||
			call(void Line.setP2(Point))				||
			call(void Point.setX(int))					||
			call(void Point.setY(int))
		);
	after(FigureElement fe) returning: move(fe) {
		Display.update(fe);
	}
}
```

## Inheritance

A Pointcut can have more than one advice.

- A pointcut declaration couldn't have an advice associated;
- Many advices can be associated to the same named pointcut;
- Module can expose certain well defined pointcuts.

Abstract pointcuts can be specialized: aspects can have abstract pointcuts and concrete advice on them

```java
import java.util.Vector;

abstract aspect Observing {
	protected interface Subject {}
	protected interface Observer {}
    public Vector<Observer> Subject.observers = new Vector<Observer>();

	public void Subject.addObserver(Subject s, Observer o) {
        s.observers.add(o);
    }
	public void Subject.removeObserver(Subject s, Observer o) {
        s.observers.remove(o);
    }
	public Vector<Observer> Subject.getObservers(Subject s) {
        return s.observers;
    }

	abstract pointcut changes(Subject s);

    after(Subject s): changes(s) {
		for (Observer obs: s.getObservers(s))
			s.notifyObserver(s, obs);
	}

	abstract public void Subject.notifyObserver(Subject s, Observer o);
}
```

## Concrete Reuse: Display Updating

```java
import it.unimi.di.adapt.figures.*;
import it.unimi.di.adapt.display.*;

aspect DisplayUpdating extends Observing {
	declare parents: FigureElement 	implements Subject;
	declare parents: Display		implements Observer;

	pointcut move(Subject s):
		target(s) && (
			execution(void *..FigureElement+.moveBy(int, int)) 	||
			execution(void *..Line.setP1(Point))				||
			execution(void *..Line.setP2(Point))				||
			execution(void *..Point.setX(int))					||
			execution(void *..Point.setY(int)));

	pointcut changes(Subject s):
		move(s) && !cflowbelow(move(Subject));

	public void Subject.notifyObserver(Subject s, Observer o) {
		((Display)o).update((FigureElement)s);
	}
}
```

```java
jshell> var d = new Display();
d ==> it.unimi.di.adapt.display.Display@6a41eaa2
jshell> FigureElement[] shapes={new Point(-1,2),new Line(new Point(-5,3),new Point(1,2))}
shapes ==> FigureElement[3] {Point(-1, 2), Lin ... t(-5, 3), Point(1, 2))}
jshell> for(FigureElement f:shapes) f.addObserver(f,d)
jshell> for (FigureElement f:shapes) f.moveBy(3,-5);
Point(2, -3) has been updated!
Line(Point(-2, -2), Point(4, -3)) has been updated!
```

## Plug & Play Tracing

Implementing a simple tracer that exposes join points and uses very simple advice.

An unpluggable aspect, a core program functionality is unaffected by the aspect.

### Tracing Without AspectJ

```java
class Point {
	void set(int x, int y) {
		TraceSupport.traceEntry("Point.set");
		this.x = x; this.y = y;
		TraceSupport.traceExit("Point.set");
	}
}
```

```java
Class TraceSupport {
    static int TRACELEVEL = 0;
    static protected PrintStream stream = null;
    static protected int callDepth = -1;
    
    static void init(PrintStream _s) {
        stream = _s;
    }
    static void traceEntry(String str) {
        if (TRACELEVEL == 0) return;
        callDepth++;
        printEntering(str);
    }
    static void traceExit(String str) {
        if (TRACELEVEL == 0) return;
        callDepth--;
        printExiting(str);
    }
}
```

### Tracing With AspectJ

All modules of the system use the trace facility in a consistent way when entering and exiting the methods.

```java
aspect PointTracing {
	pointcut trace():
		within(it.unimi.di.adapt.*) &&
			execution(* *(..));
	
    before(): trace() {
		TraceSupport.traceEntry(thisJoinPoint);
	}
	after(): trace() {
		TraceSupport.traceExit(thisJoinPoint);
	}
}
```

Plug in:

- `ajc Point.java Line.java TraceSupport.java PointTracing.aj`

Unplug:

- `ajc Point.java Line.java`

We get the following benefits:

- Turn debugging on/off without editing classes;
- Debugging disabled with no run-time cost;
- Can save debugging code between uses;
- Can be used for profiling, logging;
- Easy to be sure if is off.

## Benefits of AOP

### Aspects in Design

Objects are no longer responsible for using the trace facility: trace aspect encapsulates that responsibility for appropriate objects

If the `Trace` interface changes, the change doesn't affect the objects using the facility (only the trace aspect is affected).

Removing tracing from the design is trivial, we just have to remove the trace aspect.

### Aspect in Code

Object code contains no calls to trace functions, because the trace aspect code encapsulates those calls, for appropriate objects.

If the `Trace` interface changes, there is no need to modify the object classes, only the trace aspect class needs to be modified.

Removing tracing from the application is trivial: we just compile without the trace aspect class.

## When to use AOP

AOP is used when there is a concern that crosscuts the structure of several objects or operations, and we benefit to separate them out.

Crosscutting is a design concern that involves several objects or operations. 

Without AOP, we would have scattered pieces of code that do the same thing, like logging, or do coordinated single thing, like timing.

AOP grants us:

- Separation of concerns;
- Clarified interactions, reducing tangling;
- Easier to modify/extend;
- Plug and play

Good modularity, even in the presence of crosscutting concerns

















