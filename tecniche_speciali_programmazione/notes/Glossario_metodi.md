# Glossario metodi

*Metodo per ottenere l'oggetto Classe. In questo caso passando il nome tramite linea di comando*

``` java
Class<?> c = Class.forName(args[0]);
```



*Metodo per ottenere, tramite l'oggetto c, l'istanza della Classe*

```java
Object ist = c.getConstructor().newInstance();
```



*Metodo per ottenere un determinato metodo, in questo caso passandogli il nome del metodo da linea di comando. L'altro parametro è un array di tipo Class, il quale contiene le classi dei tipi dei parametri che richiede il metodo specificato dal nome*

```java
Method m = c.getMethod(args[1],evaluetedargs);
```



*Metodo che invoca il metodo specificato dall'oggetto m. I parametri richiesti sono l'istanza della classe e un array di tipo Object. L'array contiene gli effettivi parametri con i quali chiameremo il metodo dell'oggetto m*

**PS: i parametri nell'array devono essere dello stesso tipo dei parametri richiesti dal metodo nell'oggetto m**

```java
  Object result = m.invoke(ist,realargs);
```



*Utilizzo della classe Pattern per la costruzione dell'array di tipo Class che verrà usato in* ***getMethod***

*Il metodo* ***Pattern.matches*** *va a confrontare l'espressione regolare ,al primo argomento, con la stringa al secondo argomento*

"[0-9]+" *è l'espressione per il tipo* ***Integer***, *mentre l' espressione* "[0-9]+\\\\.[0-9]" *è quella per il tipo* ***Double*** 

```java
for (int i=0; i<argtype.length; i++) {
  if (Pattern.matches("[0-9]+",argtype[i])) {
		evaluatedargs[i]=int.class;
  }else if(Pattern.matches("[0-9]+\\.[0-9]+",argtype[i])) {
		evaluatedargs[i]=double.class;
  }else {
		evaluatedargs[i]=String.class;
  }
}
```



***TestingFields*** *è il nome della classe dalla quale vogliamo estrarre il campo attraverso il metodo* ***getDeclaredField***. *Il metodo ha come parametro il nome del campo* ***dichiarato*** *che vogliamo estrarre*

```java
Field cs = TestingFields.class.getDeclaredField("s");
```



*Questo metodo ci permette di madoficare il campo contenuto nell'oggetto cs. Come parametri di* ***set*** *abbiamo l'istanza della classe di appartenenza del campo e il valore che vogliamo inserire*

```java
cs.set(tf,"testing ... passed!!!");
```



*Questo metodo ci restituisce il valore del campo contenuto in cs e come parametri abbiamo l'istanza della classe di appartenenza del campo*

```java
cs.get(tf)
```



*Questi due metodi estraggono rispettivamente un array con tutti i metodi* ***dichiarati*** *nella classe descritta nell'oggetto c e un array dei campi* ***dichiarati*** *della medesima classe*

```java
Method m[]= c.getDeclaredMethods();
Field f[]=c.getDeclaredFields();
```



*Qui è descritta la creazione di un* ***ArrayList*** *di stringhe tramite il suo costruttore e l'aggiunta di un elemento a questo* ***ArrayList*** *di stringhe*

```java
ArrayList<String> cont = new ArrayList<String>();
cont.add(metodi_elem[j]);
```



*Questo metodo ritorna* ***true*** *se l'elemento nel parametro del metodo è presente all' interno dell'* ***ArrayList***

```java
cont.contains(metodi_elem[i])
```



*Questa è la descrizione di una* ***Annotation***.*La descrizione deve trovarsi su di un file .java apposito*

***@Retention*** *e* ***@Target*** *sono* ***Meta-Annotation*** *ovvero annotazioni che definiscono per esempio per quanto tempo viene mantenuta l'annotazione annotata o qual'è il target dell'annotazione annotata ovvero se va legata a un metodo, alla classe, ai campi, ecc...* 

*All'interno troviamo i campi dell'annotazione che sono definiti da noi*

```java
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface CFG{
	String name();
	String[] dependency();
}
```



*Riprendiamo l'esempio dell'annotazione precedente che come terget ha METHOD. tramite l'oggetto m mi vado a prelevare le annotazoini legate a quel metodo*

***PS: dobbiamo avere visibilità sul file nel quale dihiariamo l'annotazione***

```java
Class<?> c = Class.forName(args[i]);
Method[] m = c.getDeclaredMethods();
for (int j = 0; j < m.length; j++) {
	CFG s = m[j].getDeclaredAnnotation(CFG.class);
    ...
}
```



*Questo è l'effettivo utilizzo di una annotazione*

```java
@CFG(
			name=" main method of RecognizingElements class ",
			dependency = {"Es4.Recogn"}
	)
	public static void main(String[] args) {
```



*Questa è la base per la classe che va ad implementare l'* ***InvocationHandler*** *che si legherà al proxy*

```java
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class TraceHandler implements InvocationHandler {
    private Object baseObject;

    public TraceHandler(Object base){
        baseObject=base;
    }

    public Object invoke(Object proxy, Method m, Object[] args) throws Throwable {
        return null;
    }
}
```



*Inizializzazione del proxy*

```java
TestingFields test= new TestingFields(7,9.3);
ITest proxy = (ITest) java.lang.reflect.Proxy.newProxyInstance(
    test.getClass().getClassLoader(), 
    test.getClass().getInterfaces(),
    new TraceHandler(test));
```

