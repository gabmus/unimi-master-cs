# Javassist

Javassist is a class library for editing Java bytecode that enables class modification at load time and the creation of a new class at run time.

It's a high-level API and, moreover, it does not require knowledge of bytecode specifications:

- 100% Java;
- Portable;
- Based on class loading model.

Javassist, as a high-level API, make uses of source code abstractions by

- Making improvement over Java structural reflection through a parallel reification of Java classes and primitive types: `CtClass, CtMethod, CtConstructor, CtField, FieldInitializer, CtPrimitiveType, Modifier...`

- Introducing new classes and primitive to decompose the class loading process and enabling class modification (load-time): `Loader, ClassPool, ClassPath, Translator.`

## Library Components

### ClassPool

A container of `CtClass` objects, every loaded class passes through this object.

- `get(String)`: Returns a reference to a CtClass;
- `toClass()`: Translates a `CtClass` into a standard class format;
- `insertClassPath`, `appendClassPath`: Attributes to manage the class lookup process;
- `make*`: Used to create classes and interfaces from scratch;
- Use of static services to get the class pool with or without `Translator`.

### Translator

An observer of Loader, this interface should be implemented to translate a class file when the class file is loaded into the JVM.

- `start(ClassPool)`: The translator initializer;
- `onWrite(ClassPool, String)`: Before the class is given to the loader-time for modifications.

### CtClass vs Class

Javassist provides the protocols for operating on a class before it is loaded into the JVM or for dynamic creation of new classes.

Introspection:

- Same as `java.lang.reflect`;
- `isModified, isFrozen` already loaded, cannot be modified.

Intercession:

- `SetName, setSuperclass, setModifiers, ...`;
- `addInterface, addField, addMethod, ...`;
- `replaceClassName` to replace class name occurrences within a class, using a map.

Transformation:

- `getClassFile`: Returns a `ClassFile` object (`javassist.bytecode`), for lower-level bytecode manipulation;
- `instrument(CodeConverter)`: Applies a code converter on methods and constructors.

### CodeConverter

Instances of this class specifies how to instrument the byte-codes representing a method body. They are passed to `CtClass.instrument()` or to `CtMethod.instrument()` as a parameter.

High-level modifications inside method bodies enables to :

- Insert a call to a method before or after another method call;
- Redirect a method call to another method;
- Redirecting a field access to another field;
- Replace a field access by a static method call;
- Replace a new statement by a static method call.

### Example: Binary component Adaption

We aim to update classes so that they fit into a framework, transforming their bytecode automatically.

Class `Calendar` that implements a third-party interface `Writable`:

```java
class Calendar implements Writable {
	public void write(PrintStream s) { ... }
}
```

In a new version of the library, `Writable` has been replaced with `Printable`, so we have to update our `Calendar` class:

```java
class Calendar implements Printable {
	public void write(PrintStream s) {...}
	public void print() { 
        this.write(System.out);
    }
}
```

We can do that automatically by implementing an `Adapter`:

```java
class Adapter implements Translator {
    CtMethod printM;
    CtClass printable;
    
    public void start(ClassPool pool) {
        this.printable = pool.get("Printable");
        CtClass ex = pool.get("Calendar");
        this.printM = ex.getDeclaredMethod("print", null);
    }
    
    public void onWrite(String cn, ClassPool pool) {
        CtClass c = pool.get(cn);
        CtClass[] interfaces = c.getInterfaces();
        for (int i = 0; i < interfaces.lenght; i++) {
            if (interfaces[i].getName().equals("Writable")) {
                interfaces[i] = printable;
                c.setInterfaces(interfaces);
                c.addMethod(CtNewMethod.copy(printM, c, null));
                return;
            }
        }
    }
}
```

```java
public class RunAdaption {
    public static void main(String[] args) {
        Adapter adapt = new Adapter();
        ClassPool pool = ClassPool.getDefault(adapt);
        Loader cl = new Loader(pool);
        cl.run("myApplication", args);
    }
}
```

The `javassist.bytecode.*`:

- Is a low-level API: bytecode abstractions;
- Core API services are implemented with it;
- Can be used for finer-grained transformations;
- `ClassFile` is the base entity for bytecode editing:
  - `ClassFile = <ConstPool, Methods, FIelds, Attributes>`;
  - Obtained from `CtClass` stream or from scratch;
  - Write to output stream.
- `OpCode` defines all bytecode instructions as constraints:
  - `ALOAD, ASTORE, INVOKESPECIAL, ...`

### ClassFile

Intercession:

- `addAttribute(AttributeInfo)`;
- `addMethod(MethodInfo)` ;
- `addField(FieldInfo)` ;
- `renameClass, setName`;
- `setAccessFlag, setInterfaces, setSuperclass`.

Introspection;

- `getConstPool, getAttribute, getMethod, ...`;
- `isFinal, isAbstract, isInterface`.



All -Info classes enable introspection and intercession (ex.`MethodInfo` has `getCodeAttribute()`).

`CodeAttribute` is a structure with raw bytecode.

`CodeIterator` is used to parse and manipulate raw code (insert/replace code, go to next instruction, ...).



### Example: Simulating the Meta-Object Approach

Let's define our base class:

```java
package sample.reflect;
import javassist.tools.reflect.Metalevel;
import javassist.tools.reflect.Metaobject;

public class Person {
	public String name;
	public static int birth = 3;

	public Person(String name, int birthYear) { 
        this.name = name; 
        birth = birthYear; 
    }
	public String getName() { 
        return name; 
    }
	public int getAge(int year) { 
        return year - birth;
    }

	public static void main(String[] args) {
		Person p = new Person(args[0], 1948);
		System.out.println("name: " + p.getName());
		System.out.println("object: " + p.toString());

		// change the metaobject of p.
		if (p instanceof Metalevel) {
			((Metalevel)p)._setMetaobject(new Metaobject(p, null));
			System.out.println("«the metaobject was changed.»");
        }
        System.out.println("age: " + p.getAge(2018));
    }
}
```

```java
>>> java sample.reflect.Person "Gladstone Gander"
java sample.reflect.Person "Gladstone Gander"
name: Gladstone Gander
object: sample.reflect.Person@34033bd0
age: 70
```

Now let's implement the meta-object:

```java
import javassist.tools.reflect.*;

public class VerboseMetaobj extends Metaobject {
	public VerboseMetaobj(Object self, Object[] args) {
		super(self, args);
		System.out.println("** constructed: " + self.getClass().getName());
	}
	public Object trapFieldRead(String name) {
		System.out.println("** field read: " + name);
		return super.trapFieldRead(name);
	}
	public void trapFieldWrite(String name, Object value) {
		System.out.println("** field write: " + name);
		super.trapFieldWrite(name, value);
	}
	public Object trapMethodcall(int identifier, Object[] args) throws Throwable {
		System.out.println("** trap: " + getMethodName(identifier) + "() in "
							+ getClassMetaobject().getName());
		return super.trapMethodcall(identifier, args);
	}
}
```

```java
import javassist.tools.reflect.Loader;

public class Main {
	public static void main(String[] args) throws Throwable {
		Loader cl = (Loader)Main.class.getClassLoader();
		cl.makeReflective("sample.reflect.Person", "sample.reflect.VerboseMetaobj",
						  "javassist.tools.reflect.ClassMetaobject");
		cl.run("sample.reflect.Person", args);
	}
}
```

```java
>>> java javassist.tools.reflect.Loader sample.reflect.Main "Gladstone Gander"
** constructed: sample.reflect.Person
** field write: name
** trap: getName() in sample.reflect.Person
** field read: name
name: Gladstone Gander
** trap: toString() in sample.reflect.Person
** trap: hashCode() in sample.reflect.Person
object: sample.reflect.Person@3043fe0e
«the metaobject was changed.»
age: 70
```















