# Modules and Handles

Introduced in Java 9, the **Java Module Platform** is aimed to improve class and package:

- Using reliable **configuration**, to replace the error-prone class-path mechanism in favor of a programming mechanism to explicitly declare dependencies;
- Forcing **encapsulation**, allowing a component to declare witch of its public types are accessible to other components, and which are not. The programmer decides the granularity of the code, defining which class can use a specific method; 

A **Module** is a group of classes, interfaces and packages, whose dependencies are described in the *module-info.java* file included in the project (shipped inside a *jar* file). A module file has a structure like:

```java
module com.foo.bar {
	requires org.baz.foo ;
	exports com.foo.bar.abc;
}
```



### Example: Private and Reflective Access

```Java
package tsp.module.employee;

import tsp.module.reflection.* ;

class Employee implements SmartFieldAccess {
	private String name;
	private String surname;
	
    public Employee(String n, String s) { 
        this.name = n; 
        this.surname = s; }
    
	public String toString() {
        return "Employee: "+this.name+" "+this.surname;}
	}
}
```

```java
package tsp.module.reflection;

import java.lang.reflect.*;

public interface SmartFieldAccess {
	default public Object instVarAt(String name) throws Exception {
		Field f = this.getClass().getDeclaredField(name);
		f.setAccessible(true);
		if (!Modifier.isStatic(f.getModifiers())) 
            return f.get(this);
		return null;
	}
	default public void instVarAtPut(String name, Object value) throws Exception {
		Field f = this.getClass().getDeclaredField(name);
		f.setAccessible(true);
		if (!Modifier.isStatic(f.getModifiers())) 
            f.set(this, value);
	}
}
```

```java
package tsp.module.employee;

public class ReflectiveEmployeeMain {
	public static void main(String[] args) throws Exception {
		Employee angela = new Employee("Angela", "Runedottir");
		System.out.println(angela);
		angela.instVarAtPut("surname", "Odindottir");
		System.out.println(angela);
	}
}
```

```java
module tsp.module.employee {
	requires tsp.module.reflection ;
	exports tsp.module.employee ;
}
```

```java
module tsp.module.reflection {
	exports tsp.module.reflection ;
}
```

project structure:

```
|__tsp.module.employee
|		|__module-info.java
|		|__tsp
|			|__module
|				|__employee
|					|__Employee.java
|					|__ReflectionEmployeeMain.java
|__tsp.module.reflection
		|__module-info.java
		|__tsp
			|__module
				|__reflection
					|_SmartFieldAccess.java
```

The module system helps managing code dependencies.

## Variable Handles

A **Variable Handle** is a typed reference to a variable, it grants access to a variable, drawing information directly from the memory.

- *VarHandle* class provides read/write access to variables;
- All *VarHandles* are immutable and have no visible state;

Each *VarHandle* has:

- A variable type **T** that corresponds to the type of the variable;
- An abstraction for a safe access to a memory location;
- A list of coordinate types *CT1, CT2, ..., CTn*;

There are given factory methods to produce or lookup *VarHandles*

```java
MethodHandles.lookup().in(Foo.class).findVarHandle(Foo.class, "i", int.class);
```

```java
MethodHandles.privateLookupIn(Foo.class, MethodHandles.lookup())
    .findVarHandle(Foo.class, "i", int.class);
```

Possible access modes: read (ex. getVolatile), write (ex. setOpaque), atomic update (getAndSet).

















































