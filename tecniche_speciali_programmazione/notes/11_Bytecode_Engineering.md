# Bytecode Engineering

The bytecode engineering consists in a **modification of the class file content**, based on early object code modification (before instantiation).

It applied to Java as **bytecode manipulation**, as support for optimization, generics, AOP, and so on.

Several well-known tools/framework use it:

- AOP: AspectJ
- Profilers: jProbe, jProfiler, ...
- Spring, Hybernate, ...

Available tools for bytecode instrumentation:

- ASM;
- BCEL - Bytecode Engineering Library;
- Javassist - Java programming Assistant.

The process consist in:

- Creating an intermediate form where all objects must be represented and the intermediate representation can become untreatable worth size;

- Apply/chain multiple transformations. Undoing the changes can be problematic without an intermediate representative for each step;
- Keeping the class file verifiable, for consistency and size;

- Class pool size bloat, "dead entry" elimination could require whole class analysis;
- Insert correct instrumentation: correctness, verification and consistency verification (max stack, max loads, ...);

- Thread safety with race conditions and deadlocks problems.

To understand bytecode instrumentation we need to understand:

- The Java virtual machine (JVM);
- The class file format;

- The bytecode instruction set.

## JVM - The Java Virtual Machine

Programs written in the Java language are compiled into a portable binary format called bytecode.

Every class and interface is represented by a single class file.

These files are dynamically loaded and executed from the JVM.

![](https://www.viralpatel.net/app/uploads/2008/12/java-program-execution.png)

A *source file* is compiled into a *class file* which is loaded and executed by the bytecode interpreter.

The JVM simulates a stack-based CPU: each frame has an **operand stack** and an array of **local variables**. The operand stack is used for operands to computations and for receiving the return value of a called method, the local variables serve as registers and are used to pass method arguments.

## Bytecode Instruction Set

The JVM supports an instruction set of 256 opcodes at largest:

- 202 are in use;
- 51 are reserved for future uses;
- 3 are permanently reserved for internal use (2 for trap handling, 1 for breakpoints).

The instruction set is divided into 7 big groups:

1. Load and store;
2. Arithmetic and logic;
3. Type conversion;
4. Object creation and manipulation;
5. Operand stack management;
6. Control transfer;
7. Method invocation and return.

Plus some opcodes for specialized tasks such as exception throwing, synchronization, and so on.

**Control Transfer**:

- `goto, jsr, ret, if*, *cmp`.

**Method Invocation and Return**:

- Method call: `invokestatic, invokevirtual, invokeinterface, invokespecial`;
- Method return: `*return, ireturn, dreturn`.

**Stack Management and Load/Store Operation**:

- `push, pop, dup, *load, *Store, ...`

**Object Creation and Manipulation**:

- Object creation: `new, newarray, anewarray, anewmultiarray`;
- Object manipulation: ` getfield, putfield, getstatic, putstatic`.

**Arithmetic and Logic Operations**:

- `*add, *div, *mul, ...` 
  - `iadd, fdiv, ...`

**Type Conversions and Checks**:

- `f2i, checkcast, typecast, instanceof`.

## Java Class File Format

The compiled code is represented by using a hardware independent binary form, typically stored in a file, known as the **class file format**.

The class file format defines the representation of a class or interface.

A class file consists of a stream of bytes, and of a single `ClassFile` structure.

![](http://epic-alfa.kavli.tudelft.nl/share/doc/bcel-5.2/images/classfile.gif)

Class file structure:

```java
ClassFile {
	u4 magic;
	u2 minor_version;
	u2 major_version;
	u2 constant_pool_count;
	cp_info constant_pool[constant_pool_count-1];
	u2 access_flags;
	u2 this_class;
	u2 super_class;
	u2 interfaces_count;
	u2 interfaces[interfaces_count];
	u2 fields_count;
	field_info fields[fields_count];
	u2 methods_count;
	method_info methods[methods_count];
	u2 attributes_count;
	attribute_info attributes[attributes_count];
}
```

- **magic**: It provides the magic number (`0xCAFEBABE`) of the class file format;
- **minor_version**/**major_version**: They are the class file's minor and major version number;
- **constant_pool_count**: It contains the total number (+1) of entries in the constant pool;
- **constant_pool[]**: It stores strings, class, interfaces, field names, method names and signature names that are referred within the `ClassFile`;
- **access_flags**: They denote the access rights to this class/interface;
- **interfaces_count**: It denotes the number of (super-) interfaces of this class/interface;
- **interfaces[]**: Each entry must be a valid index to the constant pool table;
- **fields_count**: It denotes the number of `field_info` in the fields table;
- **fields[]:** Each entry must be a `field_info`  describing a field of the class;
- **methods_count**: It denotes the number of `method_info` in the methods table;
- **methods[]**: Each entry must be a `method_info` describing a method of the class;
- **attributes_count**: It denotes the number of `attribute_info` in the attributes table.
- **attributes[]**: Each entry must be a `attribute_info`0 describing an attribute of the class.
- **this_class**/**super_class**: References to the constant pool.

The bytecode instrumentation can be done on the whole program or selectively at class/method level, with operation of replacement, insertion/deletion at compile-time (static) or at run-time (dynamic).
