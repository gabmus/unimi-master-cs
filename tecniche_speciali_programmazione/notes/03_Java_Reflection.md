# Java Reflection

Java provides a small, type-safe and secure API that supports introspection, and it can be used in:

- Auto-generated documentation
- Useful tools for IDEs
- Object Serialization
- Remote Method Invocation (Java RMI)

Classes and interfaces for Reflection:

- java.lang.Class
- java.lang.reflect.Member
- java.lang.reflect.AccessibleObject
- java.lang.reflect.Field
- java.lang.reflect.Method
- java.lang.reflect.Constructor
- java.lang.reflect.Proxy
- java.lang.reflect.InvocationHandler
- java.lang.annotation.Annotation
- java.lang.instrument.Instrumentation

MOP is not casually connected, because applying a reflection in it could cause security problems. *Class* is declared *final*, so new meta-classes cannot be created, and there are no MOP operation to modify classes, therefore one cannote easily create and modularyze class-to-class transformations.

Class-to-class transformation:

- Cloneable
- Remote
- Serializable

Those are not really inferfaces, they are built-in class-to-class transformations. Java programmers cannot directly create such transformations.

## Class<T\>

*Object* defines methods to which all objects respond, not relevant for reflection purposes alone, but the class **Class<T\>**, as an extension of *Object*, has interesting methods to work on all objects via reflection.

Here an example of use for the class *Class<T\>* with a method that returns the name of the class passed. Note that the class passed is known only at runtime:

```Java
public static String classNameToString(Class<?> cls) {
	if (!cls.isArray()) 
		return cls.getName();
	else 
		return cls.getComponentType().getName() + "[]";
}
```

The following method returns the super class hierarchy of a given class stored in a list:

```Java
public static Class<?>[] getAllSuperClasses(Class<?> cls) {
	List<Class<?>> result = new ArrayList<Class<?>>();
	for (Class<?> x = cls; x != null; x = x.getSuperclass())
		result.add(x) ;
	return result.toArray(new Class<?>[0]);
}
```

## Method

The class **Method** provides useful tools for reflection, one of the most important is the method **invoke()**

```java
public Object invoke(Object obj, Object... args) 
	throws IllegalAccessException, IllegalArgumentException, InvocationTargetException { 
	...
}
```

When using *invoke()* individual parameters are unwrapped to match primitive formal parameters and both primitive and reference parameters are subject to method invocation conversions (as necessary). Return type is always wrapped in an *Object*.

## Field

Class **Field** is used for introspection on fields that can be written or read, via the methods:

- Object get(Object obj)
- void set(Object obj, Object value)

## Accessible Object

This class is used to mediate the use of the reflection on our elements, it enables the suppression of the access control checks with the use of the methods to set the accessibility of one or more elements and check if the object is accessible or not. If an object hasn't the permission to be introspected, an exception is risen:

- void setAccessible(boolean flag)
- void setAccessible(AccessibleObject[] array, boolean flag)
- boolean isAccessible()

**N.B.** The Java Security Manager can forbid the use of *setAccessible()*.

## Constructor

If the **newInstance()** method of *Class* invokes the default constructor, the class **Constructor** can invoke all other constructors.

## Examples

### Reflective Cloning

This cloning is coded using the Java reflection, which will allow to clone whatever class this is called in.

```java
public interface ReflectiveCloning {
	default public Object copy() throws Exception {
		Object tmp = this.getClass().getDeclaredConstructor().newInstance() ;
		Field[] fields = this.getClass().getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			fields[i].setAccessible(true);
			fields[i].set(tmp, fields[i].get(this));
		}
		return tmp;
	}
}
```

### Smart Message Sending

This method will allow you to call whatever method of the class this is implemented in.

```java
default public Object receive(String selector, Object[] args) throws Exception {
	Method mth = null; 
	Class<?>[] classes = null;
	if (args != null) {
		classes = new Class<?>[args.length];
		for (int i = 0; i < args.length; i++) 
			classes[i] = args[i].getClass();
	}
	mth = this.getClass().getMethod(selector, classes);
	return mth.invoke(this, args);
}
```

## Conclusions

Reflection in Java opens up both the structure and the execution trace of the program, but is limited to introspection and there isn't a clear separation between base-level and meta-levels. It is also quite inefficient. 

So it's a powerful tool that needs to be used with care.


















