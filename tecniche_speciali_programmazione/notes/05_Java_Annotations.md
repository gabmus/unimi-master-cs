# Java Annotations

Annotations are meta-data informations to "adorn the code", structured comments that let you describe functionalities or locate specific informations, they are information about information interpreted at some point by code or data analysis tools.

Those informations can be used to generate an automatic documentation (javadoc) or configuration files.

Java annotations are **optional**, they do not introduce new facility to the language.

Java has some standard annotation provided:

- *@Override*, to mark that a method overrides another method in its superclass.
- *@Deprecated*, to indicate that the use of that method is discouraged.
- *@SuppressWarning*, to turn off compiler warning.

But in general, anyone can create their own Java annotation. They are sorted in three different categories:

- **Marker Annotations**, annotations without parameters (*@MarkerAnnotation*).
- **Single-Value Annotations**, annotations with just a single value (*SingleValueAnnotation("value")*)
- **Full Annotations**, annotations with complex full range syntax(*@RevieVws({...}))*.

Those types are basically interfaces, created with the use of **@interface**, which tells the compiler that you are writing an annotation type.

Annotations' members are defined by method signatures, those do not take any input parameters and the return type defines the type of the member.

```Java
public @interface TODO {
	String value();
}

@TODO("Figure out the amount of interest per month")
public void calculateInterest(float amount, float rate) {
	// need to finish this method later
}
```

When you write the method signature, the compiler adds a member to the annotation with the same name and type after the method return type. The method will be the selector for such a member.

There also exists **meta-annotations**, annotations on annotations. There are four kind of predefined meta-annotations:

- **@Target**, specifies with program elements can have annotations of the defined type.
- **@Retention**, indicates when an annotation is tossed out by the compiler or retained in the compiler class file.
- **@Documented**, indicates that the annotation can be part of an external documentation API.
- **@Inherited**, indicates that the annotated type is an inherited one.

## Reflecting on Annotations

It is possible to check at runtime if an annotation is used by the boolean *isAnnotationPresent(annotation)* method.

```Java
@TODO("Everything is still missing")
class Test {}

public class TestIsAnnotated {
	public static void main(String[] args) {
		Class c = Test.class;
		boolean todo = c.isAnnotationPresent(TODO.class);
		if(todo) 
			System.out.println("The Test class has the annotation TODO.class");
		else
			System.out.println("The Test class does not have the annotation 						TODO.class");
	}
}
```

Through reification, it is possible to get annotation's members.

```Java
class Test {
	@GroupTODO{
		severity = GroupTODO.Severity.CRITICAL,
		item = "Figure out the amount of interest per month",
		assignetTO = "Walter Cazzola")
	public void calculateInterest(float amount, float rate) {
		// something
	}
}

public class GetMembers {
	public static void main(String[] args) {
		Class c = Test.class;
		Method element = c.getMethod("calculateInterest", float.class, float.class);

		GroupTODO groupTodo = element.getAnnotation(GroupTODO.class);
		String item = groupTodo.item();
		String assignedTo = groupTodo.assignedTo();
		
		System.out.println("GroupTODO item = " + item + " assignedTo = " + 						assignedTo);
	}
}

```









