# AspectJ (Part 3)

Let's consider to enable setting color for any figure element.

This requires passing a color down through the calls that lead to figure element factory.

This context passing uselessly pollutes several methods (all those invoked before the factory)

By using AspectJ, the context passing is applied only to when and where the factory methods of the figures are called.

```java
aspect ColorControl {
	pointcut CCClientCflow(ColorControllingClient client):
		cflow(call(* * (..)) && target(client));
	}
	pointcut make(): call(FigureElement Figure.make*(..));
	
	after (ColorControllingClient c) returning (FigureElement fe):
		make() && CCClientCflow(c) {
			fe.setColor(c.colorFor(fe));
	}
}
```

Full aspect definition is:

```java
[privileged] Modifiers aspect Id [extends Type] [implements TypeList] [PerClause] { 	Body
}
```

Any `PerClause` defines how the aspect is instantiated:

- `issingleton()`: A single instance is made. It's the default method;
- `perthis(«Pointcut»)`:  An instance per object executing at the selected join points;
- `pertarget(«Pointcut»)`: An instance per target object at the selected join points;
- `percflow(«Pointcut»)` and `percflowbelow(«Pointcut»)`: An instance per each entrance to the control flow (below) defined at the selected join points.

The static method `aspectOf` gives access to the aspect instance.

Let's write an aspect to randomly update a point's coordinates:

```java
import it.unimi.di.adapt.figures.*;
import java.util.Random;

public aspect RandomlyMovePoints {
	static Random rnd = Random();
	
    pointcut point(Point p): call(* *..*.*(..)) && target(p);
	
    after(Point p) : point(p) {
		p.x = rnd.nextInt(50)+1 ;
		p.y = rnd.nextInt(50)+1 ;
	}
}
```

```
>>> ajc -1.9 RandomlyMovePoints.aj it/unimi/di/adapt/figures/*java
RandomlyMovePoints.aj:10 [error] The field Point.x is not visible
p.x = rnd.nextInt(50)+1;
RandomlyMovePoints.aj:11 [error] The field Point.y is not visible
p.y = rnd.nextInt(50)+1;
2 errors
```

```java
import it.unimi.di.adapt.figures.* ;
import java.util.Random ;

privileged public aspect RandomlyMovePoints {
	static Random rnd = Random();
	
    pointcut point(Point p) : call(* *..*.*(..)) && target(p);
	
    after(Point p) : point(p) {
		p.x = rnd.nextInt(50)+1 ;
		p.y = rnd.nextInt(50)+1 ;
	}
}
```

```
>>> ajc -1.9 RandomlyMovePoints.aj it/unimi/di/adapt/figures/*java
>>> aj it/unimi/di/adapt/figures/TestFigures
Point(5, 3)
Point(-1, 2)
Line(Point(-5, 3), Point(-1, -2))
Point(14, 13)
Point(4, 11)
Line(Point(4, 10), Point(1, 23))
```

## Weaving

AspectJ weaver weaves class files with code from aspects

- **Compile-Time Weaving**:
  - It compiles from source files and it is part of the `ajc` process;
  - Must be used when the code depends on the aspects, like in case of introductions;
  - The aspect can be precompiled.
- **Post Compile-Time Weaving**:
  - It must be used to weave existing class file and `jar` files.
- **Load-Time Weaving**:
  - It is binary weaving postponed at when the class file is loaded;
  - This is supported by a *weaving class loader* that can be activated by a weaving agent or by the run-time environment.

*Example: Load-Time Weaving*

```java
public class Simple {
	public static void countSlow(int value) {
        count(value, 5);
    }
	public static void countFast(int value) {
        count(value, 0);
    }
	private static void count(int value, int delay) {
		for (int i=0;i<value;i++) {
			try { 
                Thread.sleep(delay);
            } catch (Exception e) {}            }
        }
	}

	public static void main(String[]argv) {
		countFast(1000);
		countSlow(1000);
	}
}
```

```java
public aspect WhereDoesTheTimeGo {
	private int nesting = 0;
	pointcut executions(): execution(* *(..)) && !within(WhereDoesTheTimeGo) ;
	Object around(): executions() {
		nesting++;
		long stime = System.currentTimeMillis();
		Object o = proceed();
		long etime = System.currentTimeMillis();
		nesting--;
		StringBuilder info = new StringBuilder();
		for (int i=0; i<nesting; i++) info.append(" ");
		info.append(thisJoinPoint+" took "+(etime-stime)+"ms");
		System.out.println(info.toString());
		return o;
	}
}
```

This aspect can be woven at compile weaving time.

```
>>> ajc WhereDoesTheTimeGo.aj Simple.java
>>> ls -l Simple.*
-rw-r--r-- 1 cazzola collab 6655 Jan 5 18:46 Simple.class
-rw-r--r-- 1 cazzola collab
 385 Jan 5 18:07 Simple.java
[18:49]cazzola@hymir:~> aj Simple
execution(void Simple.count(int, int)) took 0ms
execution(void Simple.countFast(int)) took 1ms
execution(void Simple.count(int, int)) took 5118ms
execution(void Simple.countSlow(int)) took 5118ms
execution(void Simple.main(String[])) took 5120ms
```

## Load-Time Weaving Debugging

```
>>> ajc -1.9 WhereDoesTheTimeGo.aj -outxml -outjar timing.jar
WhereDoesTheTimeGo.aj:8 [warning] advice defined in WhereDoesTheTimeGo has
									not been applied [Xlint:adviceDidNotMatch]
1 warning

>>> jar -tvf timing.jar
	55 Sat Jan 05 18:03:18 CET 2019 META-INF/MANIFEST.MF
  5359 Sat Jan 05 18:03:18 CET 2019 WhereDoesTheTimeGo.class
	79 Sat Jan 05 18:03:18 CET 2019 META-INF/aop-ajc.xml

>>> unzip timing.jar
>>> more META-INF/MANIFEST.MF
Manifest-Version: 1.0
Created-By: AspectJ Compiler

>>> more META-INF/aop-ajc.xml
<aspectj>
	<aspects>
		<aspect name="WhereDoesTheTimeGo"/>
	</aspects>
</aspectj>

>>> java -javaagent:«path to»/aspectjweaver.jar 
		 -classpath timing.jar:$CLASSPATH Simple
execution(void Simple.count(int, int)) took 1ms
execution(void Simple.countFast(int)) took 1ms
execution(void Simple.count(int, int)) took 5119ms
execution(void Simple.countSlow(int)) took 5119ms
execution(void Simple.main(String[])) took 5120ms

>>> ls -l Simple.*
-rw-r--r-- 1 cazzola collab
 639 Jan 5 18:09 Simple.class
-rw-r--r-- 1 cazzola collab
 385 Jan 5 18:07 Simple.java
```

To know what is going on we have to configure the `aop-ajc.xml` file:

```xml
<aspectj>
	<aspects>
		<aspect name="WhereDoesTheTimeGo"/>
	</aspects>
	<weaver options="-verbose -showWeaveInfo"/>
</aspectj>
```

- `verbose` is used to track general information about the weaver;
- `showWeaveInfo` is used to track all the weaving operations.

```
>>> java -javaagent:«path to»/aspectjweaver.jar
		 -classpath timing-verbose.jar:$CLASSPATH Simple
[...] info AspectJ Weaver Version 1.9.2 built on Wed Oct 24, 2018
[...] info register classloader 														  jdk.internal.loader.ClassLoaders$AppClassLoader@512ddf17
[...] info using configuration file:timing-verbose.jar!/META-INF/aop-ajc.xml
[...] info register aspect WhereDoesTheTimeGo
[...] weaveinfo Join point ’method-execution(void Simple.countSlow(int))’ in Type
	  ’Simple’ (Simple.java:3) advised by around advice from ’WhereDoesTheTimeGo’
	  (WhereDoesTheTimeGo.aj:8)
[...] weaveinfo Join point ’method-execution(void Simple.countFast(int))’ in Type
	  ’Simple’ (Simple.java:4) advised by around advice from ’WhereDoesTheTimeGo’
	  (WhereDoesTheTimeGo.aj:8)
[...] weaveinfo Join point ’method-execution(void Simple.count(int, int))’ in Type
	  ’Simple’ (Simple.java:7) advised by around advice from ’WhereDoesTheTimeGo’
	  (WhereDoesTheTimeGo.aj:8)
[...] weaveinfo Join point ’method-execution(void Simple.main(java.lang.String[]))’ 	  in Type ’Simple’ (Simple.java:12) advised by around advice from 					  ’WhereDoesTheTimeGo’ (WhereDoesTheTimeGo.aj:8)
[...] info processing reweavable type WhereDoesTheTimeGo: /WhereDoesTheTimeGo.aj
execution(void Simple.count(int, int)) took 0ms
execution(void Simple.countFast(int)) took 0ms
execution(void Simple.count(int, int)) took 5138ms
execution(void Simple.countSlow(int)) took 5138ms
execution(void Simple.main(String[])) took 5140ms
```



























