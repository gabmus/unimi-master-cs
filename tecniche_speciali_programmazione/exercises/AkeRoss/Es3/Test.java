import java.lang.reflect.Field;

public class Test {

	public static void main(String[] args) {
		try {
			TestingFields tf = new TestingFields(7,3.14);
			Field cs= TestingFields.class.getDeclaredField("s");
			cs.setAccessible(true);
			System.out.println("Value of s: "+ cs.get(tf));
			cs.set(tf,"testing ... passed!!!");
			System.out.println("Revalue of s: "+ cs.get(tf));
		} catch (SecurityException | NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		} 
	}

}
