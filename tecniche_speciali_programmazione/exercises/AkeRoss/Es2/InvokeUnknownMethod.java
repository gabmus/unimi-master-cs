import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Objects;
import java.util.regex.Pattern;

public class InvokeUnknownMethod{
	public static Class<?>[] TypeArg(String[] argtype) {
		Class<?>[] evaluatearg = new Class<?>[argtype.length];
		for (int i=0; i<argtype.length; i++) {
			if (Pattern.matches("[0-9]+",argtype[i])) {
				evaluatearg[i]=int.class;
			}else if(Pattern.matches("[0-9]+\\.[0-9]+",argtype[i])) {
				evaluatearg[i]=double.class;
			}else {
				evaluatearg[i]=String.class;
			}
		}
		return evaluatearg;
	}
	
	public static Object[] Convert(Class<?>[] evaarg,String[] typearg) {
		Object[] realarg = new Object[typearg.length];
		for (int i=0; i<typearg.length; i++) {
			if (evaarg[i]==int.class) {
				realarg[i]=Integer.valueOf(typearg[i]);
			}else if(evaarg[i]==double.class) {
				realarg[i]=Double.valueOf(typearg[i]);
			}else {
				realarg[i]=typearg[i];
				
			}
		}
		return realarg;
	}
	
    public static void main(String[] args){
        try{
            Class<?> calc = Class.forName(args[0]);
            Object ist = calc.getConstructor().newInstance();
            String[] typearg = Arrays.copyOfRange(args,2,args.length);
            Class<?>[] evaarg = new Class<?>[typearg.length];
            evaarg = TypeArg(typearg);
            Object[] realarg= new Object[typearg.length]; 
            realarg= Convert(evaarg,typearg);
            Method met = calc.getMethod(args[1],evaarg);
            Object result=met.invoke(ist,realarg);
            System.out.println(result);
        }catch(ArrayStoreException | IndexOutOfBoundsException | SecurityException | ClassNotFoundException | IllegalArgumentException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e){
           e.printStackTrace();
        } 
    }
}
