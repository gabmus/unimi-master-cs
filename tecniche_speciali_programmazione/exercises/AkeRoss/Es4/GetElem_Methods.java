package Es4;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class GetElem_Methods {
    @CFG(
            name=" get method of RecognizingElements class ",
            dependency = {
                    "java.util.ArrayList",
                    "java.lang.reflect.Method",
                    "java.lang.reflect.Field"}
    )
    public ArrayList<String> get(String[] nomi) throws ClassNotFoundException {
        Class<?> c;
        ArrayList<String> cont = new ArrayList<String>();
        for(int i=0; i<nomi.length; i++) {
            c = Class.forName(nomi[i]);
            Method[] m = c.getDeclaredMethods();
            Field[] f = c.getDeclaredFields();
            for(int d=0; d<f.length; d++) {
                cont.add(f[d].toString());
            }
            for(int d=0; d<m.length; d++) {
                cont.add(m[d].toString());
            }
        }
        return cont;
    }
}
