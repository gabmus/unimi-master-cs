package Es5_STATIC;

import Es4.*;
import java.lang.reflect.Method;

public class ControlFlowGraph {
    public static void main(String[] args) {
        try {
            for (int i = 0; i < args.length; i++) {
                Class<?> c = Class.forName(args[i]);
                Method[] m = c.getDeclaredMethods();
                for (int j = 0; j < m.length; j++) {
                    CFG s = m[j].getDeclaredAnnotation(CFG.class);
                    System.out.println(s.name());
                    System.out.println("dependency: ");
                    for(int z=0;z<s.dependency().length;z++) {
                        System.out.println("  "+s.dependency()[z]);
                    }
                    System.out.println("");
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
