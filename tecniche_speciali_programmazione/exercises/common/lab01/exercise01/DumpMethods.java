import java.lang.reflect.Method;

public class DumpMethods {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Usage: java DumpMethods CLASSNAME");
            System.exit(1);
        }
        String className = args[0];

        Class<?> mClass = null;
        try {
            mClass = Class.forName(className);
        } catch (ClassNotFoundException e) {
            try {
                mClass = Class.forName("java.lang."+className);
            } catch (ClassNotFoundException e1) {
                try {
                    mClass = Class.forName("java.util."+className);
                } catch (ClassNotFoundException e2) {
                    System.err.println("Fatal: class `" + className + "` not found");
                    System.exit(2);
                }
            }
        }

        if (mClass != null) for (Method method: mClass.getMethods()) {
            System.out.println(method);
        }
    }
}
