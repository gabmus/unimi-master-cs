import java.lang.reflect.Proxy;

public class MethodInvocationProxy {

    public static void normalExecution(ITestingFields tf) {
        assert tf != null;
        System.out.println(tf.message());
        tf.setAnswer(18);
        System.out.println(tf.message());
    }

    public static void main(String[] args) {
        TestingFields tf = new TestingFields(2,3);
        ITestingFields tfProxy = (ITestingFields) Proxy.newProxyInstance(
            tf.getClass().getClassLoader(), tf.getClass().getInterfaces(), new StateLoggerTraceHandler(tf)
        );
        normalExecution(tfProxy);
    }
}
