public class StringEvaluator implements TypeEvaluator {
    public StringEvaluator() {}
    @Override
    public Class<?> evaluate(String input) {
        return String.class;
    }
}
