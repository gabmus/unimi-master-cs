import java.util.regex.Pattern;

public class IntEvaluator implements TypeEvaluator {

    private TypeEvaluator next;
    public static final String INT_REGEX = "[0-9]+";
    public IntEvaluator(TypeEvaluator next) {
        this.next = next;
    }

    @Override
    public Class<?> evaluate(String input) {
        return Pattern.matches(INT_REGEX, input) ? int.class : next.evaluate(input);
    }
}
