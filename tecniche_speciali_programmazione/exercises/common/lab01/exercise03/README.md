# TSP

## Lab 1; Exercise 3: Testing Fields

Let's write a program that analyses the state of a generic class. As a test case let's use the following class:

```java
import java.util.Date;

public class TestingFields {
    private Double d[];
    private Date dd;
    public static final int i = 42;
    protected static String s = "testing ...";

    public TestingFields(int n, double val) {
        dd = new Date();
        d = new Double[n];
        for(int i=0; i<n; i++) d[i] = i*val;
    }
}
```

and one instance with `n=7` and `val=3.14`. The state of the instance is defined as the collection of the values of all the instance fields (both static, non static, public and private).

Let's change (through reflection) the value of `s` to `testing ... passed!!!`.

---

**NOTE**: playing around with running the program is useless for this exercise since the actually important part is all in the main. Look at the source code to appreciate what the program does.

### Build

```bash
make
```

### Run

```bash
make run
```
