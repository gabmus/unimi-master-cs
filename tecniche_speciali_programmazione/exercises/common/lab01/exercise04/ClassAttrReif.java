public interface ClassAttrReif {

    public boolean isMethod();
    public boolean isField();
    public String asString();
    public default void print() {
        System.out.println(this.asString());
    }
}
