import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class FieldReif implements ClassAttrReif {

    private Class<?> parentClass;
    private Field field;

    public static String fieldToString(Field field) {
        StringBuilder sb = new StringBuilder();
        sb.append(
                Modifier.toString(field.getModifiers())
        );
        sb.append(" ");
        sb.append(field.getType().toString());
        sb.append(" ");
        sb.append(field.getName());

        return sb.toString();
    }

    public FieldReif(Class<?> parentClass, String fieldName) throws NoSuchFieldException {
        this.parentClass = parentClass;
        this.field = this.parentClass.getDeclaredField(fieldName);
    }

    @Override
    public boolean isMethod() {
        return false;
    }

    @Override
    public boolean isField() {
        return true;
    }

    @Override
    public String asString() {
        return FieldReif.fieldToString(this.field);
    }

    public String toString() {return this.asString();}
}
