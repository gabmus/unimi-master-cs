# TSP

## Lab 1; Exercise 4: Recognizing Elements

Let's consider to have a collection of Java classes and two string arrays.

In example:

```java
String[] myClasses = {
    "java.lang.String", "MethodsReif", "java.util.Collections"
};
String[] myAttrs = {
    "indexOf", "iterator", "isMethod", "parentClass",
    "reverse", "emptyIterator", "valueOf", "hash", "r"
};
```

The first array contains the names of Java classes; the second array contains the names of all the fields and all the methods declared in the classes whose names are stored in the first array.

There is no relationship between the two arrays, i.e. we don't know whether and in which class the stored names are declared in the second array nor if they represent a field or a method.

Let's write a program that:

1. checks that the names in the second array are all declared in one of the classes in the first array;
2. outputs, for each name where it is declared;
3. outputs whether it is a method or a field;
4. the full signature including types, visibility and scope.

---

**NOTE**: try playing around with the arrays, make it fail to make sure it fails correctly.

### Build

```bash
make
```

### Run

```bash
make run
```
