package TspLab1.ex4;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ElementRecognizerMain {

    public static void main(String[] args) throws ClassNotFoundException {
        Class<?>[] classes = new Class<?>[1];
        String[] classNames = new String[1];
        String[] fieldsAndMethodsNames = new String[3];

        classes[0] = String.class;
        classNames[0] = "java.lang.String";
        Method[] methods = String.class.getMethods();
        Field[] fields = String.class.getFields();
        fieldsAndMethodsNames[0] = methods[0].getName();
        fieldsAndMethodsNames[1] = fields[0].getName();
        fieldsAndMethodsNames[2] = methods[1].getName();


        ElementRecognizer er = new ElementRecognizer();
        System.out.println("Check: " + er.check(classNames, (fieldsAndMethodsNames)));
    }
}
