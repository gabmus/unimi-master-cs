package TspLab1.ex6;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Arrays;

public class TraceHandler implements InvocationHandler {
    private Object baseObject;

    public TraceHandler(Object base) {
        baseObject = base;
    }

    public Object invoke(Object proxy, Method m, Object[] args) {
        try {
            System.out.println("before invocation of method " + m.getName());
            printState();
            Object result = m.invoke(baseObject, args);
            System.out.println("after invocation of method " + m.getName());
            printState();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void printState() throws IllegalAccessException {
        for (Field field : baseObject.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            String name = field.getName();
            Object value = field.get(baseObject);
            if(value instanceof Double[]) {
                ArrayList<Double> d = new ArrayList<>();
                d.addAll(Arrays.asList((Double[]) value));
                System.out.println(name + ": " + d.toString());
            }
            else System.out.printf("%s: %s%n", name, value);
        }
    }
}