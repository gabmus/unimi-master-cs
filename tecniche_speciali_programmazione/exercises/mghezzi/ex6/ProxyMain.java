package TspLab1.ex6;

import java.lang.reflect.*;

public class ProxyMain {
    public static void main(String[] args) {
        TestingFields test = new TestingFields(2,3);
        ITestingFields myProxy = (ITestingFields) Proxy.newProxyInstance(
                test.getClass().getClassLoader(), test.getClass().getInterfaces(), new TraceHandler(test));
        myProxy.setAnswer(2);
    }
}
