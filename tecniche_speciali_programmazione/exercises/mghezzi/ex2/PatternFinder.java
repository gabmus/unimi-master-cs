package TspLab1.ex2;

import java.util.regex.Pattern;

public class PatternFinder {

    private final static Pattern INT_PATTERN =
            Pattern.compile ("[\\+\\-]?\\d+");

    private final static Pattern DOUBLE_PATTERN =
            Pattern.compile ("[\\+\\-]?\\d+\\.\\d+(?:[eE][\\+\\-]?\\d+)?");

    public static Class<?> findClass(String string) {
        if (INT_PATTERN.matcher(string).matches())
            return int.class;
        else if (DOUBLE_PATTERN.matcher(string).matches())
            return double.class;
        else
            return null;
    }

    public static Object convert(String pString, Class<?> pClass) {
        if(pClass == Integer.class || pClass == int.class) {
            return Integer.parseInt(pString);
        }
        if(pClass == double.class) {
            return Double.parseDouble(pString);
        }
        return null;
    }

}
