package TspLab1.ex2;

import java.lang.reflect.Method;
import java.util.Arrays;

public class InvokeUnknownMethod {

    public static void main(String[] args) {
        // args[0]  --> Class name
        // args[1]  --> Method name
        // args[2+] --> Method param
        try {
            if (args.length < 2) {
                System.err.println("Wrong number of arguments");
                throw new IllegalArgumentException();
            }

            Class<?> cls = Class.forName(args[0]);
            Object obj = cls.getConstructor().newInstance();

            String[] paramsString = Arrays.copyOfRange(args, 2, args.length);
            Class<?>[] paramsClass = new Class<?>[paramsString.length];
            Object[] paramsObj = new Object[paramsString.length];
            for (int i = 0; i < paramsString.length; i++) {
                paramsClass[i] = PatternFinder.findClass(paramsString[i]);
                paramsObj[i] = PatternFinder.convert(paramsString[i], paramsClass[i]);
            }

            Method mth = cls.getMethod(args[1], paramsClass);
            Object result = mth.invoke(obj, paramsObj);

            System.out.println(cls.getSimpleName() + " " + args[1] + " = " + result);
        }
        catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
