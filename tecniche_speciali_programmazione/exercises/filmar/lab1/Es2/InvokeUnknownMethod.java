
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class InvokeUnknownMethod {
    public static void main(String[] args) {
        try {
            Class calc = Class.forName(args[0]);
            Class[] tempType = new Class[args.length - 2];

            Object[] param = new Object[args.length - 2];
            for (int i = 2; i < args.length; i++) {
                try {
                    param[i - 2] = Integer.valueOf(args[i]);
                    tempType[i - 2] = int.class;
                } catch (NumberFormatException f) {
                    try {
                        param[i - 2] = Double.valueOf(args[i]);
                        tempType[i - 2] = double.class;
                    } catch (NumberFormatException e) {
                        param[i - 2] = String.valueOf(args[i]);
                        tempType[i - 2] = String.class;
                    }
                }
            }
            System.out.println("yea");
            Method method = calc.getMethod(args[1], tempType);
            System.out.println(method);
            Object calculator = calc.getConstructor().newInstance();
            Object a = method.invoke(calculator, param);
            System.out.println(a);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("parametro non presente");
        } catch (ClassNotFoundException e) {

            System.out.println("classe non presente");
        } catch (NoSuchMethodException e) {
            System.out.println(e);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
