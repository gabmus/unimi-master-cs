import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class TraceHandler implements InvocationHandler {
    Object base;
    public TraceHandler(Object base){
        this.base = base;
    }

    @Override
    public Object invoke(Object proxy, Method m, Object[] args){
        try {
            System.out.println("prima di "+ m.getName());
            state();
            Object  ret = m.invoke(base, args);
            System.out.println("dopo di "+ m.getName());
            state();
            return ret;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public void state() throws Exception {
        Field[] campi = base.getClass().getFields();
        for (Field a : campi){
            a.setAccessible(true);
            System.out.println(a.getName() + " = " + a.get(base));
        }
    }
}