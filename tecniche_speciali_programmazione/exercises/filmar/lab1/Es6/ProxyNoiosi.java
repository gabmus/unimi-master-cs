import java.lang.reflect.Field;
import java.lang.reflect.Proxy;

public class ProxyNoiosi {
    public static void main(String[] args){
        TestingFields base = new TestingFields(12, 3.14);

        TraceHandler myth = new TraceHandler(base);

        ItestingFields mproxy =(ItestingFields) Proxy.newProxyInstance(base.getClass().getClassLoader(), base.getClass().getInterfaces(), myth);

        mproxy.setAnswer(40);
        System.out.println(mproxy.message());
    }
}