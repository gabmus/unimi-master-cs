import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class RobaNoiosa {
    static String[] classi = {"Calculator","TestingField"};
    static String[] elementi = {"d", "dd", "i", "s", "add","sasasasa", "mul", "div", "neg"}; 
    public static void main(String[] args){
        for (String el : elementi){
            List<Boolean> check = new ArrayList<>();
            for (String a : classi){
                try{
                    Class tempClass = Class.forName(a);

                    Method[] tempMs = tempClass.getDeclaredMethods();
                    Field tempF;
                    try{
                        tempF = tempClass.getDeclaredField(el);
                        check.add(true);
                        System.out.println(el + " è un campo della classe: " + tempClass.getName() + " con le seguenti caratteristiche: " + "\n modificatori :"+ Modifier.toString(tempF.getModifiers()) + "\n type: " + tempF.getType());
                    }catch(Exception e){
                        check.add(false);
                    }
                    Method tempM = searcMethod(tempMs, el);
                    if (tempM != null) {check.add(true);
                        System.out.println(el + " è un metodo della classe: " + tempClass.getName() + " con le seguenti caratteristiche: " + "\n modificatori :"+ Modifier.toString(tempM.getModifiers()) + "\n ritorno: " + tempM.getReturnType() + "\n rarametri: " + tempM.getParameterTypes());
                    }
                    else check.add(false);


                }catch(Exception e){
                    e.printStackTrace();
                }

            }
            System.out.println("l'elemento " + el  + " e presente come campo o metodo? " + check.contains(true) );
            System.out.println("################################################################################################" );
                        

        }
    
    }


    public static Method searcMethod(Method[] list, String name){
        for (Method temp : list){
            if (temp.getName() == name) return temp ;
        }
        return null;
    }


}