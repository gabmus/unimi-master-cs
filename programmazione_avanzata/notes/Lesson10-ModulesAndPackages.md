# Modules and Packages

## Modules

A **Module** is a simple text file of Python's statements that can be used for other scripts:

- To fetch a module as a whole a client uses: `import <module>`

- To fetch a particular name from a module: `from <module> import <object>` 
- To reload a module's code (without stopping the interpreter): `imp.reload <module>`

All imports are runtime operations that:

1. Find the module's file, by looking through a standard module search path;
2. Load module's bytecode (from a `.pyc` file named after the module). If the source file is newer than the `.pyc` or the `.pyc` is not found, the source file is recompiled;
3. Run the module's code to build the objects it defines.

Python looks for module in:

1. The current directory;
2. `PYTHONPATH` directories (if set);
3. Standard library directories;
4. The contents of any `.pth` file.

The concatenation of these four components becomes `sys.path`.

```python
>>> import sys
>>> sys.path
[’’, ’/usr/lib64/python34.zip’, ’/usr/lib64/python3.4’,
’/usr/lib64/python3.4/plat-linux’, ’/usr/lib64/python3.4/lib-dynload’,
’/usr/lib64/python3.4/site-packages’, ’/usr/lib/python3.4/site-packages’]
```

**N.B.** Modules are imported only once, so, code is executed just at import time.

### Import and From

**import** and **from** are statements, not compile-time declarations. 

They may be used in statements, in function definitions and so on, but they are not resolved or run until the execution flow reaches them.

They are assignments:

- `import` assigns an entire module object to a single name;
- `from` assigns new names to homonyms objects of another module.

The following

```python
# Copy these two names out (only)
from small import x,y
```

Is equivalent to

```python
import small			# Fetch the module object
x = small.x				# Copy names out by assignment
y = small.y
del small				# Getrid of the module name
```

### Module Namespaces

Files generate namespaces, so when a module statements run once at the first import every name that is assigned to a value at the top level of a module file becomes one of its attributes.

Module namespaces can be accessed via `__dict__` or `dir(module)`.

Modules are single scope

```python
print(’starting to load...’)
import sys
name = 42
def func(): pass
print(’done loading’)
```

```python
>>> import module2
starting to load...
done loading
>>> module2.sys
<module ’sys’ (built-in)>
>>> module2.name
42
>>> module2.func
<function func at 0xb7a0cbac>
>>> list(module2.__dict__.keys())
[’name’,’__builtins__’,’__file__’,’__package__’,’sys’,’func’,’__name__’,’__doc__’]
```

The function `imp.reload` forces an already loaded module's code to be reloaded and rerun again.

Assignments in the file's new code change the existing module object in-place.

## Packages

An `import` can name a directory path. A directory of Python code is said to be a **package**, so such imports are known as **package imports**.

A package import turns a directory into another Python namespace, with attributes corresponding to the subdirectories and module files that the directory contains.

Packages are organized in directories (`dir0/dir1/mod0`), and that makes imports independent from the file system conventions (`import dir0.dir1.mod0` loads `dir0/dir1/mod0`).

The package must be reachable via Python's search path mechanism.

Each directory named within the path of a package import statement must contain a **file named \_\_init\_\_.py** which contains standard Python code to provide a hook for package initialization time actions, generating a module namespace for a directory and supports the **form \*** when used in combination with package imports.

**Package Initialization**. The first time Python imports through a directory, it automatically runs all the code in the directory's `__init.py` file.

**Package Namespace Initialization**. In the package import model, the directory paths in your script become real nested object paths after an import.

**From \* Statement Behavior**. The `__all__` lists in `__init__.py` can be used to define what is exported when a directory is imported with the `from * ` statement.

```python
>>> import dir0.dir1.mod
dir0 init
dir1 init
in mod.py
>>> from imp import reload
>>> reload(dir0)
dir0 init
<module ’dir0’ from ’dir0/__init__.py’>
>>> reload(dir0.dir1)
dir1 init
<module ’dir0.dir1’ from ’dir0/dir1/__init__.py’>
>>> dir0.dir1
<module ’dir0.dir1’ from ’dir0/dir1/__init__.py’>
>>> dir0.dir1.mod
<module ’dir0.dir1.mod’ from ’dir0/dir1/mod.py’>
>>> dir0.x,dir0.dir1.y,dir0.dir1.mod.z
(1, 2, 3)
>>> from dir0.dir1.mod import z
>>> z
3
>>> import dir0.dir1.mod as mod
>>> mod.z
3
```

Imports in packages have a slightly different behavior: they are absolute with respect to the Python's search path and to look for modules in the package you have to use the relative path search statement `from .`

```python
mypkg/spam.py
from . import eggs
print(eggs.X)
```

**Data hiding in Python is only a convention**, to prefix a name with a '_' will prevent the `from *` statement to import such a name. Also to assign a list of strings to the `__all__` will force the `from *`  to import only the listed names.