# Decorators

Decoration is a way to specify management code for function and classes. 

**Decorators** themselves take form of of callable objects. In Python they come in two related flavors:

- **Function decorators** do name rebinding at function definition time, providing a layer of logic that can manage functions and methods, or later calls to them;
- **Class decorators** do name rebinding at class definition time, providing a layer of logic that can manage classes, or the instances created by calling them later.

In short, decorators provide a way to insert **automatically run code** at the end of function and class definition statements.

*Example 1*

```python
def decorator(F): 	# on @ decoration
	def wrapper(*args):  # on wrapped function call
		# Use F and args and then call F(*args)
		print("I’m executing the call {0}{1} ...".format(F.__name__, args))
		return F(*args)
	return wrapper

@decorator
def f(x,y):
	print("*** f({0}, {1})".format(x,y))

f(42, 7)
```

```python
>>> python3 fdecs.py
I’m executing the call f(42, 7) ...
*** f(42, 7)
```

*Example 2*

```python
class wrapper:
	def __init__(self, func): # On @ decoration
		self.func = func
	def __call__(self, *args): # On wrapped calls
		# Use func and args and then call func(*args)
		print("I’m executing the call {0}{1} ...".format(self.func.__name__, args))
		return self.func(*args)

@wrapper
def f2(x,y,z):
	print("*** f2({0}, {1}, {2})".format(x,y,z))

f2("abc",7, ’ß’)
```

```python
>>> python3 fdecs.py
I’m executing the call f2(’abc’, 7, ’ß’) ...
*** f2(abc, 7, ß)
```

**N.B.** Methods cannot be decorated by function decorators since `self` would be associated to the decorator

## Class Decorators

```python
def decorator(cls): 								# On @ decoration
	class wrapper:
		def __init__(self, *args):					# On instance creation
			print("I’m creating {0}{1} ...".format(cls.__name__, args))
			self.wrapped = cls(*args)
		def __getattr__(self, name):				# On attribute fetch
			print("I’m fetching {0}.{1} ...".format(self.wrapped, name))
			return getattr(self.wrapped, name)
		def __setattr__(self, attribute, value):	# On attribute set
			print("I’m setting {0} to {1} ...".format(attribute, value))
			if attribute == 'wrapped':				# Allow my attrs
				self.__dict__[attribute] = value	# Avoid looping
			else:
				setattr(self.wrapped, attribute, value)
	return wrapper

@decorator
class C:			# C = decorator(C)
	def __init__(self, x, y): 
        self.attr = 'spam'
	def f(self, a, b): 
        print("*** f({0}, {1})".format(a,b))
```

```python
>>> from cdecorators import *
>>> x = C(6, 7)
I’m creating C(6, 7) ...
I’m setting wrapped to <cdecorators.C object at 0xb79eb26c> ...
>>> print(x.attr)
I’m fetching <cdecorators.C object at 0xb79eb26c>.attr ...
spam
>>> x.f(x.attr, 7)
I’m fetching <cdecorators.C object at 0xb79eb26c>.f ...
I’m fetching <cdecorators.C object at 0xb79eb26c>.attr ...
*** f(spam, 7)
```

Decorators are some pretty useful tools, they can be have lots of applications, like:

- **Timing**, to add some timing description to a class for method calls an similar;

  ```python
  import time
  
  class timer:
      def __init__(self, func):
          self.func = func
          self.alltime = 0
      def __call__(self, *args, **kargs):
          start = time.clock()
          result = self.func(*args, **kargs)
          self.alltime += elapsed
          print('{0}: {1:.5f}, {2:.5f}'.format(self.func.__name__, elapsed, 						self.alltime))
  	return result
  ```

- **Tracing**, to trace what happens in the class;

  ```python
  def Tracer(aClass):
  	class Wrapper:
  		def __init__(self, *args, **kargs):
  			self.fetches = 0
  			self.wrapped = aClass(*args, **kargs)
  		def __getattr__(self, attrname):
  			print('Trace: ' + attrname)
  			self.fetches += 1
  			return getattr(self.wrapped, attrname)
  	return Wrapper
  ```

- **Singletons**,  to prevent a class from being instantiated multiple times;

  ```python
  class singleton:
  	def __init__(self, aClass):
  		self.aClass = aClass
  		self.instance = None
  	def __call__(self, *args):
  		if self.instance == None:
  			self.instance = self.aClass(*args) # One instance per class
  		return self.instance
  ```

- **Privateness**, to define some scoping rules in the specified class.
