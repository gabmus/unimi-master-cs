# Closures & Generators

## Closures

A **Regular Expression** (RE) is a pattern to describe strings: the functions in the **re** module (`import re`) let the user check if a regular expression matches a string and return the result of a match.

**Closures** are a programming technique that allows the bounding of a value defined in a scope to another value outside of it. Let see an example for the pluralization of english nouns with the following rules:

- If a word ends in S, X, or Z, add ES (e.g., fax becomes faxes);
- If a word ends in a noisy H, add ES (e.g., coach becomes coaches);
- If a word ends in a silent H, just add S (e.g., cheetah becomes cheetahs);
- If a word ends in Y that sounds like I, change the Y to IES (e.g., vacancy becomes vacancies);
- If a word ends in Y that is combined with a vowel to sound like something else, just add S (e.g., day becomes days);
- If all else fails, just add S and hope for the best.

Regular expressions will be used in the program to check those rules, find a match and generate the correct plural. Python's syntax for regular expressions module is defined as:

| Simbolo | Significato                                    |
| ------- | ---------------------------------------------- |
| .       | Any character but a newline (\n)               |
| ˆ       | The begin of the string                        |
| $       | The end of the string                          |
| *, +    | 0 or 1 or more repetitions of the preceding RE |
| ?       | 0 or 1 repetition of the preceding RE          |
| []      | A set of characters                            |
| ()      | Matching groups                                |

## Pluralizes via Regular Expressions

```python
import re
def plural(noun):
    # looks for words ending by S, X or Z
	if re.search('[sxz]$', noun): 
		return re.sub('$', 'es', noun)
    # looks for words ending by a not silent H by excluding the letters 
    # that combined with it will mute the H
	elif re.search('[^aeioudgkprt]h$', noun): 
		return re.sub('$', 'es', noun)
    # looks for words ending by a Y that doesn’t sound as a I similarly to the previous
	elif re.search('[^aeiou]y$', noun):
		return re.sub('y$', 'ies', noun)
	else: 
        return noun + 's'
```

With this approach if a new rule is required or an old rule needs improvement, the code has to be manually updated, because rules are hardwired in the program.

To generalize the approach, the code needs to has a higher level of abstraction, that will also limit the number of tests to be done:

```python
import re

def match_sxz(noun): return re.search('[sxz]$', noun)
def apply_sxz(noun): return re.sub('$', 'es', noun)
def match_h(noun): return re.search('[^aeioudgkprt]h$', noun)
def apply_h(noun): return re.sub('$', 'es', noun)
def match_y(noun): return re.search('[^aeiou]y$', noun)
def apply_y(noun): return re.sub('y$', 'ies', noun)
def match_default(noun): return True
def apply_default(noun): return noun + 's'

rules = ((match_sxz, apply_sxz), (match_h, apply_h), (match_y, apply_y),
	(match_default, apply_default))

def plural(noun):
	for matches_rule, apply_rule in rules:
		if matches_rule(noun):
			return apply_rule(noun)
```

With this structure, adding new rules simply means to add a couple of functions and a tuple in the `rules` tuple.

To do better, avoiding the writing of the single functions (boring and error prone), it is a good idea split the data (rules) from the code, moving patterns in a different file. This way it would not be necessary to change the code to add new rules, just edit the rules file.

```python
import re
def build_match_and_apply_functions(pattern, search, replace):
    def matches_rule(word):
        return re.search(pattern, word)
    apply_rule = lambda word : re.sub(search, replace, word)
    return (matches_rule, apply_rule)

# hardwired patterns and rules
patterns = ( ('[sxz]$', '$', 'es'), ('[^aeioudgkprt]h$', '$', 'es'),
             		  ('(qu|[^aeiou])y$', 'y$', 'ies'), ('$', '$', 's')
           		    )
rules = [ build_match_and_apply_functions(pattern, search, replace)
					for (pattern, search, replace) in hardwiredPatterns ]

```

The technique of binding a value withing the scope definition to a value in the outside scope is named **Closures**. 

It fixes the value of some variables in the body of the function it builds: both `matches_rules` and `apply_rules` take one parameter `word` and act on that plus three other values `pattern_search` and `replace` which were set when the functions are built.

Separate data from code by moving the patterns in a separate file.

```python
>>>cat plural-rules.txt
[sxz]$				$ es
[^aeioudgkprt]h$ 	$ es
[^aeiou]y$			y$ ies
$					$ s
```

Everything is still the same but the `rules` list will be filled as:

```python
rules = []
with open(’plural-rules.txt’, encoding=’utf-8’) as pattern_file:
	for line in pattern_file:
		pattern, search, replace = line.split(None, 3)
		rules.append(build_match_and_apply_functions(pattern, search, replace))
```

With this approach there is no need to change the code in order to add a new rule, but reading a file is slower than to hardwire the data in the code. The benefits outvalues the drawbacks.

## Generators

A **Generator** is a function that generates a value at a time, a sort of resumable function or function with a memory:

```python
def make_counter(x):
    print('entering make_counter')
    while True:
        yield x
        print('incrementing x')
        x = x + 1
```

*Executing the function*

```python
>>> counter = make_counter(2)
>>> next(counter)
entering make_counter
2
>>> next(counter)
incrementing x
3
```

A call to the function initializes the generator, each time a `next()` is called, the execution stops, an output value is generated and then it elaborates the next value.

- `yield()` suspends the function execution and returns a value;
- `next()` resumes the computation from the `yield()` and continues until it reaches another `yeald` or the function ends.

**N.B.** It is not possible to "go back" with values, only `next()` operation is allowed to "go forth".

### Fibonacci's Generator

```python
def gfib(max):
	a, b = 0, 1
	while a < max:
		yield a
		a, b = b, a + b

if __name__ == "__main__":
    for n in gfib(1000):	
		print(n, end=' ')
	print()
```

```python
$ python3 gfib.py
0 1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987
$ python3
>>> import gfib
>>> list(gfib.gfib(1000))
[0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987]
```

A Generator can be used in a `for` statement, the `next()` is automatically called at each iteration. The list constructor has a similar behavior.

It is possible to implement `rules()` and `plural()` functions with generators:

```python
def rules_generator(rules_filename):
	with open(rules_filename, encoding='utf-8') as pattern_file:
        for line in pattern_file:
			pattern, search, replace = line.split(None, 3)
			yield build_match_and_apply_functions(pattern, search, replace)

def plural(noun, rules_filename='plural-rules.txt'):
	for matches_rule, apply_rule in rules_generator(rules_filename):
		if matches_rule(noun):
			return apply_rule(noun)
	raise ValueError('no matching rule for {0}'.format(noun))
```

With this approach we gain a shorter start-up time (it just reads a row not the whole file) with a *lazy approach*, but also a performance losses, because every call to `plural()` reopens the file and reads it from the beginning again).

To get the benefits from both approaches (generators + closures) a custom iterator should be defined.