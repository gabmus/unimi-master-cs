# Comprehensions

Comprehensions are a compact way to transform a set of data into another. It applies to mostly all Python's structured type.

*Example 1 - List of the first ten integers*


```python
>>> [elem for elem in range(1,11)]
[1,2,3,4,5,6,7,8,9,10]
```

*Example 2 - Set composed of the first ten even integers*

```python
>>> [elem*2 for elem in range(1,11)]
[2,4,6,8,10,12,14,16,18,20]
```

*Example 3 - Dictionary composed of the first ten couples <n, n<sup>2</sup>>*

```python
>>> [elem:elem**2 for elem in range(1,11)]
[1: 1, 2: 4, 3: 9, 4: 16, 5: 25, 6: 36, 7: 49, 8: 64, 9: 81, 10: 100]
```

## Manipulating Datasets

Comprehensions can reduce the elements in the dataset after a constraint.

*Example 4 - Select perfect squares out of the first 100 integers*

```python
>>> [elem for elem in range(1,101) if (int(elem**.5))**2 == elem]
[1, 4, 9, 16, 25, 36, 49, 64, 81, 100]
```

The method `range(1,101)` generates a list of the first 100 integers (first extreme included, second excluded). 

*Example 5 - Selecting the odd numbers out of a tuple*

```python
>>> {x for x in (1,22,31,23,10,11,11,-1,34,76,778,10101,5,44) if x%2 != 0}
{1, 5, 11, 10101, 23, -1, 31}
```

**N.B.** The second 11 is removed from the resulting set and the tuple's order is not respected (it is not ordered at all).

Comprehensions can select multiple values out of a single or multiple dataset.

*Example 6 - Swapping keys and values in a dictionary*

```python
>>> a_dict = {’a’: 1, ’b’: 2, ’c’: 3}
>>> {value:key for key, value in a_dict.items()}
{1: ’a’, 2: ’b’, 3: ’c’}
```

*Example 7 - Merging two sets in a set of couples*

```python
>>> english = [’a’,’b’,’c’,’d’,’e’,’f’,’g’,’h’,’i’,’j’,...,’r’,’s’,’t’,’u’,’v’,’w’,’x’,’y’,’z’]
>>> greek = [  ́α ́, ́β ́, ́χ ́, ́δ ́, ́ε ́, ́φ ́, ́γ ́, ́η ́, ́ι ́, ́ω ́, ́κ ́, ́λ ́, ́μ ́, ́ν ́, ́ο ́, ́π ́, ́ϙ ́, ́ρ ́, ́σ ́, ́τ ́, ́θ ́, ́υ ́, ́ϝ ́, ́ξ ́, ́ψ ́, ́ζ ́ ]
>>> [(english[i],greek[i]) for i in range(0,len(english))]
[(’a’, ’ α ’), (’b’, ’ β ’), (’c’, ’ χ ’), (’d’, ’ δ ’), (’e’, ’ ε ’), (’f’, ’ φ ’), (’g’, ’ γ ’), (’h’, ’ η ’),
(’i’, ’ ι ’), (’j’, ’ ω ’), (’k’, ’ κ ’), (’l’, ’ λ ’), (’m’, ’ μ ’), (’n’, ’ ν ’), (’o’, ’ ο ’), (’p’, ’ π ’),
(’q’, ’ ϙ ’), (’r’, ’ ρ ’), (’s’, ’ σ ’), (’t’, ’ τ ’), (’u’, ’ θ ’), (’v’, ’ υ ’), (’w’, ’ ϝ ’), (’x’, ’ ξ ’),
(’y’, ’ ψ ’), (’z’, ’ ζ ’)]
```

*Example 8 - Cartesian product*

```python
>>> {(x,y) for x in range(3) for y in range(5)}
{(0, 1), (1, 2), (0, 0), (1, 3), (1, 0), (2, 2), (1, 4), (2, 1), (2, 0), (1, 1), (2, 3), (2, 4), (0, 4), (0, 3), (0, 2)}
```

## Why Use Comprehensions?

Some advantages of generating collections of data with a comprehension, grants more **compact, readable and intelligible** code.

The disadvantage is that it results in an *atomic* operation, in a sense that it is **not interruptible** in a simple way, but it is not considered as atomic in a multi-threading context or concurrency problems.

Comprehensions can't always optimize code, and sometimes they lose efficiency.

*Example 9 - Quicksort*

```python
def quicksort(s):
	if len(s) == 0: return []
	else:
		return quicksort([x for x in s[1:] if x < s[0]]) + \
    					[s[0]] +\
						quicksort([x for x in s[1:] if x >= s[0]])
```
