# Lil' Cheatsheet

## Special Variables

- `__doc__` = Grant access to the document string;
- `__name__` = Grant access to the name of the object:
  - If imported, contains the name of the file without path and extension;
  - If run as stand-alone program, it contains the "*main*" string:
- `__init__()` = Constructor method for objects;
- `__str__()` = "`toString()`" method for objects;
- `__dict__` = default dictionary attribute that contains the user-provided attributes (introspection);
- `__mro__` = list of the superclasses without duplicates in a predictable order;
- `__len__()` = declares the "length" attribute of your class;
- `__lt__()` = comparator method, defines that "this" class is "less than" another instance;
- `__add__()` = overriding of "+" operator;
- `__slots__` = overrides `__dict__`. Takes a sequence of instance variables and reserves just enough space in each instance to hold a value for each variable;
- `__setitem__()` = implement assignment to `self[key]`.Should only be implemented for mappings if the objects support changes to the values for keys;
- `__iter__()` = method that builds the iterator structure of an object;
- `__next__()` = gets the next element of the iterator container;
- `__get__`;
- `__set__`;
- `__getattr__`, run for fetches on undefined attributes;
- `__getattribute__`, is run for fetches on every attribute, so when using it you must be cautious to avoid recursive loops by passing attribute accesses to a superclass;
- `__setattr__`, run for setting value on attributes;
- `__delattr__`, is run fo deletion on every attribute.

## Useful Tools

- `map` = to apply a function to a sequence;
- `filter`= to extract from a list those elements which verify the passed function;

- `import math`
  - `math.sqrt` = square root function;

- `import functools`
  - `functools.reduce` = to reduce a list to a single element according to the passed function;
  - `functools.partial` = return a new partial object which when called will behave like *func* called with the positional arguments *args* and keyword arguments *keywords*;

- `import re` = regular expression management.
  - `re.sub` = to substitute a pattern with another; 
  - `re.search` = search for a specific pattern;
  - `re.findall`;
- `import.itertools`
  - `itertools.permutations()`
  - `itertools.combinations()`
  - `itertools.count()`
  - `itertools.cycle()`
  - `itertools.repeat()`
  - `itertools.zip_longest()`
  - `itertools.groupby()`
  - `itertools.islice()`