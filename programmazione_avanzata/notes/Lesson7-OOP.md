# Object-Oriented Programming

Python is a multi-paradigm programming language, supporting many paradigms like object orientation, but Python is not object-oriented, but just **object-based**.

- **Objects**: An object has a set of operations and a state that remembers the effect of the operations;
- **Class**: A class is a template from with objects may be created;
- **Inheritance**: A class may inherit operations from *superclasses* and its operation inherited by *subclasses*.

Wagner suggests three classes for programming languages:

- **Object-based** = only objects;
- **Class-based** = object + classes;
- **Object-oriented** = objects + classes + inheritance.

**Data abstraction**: A data abstraction is an object whose state is accessible only through its operations (data hiding).

**Delegation**: Delegation is a mechanism to delegate responsibility for performing an operation to one or more designed ancestors.

## Python: Class Definition

``` python
class rectangle:
    # Class fields created dynamically in the constructor
    def __init__(self, width, height):
        # Underscore used to make fields less accessible (similar to private)
        self._width = width		
        self._height = height	
    def calculate_area(self):
		return self._width*self._height
	def calculate_perimeter(self):
		return 2*(self._height+self._width)
	def __str__(self):
		return "I’m a Rectangle! My sides are: {0}, {1}\nMy area is {2}".\
				format(self._width,self._height, self.calculate_area())
```

Self is the state of the object and class fields are accessible only with it, this is why it always appear in methods call (even the constructor `__init__`).

Inheritance permits to reuse and specialize a class:

​																`SQUARE ---> RECTANGLE ---> SHAPE`

```python
class shape:
	def calculate_area(self): pass
	def calculate_perimeter(self): pass
	def __str__(self): pass
```

```python
class square(rectangle):
	def __init__(self, width):
		self._width=width
		self._height=width
	def __str__(self):
		return "I’m a Square! My side is: {0}\n My area is {1}".format( \
				self._width, self.calculate_area())
```

**N.B.** A square **is a** rectangle that **is a** shape.

Python uses **duck typing**, which means that if something "flies and honks" then its a "duck", unlike Java which uses an inverse approach,

## Conclusions

The meaning of class is changed:

- Super classes do not impose a behavior, because there is no inheritance, abstract classes or interfaces;
- Super classes are used to group and reuse functionalities.

Late-binding is useless, because:

- There is no static/dynamic type;
- Python uses duck typing.

Class vs instance members:

- No real distinction between fields and methods;
- Class is just the starting point;
- A member does not exist until you sure it (dynamic typing)