# Python Native Datatypes

In Python **every value has a datatype**, but you do not need to declare it.

Based on each variable's assignment, python figures out what type it is and keeps tracks of that internally.

## Boolean

Python provides two constants:

- **True**
- **False**

And operators:

- Logical operators: *and/or/not* 

- Relational operators: 

  - Equal and not equal: *==  !=*

  - Comparators: *<  > <= >=*

## Numbers

There are two kinds of numbers: **integers** and **floats**. There is no class declaration to distinguish them, they can be distinguished only by the presence of the decimal point.

The function `type()` provides the type of any value or variable.

The function `isinstance()` checks if a value or variable is of a given type.

Implicit type conversion:

- `int + int = int`

- `int + float = float`

Explicit type conversion:

- Function `int()` that truncates a float to an integer;
- Function `float()` tat promotes an integer to a float;

Integers can be arbitrarily large, but floats are accurate only on the first 15 decimal places.

## Lists

A python list looks very similar to an array, a direct assess to the members is done through the operator `[]`.

Python lists:

- Whit negative number, it gives access to the members backwards:
  -  `a_list[-2] == a_list[4-2] == a_list[2]`

- Have not a fixed static size;

- Are heterogeneous, they can have different types in them;

A slice of a list can be yelled by the `:` operator and specifying the position of the first item you want in the slice and of the first you want to exclude.

```python
>>> a_list=[1, 2, 3, 4, 5]
>>> a_list[1:3]
[2, 3]
>>> a_list[:-2]
[1, 2, 3]
>>> a_list[2:]
[3, 4, 5]
```

**N.B.** An omitted index refers to the first or the last item in the list.

Adding an item to a list it is possible in four different ways:

- Operator `+` for string concatenation;
- Method `append()` that appends an item to the end of a list;
- Method `extend()` that extends a list by appending another list to the end of it;
- Method `insert()` that inserts an item at a given position

It is also possible to introspect the list to:

- Check if an element is in the list (`3.14 in a_list`);
- Count the number of occurrences (`a_list.count(3.14)`);
- Look for an item position (`a_list.index(3.14)`);

Removing item from a list is possible in two ways:

- By position: `del a_list[2]`;
- By value: `a_list.remove(3.14)`;

In both cases the list is compacted to fill the gap.

## Tuples

Tuples are **immutable lists**, they are more efficient and secure of standard lists, and so they are often used for storing keys in dictionaries.

- Parenthesis instead of square brackets;
- Direct access to the elements through the position;
- Negative indexes count backward;
- Slicing available;

A tuple can be used to return multiple values from a function or for multiple assignments.

```python
>>> a_tuple = (1, 2)
>>> (a,b) = a_tuple
>>> a
1
>>> b
2
```

## Sets

Sets are unsorted bags of unique values and can be created out of a list:

- Elements added with ``add()`` function;
- No duplicates allowed, they are collapsed;
- Set union with ``update()`` function;

Elements removal:

- By value with `remove(3.14)`
- By position with `discard(3)`

Discarding a value not in a set has no effect, like adding a value already present in the set, but removing a value that is not in the set raises a `KetError` exception.

Set operations:

- `union()`
- `intersection()`
- `difference()`
- `symmetric_difference()`
- `issubset()`

## Dictionaries

A dictionary is an unordered set of key-value pairs, when you add a key to the dictionary you must also add a value for that key. A value for a key can be changed at any time.

Keys must be unique, homogeneous and all of the same type, you cannot have more than one entry with the same key. 

```python
SUFFIXES = {1000: [’KB’, ’MB’, ’GB’, ’TB’, ’PB’, ’EB’, ’ZB’, ’YB’],
			1024: [’KiB’, ’MiB’, ’GiB’, ’TiB’, ’PiB’, ’EiB’, ’ZiB’, ’YiB’]}
```

Syntax similar to sets but you list comma separate couples of key/value.

## Strings

Python's strings are a sequence of unicode characters and behaves like lists, so it is possible to:

- Get string length via `len()` function;
- Concatenate strings with the `+` operator;
- Slicing is possible as well; 

Python 3 supports **formatting** values into strings, by inserting a value into a string with a placeholder that will be replaced.

```python
for suffix in SUFFIXES[multiple]:
	size /= multiple
	if size < multiple:
		return ’{0:.1f} {1}’.format(size, suffix)
```

- `{0}` and `{1}` are placeholders that are replaced by the arguments of`format()`;
- `{0:.1f}` or `:.1f` is a format specifier, it can be used to:
  - Add space-padding;
  - Align strings;
  - Control decimal precision;
  - Convert number to hexadecimal;

Splitting utilities:

- By a specific char (`split('\n')`)
- To lowercase a sentence (`lower()`)
- To count the occurrences of a string into another (`count('f')`)

## Bytes

Bytes objects are **immutable sequence of numbers** (0-255). The syntax `b''` is used do define a bytes object.

Each byte within the byte literal can be an ASCII character or an encoded hexadecimal number from `\x00` to `\xff`.

```python
>>> by = b’abcd\x65’
>>> by += b’\xff’
>>> by
b’abcde\xff’
>>> len(by)
6
>>> by[5]
255
```

**N.B.** Byte objects are immutable, but byte arrays can be changed.

## Recursion

A function is called recursive when it is defined through itself.

Factorial:	`5! = 5*4*3*2*1 `

Potentially a recursive computation, with a mathematical inductive definition:

```
1			if n=0
n*(n-1)!	otherwise
```

`n=0` is the **base case** of the recursive computation, and `n*(n-1)!`is the **inductive step**

A function is recursive when its execution implies another invocation to itself:

- Directly, with an explicit call to itself in the function body;
- Indirectly, with a call to another function that calls the function itself;

At any invocations, the run-time environment creates an **activator record** (or **frame**) used to store the current values of local variables, parameters and location for the return value.

Having a frame for any invocation permits to:

- Trace the execution flow;
- Store the current state and restore it after the execution;
- Avoid interferences on the local variables;

**N.B.** Without any stopping rule, the inductive step could be going on forever (or until the memory reserved by the virtual machine is filled).

An iterative implementation is more efficient than using the recursion, the overhead is mainly due to the creation of the frame but this also affects the occupied memory.
