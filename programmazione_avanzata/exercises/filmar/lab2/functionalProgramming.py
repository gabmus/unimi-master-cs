#!/usr/bin/env python3

from functools import *
from math import *
from decimal import *

#Write the solutions for the following quizzes by using functional programming:

input1 = 10
input2 = 20
input3 = 30
#Sum all the natural numbers below one thousand that are multiples of 3 or 5.
filt = lambda x : x%3 == 0 or x%5 == 0
mul = lambda x,y : x*y

def puzzleOne(n):
    return reduce(mul, filter(filt, range(1,n)))

#Calculate the smallest number divisible by each of the numbers 1 to 20. 
def Scomposition(x):
    return x>2 and [i for i in range(2,x) if x%i == 0]
#####per ora non so come risolverlo

#Calculate the sum of the figures of 2^1000

def puzzleTree(x):
    return  sum([int(i) for i in str(x)])
#Calculate the first term in the Fibonacci sequence to contain 1000 digits.
def cut0():
    return (1 + sqrt(5))/2

def fibo(x):
    return str((cut0()**x) - ((-cut0())**(-x))/sqrt(5))

def puzzleFour():
    return  [fibo(i) for i in range(1000, 2000) if len(fibo(i)) == 1000]

#main

if (__name__=="__main__"):
    print(puzzleOne(input1))
    print(puzzleTree(2**1000))
    print(puzzleFour())
    #print(Scomposition(input2))