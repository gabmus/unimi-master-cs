alkM=[("barium", 56), ("beryllium", 4), ("calcium", 20), ("magnesium", 12), ("radium", 88), ("strontium", 38)]
#part1
highestAn = max(alkM, key=lambda x:x[1])
print(highestAn)
#part2
sortalk = sorted(alkM, key=lambda x:x[1])
print(sortalk)
#part3
DalkM=dict(alkM)
print(DalkM)
#part4
nGasses = {"helium":2, "neon":10, "argon":18, "krypton":36, "xenon":54, "radon":86 }

fun = lambda x:x[0]

finaldict = {}
finaldict.update(DalkM)
finaldict.update(nGasses)
sFinalDict = sorted(finaldict, key=fun)
print(sFinalDict) 
