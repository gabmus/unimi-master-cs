#part 1
import calendar
#part 2
thisYear = 2019
nextleap = 0
for i in range(0,20):
    if calendar.isleap(thisYear+i):
        nextleap=thisYear+i
        break

print(thisYear+i)

#part 3

leapnum = calendar.leapdays(2000, 2051)

print(leapnum)

#part 4

week = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"]

a = calendar.weekday(2016, 7, 29)

print(week[a])
