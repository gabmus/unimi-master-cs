#!/usr/bin/env python3

#shell simulator for python

import os

input1 = "ls ~/git/"

def customInput(*args):
    ret = ""
    n = 0
    for i in args:
        if (n == 0): pass
        else: ret += i
    return ret

class customShell:
    def shell(self):
        exitCommand = "quit()"
        cmd = ""
        while(cmd != exitCommand):
            cmd = input("shell >>")
            if (cmd == exitCommand): break
            os.system(cmd)

        
        

if (__name__=="__main__"):
    shell = customShell()
    shell.shell()
