#part 1

def identity(size):
    matrix = []
    for i in range(size):
        matrix.append( [0 for a in range(size)])
        matrix[i][i]=1
    return matrix

Identity = lambda x : [[1 if a==b else 0 for a in range(x) ] for b in range(x)]

print(identity(5), "   ", Identity(5))

#part 2

def square(n):
    return [[1+i+(n*a) for i in range(n)] for a in range(n)]

print(square(5))

#part 3

def traspose(m):
    return [[m[i][j]for i in range(len(m))]for j in range(len(m[1]))]

print([[i for i in "lungo"]for j in "co"],"  ", traspose([[i for i in "lungo"]for j in "co"]))

#part 4

def multy(a,b):
    return [[sum([a[m][j]*b[i][m] for m in range(len(a[1]))]) for i in range(len(b[1]))]for j in range(len(a))]

print(multy(square(3),square(3)))
