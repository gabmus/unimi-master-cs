#part1
cConverter = { "Fahrenheit": lambda x: x * 9/5 + 32, "kelvin": lambda x: x + 273.15, "rankine": lambda x: (x  + 273.15) * 9/5, "delisle": lambda x: (100 - x) * 3/2, "newton": lambda x: x * 33/100, "reaumur": lambda x: x * 4/5, "romer": lambda x: x * 21/40 + 7.5 }

rConverter = {"Fahrenheit": lambda x: ( x - 32) * 5/9, "kelvin": lambda x: x - 273.15, "rankine": lambda x: ( x - 491.67) * 5/9, "delisle": lambda x: 100 - x * 2/3, "newton": lambda x: x * 100/33, "reaumur": lambda x: x * 5/4, "romer": lambda x: ( x - 7.5) * 40/21 }

#part2


def table(a):
    conversion = [("celsius", a)]
    for i in cConverter:
        conversion.append((i,cConverter[i](a)))
    return conversion

print(table(int(input("inserisci temperatura: "))))


#part2


def toAll(a,scale="celsius"):
    if scale != "celsius":
        a = rConverter[scale](a)
    return table(a)

print(toAll(int(input("inserisci nemperatura: ")), input("inserisci scala: ")))
