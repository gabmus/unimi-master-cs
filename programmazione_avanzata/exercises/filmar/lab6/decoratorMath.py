#!/usr/bin/env python3
import math
import inspect

def log(F):
    def wrapper(*args):
        text = open("logMyMath", "a")
        text.write("{}{} \n".format(F.__name__, args[1:]))
        return F(*args)
    return wrapper

def memorize(F):
    cache = {}
    def wrapper(*args):
        if ("{}{}".format(F.__name__, args) in cache.keys()):
            print("qui")
            return cache["{}{}".format(F.__name__, args)]
        else:
            print("non cache")
            cache["{}{}".format(F.__name__, args)] = F(*args)
            return cache["{}{}".format(F.__name__, args)]
    return wrapper

def stack_trace(F):
    def wrapper(*args):
        stack = inspect.stack()
        print(stack)
        return F(*args)
    return wrapper

class MyMath:

    @log
    def fibo(self, n):
        print("fibonaccciiiii........................")
        return range(n)

    @memorize
    def fact(self, n):
        print("fattoriale...............")
        return math.factorial(n)

    @stack_trace
    def taylor(self, n):
        print("taaaaayyyllooooooor.......................")
        return n**n**n
    
if (__name__=="__main__"):
    a = MyMath()
    c = a.fact(10)
    a.fact(6)
    b = a.fact(10)
    print(c == b)
    a.taylor(6)