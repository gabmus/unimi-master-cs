#!/usr/bin/env python3
import statistics
from datetime import *

input1 = ["arturo", "pierangeli", "6/5/1995"]

class Pdec():
    def __init__(self, x = None):
        self.x = x
    
    def __get__(self, istance, owner):
        return self.x

    def __set__(self, istance, value):
        self.x = value

def per(a, name, lname, bdate):
    a.name = name
    a.lastname = lname
    a.birthDay = bdate

class Person:
    def __init__(self, name, lastname, birthday):
        per(self, name, lastname, birthday)
        
    def __repr__(self):
        return str(self.name) + " " + str(self.lastname) + " nato il " + str(self.birthDay)
        
    name = Pdec()
    lastname = Pdec()
    birthDay = Pdec()

class Sdec:
    def __init__(self, x={}):
        self.x = x
    
    def __get__(self, istance, owner):
        return self.x

    def __set__(self, istance, value):
        self.x[value[0]] = value[1]

class Adec:
    def __get__(self, obj, owner):
        if (obj.lectures != {} ): return statistics.mean(obj.lectures.values())
        return "non hai frequentato esami"
    



class Student(Person):
    def __init__(self, name, lastname, birthDay):
        super().__init__(name, lastname, birthDay)

    examA = Adec()    
    lectures = Sdec()

class Wdec:
    def __get__(self, istance, owner):
        hs = istance.hSalary
        to = {"hour" : hs, "day" : hs*8, "week" : hs*8*5, "month" : hs*8*5*4, "year" : hs*8*5*4*12}
        return to

    def __set__(self, istance, value):
        ot = {"hour" : value[1], "day" : value[1]/8, "week" : value[1]/5/8, "month" : value[1]/4/5/8, "year" : value[1]/12/4/5/8  }
        istance.hSalary = ot[value[0]]

def wk(a, salary):
    a.hSalary = salary
    
class Worker(Person):
    def __init__(self, name, lastname, birthDay, salary):
        super().__init__(name, lastname, birthDay)
        wk(self, salary)

    salary = Wdec()
    hSalary = Pdec()

class Wadec:
    def __get__(self, istance, owner):
        now = datetime.now().year
        temp = istance.birthDay.split("/")
        return int(now) - int(temp[2])

    def __set__(self, istance, age):
        now = datetime.now().year
        newy = now-age
        temp = istance.birthDay.split("/")
        istance.birthDay = temp[0] + "/" + temp[1] + "/" + str(newy)
    


class Wizard(Person):
    def __init__(self, name, lastname, birthDay):
        super().__init__(name, lastname, birthDay)

    age = Wadec()
    
    

if (__name__=="__main__"):
    a = Student(*input1)
    a.lectures = ("tuut", 23)
    a.lectures = ("scienze", 25)
    a.lectures = ("python", 1)
    print(a.examA)
    a.lectures = ("noma", 43)
    print(a.examA)
    print("\n\n")
    b = Worker(*input1, salary=5)
    print(b.salary["month"])
    b.salary = ("month", 10000)
    print(b.salary["hour"])
    print("\n\n")
    c = Wizard(*input1)
    print(c.age)
    c.age = 35
    print(c.age)
    print(c.birthDay)
