#!/bin/python3

alkaline_earth_metals = [('barium', 56), ('beryllium', 4),
                         ('calcium', 20), ('magnesium', 12),
                         ('radium', 88), ('strontium', 38)]

noble_gases = {'helium': 2, 'neon': 10, 'argon': 18,
               'krypton': 36, 'xenon': 54, 'radon': 86}


def highestatomicnumber():
    return max([b for (a, b) in alkaline_earth_metals])


def sortedlist():
    return sorted(alkaline_earth_metals, key=lambda x: x[1])


def alkaline_dict():
    return dict(alkaline_earth_metals)


def merged():
    tmp = dict()
    tmp.update(alkaline_dict())
    tmp.update(noble_gases)
    return tmp


if __name__ == '__main__':
    print('1) Highest atomic number: ', highestatomicnumber())
    print('2) Sorted list: ', sortedlist())
    print('3) List to dict', alkaline_dict())
    print('4) Merged dictionaries: ', merged())
