#!/bin/python3

import calendar
import datetime


def nextleapyear():
    currentyear = datetime.datetime.now().year
    year = currentyear
    while not calendar.isleap(year):
        year += 1
    return year


def leapdays(year1, year2):
    return calendar.leapdays(year1, year2)


def weekday(day, month, year):
    WEEKDAYS = ('Monday', 'Tuesday', 'Wednesday',
                'Thursday', 'Friday', 'Saturday', 'Sunday')
    return WEEKDAYS[calendar.weekday(year, month, day)]


if __name__ == '__main__':
    print('1) Next leap year: ', nextleapyear())
    print('2) Leap days between 2000 and 2050: ', leapdays(2000, 2050))
    print('3) Weekday of 29/07/2016: ', weekday(29, 7, 2016))
