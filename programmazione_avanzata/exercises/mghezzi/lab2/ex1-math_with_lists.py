#!/usr/bin/env python3

import functools
import math


def cond(x):
    return ((x % 3) == 0 and x) or ((x % 5) == 0 and x) or 0


def summultiples():
    return functools.reduce(lambda x, y: x+y, [cond(x) for x in range(1000)])


def mindiv(x):
    return min([n for n in range(1, x+1) if x % n == 0 and n > 1])


def smallestdiv():
    return list(map(mindiv, [x for x in range(2, 21)]))


def figuresum(n):
    return sum(int(digit) for digit in str(n))


goldensection = (1+math.sqrt(5))/2


def quickfib(n):
    return int((goldensection**n-(-goldensection)**-n)/math.sqrt(5))


def bigfib(maxdigit):
    n = 1
    fib = quickfib(n)
    while(not(len(str(fib)) == maxdigit)):
        n += 1
        fib = quickfib(n)
    return fib


if __name__ == '__main__':
    print("1) Sum of 1000 numbers multiples of 3 or 5: \n", summultiples())
    print("2) Smallest div for each num between 1 to 20: \n", smallestdiv())
    print("3) Sum of the figures of 2^1000: \n", figuresum(2**1000))
    print("4) First term in FIbonacci with 100 digits: \n", bigfib(100))
