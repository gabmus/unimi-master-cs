#!/usr/bin/env python3

import string
import itertools


class improvedstring(str):
    def __init__(self, st):
        self.string = st

    def ispalindrome(self):
        s = self.string.replace(" ", "")
        s = s.lower()
        s = ''.join(ch for ch in s if ch not in set(string.punctuation))
        return s == s[::-1]

    def __sub__(self, remove):
        s = self.string
        for ch in remove:
            s = s.replace(ch, "")
        self.string = s
        return self.string

    def checkanagrams(self, dic):
        for perm in itertools.permutations(self.string):
            for elem in dic:
                perm = list(perm)
                elem = list(elem)
                if perm == elem:
                    return True
        return False

    def __str__(self):
        return self.string


if __name__ == "__main__":
    r = improvedstring("Do geese see God?")
    print("1) Is \"" + r + "\" palindrome?", r.ispalindrome())
    r - "og"
    print("2) String subtraction:", r)
    s = improvedstring("arco")
    a = {"banana", "salsiccia", "panettone", "ocra"}
    b = {"cane", "torrone", "terrone"}
    print("3) Anagrams in a dictionary: ", s.checkanagrams(a))
