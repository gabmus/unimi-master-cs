#!/usr/bin/env python3

import traceback


def logging(func):
    def wrapper(*args):
        log = open("logger.txt", "w")
        method_name = func.__name__
        arguments = ""
        for i in range(len(args)):
            if not(i == 0):
                arguments += " " + str(args[i])
        log.write(method_name + arguments)
        log.close()
        return func(*args)
    return wrapper


class memorization:
    def __init__(self, func):
        self.func = func
        self.cache = {}

    def __call__(self, *args):
        key = args
        res = self.cache.get(key)
        if res is None:
            self.cache[key] = self.func(self, *args)
        return self.cache[key]


def stack_trace(func):
    def wrapper(*args):
        traceback.print_stack()
        return func(*args)
    return wrapper


class MyMath:

    @memorization
    @logging
    @stack_trace
    def fact(self, n):
        res = 1
        for i in range(n):
            res *= i+1
        return res


if __name__ == "__main__":
    calc = MyMath()
    print(calc.fact(5))
    print(calc.fact(5))
