#!/usr/bin/env python3


class Matrix:
    def __init__(self, matrix):
        self.matrix = matrix

    def __eq__(self, other):
        A = self.matrix
        B = other.matrix
        if len(A) == len(B) and len(A[0]) == len(B[0]):
            for row in range(len(A)):
                for col in range(len(A[0])):
                    if not(A[row][col] == B[row][col]):
                        return False
            return True
        else:
            return False

    def clone(self other):
        pass

    def __add__(self, other):
        pass
    
    # scalar
    def __mul__(self, other):
        pass

    def matrix_mul(self, other):
        pass

    def transpose(self):
        pass

    # matrix 1-norm
    def norm(self):
        pass


if __name__ == '__main__':
    A = Matrix([[2, 3, 4], [4, 5, 6]])
    B = Matrix([[2, 3], [4, 5], [6, 7]])
    C = Matrix([[2, 3, 4], [4, 5, 6]])
    print(A == B, A == C)
