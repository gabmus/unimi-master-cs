#!/usr/bin/env python3

from functools import lru_cache


@lru_cache(maxsize=128, typed=False)
def goldbach(n):
    # Find list of prime numbers from 2 to n
    primes = []
    for i in range(2, n+1):
        for j in range(2, i):
            if (i % j) == 0:
                break
        else:
            primes.append(i)

    # Checking the sum
    for i in primes:
        for j in primes:
            if i + j == n:
                return [i, j]
    return False


def goldbach_list(n, m):
    partitions = {}
    for i in range(n, m):
        if (i % 2) == 0:
            partitions[i] = goldbach(i)
    return partitions


if __name__ == '__main__':
    print(goldbach(50))
    print(goldbach_list(10, 20))
