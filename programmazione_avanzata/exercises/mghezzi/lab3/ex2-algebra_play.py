#!/usr/bin/env python3

ops = {"+": lambda x, y: x + y,
       "*": lambda x, y: x * y,
       "or": lambda x, y: x or y
       }


class Monoid:
    def __init__(self, s, op, i):
        self._set = s
        self._operation = op
        self._identity = i

    def check(self):
        mset = set(self._set)
        operation = self._operation
        identity = self._identity
        for i in self._set:
            if not(ops[operation](i, identity) == i):
                # print(i, identity)
                return False
            for j in self._set:
                if not(ops[operation](i, j) in mset):
                    # print(i, j)
                    return False
        return True

    def __str__(self):
        s = "(" + str(self._set) + ", " + str(self._operation) \
            + ", " + str(self._identity) + ")"
        return s


class Group:
    def __int__(s, add, mul):
        pass


class Ring:
    pass


if __name__ == '__main__':
    mon1 = Monoid([True, False], "or", False)
    print("Is monoid " + str(mon1) + " valid?", str(mon1.check()))
