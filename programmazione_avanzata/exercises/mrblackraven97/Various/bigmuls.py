#!/usr/bin/env python3



if __name__ == "__main__":
    xs = [1, 2, 3, 4]
    ys = [10, 15, 3, 22]
    bigmuls = [(x, y) for x in xs for y in ys if x*y > 25]
    print(bigmuls)
    