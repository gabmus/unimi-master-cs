#!/usr/bin/env python3

def hanoi(src, tmp, dst, n):
    if n == 1:
        dst.append(src.pop())
    else:
        
        hanoi(src, dst, tmp, n-1)
        print(n, src, tmp, dst)

        hanoi(src, tmp, dst, 1)
        print(n, src, tmp, dst)

        hanoi(tmp, src, dst, n-1)
        print(n, src, tmp, dst)

if __name__ == "__main__":
    n = 7
    p1, p2, p3 = (["p1:"]+[x for x in reversed(range(1, n+1))], ["p2:"], ["p3:"])
    print(p1, p2, p3)
    hanoi(p1, p2, p3, n)
    