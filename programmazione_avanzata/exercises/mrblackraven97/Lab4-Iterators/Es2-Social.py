import re
import itertools

class SocialException(Exception):
   def __init__(self, message):
        self.message = message

class Person():
   '''Simple class defining a Person in a social network'''
   def __init__(self, name : str, phone : str):
      self.name = name
      self.phone = phone

   def __eq__(self, other):
      return self.phone == other.phone

   def __hash__(self):
      return (hash(self.name) + hash(self.phone)) * 31
   
   def __str__(self):
      return "Name: {}, Phone: {}".format(self.name, self.phone)

   def __repr__(self):
      return self.__str__()

class Social():
   '''Simple social network graph of people'''
   def __init__(self, newPeople : dict = {}):
      self.people = newPeople

   def __contains__(self, person):
      return person in self.people.keys()

   def newPerson(self, person : Person):
      if not self.__contains__(person):
         self.people[person] = []
      else:
         raise SocialException("Person already subscribed")

   def connect(self, person1 : Person, person2 : Person):
      if person1 != person2 and self.__contains__(person1) and self.__contains__(person2):
         self.people[person1].append(person2)
         self.people[person2].append(person1)

   def __str__(self):
      return str(self.people)
   
if __name__ == "__main__":
   s = Social()
   p1 = Person("Marco","329")
   p2 = Person("Alessio","372")
   s.newPerson(p1)
   s.newPerson(p2)
   s.connect(p1,p2)
   print(s)