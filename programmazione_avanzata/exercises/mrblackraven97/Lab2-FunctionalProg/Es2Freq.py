import re

def readText(filename):
    ''' Reads a file and return the text read as a string '''
    with open(filename, "r") as words:
        return words.read()

def createCleanWordList(text):
    ''' Cleans a string from the punctuations and other escape chars, lowers and splits 
        it on whitespaces, creating a list of words from it '''
    return re.sub(r'([,.;:_^|?!\-\\\/\"\(\)\[\]\{\}\<\>\n])+','',text).lower().split(" ")

def createDictWords(listWords):
    ''' Creates a dictionary having each word as the key and its occurrence as the value '''
    return {x:listWords.count(x) for x in listWords if x != ''}

def freqs(filename, n):
    ''' Convert it to a list, sort it by occurrence and get those with count > n '''
    return sorted([(x,count) for x,count 
                    in createDictWords(createCleanWordList(readText(filename))).items()
                    if count > n], key = lambda tup:tup[1], reverse = True)


if __name__ == "__main__":
    print(freqs("book.txt",3))



