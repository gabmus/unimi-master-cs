import math

class rectangle:
    """ Basic implementation of a rectangle"""
    def __init__(self, width, height):
        self.__width = width		
        self.__height = height	
        
    def calculate_perimeter(self):
        return 2*(self.__height + self.__width)
        
    def calculate_area(self):
        return self.__width*self.__height
        
    def __str__(self):
        return "I’m a Rectangle! My sides are: {0}, {1}\nMy perimeter is {2}\nMy area is {3}".\
            format(self.__width, self.__height, self.calculate_perimeter(), self.calculate_area())

class circle:
    """ Basic implementation of a circle"""
    def __init__(self, radius):
        self.__radius = radius
    
    def calculate_perimeter(self):
        return 2*math.pi*self.__radius
        
    def calculate_area(self):
        return math.pi*(self.__radius**2)
        
    def __str__(self):
        return "I’m a Circle! My radius is: {0}\nMy perimeter is {1}\nMy area is {2}".\
            format(self.__radius, self.calculate_perimeter(), self.calculate_area())

class eqtriangle:
    """ Basic implementation of an Equilater Triangle"""
    def __init__(self, base):
        self.__base = base
    
    def calculate_perimeter(self):
        return 3*self.__base
        
    def calculate_area(self):
        return (math.sqrt(3)*(self.__base**2))/4
        
    def __str__(self):
        return "I’m an Equilat. Triangle! My base is: {0}\nMy perimeter is {1}\nMy area is {2}".\
            format(self.__base, self.calculate_perimeter(), self.calculate_area())

class square(rectangle):
    """ Basic implementation of a square"""
    def __init__(self, base):
        rectangle.__init__(self, base, base)
        self.__base = base
        
    def __str__(self):
        return "I’m a square! My base is: {0}\nMy perimeter is {1}\nMy area is {2}".\
            format(self.__base, self.calculate_perimeter(), self.calculate_area())

class pentagon:
    """ Basic implementation of a Pentagon"""
    def __init__(self, base):
        self.__base = base

    def calculate_perimeter(self):
        return 5*self.__base
        
    def calculate_area(self):
        return (0.688*self.__base)*self.calculate_perimeter()/2
        
    def __str__(self):
        return "I’m a Pentagon! My base is: {0}\nMy perimeter is {1}\nMy area is {2}".\
            format(self.__base, self.calculate_perimeter(), self.calculate_area())

class hexagon:
    """ Basic implementation of an Hexagon"""
    def __init__(self, base):
        self.__base = base

    def calculate_perimeter(self):
        return 6*self.__base
        
    def calculate_area(self):
        return (0.866*self.__base)*self.calculate_perimeter()/2
        
    def __str__(self):
        return "I’m an Hexagon! My base is: {0}\nMy perimeter is {1}\nMy area is {2}".\
            format(self.__base, self.calculate_perimeter(), self.calculate_area())

class iterGrowingAreas:
    """ Basic implementation of an iterator cycling on a polygon list following increasing areas"""
    def __init__(self, polyList):
        self.__polyList = sorted(polyList, key= lambda x: x.calculate_area())
        self.__i = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.__i < len(self.__polyList):
            self.__i += 1
            return self.__polyList[self.__i-1]
        else:
            raise StopIteration


if __name__ == "__main__":
    polyList = [circle(3), rectangle(1,2), square(2), eqtriangle(3), pentagon(5), hexagon(3)]
    
    for poly in iterGrowingAreas(polyList):
        print(poly,"\n")

    # polyList.sort(key= lambda x: x.calculate_perimeter())
    # for poly in iterGrowingAreas(polyList):
    #    print(poly,"\n")
