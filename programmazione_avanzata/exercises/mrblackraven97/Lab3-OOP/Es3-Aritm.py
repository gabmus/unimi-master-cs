class Stack:
    """ Basic implementation of a stack"""
    def __init__(self, initialElems=[]):
        self.__elems = initialElems

    def push(self,elem):
        self.__elems.append(elem)
    
    def pop(self):
        return self.__elems.pop()

    def top(self):
        t = self.pop()
        self.push(t)
        return t

    def isEmpty(self):
        return len(self.__elems) == 0

    def __str__(self):
        return self.__elems.__str__()
        
operators = {   "+" : lambda x, y : x + y,
                "-" : lambda x, y : x - y,
                "*" : lambda x, y : x * y, 
                "/" : lambda x, y : x / y,
                "**" : lambda x, y : x ** y,
                "or" : lambda x, y : x or y,
                "and" : lambda x, y : x and y,
                "not" : lambda x : not x
            }

opPrecedence = {    "+" : 1,
                    "-" : 1,
                    "*" : 2, 
                    "/" : 2,
                    "**" : 3,
                    "or" : 1,
                    "and" : 2,
                    "not" : 3
    
}

class PolishCalculator:
    """ Basic implementation of an calculator following the polish notation"""
    def __init__(self):
        self._opStack = Stack()

    def eval(self, expr):
        tokens = expr.split(" ")
        result = ""
        for t in tokens: # 2+3*4-2 => 234*+2-
            if t in operators:
                while not self._opStack.isEmpty() and opPrecedence[t] < opPrecedence[self._opStack.top()]:
                    result += self._opStack.pop() + " "
                
                self._opStack.push(t)
            else:
                result += t+" "

        
        while not self._opStack.isEmpty():
            result += self._opStack.pop()
            result += " "

        return result.strip()

    def calculate(self, expr, infix):
        if(infix):
            expr = self.eval(expr)
    
        tokens = expr.split(" ")
        calcStack = Stack()
        for t in tokens: 
            if t in operators:
                op2 = float(calcStack.pop())
                op1 = float(calcStack.pop())
                calcStack.push(operators[t](op1,op2))
            else:
                calcStack.push(t)

        res = calcStack.pop()
        return res
 
    def __str__(self):
        return self._opStack.__str__()

if __name__ == "__main__":
    pc = PolishCalculator()
    a = pc.eval("2 + 3 * 4 - 14 / 7")
    print(a)
    r = pc.calculate("2 3 4 5 * + -", False)
    print(r)