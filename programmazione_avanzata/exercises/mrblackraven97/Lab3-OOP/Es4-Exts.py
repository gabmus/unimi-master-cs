class sortedDict(dict):
    # devo modificare la funzionalità del metodo __setitem__ per implementare l'ordine per chiave
    def __setitem__(self, key, value):
        # ottengo la lista delle coppie del dizionario
        listDict = list(self.items())

        # se vuoto, inserisco semplicemente il nuovo valore
        if (not listDict):
            listDict.append((key,value))
        else:
            # se non è vuoto, devo scorrere tutti i valori fino a che non lo posso inserire in modo ordinato
            i = 0
            inserted = False
            while i < len(listDict) and not inserted:
                if(listDict[i][0] > key):
                    listDict.insert(i,(key,value))
                    inserted = True
                i += 1
        
        self.clear()
        self.update(dict(listDict))
        #super().__setitem__(key, value)#dict(listDict)

if __name__ == "__main__":
    diz = sortedDict()
    diz["Nome"] = "Paolo"
    diz["Cognome"] = "Rossi"
    diz["Eta"] = 18
    print(diz)