import os
import sys
import time

from stat import *

modes = {'r': (S_IRUSR, S_IRGRP, S_IROTH), 'w': (
    S_IWUSR, S_IWGRP, S_IWOTH), 'x': (S_IXUSR, S_IXGRP, S_IXOTH)}

SUFFIXES = {1000: ['KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
				1024: ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB']}
def approximate_size(size, a_kilobyte_is_1024_bytes=True):
	if size < 0:
		raise ValueError('number must be non-negative')
	multiple = 1024 if a_kilobyte_is_1024_bytes else 1000
	for suffix in SUFFIXES[multiple]:
		size /= multiple
		if size < multiple:
			return '{0:.1f} {1}'.format(size, suffix)
	raise ValueError('number too large')


def format_mode(mode):
	s = 'd' if S_ISDIR(mode) else "-"
	for i in range(3):
		for j in ['r', 'w', 'x']:
			s += j if S_IMODE(mode) & modes[j][i] else '-'
	return s


def format_date(date):
    d = time.localtime(date)
    return "{0:4}-{1:02d}-{2:02d} {3:02d}:{4:02d}:{5:02d}".format(
        d.tm_year, d.tm_mon, d.tm_mday, d.tm_hour, d.tm_min, d.tm_sec)


def ls(dir):
	print("List of {0}:".format(os.getcwd()))
	for file in os.listdir(dir):
		metadata = os.stat(file)
		print("{2} {1:6} {3} {0} ".format(file, approximate_size(metadata.st_size, False),
                                  format_mode(metadata.st_mode), format_date(metadata.st_mtime)))

def cat(filepath):
	(path, filename) = os.path.split(filepath)
	if(path != ""):
		os.chdir(path)
	print("Viewing file {}".format(filename))
	with open(filename, 'r') as f:
		print(f.read())

if __name__ == "__main__":
		ls(".")
		cat(".gitconfig")
