def identity(size):
	return [
		[1 if j == i else 0 for j in range(0, size) ] for i in range(0, size)
	]
def square(n):
	return [
		[i*n+j for j in range(0, n)] for i in range(0, n)
	]

def transpose(mat):
	return [
		[mat[i][j] for i in range(0, len(mat))] for j in range(0, len(mat[0]))
	]

def multiply(mat1, mat2):
	# check if matrix are compatible n*m and m*q
	if(len(mat1[0]) != len(mat2)):
		raise ValueError("Incompatible matrix sizes")

	return [
		[
			sum([mat1[i][k]*mat2[k][j] for k in range(0, len(mat1[0]))])
			for j in range(0, len(mat2[0]))
		] 
		for i in range(0, len(mat1))
	]
	

def prettyprint(mat):
	for row in mat:
		print(row)
	print()

if __name__== "__main__":
	prettyprint(square(3))
	prettyprint(identity(3))
	prettyprint(transpose(square(3)))
	prettyprint(multiply(square(3),[[3],[2],[1]]))