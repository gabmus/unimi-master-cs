alkaline_earth_metals = [
        ("Barium",56),
        ("Beryllium",4),
        ("Calcium",20),
        ("Magnesium",12),
        ("Radium",88),
        ("Strontium",38)
]

if __name__== "__main__":
        # 1. Write a one-liner that returns the highest atomic number in alkaline_earth_metals.
        print(sorted(alkaline_earth_metals, key = lambda x: x[1],  reverse = True)[0][1])
        
        # 2. Using one of the list methods, sort alkaline_earth_metals in ascending order (from the lightest to the heaviest).
        alkaline_earth_metals.sort(key = lambda x: x[1])
        print(alkaline_earth_metals)

        # 3. Transform the alkaline_earth_metals into a dictionary using the name of the metals as the dictionary's key.
        alkaline_earth_metals_dict = {name:weight for (name,weight) in alkaline_earth_metals} # done with comprehension
        alkaline_earth_metals_dict2 = dict(alkaline_earth_metals) #done with dict function
        print(alkaline_earth_metals_dict)
        print(alkaline_earth_metals_dict2) #they're equivalent

        # 4. Create a second dictionary containing the noble gases -- helium (2), neon (10), argon (18), krypton (36), xenon (54), and radon (86) -- and store it in the variable noble_gases.
        noble_gases = {"Helium": 2,
                        "Neon" : 10,
                        "Argon" : 18,
                        "Krypton" : 36,
                        "Xenon" : 54,
                        "Radon" : 86}
        

        # 5. Merge the two dictionaries and print the result as couples (name, atomic number) sorted in ascending order on the element names.
        alkaline_earth_metals.extend(noble_gases.items())
        print(dict(sorted(alkaline_earth_metals)))
        
        # Alternative solution
        # 5. Merge the two dictionaries and print the result as couples (name, atomic number) sorted in ascending order on the element names.
        noble_gases_and_alk_metals = {**alkaline_earth_metals, **noble_gases}
        print(noble_gases_and_alk_metals)


