import calendar
import datetime

def nextLeapYear():
	''' Basandosi sulla data corrente, determina il prossimo anno bisestile'''
	currentYear = datetime.date.today().year
	while not calendar.isleap(currentYear):
		currentYear += 1
	return currentYear

def weekdayv2(weekday):
        weekdays = ["Lunedì","Martedì","Mercoledì","Giovedì","Venerdì","Sabato","Domenica"]
        return weekdays[weekday-1]

if __name__== "__main__":
	#anno bisestile
	print("Il prossimo anno bisestile è:",nextLeapYear())
	
	#calcola quanti anni bisestili ci sono tra il 2000 e il 2050 inclusi
	print("Il numero di anni bisestili fra il 2000 e il 2050 è",calendar.leapdays(1999,2051))

	#calcola il weekday della data
	print("Il weekday del 29 luglio 2016 è",weekdayv2(calendar.weekday(2016,7,29)))
