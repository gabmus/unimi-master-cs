# 6loWPAN: IPv6 Over Low Power WPAN

Last time the 6TiSCH architecture was inspected and talked about the TSCH protocol for the MAC layer paired with 6top for the LLC layer. 

![](img/6tisch.png)

In this lecture the rest of the relevant parts of the architecture will be covered, in particular: 

- The 6LoWPAN HC (Header Compression) middleware between the MAC/LLC layer and the network layer;
- The 6LoWPAN ND (Neighbour Discovery) protocol at network layer;
- The RPL which is exactly a Routing Protocol for LLNs.

## LoWPAN Features 

It is a Low-Powered Wireless Personal Area Network, characterized by a network composed by many devices `O(10^3-10^5)` with limited computational, energy and memory resources. These devices can:

- Have a duty cycle to improve their lifetime by working only when it's necessary; 
- Have some form of mobility;
- Be a Reduced-function or Full-function depending on the hardware or software of it:
  - A **reduced-function device** can only communicate using the protocol it was meant for it and have even less resources because their scope is narrower;
  - A **full-function device** has usual better equipment so that he's able to aggregate data from the other devices, be connected to Internet and work as intermediary between the reduced-function nodes and the rest of the Internet of Things. 
  - An analogy of this can be found in heterogeneous WSNs, where the normal sensor nodes are the reduced-function devices since they are limited to collecting data and sending it over, while micro-server or aggregator nodes are richer in capabilities thus can send over the Internet the accumulated data or elaborate it directly himself.

The channel is characterized by packets of small dimensions, a limited bandwidth, short signal range(~10m) and, consequently, an high error rate. The PHY protocol used in these networks is the 802.15.4 standard which was chosen because it matches the characteristics of this channel.

This protocol has frame of limited dimensions, with a payload varying from 81 to 102 byte and a maximum bandwidth of 250 Kbps. Considering that to encapsulate the header of an IPv6 packet we already need 40 byte(16+16 for the addresses + 8 for rest of the header), we're left with little space left for both payload and header in the frame size.

The topology of the network is strictly mesh, given how short is the range of communication for each device, so when designing the routing algorithm, the multi-hop approach has to be taken into account .

## IPv6 over LoWPAN: Benefits

IPv6 is already an established and widespread standard, with several services already implemented, so there is no reason to reinvent the wheel, plus the large addressing space offered by IPv6 is needed in order to be able to uniquely address every single IoT device in our LLNs.

Our job here is adapt IPv6 technology to our lossy and heterogeneous devices, in order to create an interface between these two worlds.

![](img/ipv6.png)

Here's an example of a PDU (Protocol Data Unit) for LoWPAN, encapsulated as a payload for IPv6. This is an old example, where the RFID was considered as the way to go for the lower layers (which is not true now). 

The main problem of the IPv6, in this context, is that its **payload too large to fit in a frame**, only the header is already 40 byte (almost half the size of our frame in 802.15.4). What's worse, though, is that the MTU (Maximum Transmission Unit) of the IPv6 packet is bigger than 1280 byte. It's easy to see that a full IPv6 payload can't fit into a frame in order to send it towards a device in the LLN.

And this is where 6LoWPAN comes in.

## IPv6 Overview

Protocol whose message are made by:

- Main header of 40 byte, with an added extension header;
- MTU ≥ 1280 byte;
- Auto-configuration tools for addressing and neighbor discover: `local_address = routing_prefix + MAC address`;
- Fragmentation only over the border of the web (source, border router);
- Hierarchical addresses scheme;
- Link-local address: with `hop_count = 1`:
  - Unicast: for auto-configuration, neighbor discovery;
  - Multicast: for communication in broadcast host domain.

![](img/ipv6o.png)

## 6LoWPAN

6LoWPAN is a middleware protocol specialized to **adapt the use of IPv6 over the limitations of 802.15.4** by offering an adaptation layer.

The main functionalities offered try to solve the problems previously mentioned:

- **Header compression**: being able to compress the header or remove unneeded information from it to reduce it in size;
- **Fragmentation handling**: compressing the header won't be enough, to deliver a full IPv6 payload it is necessary to fragment the packet and handle them separately;
- **Routing protocol over mesh network**: the same routing protocol cannot be used in IPv6 since calculating routes would be overkill for the limited capabilities of the LLN nodes, and for this reason, a different way involving the 6LBR routers must be found.

The pre-requisites it must satisfy are:

- Low memory usage (~Kbytes) and minimal node computation because of their limited resources;
- Low bandwidth usage for control messages used for routing.

## Overall System Structure

![](img/stubn.png)

No assumptions can be made on the technologies used by the stub networks (the LLNs basically).

## 6LoWPAN: Header Structure

All the possibly useless information inside the header have to be removed and represent it in an efficient way the rest of the essential data. 

*Example*: something that has been thrown away is the flow information. In this case interactions in LoWPAN are typically simple and straight-forward request-responses given the low capabilities of the nodes (some kind of long-lasting interaction are really needed PSFQ could be implemented internally);

Generally, the extension header for 6LoWPAN should contain:

- **Mesh addressing**, used only for level 2 routing and if the two nodes are not at 1 hop distance (are not neighbours):
  - Externally, it could be decided to appear as a normal WLAN network or as what it is, a mesh network. The latter is usually the best practice since the illusion of being something else won't be given away because it could backfire (since it appears as WLAN, an external node could decide to send messages in broadcast, thus wasting the devices already low computation power to spread the message when usually a unicast towards the interested node is enough);
- **Fragment**, which is used for big datagrams that need to be split in multiple frames;
- **Header compression**, which is a summary of the essential header informations of the upper layers;
- **NALP (Not-A-LoWPAN)**, a custom space included for co-existence with other protocols that operate on the link that don't support LoWPAN. Their headers are compressed and stored here until the packet circulates on the LoWPAN.

Let's see each of them singularly

### Mesh Addressing Header

![](img/mhd.png)

- **Type**: The first two bits represent the type of the header, which is the mesh addressing header (type "10").

- **S/D flags**: Two flag bits that specify what kind of addressing is used for the end nodes of an IP-hop (802.15.4 short or extended). 

  If the communication is happening inside of the network, there is no need to specify the network ID so bits can be saved by just specifying the host ID (short address, 16 bits). Otherwise it's needed to specify the network ID too, and use the full IPv6 address (128 bits).

- **Hop limit**: Four bits number (0-15) that represents the maximum number of radio hops between non-IP nodes possible for a IPv6 hop.
  
- **Source address** (short or extended);

- **Destination address** (short or extended);

### Fragment Header

![](img/fhd.png)

- **Type**: the first three bits represent the fragment type of the header (type "110");
- **Rsv**: two reserved bits for a later use;
- **Datagram size**: 11 bits to represent the total size of the original non-fragmented data;
- **Datagram tag**: 16 bits to represent a common tag that identifies all fragments of the fragmented data;
- **Datagram offset**: 8 bits to represent the fragment offset starting from the original data (the first fragment has offset zero).

### Header Compression

Let's proceed to compress the **network layer header**:

![](img/chd.png)

- **Compression type HC1**: those 8 bits represent the type of compression in use and typically it's HC1 compression standard in IoT ("01000010").
- **Source / destination address flags**: a couple of bits for both source and destination in which:
  - The first bit represents if the source or destination address are in short version because they belong to the same LoWPAN or not;
  - The second bit represents if it's possible to compress the address even more because it is derivable from the MAC address of 802.15.4.
- **Traffic class / Flow label**: one bit flag to represent if these QoS improvements should be enabled for the packet or not;
- **Next header**: two bits to identify the type of transport header, which means if it's UDP, TCP or another;
- **HC2**: one bit flag that represents how the next headers (transport layer) should be compressed;
- **IPv6 Hop Limit**: 8 bits to represent the hop limit between one IPv6 node to another (not counting the hop between no-IP reduced-function devices to reach the other IPv6 node).

The **payload length** is always removed because it can be calculated through the Fragment header informations or by the frame size if there is no fragmentation.

The **IP version** is removed because it's implied that only IPv6 is used.

Then let's proceed to compress the **transport layer header** too with HC2: generally it is UDP, since it's unlikely that the TCP could be used, given all the problems to calculate the congestion window of the packets in an LLNs (if a reliable transport protocol is preferred it's always possible to  use PSFQ).

By defining a range of well-known ports for UDP (61616-61631) it's possible to represent them with only 4 bits (the less significant ones). In total only a byte is required to store both source and destination port (4 + 4 bits).

![](img/hc2.png)

- **Src port**: a one bit flag to tell that the source port is inside that well-known port range (thus can be compressed);
- **Dst port**: a one bit flag to tell that the destination port is inside that well-known port range (thus can be compressed);
- **Len**: a one bit flag to tell if the length field should be removed to save space (for example when the payload is not fragmented we don't need it);
- **Unused 5 bits**.

Compression is not possible if nodes are not in the same LoWPAN or if multicast is used. it's possible to think about pre-configuring some shared contexts and, instead of using these compressed headers, and just represent the context ID so that each node can consult his context table and understand the situation.

## Routing Intra-LoWPAN

There are two approaches for routing in LLNs:

- **Mesh under**: Where the LLN network appear as a LAN at level 3, hiding the mesh structure underneath it, and do the routing at level 2 (MAC) on multiple radio hops. Broadcast could be emulated to cover the entire LoWPAN network and this is not the recommended approach because wastes too much of the nodes resources;
- **Route over**: the LLN mesh structure isn't hidden to the public so routing must be done at level 3 directly. This is the RPL approach and has been standardized.

The IPv6 Neighbor discovery procedure can be used by both of those.

## RPL - Routing Protocol over LoWPAN 

RPL, as "Ripple", is a route over routing algorithm that needs to operate over heterogeneous and instable links, because of mobility, interferences, obstacles and low transmission power.

It has to consume the least amount of resources possible on the nodes, thus very little state information should be stored and, as always, it's important to **avoid the creation of loops** to avoid energy waste.

Differently from other routing solutions, here it's important to **under-react if there is any problem on the link** (since it's common that a link temporarily breaks), which means that instead of trying to repair the path immediately, RPL waits for a bit to see if the links are re-established because of a temporary interference.

Overhead is proportional to the network instability, because if the network appear to be stable, RPL reduces the overhead to improve efficiency.

## The RPL Approach

RPL is a **distance vector routing algorithm**, in which every node knows the "distance" between them and its neighbours and shares this information with them so that each other can update their personal routing tables.

To do so, RPL builds a DODAG, which is a Destination Oriented Directed Acyclic Graph:

- It's an **acyclic graph**, in other words a **tree**, similar to the sink tree seen for WSNs;
- It's **destination oriented**, so the destination is root of this tree, which usually corresponds to a 6LBR or a full-function device, surely not a reduced one;
- It's **directed** towards the destination (toward the root of the tree), however the links are bi-directional to allow communication back and forth.

This tree should cover the whole network so that every node has a path towards the 6LBR border router, allowing interconnection towards the Internet or other networks.

There can be different graphs supported by different 6LBR or full-function nodes following a different construction technique depending on the network constraints. 

*Example*:

- It's possible to have a ripple sub-network in which security isn't a problem but need an high bandwidth to pass data around;
- It's possible to have another ripple sub-network in which low latency is required involving only power supplied nodes (non-battery nodes).

## RPL Construction

![](img/dod.png)

The construction of the DODAG starts with the 6LBR sending a DIO (DODAG Information Object) to its neighbours, which includes the routing goal and constraints.

For each node that receive a DIO which satisfies those constraints and accepts to be part of the DODAG:

- It will remember its parent (soft state);
- Calculates its own rank in the tree hierarchy;
- Depending on the node capabilities it will decide its role:
  - If it's a reduced function device, it will just act as an host and be a leaf node;
  - If it's a full function device, he can afford to have some nodes under him and act as an intermediate node for them, and in this case the node will forward the DIO message to its neighbours, if he has some;
  - It's nickname "Ripple" derives exactly from this chain reaction of nodes propagating the DIO message along the network in order to create a tree that covers all of it.

If a node receives many DIO messages, he can choose its parent among them.

## DODAG Usage

Nodes can forward messages along two directions in the three: up, towards their parent, and down, towards their children.

- In the **UP direction**, nodes forward messages to their parents in order to reach the root:
  - This can be useful for nodes that need a path to reach the core network. Since a node knows the 6LBR he sends a DIS (DODAG Information Solicitation) towards the root to ask for the DODAG reconstruction to include him.
- In the **DOWN direction**, nodes forward messages from the core network to other nodes or to communicate between themselves:
  - A node that accept to be a child of a certain node sends him a DAO message (DODAG Destination Advertisement Object) to notify him of his existence as his child. These DAO can be sent spontaneously by the nodes when choosing a parent or after the DIO solicitation (depends on the node configuration);
  - External messages (from the Internet) flow down from the root and along his branches toward the interested node/nodes;
  - For intra-LoWPAN communications, a message generated by a source node flows up until he reaches a common parent node (between source and destination), then it flows down toward the destination node.

DAO messages also contain the addressing information (prefix of the sub-tree) which are sent to the root. This is useful to resemble the IPv6 behaviour of dividing the nodes in geographical areas, where nodes gets divided into different subtrees.

While sending these informations to the root, they can be assembled in two ways:

- **Storing mode**: each node creates a routing table containing the information on his children along with his prefix, which is the sub-network portion reachable from them.
  - *Downside*: nodes need to store more informations about their children (if they have), but usually those nodes have limited resources.
- **Non-storing mode**: DAO messages are aggregated on the go while they travel upwards toward the root. This means that only the root knows these informations (thus itself has to operate in storing mode), so every routing request must go up to the root (source routing).
  - *Downside*: packets gets bigger since they grow in size by going upwards toward the root, aggregating all the informations of the nodes they come across.

Depending on the network it's important to choose which mode implement once and for all, since all nodes need to comply with the same mode.

## Loop Prevention

How does RPL prevent loops from happening? By using only the up/down directions in the DODAG it is assured that nothing is circling in the wrong direction, preventing loop from happening.

- Ancestor -> Children = Down direction flag at 1;
- Children -> Ancestor = Up direction flag at 1.

*Example*: if I receive a packet with the "down" flag at 1 and I received it from one of my children, something in the DODAG is wrong so it must be reconstructed correctly.

Another thing that reminds the correctness of the packet direction is the rank of the nodes, because children have an higher ranks than their parents, so the direction is determined by checking the source and intermediate node rank, highlighting eventual anomalies.

Since different graphs can exist for different goals they must be specified through instances.

**An instance contains one or more root and corresponds to a certain service for application level traffic**. Many instances can coexist and every packet is related to one of them.

Each node in an instance can belong to only a DODAG at maximum, however can be associated to more instances simultaneously.

*Example*: if a service requires low latency, it's possible to associate a node to only one of the trees in the "low latency" instance, otherwise there will be a loop risk.

## Link Repair

LoWPAN exchange little data and link are easily broken or lost because of their instability. For this reason RPL decides to under-react, which means that the repairs aren't done immediately but only after a certain amount of time because maybe it was just a temporary problem.

However, if the problems persists, there are different type of repairs that can be used:

- **Local repair**: if a node lost his parent node, he tries to search for a substitutive parent by checking the rank values of the neighboring nodes that belongs to his same DODAG. This is done to keep the DODAG correct and loop-free.
- **Global repair**: It could happen that it's best to reconstruct the DODAG from scratch, and the only node capable of doing this is the root node 6LBR. 
  - *Example*: it could be useful to do this at night, when the network is not used, where links are fresh and updated for the next day.
- **Trickle timer**: periodically sending DIO messages from the root to the children to notify them that the tree still exists, with the nodes replying with a DAO to their parents telling them they are still choosing them as parent nodes. 
  - This periodical check is initially done more frequently and, as long as the network appears stable, reduce the frequency. If an inconsistency is detected, the frequency value is reset to the initial value (less time between each check).

## Conclusion

RPL assumes that a neighbour discovery mechanism is present to support the DIS, DIO and DOA messages. The mechanism used in the 6TiSCH architecture is 6LoWPAN ND.

Simulations suggest that RPL is promising

- Traffic for control information (DIO, DOA, DIS) is negligible if links are stable;
- Calculated paths are good enough;
- In storing mode, the nodes closer to the root have a bigger routing table.

