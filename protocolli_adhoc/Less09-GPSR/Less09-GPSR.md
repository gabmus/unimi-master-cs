# Position Based Routing 

In this lecture, the topic will be how to forward data in VANETs while taking advantage of the position of our nodes to **reduce, hop by hop, the distance from the destination**. 

In order to do so nodes must:

- Know their **own position**: this is granted by the GPS, available for each node in our network;
- Know the **position of their neighbours**: through beaconing, each node can know information about their neighbours, including their location;
- Know the **position of the destination**: this is done through a location service, such as GLS.

Let's remind the **stateless and reactive approach**, in which nodes don't know the network topology at all, except from their neighbours. This lead to several **local choices** when forwarding the message from a node to another. For these reason, many call this **position based forwarding instead of routing**, since there isn't a real known route but a series of local choices.

There are several proposal for position based routing algorithms:

- Some of them are based on **directional flooding**:
  - Instead of the usual flooding, in which every node will forward the packet to all of their neighbours, we can take advantage of the additional informations we have about the destination (its position) in order to **flood only towards neighbours in the destination direction**.
  
  - These are useful in some situation in which a multicast on an area is needed. 
  
    *Example*: when distributing a traffic warning to all vehicles on a certain street portion. In this case directional flooding is fast and reliable since he tries at the paths, sending multiple copies of the packet at once.
  
  - However, even if less costly, it's still not recommended for communication unicast like, since the multiple copies can create unneeded traffic and can interfere and collide with other packets, potentially losing them.
  
  - This technique has been discussed already when talking about Directed Diffusion in WSNs routing (geographic routing to spread the interests towards a specific area).
- Others are based on an **on-demand unicast** towards the most appropriate neighbour (closer to the destination or in its direction).

However all of them, as MANET discovered before them, are reactive instead of proactive since it's useless to create paths when they are easily broken in VANETs.

## Desirable Features and Performance

When designing an algorithm, one should always remember which features are the most important, given the challenged network needs:

- **Loop freedom**: to avoid loops in paths created by local choices, because otherwise it could be possible to travel node to node in circles without ever reaching the destination. If this was a problem in normal networks who knew the topology, here in stateless ones it's even harder.
- **Guaranteed delivery**: ideally a guaranteed delivery is aimed for whenever a node injects data into a network towards a destination. However in the real world there are partitions and other "pathing" problems so it is desirable, at least, to have an high ratio of successful deliveries. 
  - Since now these are not  delay-tolerant networks or opportunistic ones, it's not possible to rely on a store-carry-forward approach, so unfortunately if the destination is a partition away from an intermediate node he has to discard the packet because he can't reach it.
- **Scalability**: ideally, the algorithm should work fast and correctly as the number of nodes grows in size. However, realistically it's unpleasant if the cost of the algorithm grows with the number of nodes following a logarithmic or linear curve, surely not an exponential one.
- **Robustness**: the network must be reliable enough to deliver the messages even when something fails. In the previous example, directional flooding was robust since he uses many paths to reach the destination, so even if one or two are obstructed, others can reach it.
- **Distributed and stateless**: it's useless to analyze a topology that is ever-changing. For this reason, a distributed and stateless algorithm that runs on each node is preferred, in a peer-to-peer fashion.
- **Path strategy**: the strategy with which a packet is forwarded:
  - *Single-path*: The information is single and forwarded to a single neighbour, selected by some logic (unicast);
  - *Multi-path*: The information is duplicated and forwarded to different neighbours, selected by some logic (directional flooding);
  - *Flooding based*: The information is duplicated and forwarded to all of the neighbours indistinctly.
- **Metrics** used to evaluate the algorithm (hop count, power spent or cost): sometimes minimizing the hop count isn't always the right strategy, since it could require that nodes would increase the transmission power to communicate at longer distances. Instead, it's preferred an higher hop count with less overall power spent, since closer nodes spend less energy to communicate.
- **Physical requirements, additional system requirements** (map knowledge): for now, there is only GPS, beacons and location service as requirements, but depending on the technologies and solutions one can come up with, there could be needed additional antennas or services.
- **Location updates**: useful especially for updating destination position.
- **Quality of service** requirements: minimum requirements needed by the system to work correctly in order to provide a service reliably (more than just best effort).
- **Congestion**: one can analyze how the algorithm can congestion the network and how it impacts the nodes and the communication between them (how much congestion make packets collide?). Flooding algorithms are the most sensible ones in this matter.
- **Scheduling node activity**.
- **Topology construction, broadcasting**: of course nobody wants to construct a topology for the many reasons previously said but there were some strategies involving a partial construction, so they're going to be inspected too.

As in Wireless Sensor Networks, there are different algorithms useful in different situations and only in the end there is no one that really wins over them.

## Greedy Policies: Progress

Greedy policies are common in the algorithm world because they are easy to understand and implement, even if they don't always follow the best possible path because of the local choices, thus the inability of viewing the bigger picture.

However, since this are stateless solutions and it's not possible to build a topology of the network, this is the best approach one can think about, especially if trying to achieve a unicast communication.

When forwarding from a node to another, for example from a source or intermediate node towards a destination, the goal is to **choose the best node which can bring us closer to the recipient**. 

The best node is usually measured via the **progress metric**: given a transmitting node S, the progress of a node A is defined as the projection onto the line connecting S and the final destination of the distance between S and the receiving node A.

![](img/grprog.png)

In this example, by projecting A onto the `{SD}` segment and if `{A'D} < {SD}`, then forwarding towards A helps reaching the destination and closing the distance.

So, the main idea is to **forward the packet to a neighbour with positive progress**: but if there are more than one node with positive progress, how to choose the next one? Let's see some algorithms which use different heuristics to make this choice, each with its tradeoffs:

- **Random Progress Method (RPM)**: The next node chosen is just a random one amongst those with positive progress.
  - *Pros*: Different path testing. By sending a packet to a different neighbour every time, the probability that the data arrives successfully at the destination is increased because it will try many different paths to reach it. 
- **Most Forward in Radius (MFR)**: The next node chosen is the neighbour with the greatest positive progress to the destination, which means the one with the shortest segment when projected on `{SD}`. Basically, this consists in doing the longest hop possible in order to achieve the most progress possible per hop.
  - *Pros*: Minimizes the number of hops. By transmitting at longer distances towards the farthest node which maximizes the progress, reducing the number of hops but increasing the energy spent transmitting (since it's a long distance). If saving energy is not a big deal, it's possible to use these long hops to reduce the overall time used to reach the destination, since elaboration is cut and buffer time derived from the packet short-hopping from a node to the other.
- **Nearest with Forward Progress (NFP)**: The next node chosen is the one with the most progress among the nearest nodes, all within a certain distance from the source. It works in a complete opposite way to MFR, since the latter favors long hops while NFP favors short hops.
  - *Pros*: Minimizes the energy spent and collision probability. Instead of using the whole radius, a limit is set to avoid long hops in favour of short hops, thus reducing energy consumption. Favoring short hops also avoids unnecessary collisions, because a long hop, with a long transmission time and range, could collide with another message towards the same direction. Using a lower transmissive power, thus using short hops instead also minimizes the collision probability and interferences.

There are two other algorithms, which don't use the "progress" concept as these three did:

- **Greedy Scheme (GS)**: The next node chosen is the neighbour with the minimum linear distance to the destination. This is different from MFR because we are not looking at a progress measure, by projecting the node onto the `{SD}` segment, but instead the comparison comparing is made on all segments from all the neighboring nodes to the destination and choose the one with the shorter segment.

  <img src="img/grsch.png" style="zoom:80%;" />

  *Example*: in this example, the `{AD}` segment is shorter than `{BD}` segment, thus S will choose A as the next node.

- **Compass Routing (DIR)**: The next node chosen is the neighbour N of the current node S such that the direction `{SN}` is closer to the direction  `{SD}`.

  <img src="img/grdir.png" style="zoom:80%;" />

  *Example*: In this example, the angle between the vector `{SD}` and `{SA}` is smaller than the one between `{SD}` and `{SC}`, this means that A is "more in the direction" of D since their versors are more similar (just need to do a dot product between the versors to obtain the similarity value, which is simply the cosine of the angle in that case).

  - Some simulations showed that this approach is not loop free, so we must be careful if we use it, because packets should not keep circling around the network indefinitely without reaching the destination.

### Algorithms Comparison

In the next picture let's see what each algorithm would choose in a situation with a certain neighbourhood starting from a node S with range `r`:

![](img/algsum.png)

- MFR chooses C, since it's the node with the highest "progress" in range of S;
- NFP chooses A, since it's the closest node to S that also has some "progress" towards D;
- GREEDY chooses C, since it's the closest in a linear way to D;
- DIR chooses B, since it's the node which direction is more similar to the one from S towards D (in fact, it's exactly on the same line).

## Greedy Approach Problem

The principal problem of these greedy approaches is the **local maximum problem**: hop by hop, choice by choice, future options are limited because there is no clear view of the topology and nodes are forced to make only local choices. 

![](img/grf.png)

Because of this, the delivery could fail because a message force itself into a path of nodes that leads to an impasse, instead of the destination.

Some **recovery strategies were developed** for this this situation, whenever a local maximum is reached (those will be for MFR but are similar for the other methods):

- **Flooding MFR**: Upon local maximum, current node forwards packet to all its neighbours, then, these nodes will continue the forwarding using the standard MFR approach to reach the destination.
  - This mechanism however involves flooding, so multiple copies are generated, defeating the unicast purpose previously discussed (avoid that multiple copies could collide with themselves or other packets).
- **Alternate MFR**: Upon local maximum, the packet is forwarded to k best neighbours, however what is meant as "best" must be defined and there are still multiple copies, even if surely less than with flooding.
  - *Example*: it's possible to decide to go back on steps and choose a less "better" node who maybe can bring the message to another path towards the solution (if 5 forwards to 8 instead of 6).
- **Disjoint MFR** - Upon local maximum, every node forwards to the best neighbours it has, excluded those to which the packet has been already sent or from which the packet has been previously received.
  - *Example*: 5 won't resend the packet to 4 (since he received it from him) or to 6 (since it reached a local maximum) so maybe he will try 8 since he's the best node (2 is not toward the destination so he doesn't add "progress").

However **these are only mere patches** and not a real solution to the problem. Let's see then the state-of-the-art algorithm for geographic routing, which is also formally more correct and solid.

## Greedy Perimeter Stateless Routing (GPSR)

GPSR is a geographic routing algorithm which uses the exact same greedy policies we talked about before, but with proper mechanisms to solve the local maximum problem, derived from the graph theory, so **if there is a path from the local maximum to destination, GPSR will surely find it**, and if it doesn't, that means that there's a partition, thus no path exists toward the destination.

This is what makes this algorithm better then the ones previously discussed because, instead of using patches, it **takes advantage of graph theory to find a correct and formal solution** to the local maximum problem.

There are still a few assumptions for GPSR to work:

- **Bidirectional links**, because once reached the local maximum, it should be possible to go back to previous nodes, traversing link in the opposite way from where they came from. 
  - Since VANETs will use 802.11p, this assumption has no issue since two nodes that are in mutual range can always communicate back and forth.
- **Location servers** in order to be able to obtain the coordinates of the nodes. 
  - GLS is normally used for the location service.
- **Periodical beaconing** between neighboring nodes, with each node sharing their ID and position (which is granted by GPS) in order to memorize which nodes are currently neighbours.
  - To ensure this, each node keeps a soft-state neighbour table. Each entry represents a neighbour and has a time to live, if this information isn't refreshed in that amount of time, it will become obsolete and will be discarded (since probably that node moved away from the neighbourhood).
- Every **packet forwarded piggybacks the position of the sender**, so that, if the destination need to reply back the sender, he knows its location (exactly how in GLS). This is useful to avoid to keep contacting the location service which, even if it is not that expensive (using GLS), saves the hassle to make a query and permits a direct answer to the sender.
- A **NICs (Network Interface Cards) can be set in promiscuous mode**, in which whenever the transceiver overhears a message in his range, he will activate and make a copy of it. This will be useful in the recovery mechanism and, unlike in WSNs, wasting energy by continuously awakening the node is not important since in VANETs batteries are not a problem.

## Graph Theory Based Recovery

The main solution places its roots into Graph Theory notions, so an introduction to it will be presented and how it is used to escape the local maximum problem.

Firstly, the surrealistic assumption that the **nodes are dense enough to not have partitions and that the local maximum has at least one neighbour to fall back onto** has to be made. Later, this assumption will be removed and consider this problem but for now let's just simplify it for better understanding.

![](img/graphrt.png)

Let's suppose to have reached a local maximum at node S: the only neighbour he has provides a negative "progress" so, with a simple greedy algorithm the communication would be stuck here. 

The objective here is to use that neighbour, even if he gets us away from the destination, to **circumvent that blue circle that represents an empty and node-free area**. However, each node should:

- **Avoid backtracking**, because, since the nodes from which the packet arrived brought him to a local maximum, retrying the algorithm from those would be pointless since they would lead him again to the same local maximum.
  - In the example, S should avoid sending the packet to the node from which he received it.
- **Use only local knowledge** to delimit and circumvent the empty area. As always, saving informations about the topology of the network is not possible because it would change too quickly, the computation must be done only with local information about neighbours.

## Graph Theory: Base notions

Let's introduce some graph theory base concepts that will help us understand how GPSR works.

### Face

A face is a **polygonal region delimited by some edges that can be traveled in both directions and arrive back from where it started**.

![](img/grfac.png)

This void area is modeled by this face composed by the nodes `{x, w, v, D, z, y}`. It is a face because it's possible to travel along the edges and arrive back from the start in two ways: `x -> w -> v -> D -> z -> y -> x` and  `x -> y -> z -> D -> v -> w -> x`.

The example from before is also a face, since links are bidirectional and can be traveled back and forth, starting from a node.

![](img/graphrt2.png)

Even if there is no real closed polygon, starting from S it's possible to travel along this path `S -> N1 -> N2 -> N3 -> N4 -> D -> N4 -> N3 -> N2 -> N1 -> S`.

### Right-Hand Rule

<img src="img/rhr.png" style="zoom:250%;" />

When traveling a labyrinth, the right-hand rule guarantees to find the exit or the entrance from which one came from by keeping the right hand on the wall and following it wherever it turns. The same can be said by holding the left hand on the wall, it's equivalent as long as you keep the hand on the wall.

In the same way in algorithm to travel to a node in a face, it's possible to do it following this rule.

![](img/grfac2.png)

In order to travel from `x` towards `y`, it can be done in two ways:

- **Directly**, going counter-clockwise (if it's possible);
- **Indirectly**, by going clockwise and going through all the other nodes to reach it.

By using the right hand rule, thus always the rightmost clockwise node, the destination node can be reached, even if traveling through different faces in our network. However, if the node is reached from the start instead of the destination, then a loop is formed and no possible path exists to reach it because there must be a partition in between.

## Graph Planarization

To use these concepts of face and right hand rule, the graph needs to be planar, which means that **no edges are overlapping if not upon vertices**.

![](img/pla.png)

So it comes another problem: how to make a graph planar in order to identify all the possible faces, thus all the polygonal regions? Reminder that this must be done only via local computation, since there is no full topology!

A solution for this problem is **deleting some edges but keeping the graph connected**, otherwise partitions could be created. Unfortunately **planarization must be done every time the neighborhood of a node changes**, since the graph is calculated on the neighbours.

Let's see some planarization techniques derived from graph theory.

## Relative Neighborhood Graph (RNG)

This algorithm is able to planarize a graph without disconnecting it.

![](img/rng.png)

Let's suppose to have two nodes `u`, `v` connected by an edge. **If there is another node in between the intersection of the circles centered on both nodes then that edge should be deleted, otherwise, if there are no nodes in the intersection, that edge should be kept**. Those circles of course represent the signal range of that node. Obviously, if there is a node `w` in between them, it could just be used as a bridge to connect `u` and `v`.

Formally, this can be defined this way: an edge `(u,v)` should exist between two vertices `u` and `v` if: `∀ w≠u, v: d(u,v) ≤ max[d(u,w), d(v,w)]`

which means that if the distance between `u` and `v` is less than both the distances `(u,w)` and `(v,w)`, that means that `w` is not in the intersection, thus the edge should be kept.

In GPSR, there are only edges of unitary size based on the signal range of the nodes.

## Gabriel Graph (GG)

This algorithm is similar to RNG and is also able to planarize a graph without disconnecting it.

![](img/gg.png)

Let's suppose we have two nodes `u`, `v` connected by an edge. **If there is another node in between a circle of diameter `d(u,v)` centered in a middle point between `u` and `v` then that edge should be deleted, otherwise, if there are no nodes in that circle, that edge should be kept**.

Formally, it can be defined this way: an edge `(u,v)` should exist between two vertices `u` and `v` if: `∀ w≠u, v: d^2(u,v) ≤ [d^2(u,w) + d^2(v,w)]`.

The GG approach is more conservative than the RNG one, since it removes less edges.

![](img/expl.png)

In the image we can see an example planarization of an intricate graph by using RNG.

Both algorithms are good, what matters is that all nodes use the same identical code so that they behave all the same.

## Greedy Perimeter Stateless Routing

Given the notions previously introduced, let's see how the GPSR algorithm work in its entirety.

First of all, let's adopt one of the previous greedy techniques, it doesn't matter which one of them, just use the same for all nodes. For simplicity, let's suppose to chose also the MFR as a basic routing strategy.

Whenever a local maximum is reached, GPSR switches from the Greedy mode to the Perimeter mode:

- Each node builds a planar graph using RNG or GG with the information gathered from his neighbours;
- The right hand rule is adopted as the recovery strategy, in order to the face that intersect the joining segment between node and destination and choose the right-most counter-clockwise node which doesn't cross that segment (in fact, it tries to go around it).

When the local maximum is overcome, the routing strategy switches back to Greedy mode.

### Example

<img src="img/clk.png" style="zoom:125%;" />

Let's suppose to have reached the local maximum at `x` (even if there is still 2 as a potential maximum node for the greedy algorithm). GPSR switches to the perimeter mode and finds the face which intersects the segment `{xD}`: the` X12` face.

This same segment will be used for all the subsequent nodes in the Perimeter mode, which explains why piggyback to the packet the position of the local maximum.

From here:

1. Node `x` will forward the message to the first counter-clockwise node along the face it encounters starting from the `{xD}` segment, which is node 1. 
2. Node 1 will do the same, finding the face intersecting `{xD}` which is 132. 1 will follow the same principle and send the packet to 3.
3. And so on...

It can be proved that the right hand rule in a planar graph finds a path to the destination, if such a path exists.

Of course here the right hand rule is the only one used for the complete path, however after overcoming the local maximum at 3 we could have switched back to Greedy mode, obtaining the same result.

## GPSR: Pseudocode

```pseudocode
msg_mode <- greedy; x <- ∞;
while (destination not reached OR (x≠∞ AND dist(me,D) < dist(x,D))) do
	if (neighbor exists nearer than me to D) then
		MFR(msg); 
		msg_mode <- greedy;
        x <- ∞
	else 
		if (msg_mode == greedy) then
			x <- current node;
		msg_mode <- perimeter;
		next_edge <- right_hand_rule(incoming edge, RNG);
		if (next_edge crosses xD line) then
			next_edge <- 1st edge counterclockwise w.r.t. next_edge in RNG;
		forward msg along next_edge
end while
```

## GPSR - Construction of RNG

Let's suppose, in the following figure, to build a RNG from the node S

![](img/rng1.png)

so we keep this edge (marking it as red)There are many overlapping edges, so the local graph has to be planarized and come up with a face. Let's start with a comparison between `S-1`, `S-3`, `S-4`, `S-5`, `1-3`, `1-4`, `3-4` and `4-5`.

1. Let's start the `S-1` comparison:

   ![](img/rng2.png)

   - There are no nodes in the intersection between the ranges, so this edge is kept (and marked as red).

![](img/rng3.png)

2. Now let's start `S-3` comparison: 

   ![](img/rng4.png)

   - The intersection contains two nodes (1,4), so this edge is deleted (and marked it as blue).

   ![](img/rng5.png)

3. Now let's start `S-4` comparison:

   ![](img/rng6.png)

   - The intersection contains no node, so this edge is kept (and marked as red).

   ![](img/rng7.png)

4. Now let's start `S-5` comparison: 

   ![](img/rng8.png)

   - The intersection contains one node (4), so this edge is deleted (and marked as blue).

   ![](img/rng9.png)

5. Now let's start `1-3` comparison: 

   ![](img/rng11.png)

   - The intersection contains no node, so this edge is kept (and marked as red).

   ![](img/rng12.png)
   
6. Now let's start `3-4` comparison: 

   ![](img/rng13.png)

   - The intersection contains no node, so this edge is kept (and marked as red).

   ![](img/rng14.png)
   
7. Now the procedure stops since a face has been found.

## GPSR: Greedy + Perimeter

Let's simulate the routing path taken by a packet going from node S to node D using GPSR with MFR:

1. The joining segment `{SD}` is traced in order to see which neighbour projection on it brings more progress.

   ![](img/gp1.png)

2. Node 4 is the one that brings more progress so the message is forwarded to him. Node 4 will trace the joining segment `{4D}` and looks for the next best node.

   ![](img/gp2.png)

3. Node 7 is the one that brings more progress the message is forwarded to him. Node 7 will trace the joining segment `{7D}` and looks for the next best node.

   ![](img/gp3.png)

4. There are no better nodes that bring more progress. This means that this is a local maximum and the recovery procedure to overcome it must begun.

   ![](img/gp4.png)

5. First thing to do is switch to perimeter mode, in order to use the right hand rule. However, to do so, the neighbour graph must be planarized. Using RNG, the edge `{76}` is removed while keeping the others.

   ![](img/gp5.png)

6. The joining segment `{7D}` is used to choose the next node by rotating counter-clockwise from the segment. The first node found is 3, so by following the right hand rule, the message is forwarded to him, trying to escape from the local maximum.

   ![](img/gp6.png)

7. Again, there are no better node that brings more progress and the packet can't be sent back to 7. This means this is still a local maximum and the recovery procedure must be used again to overcome it. Perimeter mode is being used again and, to do so, the neighbour graph must be planarized. Using RNG, the edge `{13}` is removed while keeping the others.

   ![](img/gp7.png)

8. After planarization, the same joining segment `{7D}` is used to choose the next node by rotating counter-clockwise from the segment. The first node found is 2, so by following the right hand rule, the message is forwarded to him, trying again to escape from the local maximum.

   ![](img/gp8.png)

9. The next best node is searched in a greedy way and it can actually being found, since 8 provides positive progress along the `{2D}` segment. Then, finally perimeter mode can be abandoned, switching back to greedy mode, since this isn't anymore a local maximum.

![](img/gp9.png)

10. Greedy mode can be used again and finally the destination node is found. The recover procedure from the local maximum was competed successfully.

![](img/gp10.png)

## Performance Analysis

GPSR has been tested in a simulator along with DSR, a MANET routing algorithms, and compared with it. Let's see the setup and the conclusion of the test.

Firstly, the protocol used to communicate was (of course) 802.11p with a radio range of 250m: even if the maximum range is 300m it is not always guaranteed because of the usual wireless signal problems.

The vehicles followed the random way-point testing model in a [0, 72] Km/h range of speed.

Whenever a vehicle reaches the destination, it will wait for a certain amount of seconds (pause time) [0, 120]. This is a configurable parameter that can be manipulated in order to regulate the mobility of the network (an higher pause time means that nodes stay stationary for a longer period of time, thus less mobile). 

While moving, there were 30 nodes simultaneously trying to communicate toward their 30 destination with a constant bit rate (CBR) of 2Kbps.

The network was tested in a peculiar way, in which they tried to keep the density of the nodes constant (20 average neighbours per node) while testing with different amounts of nodes in different area sizes (thus keeping their ratio constant):

- 50 nodes on a 1500 x 300 m area;
- 112 nodes on a 2250 x 450 m area;
- 200 nodes on a 3000 x 600 m area;

GPSR has less overhead and chooses better paths than DSR and the charts prove it.

![](img/gpv1.png)

In the first one every possible GPSR configuration, with different beaconing intervals (every 1s, every 1.5s and every 3s), surpasses DSR in successful data delivery percentages in a network of 50 nodes. Especially with 0 pause time (nodes always mobile) DSR struggles a lot, while GPSR doesn't suffer much from it.

![](img/gpv2.png)

In the second one, by keeping the beaconing constant at 1.5 and varying the number of nodes, GPSR is still top notch on delivery probability (almost close to 100%) while DSR struggles especially with the increasing amount of nodes: at 200 nodes, at 0 pause time, probability of delivery collapses to less than 30%.

## GLS and GPSR

GLS in combination with GPSR is the definitive solution? Unfortunately no, even if it's actually the best one at the moment.

**GLS requires dense networks** while **GPSR requires that no partitions are present**, and that there is always an alternative node to use to escape a local maximum in the network to ensure the delivery with the right hand rule. These properties are not so common to find in cities given how streets are structured, thus these algorithms could still struggle a bit.

Is there another solution? Yes, but it's still a quite uncharted research argument: the Delay Tolerant Networks.

In DTNs, also called opportunistic networks, the main paradigm supported is Store, Carry & Forward instead of GPSR's and GLS' Store and Forward. Here, if there is a partition and a path to the destination doesn't exist the messaged is carried along by the vehicle until it overcomes the partition and restarts the routing algorithm.

