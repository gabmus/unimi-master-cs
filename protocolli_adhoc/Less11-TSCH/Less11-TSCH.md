# 6TiSCH Architecture

In this lecture the main topic will be how to create and support the interoperability in LLNs, where heterogeneity is king. But before introducing the 6TiSCH stack, let's do a little revision of some basic concepts.

## CSMA/CA

Since the work is based on wireless networks, collision detection cannot be done, because transceivers which can receive and transmit at the same time to listen for a collision are too complex, thus too costly to realize. For this reason, **instead of detecting a collision, the goal is to avoid it entirely**. From this CSMA with collision avoidance (CA) was developed.

The basic functioning of this algorithm can be summarized in the following pseudocode:

```pseudocode
// This is the initial code starter for the algorithm
main()
{
	// First of all, with CCA it is assured that nobody is talking in the 	  // channel right now
	sense channel; // Clear Channel Assessment
	if (channel free)
		wait for DIFS; // Waits for Distributed Inter-Frame Space
		sense channel again;
		if (channel still free)
			send frame
		else
			backoff() // Using BEB, waits before trying to sense again
	else
		backoff() // uses BEB and waits
}

backoff()
{
	// Using the Binary Exponential Backoff algorithm it is determined how     // a set amount of time to wait
	set timer <- BEB();
	
	// Awaits for an event on channel
	sense channel;
	if(channel busy)
		pause timer;
		wait free channel;
		wait for DIFS;
		resume timer;
    if(timeout)
    	sense channel;
    	if(channel free)
    		send frame;
    	else 
    		backoff();
}
```

In a few words:

1. Node senses channel;

2. If it's free, wait a little amount of time for node desynchronization (DIFS), to avoid that two nodes that acknowledged that the channel is free, try to communicate simultaneously, thus provoking a collision. If not free, go in back-off.

3. Node senses again and if channel is still free it transmits the frame, otherwise goes in **back-off mode**:

   1. In back-off mode, the node waits for a time set by the Binary Exponential Backoff algorithm (amount is chosen in an interval from zero and increasing powers of 2) then re-checks the channel, and If it's busy (used by another node), wait for it to be free again, wait for the small DIFS time and resume the timer;
      
2. When timer ends, the node senses the channel again:
      - If free, send frame;
      - If not free, restart back-off procedure.

## Slotted Aloha

Slotted Aloha is a technology that was designed and adopted for broadcast satellite networks. In this protocol, nodes are synchronized and know at what time each transmission slot starts. **A transmission slot is a periodic amount of time long enough to transmit a frame (usually MAC) of the maximum size**.

<img src="img/aloh.png" style="zoom: 50%;" />

Each station has to **transmit at the beginning of that time slot**, otherwise he has to wait for the next one. If transmitting nodes choose different time slots to send the frame there are no collision, like with A in the second time slot of the figure. In the other way, if two or more nodes decide to transmit in the same slot we have a collision, like with C, F and H in the fourth slot of the figure.

It was studied that:

- The probability of success, which is the probability of transmitting on an empty slot is 36.8%;
- The probability of collision, which is the probability that two or more nodes transmit in the same slot is 26.4%;
- The probability that the slot remains empty, which means no one is transmitting is 36.8%.

Basically, **in pure Slotted Aloha, it's probable that 1 in 4 packets will be lost due to a collision**, which is not really ideal but we can work out something from here. 

## Deterministic Network (DetNet)

A deterministic network, also called DetNet, is a **network with guaranteed throughput, low latency and high reliability**. It seems like the ideal network for Industry 4.0.

![](img/det.png)

It is based on one of these two:

- **TDMA** (Time Division Multiple Access) which is based on the Time Division Multiplexing and offers reserved slots for each node, guaranteeing full bandwidth and no collision for him in that time slot.
  - Because of it's properties, it has **predictable delay** for transmission (it is known that the delay for a node is the interval of time until it's his slot turn to communicate).
- **FDMA** (Frequency Division Multiple Access) which is based on the Frequency Division Multiplexing and offers a different sub-band/sub-channel for each node to communicate onto.
  - Instead of a pure FDM, instead of assigning a channel for each node it's possible to use *channel hopping*: whenever a node wants to communicate, it hops to the first free channel; if they're all full it will wait for one to get freed.
  - Paired with channel hopping this method offers a **greater robustness** to interferences, since each node has its own channel and by hopping to another channel it's possible to avoid the interference that plagued the other one.

## 6TiSCH Architecture

The 6TiSCH architecture is a **solution for energy and resource constrained wireless devices** in LLNs (Low-powered Lossy Network).

In the next figure it can be seen how the 6TiSCH stack differs from the usual WiFi stack:

![](img/6tisch.png)

Assuming at physical level to have the standard 802.15.4, which is the Wireless Personal Area Network protocol (WPAN). In this and the next lecture, the protocols in the yellow rectangles are going to be explored.

In the stack a special version of the WPAN protocol is used (the 802.15.4e) which, instead of using CSMA/CA at data-link level, uses TSCH protocol for MAC and 6top for LLC (Logical Link Control). 

Next there is **6LoWPAN HC, which is a middleware for Low-powered WPAN used for Header Compression**, because since IPv6 addresses weigh a lot (128 bit per address), a way to compress the headers of the upper layer packets is required, in a way that they can fit the data-link frame size.

Furthermore, at network layer, upon IPv6, there is 6LoWPAN ND, a protocol for Neighbour Discover which replaces ARP in its purpose and **RPL (read as Ripple) as a routing algorithm which can connect different types of networks** (for example WSNs with VANETs).

The common "6" in the names of these protocols suggests that they all use IPv6 as base. 

To avoid confusion, note that 6TiSCH is the whole architecture, while TSCH is just a layer of it.

This architecture is already well supported and implemented in several operative systems for IoT devices, such as Contiki OS, OpenWSN OS and RIOT OS.

![](img/ostsc.png)

## IEEE 802.15 Working Group

The IEEE 802.15 was born as a working group on WPANs, especially focused on Bluetooth technology. Now, with the diversification of the technologies developed by this group, the protocol involving Bluetooth is now categorized as 802.15.1 to make space to other short-ranged network protocols.

One of them, **802.15.4**, was originally born for the data-link and physical layers (MAC/PHY) of WSNs and was characterized by a **low bitrate and range**,  because nodes in WSNs are usually sensors, thus constrained devices with limited computational power and battery.

The variation **802.15.4e** was introduced to **involve determinism** in this protocol and in this kind of network, where instead of leaving free access to channels, eventually having to actively avoid collisions, a deterministic access to them is set in order to **avoid any collision and improve reliability** drastically. The only source of problems would be an unforeseen external source of interference, so once removed that, it remains a reliable network setup, at least in the MAC/PHY layers.

This brings another benefit: by avoiding re-transmissions caused by collisions **nodes lifetime is greatly improved** by reducing energy waste (from here the "e" in the name).

Remind that, even if Bluetooth is another name for 802.15.1 as Wi-Fi is another name for 802.11, ZigBee is NOT another name for 802.15.4: on the contrary, as seen in the 6TiSCH architecture figure, it only uses 802.15.4 for the MAC and PHY layers, while ZigBee is a set of protocols from the network layer up (>=L3). *802.15.4 only involves MAC and PHY layers.*

There are others sub-groups of 802.15:

- 802.15.3 is a protocol that provides an high bitrate for multimedia WPANs;
- 802.15.5 is a protocol for mesh networking;
- 802.15.6 is a protocol for Body Area Networks (BANs):
  - Differently from PANs, which target is the person, BANs focus particularly on the body of a person, as it is used for wearable devices that can measure the life parameters and other physical information about a person (useful in an health-care scenario).

But how low is the bitrate discussed? Let's make a comparison:

- 802.15.1 aka Bluetooth has a bitrate ranging from 0.1 to 2 Mbps;
- 802.15.3 aka the protocol for multimedia WPANs has a bitrate ranging from 100 to 1000 Mbps;
- 802.15.4 instead only support bitrates from 0.001 to 0.25 Mbps, which roughly means 1 to 250 Kbps.

Even if a low bitrate is requested to save energy, this limitation brings many transmission problems. This is why the *6LoWPAN Header Compression* was introduced, not only to make L3 packets fit into the frame, but also to speed up the delivery of the frames with this poor bitrate.

## 6TiSCH Network Structure

![](img/struct.png)

The picture shows a generic 6TiSCH network structure, in which there are various LLNs, with their lossy behaviour and the usual Internet cloud, based on IPv6. 

Between these two networks there is a peculiar intermediary: the **6LBR, which stands for IPv6-LLN Border Router**. These devices have to perform many different operations, so they need to be properly equipped with enough computation power. 

These routers have to:

- Be an **interface between the Internet and the LLN** they refer to, thus it:
  - Needs to accept packets from one network and re-convert them in the format of the other network;
  - Needs to translate addresses from a network to another so that every device in an LLN is uniquely addressable from the Internet, even reduced-function devices which cannot comprehend IPv6.
- Be **time source neighbours** for the nodes in the LLN:
  - All nodes in the LLN needs to be synchronized because TSCH uses a deterministic way to let nodes communicate based on time slots;
  - The access to communication channels through time slots is scheduled by 6top in the LLC layer.
- Be centralized entities for LLN **management and routing**:
  - Routing towards an LLN is always done by the router by using RPL and, for this reason, it will always be the root of paths since routing is done by creating a tree

Talking about **LLNs, they are mesh networks**. Differently from LANs, where nodes could broadcast and be connected to each other, here there are devices with such low range that they cannot be always in the same signal range. This leads eventually to multi-hop path to reach a destination even within a LLN.

## Time-Slotted Channel Hopping (TSCH)

Before TDM and FDM were introduced as possible candidates for the MAC/PHY layer in a deterministic network. **TSCH adopts both of them, by splitting the network in different channels**, which can be accessed in certain time slots.

Obviously to use this time synchronization is needed amongst all nodes, with a granularity of microseconds or milliseconds, depending on the application, but this is not a big deal since it was said before that 6LBR routers can be used as a common time source for every node in the LLN.

Essentially, TSCH **combines the advantages of FDM with channel hopping (reliability and robustness) with TDM ones (predictable delay and reduced energy consumption)**. 

The main frequency used by TSCH is 2.4GHz which is then split by FDM into 16 sub-channels: a graph of sub-channels and time slot can appear as the following figure, in a similar fashion to Slotted Aloha.

![](img/tsch.png)

The matrix in the picture is called **Channel Distribution and Usage Matrix (CDU)**. The 6top protocol in the LLC layer will manage how this matrix will be filled, effectively deciding which node can communicate with another at a certain timeslot. In the example this consists in: 

- Channel 2 at time 1, only B can transmit a frame to D;
- In channel 2 at time 2, D can transmit a frame to B (replying to him probably);
- In channel 0 at time 1, A can transmit a frame to C;
- and so on...

All of these decisions come from a **reservation made from a node that wants to transmit data**, so B for example expressed the need to communicate to D. Remind that these timeslots are long enough to let a frame of maximum size be transmitted in its entirety.

The shared slot tells the nodes that anyone can communicate in that timeslot without the need of a reservation. This timeslot works similarly to slotted aloha, so it's possible that some collisions happen in this shared slot, with a probability of 26.4%.

With TDM **the node turns on only when it has to send a message or when it might receive one**. In the picture example, node C is always off except when he know that he has to transmit to A or receive from him (blue timeslots of the picture).

This time determinism let's us pinpoint the exact timeslot in which a node needs to be on and when it can be kept off, effectively extending the nodes lifetime to potentially more than a year. With CSMA/CA, instead, the node is on all the time and probably wastes a lot of energy while waiting for a free spot in the channel to communicate, with a lifetime expectancy of just one week!

## TSCH: Channel Structure

The matrix previously seen is called Channel Distribution and Usage matrix, and the main problem to face here is "how long this matrix should be?", since every timeslot has to be mapped and time goes on forever. 

First of all, let's define precisely what is a timeslot: **1 timeslot equals the amount of time needed to transmit a maximum size frame plus the time of a quick ACK transmission from the receiver**. This time typically amounts to 10ms.

A **scheduled cell is a timeslot reserved for a node X to transmit to a node Y**.

A **shared cell is, instead, an unreserved and shared timeslot**, in which nodes enter in a contention to get access to it (is prone to collision)

A **bundle is a set of all cells scheduled between a node X and Y to communicate in full-duplex** (X <----> Y), effectively creating a virtual channel, an abstraction of an IPv6 channel with a certain bandwidth, between the two nodes.

**A bunch of timeslots are organized in a slotframe for each channel**. The slotframe has variable length, depending on how many nodes need to communicate, which means how many scheduled cells are needed. At the end of the slotframe, it is repeated cyclically.

To get a clearer idea, let's look at that picture again for reference:

![](img/tsch.png)

The blue cells are scheduled cells, while the red ones are the shared cells.

The slotframe of this CDU matrix is long 4 timeslots and then repeats For this reason there is a relative timeslot number (TSN) which is cyclical and is derived from the absolute slot number (perpetually increasing ASN) modulo the slotframe length.

The scheduled cell `B->D` and the next one `D->B` form a bundle of cell.

Nodes must be synchronized frequently on timeslots and for this reason the 6LBR routers often sends time information through-out the network. The amount of de-synchronization tolerated must be less than 100 micro-seconds of delay.

The time information is also piggybacked on either frame or ACK, if there if some space available, to maintain synchronization. If it's not possible, the nodes will send a periodic (every 30 or so seconds) control frame if there is no traffic in that timeslot to keep the devices as synchronized as possible and avoid any kind of clock drift.

## Timeslot with Acknowledged Transmission

Let's see in more detail how a transmitter and a receiver node behave when the current timeslot contains their scheduled cell.

In reality, timeslot duration also has to include data processing (also for security), ACK generation/transmission and reception, radio switch between transmit and listen mode and some various offsets.

![](img/tslot.png)

Transmitter node:

1. The transmitter node wait for a certain amount of time (TsCCAOffset) before turning on and checking with CCA if the channel is clear to use at T1. It also waits for a certain offset before transmitting (TsTxOffset). This is done as a tolerance measure to avoid signal overlapping if the previous nodes are still finishing to transmit in overtime.
2. Once everything should be clear the node turns on again and begins the transmission of the frame at T2.
3. Once transmitted, he waits for a certain amount of time, preparing to receive the ACK (TsRxAckDelay).
4. Turns on and waits for the ACK to be transmitted. Note that the transmitter node expects to start receiving the frame between time T3 and T4, so it turns on in that period of time. If the receiver fails to send the ACK in that amount of time, the transmitter node goes back off to avoid wasting battery.

Receiver node:

1. The receiver prepares to receive while waiting for an offset (TsRxOffset) for the same reason the transmitter node waited for. 
2. The receiver turns on, waiting for the frame to be transmitted. Note that the receiver expects to start receiving the frame between time R1 and R2, so it turns on in that period of time. If the transmitter node fails to send the frame in that amount of time, the receiver goes back off to avoid wasting battery.
3. If the reception of the frame was successful, the receiver processes the frame and prepares the ACK in a certain amount of time (TsTxAckDelay).
4. Turns on and sends the ACK to the transmitter node at R3.

At the end, there is more free space time to further avoid timeslots overlapping because of desynchronization.

## Timeslot Assignment: Problems

When 6top needs to schedule the timeslots in which nodes communicate he must be careful that:

- Two neighbour nodes cannot transmit in the same timeslot on the same frequency, because they would hear and interfere with each other
- Two nodes with a neighbour in common cannot transmit in the same timeslot on the same frequency, because they would be heard by the common neighbour and interfere with his eventual transmissions (***hidden station problem***)

All of this is to **ensure that there are no collisions**, thus frame loss. To solve these problems it's possible to just allocate them on different frequencies, if time is the critical matter.

![](img/assp.png)

All of this is **reduced to a mere graph coloring problem**, in which each color represents a frequency assigned for a certain node. Since the goal is to re-use all the possible channels in far away nodes, the minimization of colors used for graph coloring is wanted in order to maximize channel parallelism, utilization and minimize end-to-end delay because destination doesn't need to wait more for the channel to be freed.

## TSCH: Scheduled Timeslot Processing

Let's see the complete behaviour of a node when it's his timeslot:

<img src="img/slpr.png" style="zoom:65%;" />

1. Node awakens, since in CDU matrix it's scheduled that he should send or receive a frame;
2. Check if the link assigned by the slot is Tx (transmission) or Rx(reception). There are three cases:
   - If the link is Tx, then node goes in Transmission behaviour;
   - If the link is Rx, then node goes in Reception behaviour;
   - If neither, then node goes into the End behaviour.

Let's see the Tx behaviour:

1. Node needed to transmit something, so he proceeds to do it and check afterwards if the transmission required was unicast:
   - If it was then he waits for the reception of an ACK or a NACK, in order to tell if the frame arrived successfully to destination;
   - If, instead, it was a broadcast transmission, he doesn't expect for any ACK. This is done to avoid causing an ACK implosion, collapsing the network communication capabilities.
2. If an ACK was received or the broadcast communication happened successfully, the frame is removed from the MAC queue, since the job was done. In every other case, something went wrong:
   - If I received a NACK, the sent frame was corrupted or non-valid, so it must be resent;
   - If I didn't receive a NACK, probably an interference destroyed it, so no one knows what happened to it. The frame must still be resent.
3. After this, the node goes in End Behaviour.

Let's inspect now the Rx behaviour:

1. Node needed to receive something, so he waits for the packet for a short amount of time. Different things can go wrong:
   - If he didn't receive anything, thus the buffer is empty, it goes to End Behaviour;
   - If he received a non-valid or corrupt frame, send a NACK back to the sender.
2. If instead the node received the packet correctly, it's going to check the transmission mode: 
   - If it was an unicast communication, it needs to send back an ACK to the sender saying that everything is ok;
   - If it was a broadcast communication, don't send any ACK, since it could cause an ACK implosion if all the other nodes who received it did the same.
3. After this, the node proceeds to process the received packet and goes in End Behaviour.

In the End Behaviour, the node processes all link statistics during the usage of the timeslot. The LLC protocol (6top) will use them to adapt and fine-tune the schedule according to this data. After this, he'll return to sleep.

## TSCH: Management of Shared Cells

In the following figure the behaviour of the nodes when communicating in a shared cell is inspected.

![](img/slsh.png)

It works kinda similarly to CSMA/CA and Slotted Aloha, with collision detection provided by ACK failure:

1. The node check if the devices has a frame to transmit and, if it does, checks for the `backOffCounter` value:
   - If it's not zero, it means that there is still to wait more because of the Binary Exponential Back-off, so the counter is just decremented and the node gives up for this shared timeslot;
   - If it's zero, it means it can proceed to the next step since there's no need to wait.
2. Its important to check if the channel is really free through CCA, so the node listens to the channel and if detects any activity on it (energy on link) then it deduces that it's busy:
   - If the channel is really busy, the node cannot transmit so it gives up for this shared timeslot;
   - If the channel is clear then it can transmit the frame. 
3. Now check if a collision happened: did the node receive the ACK from its recipient?
   - If the transmission was successful, then the `backOffExponent` is set to 0 and the operation is concluded.
   - If not, then the `backOffExponent` is incremented by 1, since some node may have communicated at the same time and destroyed the frame through a collision, so the wait time for the back-off is extended.
4. At some point, since an exponent is being increased, the wait will be too long, so a `maxBackoffExponent` must be provided so that if the exponent reaches this peak, the node should just give up trying to communicate on the shared channel since that's a proper channel failure. So:
   - If `maxBackoffExponent` was not reached, then the node sets a back-off counter in the interval `[1, 2^backOffExponent]` and will retry in another shared timeslot;
   - If instead the max was reached, then the node should really give up for channel failure, but before doing so, resets to 0 the `backOffExponent`.

## Synchronization and Channel Hopping

Whenever a new device enters the LLN, the 6LBR must handle this issue by giving all the needed informations to the node to let him able to communicate in the network. For starters, **the node must be synchronized and receive a copy of the CDU matrix** in order to be aware in which timeslots he can communicate.

Before the Absolute Slot Number (ASN) was introduced, which is a 40 bits number initialized to 0 whenever a network is created and is incremented at each timeslot.

![](img/tsch.png)

As the TSN resets at each slotframe, the ASN keeps going on. This is the measure used to synchronize the nodes on the same timeslot.

Every node can compute the **Absolute Slot Number as `ASN = k * S + t`** where:

- `k` represents the **number of slotframe repetitions** since network creation (how many slotframes have passed);
- `S` Represents the **slotframe period** (in the picture 4, since every 4 timeslots the TSN restarts);
- `t` represents the **slot offset**, which is the position of the current timeslot in the slotframe (which is the TSN of that timeslot.)

Whenever a new node enters the network, he gets to know by the neighbours or by the 6LBR router the current ASN.

The ASN is also very important to determine at which frequency the node should transmit. Since each channel contains a wide range of frequencies, different ones for each channel are used at each slotframe to increase collision avoidance.

The **transmission frequency is thus calculated as `F((ASN + channelOffset) mod nFreq)`**  where:

- `channelOffset` represents the **number of the sub-channel** for considered slotframe (in example 0 to 3);
- `nFreq` represent the **number of available frequencies** for use, thus the total number of channels (in example we have 4 channels);
- `F` is a **function that translates the input** into an entry of a frequencies lookup table.

At every iteration, ASN changes, so too will the frequency used for transmission, even if the channel used is the same (it will be a frequency value in between the [minFreq, maxFreq) range for that sub-channel).

This method is also used for **beacons** in shared slots. If some devices are listening only to certain frequencies, the fact that every time is used a different one helps those limited nodes to find the network through these beacons and join it.

## Slotframe Formation: Examples & Problems

The 6top layer has to schedule the timeslots in the most efficient way in order to have optimized and collision free slotframe formations. Considering the neighbour and hidden station problems, what is the best method to organize them?

1. One timeslot for each node -> scales as O(n):

   - If a node needs to send messages to k > 1 neighbors, it has to wait k different slotframes, because he only gets one timeslot per slotframe for transmission;
   - This is good for energy saving because the node knows exactly when waking up to transmit, however it increases greatly communication latency, since it could happen that a message is sent after several slotframes, and this is not ideal for critical IoT and Industry 4.0 applications.

2. One timeslot for each "color" -> scales with the network topology, based on the maximum number of colors needed (max node degree):

   - This could lead to a better optimization of the slotframe since it tries to maximize the bandwidth utilization, reducing the overall latency;

   - However, this can be a problem for "central" thus highly connected nodes, which in the worst case need to be always on (they have to accept slots from each neighbour of different color). This inevitably leads to a reduced battery lifetime for those nodes.

3. One chance to transmit to each one of its neighbours for each node:

   - Let's suppose to "color" the nodes basing on how many neighbours they have: for example, if "red" nodes represent the nodes with maximum 3 neighbours, then at least 3 red slots should be put in the slotframe so that the red node has enough chances to communicate with all three neighbours if needed;
   - It greatly reduces the latency, especially comparing it to method 1 and consumes less energy per node than method 2, so it could be the best approach for now.

These are only few of all the proposals made on how to face the slotframe formation challenge. It is an hard problem thus research has yet to identify the best solution.

## Scheduling for Multi-hop Paths

We said LLNs often involve devices with a really short range and, consequentially, are mesh-like networks, meaning that **a node that want to communicate with another far from him need to find a multi-hop path**. So the question is: how can we schedule the slotframe in a way that minimizes the number of slotframes needed to complete the multi-hop path?

![](img/mhsl.png)

Let's suppose that the yellow node in the right corner of the image wants to transmit a message to the circled red not in the upper left corner.

To reach it, 4 transmissions are needed so how to sort the timeslot colors in the slotframe to minimize the number of slotframes needed to reach the destination?

The length of the slotframe should be 4, since we need only 4 transmissions to reach the destination, but how the colors should be arranged for the best result?

![](img/sl1.png)

In this ideal case, it's only needed one slotframe to travel down all the path from source to destination, since colors are in the same order as the nodes in the path.

![](img/sl2.png)

However the slotframe isn't always organized as the ideal structure, in fact in this figure there is the worst case scenario, in which at least 4 slotframes have to be used because the color order is reversed compared to the order of the nodes in the path. 

Optimizing the slotframe structure for all paths in the graph is a combinatorial problem and thus really hard to find a proper solution to it.

## 6top: Functionalities

**6top is the LLC layer protocol for our 6TiSCH architecture**. Because of the problems mentioned it doesn't have much standardized as research is still going on. However it's main functionalities he should provide are:

- **Network formation**
  - 6top can be used by 6LBR routers or full-function nodes to advertise the network to other devices through periodical beacons, aiding them in the joining procedure (giving them informations about the CDU matrix, the ASN and so on...);
  - 6top can be used to choose a proper time source neighbour for each joined node (usually 6LBR nodes since they're connected to the internet);
  - 6top can be used to implement some kind of security, by using authentication and key distribution mechanism for data encryption and decryption.
- **Centralized path computation** (used as an alternative to RPL), executed by 6LBR routers;
- **Scheduling computation and adaptation**
  - The CDU matrix should be computed and adapted through time by considering the link statistics. Ideally more slots should be provided to nodes that need to transmit more, and less to more sleepy ones to keep performances at maximum;
  - Since this is an hard problem, for now the mechanism used is the shared one for each timeslot (similar to Slotted Aloha).
- **Queue management**
  - 6top should also organized the queues of messages, the ideal approach is to use a FIFO queue for each destination instead of a FIFO for all transmissions.
- **Performance monitoring**
  - 6top should perform some monitoring and data analysis on the network performance in order to adapt the scheduling.

## Discussion

Ideally, if by using only scheduled cells, its possible to eliminate all possible collisions (except the ones caused by external interference sources), guaranteeing even lower latencies, however if no frame for the current timeslot are available (but frames to other neighbor nodes) means that there is an unused timeslot, and that's a waste!

- A frame waiting for an appropriate slot experiments increased latency;
- Queues grows quickly with possible overflow and frames dropped.

Documentation suggests to minimize the number of scheduled cells, meaning that their use is quite exclusive and shared cells are preferred.

6top channel scheduling mechanism are of paramount relevance.