# Message Passing

In this lecture we'll see how new communication paradigms were created to face the many adversities of the challenged networks we saw last time.

## Synchronous Communication

In Internet there's a synchronous communication, in which there's the handshake model as a way to **make sure the receiver is ready** to receive data, called **rendezvous protocol**. 

There is even a **strict rendezvous** protocol in which the sender remains blocked until the receiver lets him know that he received all the data. 

This is very common in typical request-response models, such as HTTP client-server, or in other synchronous models, such as RPC, sockets and so on.

In other words, I know to whom to talk to (the receiver exists) and I'm sure that, after the handshake, the receiver is ready to get my sent data.

## Asynchronous Communication

Usually, a synchronous communication implies a one-to-one, reliable and secure communication channel. However, this synchronization is not viable in our challenged networks for many reasons:

- Need **one-to-many** communication, because, generally, sensors wants to communicate with all the nearby sensors in order to pass the data around, or if a broadcast message is sent to an array of sensors;
- Need **mobility**, thus the ability to **partition the network** and to reunite it at any time. This is because mobile sensors can lose momentarily connection for several reasons and cut the rest of the network for an unpredictable amount of time before reappearing;
- We need not an address-centric communication but a **data-centric communication**, don't want to search for a specific sensor X but all sensors in an area (like all the sensors that feel hot temperature and so on...).

The possible solution to face these needs is an **asynchronous message passing communication**:

- Have a middleware that handles the message exchange;
- It will handle distributed queues and buffers of messages all over the network, waiting for favorable network condition;
- Whenever there are favorable conditions, such that the receivers of the messages in the buffers are reachable, the middleware sends the stored messages to them.

In other words, the receiver will answer whenever he can, given the network conditions.

### Middleware Magic

Let's look at what happens when writing in a URL bar something like "google.com": 

- There's a DNS, that converts the name into an IP;
- There's a middleware that, given our location, connects us to the closest server and with the same language from our country;
- And so on...

All this middleware makes the service smarter, since it's customized for the user. For this reason, today’s applications are typically written in terms of **what data users want rather than where it is located**, then application-specific middleware is used to map between the application model and the Internet’s.

Following these trails, this technology can be adapted to better suit our challenged network needs for a data-centric communication.

## CCN / ICN / NDN

These protocols follow the same paradigm which **replace where (IP address of a node) with what (content the node provides)**. Essentially, we have a **Content-Centric Networking** (CCN)

The main idea is that a node that want informations from the rest of network:

- Knows **what** kind of information wants;
- Doesn't know **where** is it located;
- Doesn't know **which** other node holds it. 

For this reason, he has to **broadcast to all the reachable nodes** that he's in search for that content with some name, but how do the other nodes know that they hold the information that is needed by him? We need a **unambiguous naming system**.

Each time a source node is in search for content:

- It broadcasts its Interest for a content name;
- A data item fits an Interest if the content name in the Interest is a **prefix of the data item name** 
- One Interest retrieves one data item. It is a unicast system, so the only data item returned from the Interest query is the one with the longest common prefix;
- A certain data item **may satisfy many Interests**.

With this naming system CCN nodes maintain a sort of **"routing tables" leading to content** rather than to addresses. 

The Interest could use some keywords in his prefix to obtain certain data items, however it's not 100% precise.

![](img/ccnodes.png)

To optimize the transmission of the data item, each intermediate node that receives an Interest from a source node will **memorize who asked for it in a table and then will forward the request** Interest to its neighbours. This is done because,  when that intermediate node receives the corresponding data item, he has to remember which node asked for it in order to send it to him. 

However, there are another benefits from this: if any other node requests the same Interest to that intermediate node and he's still waiting for an answer, he will not repeat the forwarding, **avoiding redundant traffic**. 

Whenever the intermediate node receives the answer, he will forward it to all the requesters of that Interest, acting as a proxy. He will also remember the node from which he received the answer in order to satisfy future equal requests faster.

Let's see how a CCN node works internally:

![](img/ccn.png)

- **Faces**, are a different name to identify the **network interfaces** to which the node is connected:

  - Face 0 - An antenna (WiFi, 5G, Bluetooth, ...) through which the node can communicate;
  - Face 1 - An interface connected to the global web (Internet);
  - Face 2 - An interface connected with the application.

  Faces makes these nodes Interesting because they have multiple functions:

  - They offer services to clients through the connection with the application;

  - They act as routers in the network, forwarding data through other nodes and collaborating with them.

- **The Content Store**, a table that stores all the information produced by the node itself, and if it's a sensor, it will **hold here the data items**.

  This data structure also caches all the data items that passed through here when they were requested by other nodes: this way it acts as a proxy, avoiding asking again all the way to the destination node.

  It can be considered as a cache for content sharing with all the other nodes, following a LRU (Least Recently Used) or LFU (Least Frequently used) policy to decide which contents to keep and which to discard.

- **The PIT (Pending Interest Table)**, a table that stores all the pending **requests for Interests** that has been received with the Faces who requested it (Requesting Faces).

- **The FIB (Forwarding Information Base)**, a table which, similar to a traditional routing table, contains known prefixes to data items and the Faces on which request them.

### CCN Example

This is what happens when a CCN node receives an Interest request:

1. It will check if the data item corresponding to that Interest is cached in the content store;

2. If it is, then it sends it to the requester, otherwise, it will check if there are already pending request for that Interest in the PIT table;

3. If it is, it will wait for an answer, otherwise, it will make a request to its neighbours, but in both cases, he will remember the requesting Face who asked for it;

4. If the node needs to make a request to its neighbours, then it will rely on the FIB table:

   1. When looking at the prefixes, the node will chose the line on the table with the longest common prefix with the request Interest;

   2. From the corresponding Face list, it will choose a single Face (since it's unicast) to which forward the request following a certain policy (round-robin, random, ...). 

      These policies are used because all possible Faces in the list are worthy, especially in our challenged networks, because a node could get an answer from one Face while the others are unreachable!

## Publish-Subscribe Paradigm

The publish-subscribe paradigm is extremely useful in our challenged networks, it consists in a "detachment of communicating entities, in time, space and synchronization". 

![](img/pubsub.png)

This paradigm has three main entities:

- **Publisher** - An entity that **publishes tagged content** with a certain Interest in dedicated "channel";
- **Subscriber** - An entity that has one or more Interests and, for that reason, is subscribed to all the information flowing in the related channels;
- **Broker** - An intermediary entity between the publisher and the subscriber that:
  - Receives content from the publishers, sorting the content in various channels, based on their tag;
  - Forwards content to the subscribers, based on which channels and content they are Interested and subscribed to.

Challenged networks are usually heterogeneous, made of nodes with different functionalities, and because of this it is possible to consider the information gatherers as *publishers* while nodes who require those informations are *subscribers*.

Even better, the whole network can be considered as the broker, since we saw in CCN how each node can be both publisher and subscriber, so there is a **distributed broker** instead of a centralized one (but it can be used in both ways.

The publish-subscribe paradigm is a communication model in which there's a **complete detachment** from the communicating nodes in three ways:

- **Spatial**, where communicating entities don't know how many entities there are or their identities:

  - Publishers don't know how many or which are the subscribers;
  - Subscribers don't know many or which are the sources of their Interests;

  Managing the network becomes easy, because there is no need to handle disconnection problems generated by a node becoming unreachable (a common event in challenged network).

- **Temporal**, publishers and subscribers don't care if the other end is connected or not, they just need the broker, which is every other node in the network, since it is distributed:

  - Publishers sends content to the broker;
  - Subscribers gets new content by the broker; 

  This is also good to contrast the fragility of our kind of network.

- **Synchronization**, because a publisher doesn't wait for the subscriber to consume its content, it can continue its production indefinitely.

  This property ensures that in a challenged network there isn't any soft lock caused by producers waiting indefinitely for a consumer to get the content because the latter is unreachable.

The distributed broker grants "memory" to the network, which is an especially good property for our needs.

## Topic-Based Publish-Subscribe

This model is based on **channels tagged by a topic and an appropriate keyword**.

A publisher can choose a channel in which published content is relative to a topic and a consumer can subscribe to it if their Interest match that topic.

Topics are usually organized following a **taxonomy** in which there's a base general topic and all of its descendants will be more and more specialized. This way, when a consumer subscribes to a channel (with a certain topic), he will automatically subscribe to all of its sub-channels (with their sub-topics).

Let's see an IoT pseudo-code example of this system, it consists in:

- A temperature sensor as a publisher, producing content on the channel with topic "heating";
- Actuator as a subscriber, subscribed to channels with topic "heating" in order to activate the central heating system to heat up the house when it's too cold

```java
// Create the subscriber object for the actuator
Subscriber actuator = new HeatingSubscriber();

// Subscribe it to the channel with "heating" topic and all his subchannels 
EventService.subscribe(actuator, "heating");

// Heating subscriber class will implement subscriber as interface 
public class HeatingSubscriber implements Subscriber 
{
    // when receiving an object O from channel "heating" 
	public void notify (Object o) 
    {
        ...
        // if its temperature field id less than 19 degrees, turn up the heat
 		if(o.temperature < 19°C)
			heating_ON();
 	}
}
```

### Problems

The problems in a topic-based P/S reside in the definition of the taxonomy: since the topics are predefined, this means

- We have to define which data is going to be measured and published by the sensors through the broker
- We have to define which data is Interesting for actuators and other subscribers in the environment and their structure, in order for them to understand how to read and work with it
- We have to organize a taxonomy, which gives rise to rigidity in the structure and a lack of choice in the topics for the subscribers (since we have predefined channels). 
  - A subscriber, for example, Interested only in the wind speed has to subscribe for meteorological topic if the granularity is coarse, having him receive lots of useless and unwanted information (temperature, pressure, ...)
  - Having a coarse granularity in topics is even worse in challenged networks, because we are wasting bandwidth, battery power and occupying the network for useless information! 
  - Having a fine granularity is a bit better but the subscriber maybe needs to subscribe to a lot of different topics and consuming a lot of resources to receive them all

## Content-Based Publish-Subscriber

This model doesn't focus on channels but on a **set of properties of the content** they receive, like meta-data or tags. 

In order to decide if a content satisfies the requested properties, each node defines a filter, creating a subscription pattern to filter and identify interesting content.

Each node in our network can choose to **subscribe on data that has certain properties and characteristics**, and since each node can be a broker itself, he analyzes each content he comes across: if it contains properties that matches his neighbours Interests, then he forwards it to them.

Following the same IoT example:

```java
// Creating a filter predicate that checks that, if data contains a
// temperature field, it should be less than 19 degrees to be interesting
String criteria = (temperature < 19°C)

// Create the subscriber object for the actuator and sub it
Subscriber actuator = new HeatingSubscriber();
EventService.subscribe(actuator, criteria);

public class HeatingSubscriber implements Subscriber 
{
    // If something is received, then it surely matches the filter
    // let's turn up the heat then
	public void notify (Object o) 
    {
		heating_ON();
	}
}
```

### Problems

There is surely more freedom of choice than channels, since: 

- Channels will be deprecated in the long run (Interests can change through time); 
- Personalized filters can be defined.

However, once the filter is defined in a sensor, it's difficult to change it and redefine it: for specialized sensors, though, it isn't really an issue.

## Type-Based Publish-Subscriber

This model is extremely linked to its implementation: this, because **each node filters content according to its type**, and therefore its structure. 

Following the same IoT example, each node checks if the content received is of type `HeatingSensor` or `User` (literally of that class). If it is, then he consumes it and adjusts the temperature accordingly. This way I can save a lot of power, since **when content is not of the required type I don't have to do any computation**.

```java
// this class defines the type of the content coming from temperature sensors, which
// are the content publishers
public class HeatingSensor { public float temperature; }

// this class represents a user device that defines a custom temperature range
public class User { public float minT, maxT; }

// this subscriber only gets notified when it receives content from HeatingSensor publishers
// content from User publishers is ignored (maybe because we want our actuators to only)
public class HeatingSubscriber implements Subscriber<HeatingSensor> 
{
    public void notify (HeatingSensor o) {
        if(o.temperature < u.minT)
            heating_ON();
    } 
}
```

### Problems

This model is pretty strict in choices since the types are defined in the implementation, so I can abstract less than content-based model. For this reason, this model is less flexible.

## Paradigm Comparison

The publish-subscribe model should serve as middleware for the communications in our challenged network, using a content-based model is usually the best approach, since we have a high level of abstraction (needed in the middleware). In the application, it is often required a more fine-grained detail level about the filters used by sensors. 

The other two models limit freedom of movement already in the middleware level, so they work best only in particular scenarios.

- **Topic-Based** has a fixed taxonomy, excess of unwanted traffic (coarse) or computation complexity (fine).
- In the example: All temperature information come through, even if they have value > threshold or come from user devices (inefficient).
  
- **Content-Based** is more abstract and flexible, freedom of choosing filters and predicates on high level (more granularity without expenses).
  - In the example: Only temperature informations that match with filter come through (efficient in bandwidth and power).

- **Type-based** is a fixed implementation, a mix between topic and content, can help saving some power.
- In the example: Almost all temperature information come through, even if they have value > threshold, since I can limit user devices (slightly inefficient)


These networks are completely different from Internet ones, and for this reason, sources and receivers can be multiple, unknown and, at times, unreachable. We need a resilient model in which we can obtain the information through Interests and contents, not through an address.