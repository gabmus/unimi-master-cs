# Wireless Sensor Networks

## Sensors

A **sensor** is a device able to detect signals from the physical world that surrounds it, quantifying and digitalizing them. 

![](img/sensor.png)

They usually are equipped with:

- An **embedded OS**, optimizing the sensor management to minimize the energy spent for its activities;
- A **micro-controller**, to elaborate the signals received and digitalized by the ADC:
- A **transceiver**, to communicate with a base station or other sensors;
- An **ADC**, or analog-to-digital converter. Since all signals from physical world are analog, a conversion to digital is needed to transform them into data, making their elaboration and transmission possible;
- An **external memory**, to store the elaborated data for later use or transmission;
- The **effective sensor parts**, which are the specialized devices that gather information from the surroundings. There are different kinds of them for different physical dimensions:
  - **Passive omnidirectional**, gathering data passively without a particular sensing direction (thermometers, microphones, hygrometers, ...);
  - **Passive unidirectional**, gathering data passively only towards a sensing direction (cameras, directional microphones, ...);
  - **Active**, acting on the surrounding environment in order to gather information from it (like sonars and radars, which sends waves and wait for them to bounce back).

## Other Devices 

Sensors are usually paired with other devices which enrich their sensing capabilities. Others can react to the gathered data, others handle and reconfigure sensors purpose and so on... 

Let's see them in detail:

- **Actuators**
  
  - These devices are capable to **alter the environment** in which it is immersed;
  
  - Being paired and coordinated with a sensor is usually their best use, since they can react to the information gathered by it; 
  
  - *Example*: An actuator could activate the heating system because the temperature sensor detected that the house was too cold.
  
    Water sprinklers paired with hygrometers sensors to check terrain humidity, smart light switches with light sensors. 
- **Sinks**
  
  - These devices are **collection points** of the information gathered by the sensors;
  - There can be more of them, specialized in different types of data or not;
  - They're usually connected to the both an electricity grid and Internet;
  - Two kinds of sink:
    - *Active sinks*: they can elaborate data received directly and take a decision by sending a signal to the coordinator; 
    - *Passive sinks*: they only collect information from sensors and delegate the elaboration to a remote node.
- **Coordinators**
  
  - These devices can **reconfigure the sensors' behaviour** and **activate actuators** depending on the elaborated data; 
  - *Example*: if the sink receives a lot of data from smoke sensors, the coordinator can activate sprinklers' actuators to deter a possible fire in the building.

## WSN: Features

Wireless sensor networks are characterized by small devices which can be:

- Fixed in place or mobile;
- Powered by electricity or by battery (or photo-voltaic panels);
- Distributed in the environment we need to monitor, which means that they could be hardly reachable, so that repairs or substitutions are not possible;
- Equipped with a wireless interface for communication. This is an heterogeneous network, each device has different transmitting power, different battery capacity and so on...

Under this light, we must tailor our network behaviour to these challenges.

## Multi-Path Fading

Another issues in a wireless network is the multi-path fading. In this picture a wireless source extends its waves through the rooms of this house: many of them bounce off the walls in other directions, taking, effectively, different paths. 

![](img/multipath.png)

A device that wants to communicate back:

- If there is line of sight, there shouldn't be any problem;
- Otherwise, he has to rely on reflections in order to make the message arrive to the wireless source.

As it's easy to imagine, all these reflections and waves bouncing off can create **many interferences**, at the point of making the signal distorted and the communication hardly achievable.

## Requirements and Goals

Unlike in the TCP/IP stack, where the underlying layers are used as a black-box for services, here we have to manage and design the full stack of our network (cross layer design). The layers aren't distinguishable and we're forced to handle simultaneously the hardware, the protocols and the software.

Keeping that in mind, our WSN should be able to guarantee:

- **Monitoring accuracy**, corresponding to the need to guarantee that a network of sensors covers the most of the interesting territory, which is where *events* occur with higher probability.

  Events are data pertaining to critical moments, for example when temperature goes lower than the programmed threshold.

- **Self-organization and autonomy**, making sure that the devices in the WSN operate correctly, regardless of any environmental condition, and program them accordingly, so nodes must:

  - Coordinate to offer the service;
  - Be flexible and re-configure themselves in the event of mutations, without human intervention.

- **Energy saving**, by programing devices in a way that they can automatically balance the energy consumption, switching between the idle, send or receive mode. 

  For this reason, it's important to balance how much energy each mode should consume, for example, if the communication modes consume more battery, they can communicate for shorter amount of times but with an higher probability that they're going to be heard by other devices.

  It varies from device to device because each can have different capabilities, some are more apt to transmit, others to receive, others to elaborate. Build a network accordingly to specific needs.

  Waking up a sensor from idle mode is costly, because there's an **energy spike** required in order to turn on all its components before going to the normal wake consumption. Designers should consider this when designing when the sensors should sleep and when he should wake up, because if waking him up too often, he would spend more energy than just keeping it on normal mode.

- **Service with limited resources**, because devices in WSN have typically low computation power and poor memory. It's important, nonetheless, to achieve given goals with the limited resources we have.
  
- **Lifetime of sensors and network**. When developing the communication protocols for our network, designers must try to maximize the lifetime of the single sensors but also of the network as a whole, trying to find a balance between these two.

  For example, one could program some nodes to be selfish, which means they get activated only when they receive data they're interested into, minimizing as much as possible the energy consumed by it. However, this behaviour reduces the network topology, since these selfish sensors are excluded as intermediate nodes for messages to pass by. This can constrain some other nodes to transmit with more power at the expense of battery, reducing their lifetime.

  The best balance we can achieve is to program nodes to be less selfish: sometimes they will work as an intermediate node and sometimes not. This way, every node will progressively consume equally a bit of their lifetime by alternating their job, extending the whole network lifetime. This is preferable to having a few almost full nodes and the others half dead, thus having a disrupted network.

- **Scalability**. Networks will contain a lot of devices, there are sinks, sensors, actuators and such, the network could even reach to thousands of nodes! For this reason, it's important to optimize solutions in order to support high density sensor networks.
  
- **Reliability**, because the environment can be disruptive for our network: sensors are fragile and can be easily moved or damaged by dust, rain, heat or battery consumption.

  Even under harsh conditions, a designer must be able to preserve accuracy and lifetime by making the network reconfigure itself accordingly.

  In case of an irreparable error, instead of an abrupt shutdown of the network, one must guarantee a "graceful decay", in which the network performances drop gradually through time.

- **Consensus**, were warnings received from a single sensor are often not reliable.

  For example, a sensor could say that he detected a fire in the building while it was just the smoke coming from a cigarette. Consensus on data of many sensors in an area that they are detecting a fire is needed to be sure the threat is real and not a false alarm.

  For this reason, a consensus mechanisms is given to aggregate sensors' data, analyze it, draw a presumable conclusion and act accordingly (just like processes in a distributed system).

## Sensor Deployment

#### Planned

The effective deployment of the sensors plays a key role in the success of our WSN. A correct positioning of them can guarantee a good accuracy and a lower probability of network partitions because of nodes blacking out.

Coming across a fine-tuning and optimization problem: while positioning devices, we want to maximize the accuracy but also minimize costs, otherwise, we could want to maximize the reliability and connectivity, but that trades off with the network lifetime.

We need to choose which requisites are more important for our network, choose our constraints accordingly and maximize by those: this is what is needed by a planned position sensor deployment.

#### Unplanned

When working with smaller and smaller sensors, usually it's better (and easier) to just scatter the sensors randomly on the target area. However, this is not done totally randomly: firstly, we study where the events effectively happen with an higher probability in our area, plotting it in our **minimum detection probability** graph.

![](img/deploy.png)

Then, when scattering the sensors, we will put more in the areas with higher probability of events, in order to increase accuracy of the network. 

However, as we can see in the **generated detection probability**, some other sensors are thrown very far away from the red areas marked in the first graph: why? It is done to ensure alternative communication routes for the sensors to sinks and other nodes in the network, reducing the probability of a partition while increasing reliability and, possibly, lifetime of the network.

Of course, in the unplanned deployment, the auto-configuration is harder to achieve, since the sensors are dropped into the wild without any information of the surrounding nodes and have to do it on boot.

## Location-Awareness

Another important challenge we have to face is letting the **sensors know where they are** and communicate their position along with their data. This could be useful because data tagged with the location from which came makes information a lot more valuable. It is also important because if sensors are able to map their surroundings, they can understand which other sensors are closer to them, enhancing message routing.

The first mechanism that could instantly come in anyone mind is the GPS but, unfortunately, is not the best for two reasons:

- It is **useless indoors or in highly shielded environments**: buildings, the skyscrapers of the urban jungle as well as thick forests can shield all communications from satellites, making GPS positioning impracticable; 
- **Uses a lot of battery**: battery and sensors lifetime is already at risk by normal usage of the network, adding the GPS weight makes it unsustainable if devices are not under constant power supply.

For this reason, the triangulation system is usually preferred: since we work in heterogeneous networks, we can **deploy some sensors in some carefully planned positions** and their coordinates are hard-wired in them.

They communicate periodically, acting as reference points for the other sensors deployed in an unplanned way: each of these listen to the reference points and pings them, measuring how much it takes to message them and receiving a response (another method of measuring distance is the signal-to-noise ratio). 

This way, all sensors can have a rough map of what surrounds them and the relative distance from the reference points.

![](img/loc.png)

Since now we know roughly from where the data is coming from, we can enhance the accuracy of our network: we said that the data from a single sensor is less reliable than the combined data from all the sensors. Now, knowing which sensors are being activated on our "map", we can understand the magnitude of the event we're measuring. 

For example, if the sensors in the image are smoke detectors, we can understand immediately that there's probably a fire going on and that it is quite extended by now.

### Temporal-awareness and synchronization

As tagging data with their location is useful, tagging it with the time at which is was generated could be even more helpful: we can monitor variable events in time or the movement of an event from an area of sensors to another.

However it can be difficult for this feature to be implemented because we need that every single node in our network is synchronized in some way, otherwise the data information we will receive could be misleading.

## Naming and routing

When making a query to our network, we won't ask for a specific node, but for a specific content:

- "Let me know when temperature exceeds 40°C "
- "Which is the average humidity in the coordinate area [x<sub>1</sub>, y<sub>1</sub>, x<sub>2</sub>, y<sub>2</sub>]"
- "Sensors that detected animal presence go in report mode every 30 seconds instead of every 5 minutes"

Because of this routing can't be like in Internet, so we base our network off the data-centric routing we saw before.

### Communication range

How should we make our sensors communicate in our network? Using long hops or short intermediate hops? Power needed for communication increases highly with the distance, knowing positions could grant some power saving.

![](img/hops.png)

Let's suppose we have a sink (orange node) and several sensors (blue nodes): since we (humans) or other devices gather our informations from the sink, we want all sensors to send their data to it. They can do it in two ways:

- **Long hops** - Each sensor communicates directly with the sink. It's easy to understand that this is generally the worse way to communicate in our WSN, there are three problems:

  - Since we are talking about wireless communication, nodes far away from the sink **need a clear line-of-sight** to the sink to reach it, otherwise physical obstacles will render communications impossible;
  - Moreover, we have to consider that wireless transmissions are not linear like in the drawing: antennas transmits in a sphere of that radius. We can imagine that if two or more far sensors are trying to communicate simultaneously, their waves will suffer **collisions**, creating possible interferences and making the sink to be unable to understand the messages. If collisions are detected, we have to spend even more battery to re-transmit collided messages;
  - The **power needed to communicate grows** roughly with the square of the distance: this means that nodes far away from the sink will deplete battery faster and this is in contrast to our network lifetime principles.

- **Short hops** - Each node communicates with his neighbours, using them as a medium to deliver their data to the sink. It is supposed that nodes will be cooperative, but this method is better in any way:

  - There is a **better communication load distribution** between the sensors, because since each node communicates with only the closest neighbours, they almost all consume the same amount of battery for communication. We'll see soon in the problems why we said "almost";
  - Because of the better load balancing, we achieve a more **homogeneous lifetime** of sensors and network;
  - We can implement optimized **data aggregation** algorithms that makes the most of the bandwidth used.

  There are, however, some problems we should solve:

  - The sensor closest to the sink will be a single point of failure: since it's the only gateway to the sink he will be overwhelmed by communications back and forth, making it deplete its battery faster.
  - How can we construct the underlying tree structure we see in the image, especially in unplanned networks? Surely, a tree structure is needed to avoid looping paths which can make messages circulate indefinitely without reaching the destination. 

## Data Aggregation

To solve the first short hop problem and avoid data redundancy and sink implosion, we can think about **aggregating the events data**: for example, not all the temperature readings are necessary as we can express them in a more concise manner <average, variance>. 

Implementing this kind of **pre-processing** reduces communication length, since we are transmitting less data, with all the benefits coming with it: saving battery power, less possible collisions on the communication medium and so on.

However, we must teach the nodes how to aggregate effectively the data, depending on what it represents: in other words, we need to implement **application-awareness**.

We also must remind ourselves that there are no standardized or absolute solutions, so we must create our own way or choose an existing method to aggregate the data, fit for our WSN needs.

## Other Aspects

When developing a WSN there are some other aspects on which we won't focus on but are worth mentioning:

- **Database** - A database needs to be distributed, given the network structure and how fragile each node can be (especially sensors). It's not easy to manage it, since it's also continuously flooded with real-time data and there could be consistency problems.
- **Security** - Since we're working on a wireless connection, it's hard to guarantee security or privacy of nodes and data. While I could implement a consensus mechanism to avoid malicious data injection in the network, avoiding data to be sniffed is not practicable since data encryption is too computationally expensive.
- **Actuators** **and ferries** - We could mount sensors on mobile actuators (such as robots) to respond to an event: for example, if some sensors detect an event that occurred, they will move in the corresponding area to gather more accurate information. They could even move to facilitate communication, cover partitioned areas and so on.

## Micro-server Architecture

WSNs can be organized in a way to appear as a three-tiered architecture made by sensors, micro-servers and Internet.

![](img/hierarchy.png)

- **Sensors**: base of the pile. 

  They usually have little memory and computation resources, with a dedicated OS, they can have an internal hierarchy, in which weaker sensors refer to stronger sensors (in terms of energy supply, communication and computation power);

- **Micro-servers / Aggregators**: middle-ground.

  They have an higher resource cap than mere sensors (they're typically comparable to a tablet). Thanks to that, they're more apt to aggregate information coming from sensors and sending it over the Internet.

- **Internet-connected coordinators and sinks**: top of the pile.

  They receive information from the micro-servers and queries for data or changes sensors configurations according to data received

