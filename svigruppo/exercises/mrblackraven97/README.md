# Exercises

## Objective

Getting acquainted with Git commands and recreating with a script repo configurations.

You can use all Git commands plus:

- printf
- cp
- mkdir

## Usage of my scripts

In my scripts I've added some useful folder creation commands, to automate the testing of them.

In order to build the exercise repo (auto-contained in its folder) you can execute the relative script or you can build them all with the `createAll.sh` script. Remember to set them as executables with chmod +x !

