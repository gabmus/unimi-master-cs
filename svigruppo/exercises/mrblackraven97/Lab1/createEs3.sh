#!/bin/bash

# auxiliary setup: NOT TO INCLUDE IN THE EXAM
start="${0##*e}"
esName="${start%.*}"
mkdir -p $esName
cd $esName
rm -rf * .* 2> /dev/null

# start git script
git init
printf "text A\n" >> A
printf "text B\n" >> B
git add A
git add B
git commit -m "root"

git branch uno
git checkout uno
printf "text C\n" >> C
git add C
git commit -m "first"

git checkout master
mkdir D 
git mv A D
git mv B D
printf "text E\n" >> D/E
git add D/E
git commit -m "bis"

git merge uno -m "ooohhh"