# auxiliary setup: NOT TO INCLUDE IN THE EXAM
start="${0##*e}"
esName="${start%.*}"
mkdir -p $esName
cd $esName
rm -rf * .* 2> /dev/null

# start git script
git init
printf "text A\n" >> A
printf "text B\n" >> B
git add A
git add B
git commit -m "uno"

git branch student
git mv B C
git mv A D
git commit -m "due"

printf "text C\n" >> A
printf "text C\n" >> B
git add A
git add B
git mv C cc
mkdir C
git mv cc C/C
git mv D C
git commit -m "tre"

git checkout student


