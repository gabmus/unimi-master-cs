#!/bin/bash

# Initialize environment
rm -rf mySolution
mkdir mySolution
cd mySolution

# Start
git init
printf "uno" > U
printf "due" > Z
git add -A
git commit -m "start"

mkdir X
git mv Z X
git add -A
git commit -m "due"

git reset --hard HEAD^
mkdir X
git mv Z X
git mv U X
git add -A
git commit -m "nuovo"

# per generare "nuovo" il seguente codice è equivalente
#git reset --soft HEAD^
#git mv U X
#git add -A
#git commit -m "nuovo"

git tag "v1" HEAD
git checkout -b corretto
printf "tre" > O
git add -A
git commit -m "ultimo"

printf "due due" > X/Z
printf "uno" > B
printf "due" > A
printf "tre tre tre" > O

git add X/Z
git add B


