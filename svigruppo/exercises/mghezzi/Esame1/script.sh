#!/bin/bash

mkdir repo
cd repo

git init
printf "uno" > U
printf "due" > Z
git add U Z
git commit -m "start"

mkdir X
git mv Z X/Z
git commit -m "due"
git reset --hard HEAD^

mkdir X
git mv U X/U
git mv Z X/Z
git commit -m "nuovo"

git tag "v1"
git checkout -b corretto

printf "tre" > O
git add O
git commit -m "ultimo"

printf "uno" > B
printf "due due" > X/Z
git add B X/Z
printf "tre tre tre" > O
