#!/bin/bash

mkdir repo
cd repo

git init
printf "pluto\n" > X
printf "pippo\n" > Y
mkdir Z
cp Y Z/Y
git add -A
git commit -m "primo"

printf "pippo e minnie\n" > D
printf "pippo\n" > X
git rm Y
git add -A
git commit -m "successivo"

git tag "delivered"
git branch student

mkdir zz
git mv D zz
git mv X zz
git mv Z zz
git mv zz Z
git commit -m "ultimo?"

git checkout student
git rm X
mkdir zz
git mv D zz
git mv Z zz
git mv zz Z
printf "sbagliato\n" > A
git add A
git rm -f A
printf "Ultimo\n" > O

