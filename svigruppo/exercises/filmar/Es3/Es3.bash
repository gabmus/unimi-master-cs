#!/bin/bash

printf "esercizio 3 di svigruppo"

if [ -d es3git ]; then
    rm -rf es3git
fi

mkdir es3git
cd es3git
git init

printf "text B\n" > B
printf "text A\n" > A

git add *
git commit -m "root"

git branch uno
git checkout uno

printf "text C\n" > C

git add C 
git commit -m "first"

git checkout master

mkdir D
git mv A D/A
git mv B D/B
printf "text E\n" > D/E

git add *
git commit -m "bis"

git merge uno
