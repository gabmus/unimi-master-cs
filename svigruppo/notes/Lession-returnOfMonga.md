# dove siamo? #

- come ci si organizza
- come si gestiscono i manufatti
  - software configuration managament
  - management distribuito

## problemi comuni al management ##

> sebastiano vigna

la gestione del lavoro di gruppo genera molto lavoro non di programmazione che deve necessariamente fare il programmatore

> giorgio udino (blumber) verrà a gennaio per parlare delle tecniche agili che adoperano, antipatia per le porte
> blumber incontro 28 novembre 

## supporto tools ##

questa gestione del lavoro di gruppo sarebbe infattibile senza un insieme di tools (es: git, github, etc... sono esempi ma ne esistono molti)

### qualche esempio di tools, (non è importante il tools in se quanto il problema e l'approccio di soluzione ad esso) ###

primo fondamentale problema è il **dependency hell**, consiste nel fatto che un software è dipendente da molti software di terze parti che non necessariamente controlliamo direttamente, inoltre non tutte le dipendenze sono esplicite(es: ho importato una libreria).
Windows per esempio ha il suo approccio di sviluppo libero si vincola moltissimo a causa della retrocompatibilità(i giochi scaricano le dll, delle librerie personali del software, quindi nel pc capita di avere moltissime versioni della stessa dll).
L'approccio store centralizza la distribuzione standardizando le librerie.

 - versionamento semantico(il nome è fuorviante)
  - versioni MAJOR.MINOR.PATCH
  - cambiando MAJOR non garantisco totale compatibilità
  - cambiando MINOR cambio alcune funzioni mantenendo compatibilità 
  - cambiando PATCH non cambio nulla

**make**: programma che permette di standardizzare la compilazione delle varie dipendenze
> funziona esattamente come la canzone *per fare un tavolo ci vuole il legno..*

- per fare A : ci vuole B
  - avendo B, A si fa con il comando C

in questo modo si crea una relazione di ordine: 

B->A->...

make presuppone che l'ambiente di build sia statico che quasi mai l'ambiente è statico
make risolve con del codice adattivo:

- #if/#else
- substitution macros
- substitution functions

**configure** testo l'ambiente di sviluppo e produco un config.h che contiene le definizioni delle macro
permette anche di produrre automaticamente il makefile dando a configure il software(autoconf alternativa più completa e complessa di configure)

**ant** unisce tutti gli strumenti : dipendenze + processi di generazione + riconfigurazione dell'ambiente di build
- java e xml(xml è difficilissimo da leggere per l'uomo)
- plugin in java
