# The Man Month

Frederick Brooks, author of this book, describes how difficult can be designing and developing software in large teams. 

> Large-system programming has over the past decade been such a tar pit, and many great and powerful beasts have thrashed violently in it. 
>
> Most have emerged with running systems, few have met goals, schedules, and budgets. Large and small, massive or wiry, team after team has become entangled in the tar. No one thing seems to cause the difficult, any particular paw can be pulled away. But the accumulation of simultaneous and interacting factors brings slower and slower motion. 
>
> Everyone seems to have been surprised by the stickiness of the problem, and it is hard to discern the nature of it. But we must try to understand it if we are to solve it.

Paraphrasing his words, we can see that the issue with the developing teams is not one particularly hard problem, but many of little problems (easy to solve by themselves) which accumulate and bring the "beast" down. 

Let's start by analyzing how the development process work.

## The Joys and Woes of Crafting

Designing software and programming it is generally a fun and engaging activity. Additionally, many of us programmers while coding go into a "flow" mode, in which we are totally focused on the task, similar to a meditative and creative state. So why programming is such a great experience (for most)?

- **Sheer joy of making things**, just as a child building its own sand castle;

- **Pleasure of making things useful to others** (and getting credit for it);

- **Fascination of creating complex things to solve complex problems**;

- **Joy of always learning something new**;

- **The media of creation** (the computer) **is flexible and powerful**, so that the programmer can build his castles in the air and concretize them successfully.

As all things, there's the other (evil) side of the coin:

- **One must perform perfectly**, as a mage casts his spell: one wrong word, character or pause and the magic doesn't work; 

- Human beings are not accustomed to being perfect and this is one of the few areas of human activity that requires it. That said, *software is prone to bugs* and is *not linear* (unlike other engineering areas), so **debugging code can be an harsh and boring task**, but it must be done to make it at least usable;

- **The final goal isn't always well defined**, since software requirements, technology and techniques are ever changing;

- **One rarely controls the circumstances of his work**, since other people set one's objectives, provide one's resources and furnish one's information; 

- **Effort and progress are NOT equivalent**, since we have underdeveloped techniques to track one's progress to the goal (or milestone);

- **Working hard doesn't always mean progress**. One can work as hard as he can, but if he doesn't gain something towards the goal, no progress is made;

- Moreover, differently from other engineering areas, **the man/month unit of measuring progress doesn't work**. If a bridge construction is late, just bring more builders to reduce the time required to finish it: we can see that the man/month is valid. But what about the software development?

  Frederick Brooks, which lived the failure of one of his biggest projects, the OS/360, states in his *Brooks' Law* that

  > " Adding manpower to a late software project makes it later "

  

  The reason behind this sentence is that *intercommunication between members of a project is key*. First of all, **each of the extra worker must be trained** in the technology, the goals of the effort, the overall strategy applied and the plan of work. Adding to this, you must account that, if the workload isn't modular (splittable in parallel independent tasks), which is the majority of the time, then each worker has to coordinate with each of his coworkers: the cost of the intercommunication increases dramatically since it follows this formula 


```math
  \frac{n(n-1)}{2} \sim n^2 
```




<div align="center">
  <img src="complGraph.png" />
  <br>
  <i>We can see the communication between the team members as a complete graph, in which everyone has to communicate with eachother</i>
</div>

## Conclusions

We can conclude that, **to escape the tar pit** we have to:

- Avoid adding manpower to fix lateness;
- We have to design better techniques to measure progress (to avoid being late);
- Reduce the intercommunication cost;
- Methods to parallelize the development steps as much as possible.
