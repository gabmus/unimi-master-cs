# The Models of Success

We have previously discussed the problems of working in large groups and the *tar pit* figure, Brooks' idea to overcome these difficulties was to **preserve the conceptual integrity** of the project. 

Essentially, the creative part of the project (the design, the technology choices ...) was the only part affected by intercommunication issues, while the implementation of it could be totally distributed and parallelized. 

Following this philosophy, Brooks cited two strategies to solve the problems: the ***Cathedral*** and the ***Surgical team***.

## The Cathedral

The Cathedral strategy follows a simple principle: **creating a hierarchy structure from the development team** and **dividing the design work from the implementation one** in order to reduce the intercommunication cost.

Let's think about an analogy: the military hierarchy exists in order to enforce onto the underlings the conceptual integrity of the thinking and the ideology of the generals.

In the same way, if we can concentrate the creative part of the project in a restricted group of people (the "thinkers"), which intercommunication cost is lower, then we can distribute the implementation part onto the "workers" without problems. Now the only communication that can happen is between the "workers" and their direct superior.

<div align=center>
	<img src="hierarchy.png" /><br>
	<i>We can clearly see how a tree hierarchy helps reducing the communication needed since everyone talks only with his superior, instead than everyone else on the team</i>
</div>

We must also account that if we reduce the communications cost, then we can **reduce the difficulties cited by the Brooks' Law**: we can hire more people without too many issues since the implementation can be distributed and parallelized.

As the conceptual integrity remains intact, we can also see some problems with this strategy: there are evident **concentrated single points of failure**. What if someone in the highest sphere makes a wrong design choice or goes missing? What if, for example, the general is killed in the military analogy? That's one of the risks for running this model to be accounted for.

## The Surgical Team

> Mills proposes that each segment of a large job be tackled a team, but that the team be organized like a surgical team rather than a hog-butchering team. That is, instead of each member cutting away on the problem, one does the cutting and the others give him every support that will enhance his effectiveness and productivity.

This model is similar to the Cathedral in some way but has important differences.

Instead of a hierarchy let's imagine a surgical team: one or more surgeons performing an operation on a patient surrounded by different support figures, such as nurses, doctors and all kind of helpers. This way the surgeons are helped thoroughly during the operation, leaving puny and accessory tasks to their helpers while they focus on the main duty.

In the same way, we can see the Surgical Software Team composed by a main surgeon, a co-pilot and a corollary of helpers with different roles. The surgeon and the co-pilot are not chosen randomly, but are selected between the most productive and capable programmers in the team: these are the so called "10x programmers".

The main idea is to **leave the design and implementation of the system to the surgeon and co-pilot** because this, like in the Cathedral, preserves the conceptual integrity. **The rest of the complementary work is left to the helpers** since it's believed to be parallelizable in some way.

<div align=center>
	<img src="surg.png" /><br>
	<i>We can idealize a surgical team as star network: the single point of failure persists here too</i>
</div>

An interesting example that this model strength and weakness is the localization of the system: it's easy to see that this is a secondary task that can be parallelized and that can be distributed among helpers. 

However, during the translation, some sentences can be longer in the translated version than in the original: this could cause some problems in the placement of the translated text in the program (for example text getting out of the bounds). Because of this flaw, all the design for the text labels must be rethought from scratch to accommodate that feature.

## The Second-System Effect

Brooks' in his OS/360 failure notices that this was his second system to design and develop: this could be one of the reasons it failed. 

Essentially, the **second-system effect** (also known as **second-system syndrome**) is the tendency of small, elegant, and successful systems, to be succeeded by over-engineered, bloated systems, due to inflated expectations and overconfidence by the developer. 

This syndrome is common to all engineering fields and, particularly, to skydivers: the first time performing a jump the diver is afraid so all the rules are respected and the checklist passed two or more times, just to be sure. The second jump is the riskier one because of the acquired overconfidence that makes the diver minimize the dangers and be reckless.

## The Bazaar

There are successful systems that, not only doesn't follow Brooks' suggested models to get out of the tar pit, but that follow and exact counterpart of them: these systems are *fetchmail* and the *Linux Kernel*. But how these systems are successful? Where did Brooks' go wrong?

Eric Raymond, the creator of *fetchmail*, in his article/book *"The Cathedral and the Bazaar"* talks exactly about this topic, following Brooks' predicaments, discussing about them and opposing them with the open-source world methodology.

Eric identifies a new development model, **the Bazaar**, which is the complete **opposite of the Cathedral**. He also acknowledges that *fetchmail* and *Linux* had the same exact problems Brooks' found in OS/360, so how did these projects not collapse under the weight of the thousands of developers? 

We must note that **the development model is independent** from the intellectual property management. 

So what is the Bazaar? In the real world, a bazaar is an independent and auto-organized market where everyone can sell and everyone can buy. Following this metaphor, the Bazaar is a place where **everyone follows it's own goals** in a shared and public place, which is "anarchically administered". Basically anyone can code it's own solution to it's own problem (because the code base is public and accessible) and publishes it so that others can rely on that code and even build upon it.

**There is NOT a common goal**, because everyone "scratches his own itches" as Eric says. However there's a secondary hidden goal, that is not followed by anyone directly, which is trying to maintain the code base functioning, accessible and updated.

So what are the basic principles of a good Bazaar?

- **Start by scratching a developer's personal itch.** 
  
  People are not drawn to contribute to the project because of money or are forced to do so, but because they are willing to do it to find a solution to a personal problem.
- **Treating your users as co-developers is your least-hassle route to rapid code improvement and effective debugging.**
  
  It's important that the code base remains as public and accessible as possible because, at that point, willing users will try to help you fix your bugs (or fix them for you) in order to have a better experience while using your application.
- **Release early, release often and listen to your customers**
  
  One of the major problems according to Brooks was that effort and progress are often confused. Releasing early and often is a form of measuring progress, because even if the software is not finished and buggy, your customers will give precious advice and help in order to satisfy their needs.
- **Given enough eyeballs, all bugs are shallow *(Linus' Law)***
  
  To rephrase it "Given a large enough beta-tester and co-developer base, almost every problem will be characterized quickly and the fix will be obvious to someone".
  
  This starts from a Brooks' sentence which says "The total cost of maintaining a widely used program is typically 40 percent or more of the cost of developing it. Surprisingly this cost is strongly affected by the number of users. *More users find more bugs*." This was a problem in Brooks' view but in the Bazaar style it was gold. Basically what took the time of few expert to solve a single bug could be solved in less time by thousands of the babbling Bazaar users: someone of them will find the bug, and someone else will fix it.
  
  This discovery practically means that debugging is parallelizable in this model: this is a HUGE improvement.

In summary, provided the development coordinator has a medium at least as good as the Internet and knows how to lead without coercion, many heads are inevitably better than one.

## Reality Check

We have to remind that **these models are always ideal** and don't represent the reality perfectly: as we know *Linux* follows the Bazaar model, however Linus Torvalds decides which contributes are added in the new release; so everyone scratches his own itches only if the solution provided is appealing to the development coordinator.

We also know that it's almost impossible to plan every single development step or milestone in a project: it's important to have an overview of the requirements but also to be reactive to changes.

## The Kibbutz

After the second World War the Jews spread all over the world were relocated in the new found state of Israel: in order to create a basic economy, the Kibbutz were established. They were a collective community based on agriculture, in which everyone worked with an individual goal of survival but also a common goal of growth.

The Kibbutz model is somewhat **a middle ground between the Bazaar and the Cathedral**: everyone works for himself, scratching his own itches, but following certain rules and policies, while also working collectively for a greater ideal (the completion of the project). Not as free as the Bazaar, not as strict as the Cathedral, the Kibbutz merges the pros from both sides.

Debian, a Linux distribution, follows the Kibbutz model and it does that by

- Giving an exam to anyone who wants to contribute to the project in order to check his capabilities;
- Enforcing certain policies in a way to establish a common language (Debian packet manager, mailing lists);
- Accepting/revoking changes through a committee .