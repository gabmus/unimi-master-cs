## Source code management (versioning)

La gestione del versioning è cambiata drasticamente degli ultimi 10 anni e una delle funzioni molto overused in Subversion era il branching ma ciò era gestito malissimo già fin da CVS. Bisogna cercare di mantenere il branching al minimo e il più semplice possibile e quindi sviluppare attorno ai branch (di sviluppo e di manutenzione ad esempio)

### Versioning distribuito

Il versioning distribuito cerca di risolvere questo problema e il programma di punta è Git.

Git è un wrapper di comandi pertanto "git init" in realtà va a chiamare "git-init" pertanto esiste una pagina di man per ogni comando di git. Inoltre per questa proprietà è possibile estendere le funzionalità con altri nuovi comandi.

Osservando la struttura del .git dopo l'init abbiamo un sacco di file di configurazione ma le componenti essenziali per un repo sono

.
└── .git
    ├── HEAD		// file testuale contenente almeno ref: refs/nomebranch
    ├── objects	  // folder vuota
    └── refs		   // folder vuota

Se aggiungo un qualsiasi file nella cartella della repo essa non viene considerata negli object fino a quando non viene aggiunta semplicemente con git add. Svolgendo git add verrà creata una reference 

.
├── .git
│   ├── HEAD
│   ├── objects
│   │   ├── info
│   │   └── pack
│   └── refs
│       ├── heads
│       └── tags
└── pippo

10 directories, 16 files

Dopo l'add

.
├── .git
│   ├── HEAD
│   ├── objects
**│   │   ├── bf
│   │   │   └── a54249783be226a2ba6c3e44ee213890d0cc4e**
│   │   ├── info
│   │   └── pack
│   └── refs
│       ├── heads
│       └── tags
└── pippo

Ora pippo è staged per il commit, infatti si è creato un hash per il file in objects: viene creata una folder con i primi due caratteri dell'hash (bf) e un file col resto dell'hash (a54249...) così da organizzare insieme tutti i file che iniziano con lo stesso hash. Il contenuto stesso del file viene zippato e salvato nel file con quel nome hash (a54249...)

Con il comando cat-file posso ottenere informazioni sul file tracciato dandogli in input l'hash o parte di esso

``` shell
$ git cat-file -p bfa54			// -p indica il nome del file con hash bfa54...
pippo
$ git cat-file -t bfa54			// -t indica il tipo del file con hash bfa54...
blob
```

<...> codice ruby che gestisce il salvataggio raw object

Ora modifichiamo il file "pippo" e appendiamo una stringa "e pluto": possiamo vedere come si è creata una nuova cartella con un nuovo hash

objects
│   │   ├── bf
│   │   │   └── a54249783be226a2ba6c3e44ee213890d0cc4e
│   │   **├── f7
│   │   │   └── e7e7daaf3287c5f7fe9dd9ae8ff66e2c52af6c**

Questo perchè ha fatto un nuovo backup se volessi tornare indietro.

Ora l'index punta al file più aggiornato ovvero a f7e7e7... (ovvero quello con "pippo e pluto"). Quindi è l'index quello che tiene traccia, con dei puntatori, ai file staged che devono essere committati.

Aggiungiamo un file "pro" e una folder/file pio/pro.

Una volta staged e fatto il commit vediamo cosa succede

```shell
dige@octane:~/Scrivania/Svigruppo/testgit$ git log
commit da69af19b0dfc69cc671982404514320cd1132b9 (HEAD -> master)
Author: dige <mrblackraven97@gmail.com>
Date:   Thu Oct 31 17:03:23 2019 +0100

    Partiamo
dige@octane:~/Scrivania/Svigruppo/testgit$ git cat-file -p da69af
tree 21e9ff73259db1473c0effababfc55f229d48b94
author dige <mrblackraven97@gmail.com> 1572537803 +0100
committer dige <mrblackraven97@gmail.com> 1572537803 +0100

Partiamo
dige@octane:~/Scrivania/Svigruppo/testgit$ 
```

Possiamo vedere come l'hash del commit (che corrisponde a un oggetto nella cartella objects) è considerato di tipo tree, perchè presenta file, cartelle e sottocartelle.

```shell
dige@octane:~/Scrivania/Svigruppo/testgit$ git cat-file -p 21e9ff
040000 tree 71d21cf96bbb93094dd3087058163d6ffb88b69d	pio
100644 blob f7e7e7daaf3287c5f7fe9dd9ae8ff66e2c52af6c	pippo
100644 blob aaf960a96905a44545f33a77f51a571fe6d4626a	pro
```

Facendo cat-file sul tree del commit possiamo vedere tutti i file che abbiamo staged con quel commit.

```bash
dige@octane:~/Scrivania/Svigruppo/testgit$ mv pio pio1
dige@octane:~/Scrivania/Svigruppo/testgit$ git status
Sul branch master
Changes not staged for commit:
  (use "git add/rm <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	deleted:    pio/pro

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	pio1/

no changes added to commit (use "git add" and/or "git commit -a")
dige@octane:~/Scrivania/Svigruppo/testgit$ git add -A
dige@octane:~/Scrivania/Svigruppo/testgit$ git status
Sul branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	renamed:    pio/pro -> pio1/pro

dige@octane:~/Scrivania/Svigruppo/testgit$ 
```

Cambiando il nome della directory pio in pio1 inizialmente per git sembra che pio sia stata rimossa e pio1 creata nuova. Facendo git add -A però do a git l'opportunità di avere insight su tutti file e sulla storia e notare che in realtà pio è stata solo rinominata; infatti lui se ne accorge e mi restituisce renamed nel nuovo git status.

Con ciò è cambiato anche l'hash di pio1 ovviamente 

(100644 3dbbf0fb8d96cd3d9878b4b169702d2b4ab64385 0	pio1/pro)

In sostanza quando faccio uno stage non c'è solo quello di nuovo rispetto a prima, ma c'è tutto ciò che ci sarà nella release della commit che si porta dietro.

Come faccio a riferirmi a un commit? Posso usare diverse sue informazioni come 

- Nome: ovvero il suo hash o le prime sue lettere
- Nickname: ovvero un tag, un branch o HEAD
- Parentela:

immagine albero

A è il commit più recente, mentre O la radice.

*esempio* Per nominare F partendo da A ho due strade: A -> B -> F oppure A -> C -> F

***A -> B -> F***

\^1\^3 A 

***A -> C -> F***

\^2\^1 A

## Problem solving

![](http://justinhileman.info/article/git-pretty/git-pretty.png)

### git commit --amend

Se mi pento di aver fatto un commit (per un typo o altro) posso fare amend ripartendo dalla situazione precedente rimuovendo il commit padre (da cancellare) e tornando al commit nonno (backup)

### git checkout

Vengono copiati i files nella versione presente in ref in index e working directory: se non sono presenti file allora viene spostata la HEAD

### git reset

Simile a checkout ma

- Con --files, non modifica la working directory
- Senza --files, sposta non solo la HEAD ma anche il branch puntato

### git revert

Comando che crea un commit che inverte gli effetti di un commit citato nella storia. Questa operazione, diversmente dal reset, non altera o distrugge la storia.

![](https://gcapes.github.io/git-course-mace/fig/git-revert-vs-reset.svg)

### git rebase

Linearizza storie di più branch in una unica storia concatenandole come se fossero accadute in successione.

Agisce quasi come il merge perchè unisce due o più storie in una sola, tuttavia lo fa in modo diverso.

### stash

Mette da parte workingspace e index (ne fa appunto uno stash) e poi esegue un reset.

Alcuni scenari di utilizzo sono:

- Workflow interrotto
- Pulling in un "dirty" tree

### git filter-branch

Comando avanzato (quindi pericoloso se usato male) che permette di riscrivere la storia del mio repo in maniera ancora più completa: esempi:

- fare come se un file non fosse stato mai versionato

  `git filter-branch --index.filter ...`

  In questo modo il file sparisce da tutta la storia, tuttavia ti fa un backup di tutta la storia originale in un branch a parte in refs/original. 

- more bad things

- ...

Come accennato, eseguire un comando globale come filter-branch è una operazione critica quindi crea un backup della storia in refs/original e non posso eseguire altri comandi globali finchè non confermo che è andato tutto bene (oppure forzando il comando sovrascrivendo il backup con -f)

### git add -p

E' una opzione del comando add che permette di scegliere quali hunk e righe scegliere dai file modificati da committare. 

Sottocomandi hunk:

- y, accetta questo hunk di modifiche
- n, rifiuta questo hunk di modifiche
- s, cerca di splittare l'hunk in sottohunk più maneggiabili
- e, modifica manualmente gli hunk e le righe

## Git hooks

I git hooks sono script predefiniti che vengono eseguiti insieme ad alcuni comandi tipici di git.

### pre-commit

pre-commit è uno script lanciato subito dopo un comando commit prima di editare il messaggio (a meno che non sia usata l'opzione --no-verify). E' utile per eseguire test veloci e di integrazione sulle parti prima del commit.

un pre-commit sample può essere del tipo:

```sh
<code>
```

### post-commit

<...>

## Git flow

Git flow è una aggiunta di operazioni guidate di alto livello che cercano di dare un valore semantico ai rami modificando il behaviour dei comandi di git.

Ho due rami fondamentali con "vita infinita"

- Master (origin/master) che contiene le versioni del software stabili e pronte alla consegna
- Develop è il ramo che di integrazione sui cui sviluppare che però è comunque funzionante

Poi ho diversi rami dalle diverse utilità

- Feature, su cui ogni sviluppatore può sviluppare separatamente la propria feature senza disturbare il ramo develop di integrazione. Terminata la feature e mergiato con develop il ramo termina.

  start feature commands

  ```bash
  git checkout develop
  git branch myfeature
  git checkout myfeature
  ```

  end feature commands

- Release, su cui si sceglie un tot di feature da implementare in una versione stabile, si congelano e si crea il branch dal commit corrispondente di develop. Su di esso si svolgerà solo bugfixing fino a quando non è stabile: a quel punto viene passata al ramo master e il ramo Release termina.

## Limiti di git

Alcune questioni non gestite nativamente da git sono 

- Singolo livello di autorizzazione
  - Ognuno può fare quello che vuole nel proprio git, ma come fare se più persone collaborano per lo stesso repo? Servono servizi aggiuntivi di autenticazione e autorizzazione delle modifiche
- Nessun livello di review
  - Non posso scegliere quali modifiche fare e quali no, dato un nuovo commit entrante: o lo accetto tutto o lo rifiuto tutto

Di questi limiti soffrono particolarmente i progetti Open Source, pertanto gli ambienti di hosting di repo remoti hanno cercato soluzioni alternative inventandosi nuovi meccanismi (permessi di push, pull ...) oppure provando a "imporre" nuovi tipi di workflow (Git Flow).

Comunque git fornisce un meccanismo di default base per collaborare e si chiama git request pull, in cui si richiede al proprietario del repo di pullare le proprie modifiche in modo da incorporarle in una nuova versione del repo.

...

### gerrit

....