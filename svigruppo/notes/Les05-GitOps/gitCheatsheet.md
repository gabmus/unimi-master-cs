# Cheatsheet Git

| Funzione             | Command                          | Meaning                                                      |
| -------------------- | -------------------------------- | ------------------------------------------------------------ |
| *Inspect repo*       |                                  |                                                              |
|                      | `git ls-files -s`                | Shows the files pointed by the staging index with their hash code |
|                      | `git cat-file -p <hash>`         | Shows the content of the object with the hash-code `<hash>`  |
|                      | `git cat-file -t <hash>`         | Shows the type (blob, tree, commit) of the object with the hash-code `<hash>` |
|                      | `git cat-file -s <hash>`         | Shows the size of the file with the hash-code `<hash>`       |
|                      | `git hash-object <file>`         | Shows the calculated hash of the file passed with name `<file>` |
|                      | `tree -a`                        | Shows the tree structure of the git folder (.git + working directory) |
|                      | `git hash-object -w <file>`      | Shows the calculated hash of the file passed with name `<file>` and writes it in the object folder |
| *Branch management*  |                                  |                                                              |
|                      | `git tag <tagname> <hash>`       | Generates a tag named  `<tagname>` corresponding to the object identified by the hash-code `<hash>` |
|                      | `git branch <name>`              | Creates a new branch called `<name>`                         |
|                      | `git checkout <branch>`          | To prepare for working on `<branch>`, switch to it by updating the index and the files in the working tree, and by pointing `HEAD` at the branch |
|                      | `git checkout -b <branch>`       | Create a new branch on the fly called `<branch>` and performs a checkout on it |
|                      | `git checkout --detach <commit>` | Perform the checkout on a particular commit hash to view it in detail, in a detached `HEAD` state |
| *History management* |                                  |                                                              |
|                      | `git revert <commit>`            | Reverts the changes introduced in `<commit>` in a new commit; it is then pushed into the history |
|                      | `git reset --hard <commit>`      | Resets the HEAD to `<commit>`, restoring that commit staging index and working directory (undoing all changes made until there) |
|                      | `git reset [--mixed] <commit>`   | Resets the HEAD to `<commit>`, restoring that commit staging index (undoing all changes made until there) |
|                      | `git reset --soft <commit>`      | Resets the HEAD to `<commit>`. Same as git checkout `<commit>` but without the detached head state. |
|                      |                                  |                                                              |
|                      |                                  |                                                              |
|                      |                                  |                                                              |
|                      |                                  |                                                              |
|                      |                                  |                                                              |
|                      |                                  |                                                              |
|                      |                                  |                                                              |
|                      |                                  |                                                              |
|                      |                                  |                                                              |
|                      |                                  |                                                              |
|                      |                                  |                                                              |
|                      |                                  |                                                              |
|                      |                                  |                                                              |

## Sample helper script code

```sh
# helper code for quick git folder reinitialization
rm -rf mySolution
mkdir mySolution
cd mySolution

# start git script
git init
...
```



## REMIND THIS STUFF

Don't forget to:

- **Check for the tree structure of both real solution and your solution**
  - If they look like the same, you probably did all right
- **Check the history thoroughly**
  - Remember ANY tags, branches, detached heads
- **Check for dangling pointers and objects**
  - Use `git fsck`
- **When using `printf` remembering the `\n` is crucial**
  - Always check the hash to see if it's necessary in the solution

## Extras:

### Deflate hash

```sh
$ cat .git/objects/72/943a16fb2c8f38f9dde202b7a70ccc19c52f34 | python3 -c "import zlib,sys; print(repr(zlib.decompress(sys.stdin.buffer.read()).decode('utf8')))"

'blob 4\x00aaa\n'
```

Shows the effective content of the hash object which is `type`, `size`, `content`.

