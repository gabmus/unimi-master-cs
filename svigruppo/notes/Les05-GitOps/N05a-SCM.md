# Software Configuration Management (SCM)

Originally, Configuration Management was used in the aerospace industry: only later it was introduced in software development with the name of Software Configuration Management (SCM). But what is SCM? 

SCM is a series of practices which goal is to make the development process systematic, **keeping track of every change** that has been made to the product in order to have a configuration (state) of it at every instant.

The objects which evolution is tracked are called **configuration items** or **artifacts** (in Italian "**manufatto**",  NOT "artefatto").

## Configurations (versions)

Usually artifacts are files, rarely they are directories.

Old SCM systems focused only on artifacts, keeping track of every revision (configuration) of them. But what if I wanted to track different revisions of files and treat them as one?

With new SCM systems it's possible to create and track versions, which is a set of items on a particular revision. 

![](versionconf.png)

For example, in my product I have two artifacts, *q* and *r*. I'm tracking them with an SCM, so i have different versions of them (until *q<sub>n</sub>* and  *r<sub>n</sub>* ). With new SCM systems, I can create a version of my product which has non-lined revisions: I can create Version (*q<sub>1</sub>*,*r<sub>1</sub>*) as well as Version (*q<sub>2</sub>*,*r<sub>1</sub>*). 

## SCM Systems

Evolution of SCM tools:

- 80s : Local SCM systems (SCCS, rcs, ...) - **Pessimistic model**
- 90s : Client-server SCM systems (cvs, subversion, ...) - **Optimistic model**
- 2000s : Distributed (peer-to-peer) SCM systems  (git, mercurial, ...) - **Optimistic model**

SCM systems are usually language and application independent: that's because they work on files and giving a semantic to their content can lead to many problems. 

These systems work best treating **text files**, made of text lines (code sources, text manuals, ...): this leads to better tracking of changes on them, by checking which lines are changed and how (we'll see this in a bit).

Binary files instead (images, word files, executables...) are difficult to impossible to track, because calculating changes from the original file is often pointless and uninformative.

The base mechanism to control the revision evolution is regulated by

- **Check-out** 
  - Expresses the will to change a determinate artifact
- **Check-in (commit)**
  - Expresses the will to store and save a determinate change-set (a new version)

The **repository** (repo) is the whole set of artifacts and all the information and meta-data the SCM system uses to manage the history of all the versions, configurations and revisions. 

In client-server SCM systems the repo is contained only in the central server, while in distributed every machine working on it has it's own copy of the repo.

One of the problems to solve was managing the concurrent access to the artifacts when a repository is shared in a work group: accessing an artifact to read it wasn't a real issue, but trying to modify it was. 

Basing on how the SCM systems solved this problem, two models were made:

- **Pessimistic model**
  - The solution proposed by this model is to make sure that nobody could edit an artifact contemporarily with other people in order to avoid discrepancies and inconsistencies. This was done by implementing a mutually exclusive write access on the artifacts, setting a lock on their checkout.
  - ***Pros:***
    - Impossible to have artifacts in an inconsistent state
  - ***Cons:***
    - Reduced productivity and multitasking: lock system is very restrictive because only one person can work on an artifact at a time (what if someone forgets to unlock?)
- **Optimistic model**
  - This model doesn't even try to manage the concurrent access, however it provides some support for merge activities for parallel change-sets hoping that the users will be able to coalesce their work seamlessly.
  - ***Pros:***
    - Anyone can work on any artifact in their local repo
    - Merge mechanisms are eased and interactive if needed (the user chooses which change he has to keep)
  - ***Cons:***
    - Merges are critical operations and sometimes can go wrong, making the user have to waste a lot of time to fix everything

The pessimistic model is as unrealistic and ideal as the waterfall development process (a "cascata"). The optimistic one, however, is doable and can be managed with the introduction of multiple development parallel paths (called **branches**).

## How artifacts are tracked

### Base-delta calculation

As we said, SCM systems work best while tracking artifacts made of text lines: but how do they check changes? Do they store all the possible artifact revisions? Of course not, otherwise it would take too much space in the long run.

One possible way is to keep the base artifact and, when the changes are made, a difference delta is calculated between the base and the modified revision: so if we store the delta, we can always reconstruct the modified artifact starting from the base.

However, as we go forward through the history, calculating new versions would be longer and longer, since many delta need to be added to the base.

To solve this issue, we can simply do the opposite: instead of saving the base artifact, we save the modified new artifact and keep the delta, if we need to go back to the base revision. This way updating artifacts doesn't take too long. It's true, now we have the same problem if we need to go a lot back in history, but usually this doesn't happen as frequently as updating artifacts.

### Git-style : Hashes and refs

Git was created by Linus Torvalds, the same that gave birth to the Linux OS. He's an expert in file-system optimization since he had to handle it in Linux so he applied his knowledge in the SCM field.

Git is the only SCM system that saves the whole artifact, but it does it in a peculiar way: instead of saving the file (which is obviously memory expensive) Git compresses it with information about it's size and type and hashes it. In this way, if I will ever need that artifact again in one of my versions, I just need to reference that hash with a symbolic link.

But won't this take too much memory in the long run, even when compressed? Git keeps a copy of each hashed artifact only if it's referenced by a version (in Git terms, if referenced by a tree, a commit or a tag). If it's not referenced by anyone anymore it will be considered dangling and will be removed eventually by Git garbage collector.

## How merges work

As we said, most successful SCM systems follow an optimistic model, providing method to ease merging of the parallel development branches. So how does it work?

The merge is a critical operation and to understand how to solve it we must identify the different merge situations that could come up:

- Parallel work on different artifacts
- Parallel work on the same artifact on different hunks
- Parallel work on the same artifact on the same hunk

The last one is the one which require must work, since the automatic merge techniques can do little: surely some interactive merging will be needed to evaluate the differences and solve the conflict manually. The other two are solvable with automatic merging probably since there won't be any particular conflict.

### hunks, diff and patch

***diff*** is a POSIX program which calculates the difference between two revisions of an artifact: it is calculated line by line, minimizing the number of insertions and deletion (longest subsequence research)

Example:

`R  = 	a	b	c	d	f	g	h	j	q	z`

`R' = 	a	b	c	d	e	f	g	i	j	k	r	x	y	z`

The intersection is calculated `I = a	b	c	d	f	g	j	z`

So the differences are the the complement of the intersection I, and for each state if it's an insertion (+) or a deletion (-)

`e	h	i	q	k	r	x	y`

`+	-	+	-	+	+	+	+`

The result of *diff* is then divided into **hunks**, which are areas of correlated lines (because of proximity or other reasons). This result can be used by ***patch*** to apply the differences to R to get R', but also can be used on any other R* similar to R to get R*'.

### 3-way merge

Parallel work on the same artifact implies that the two revisions to merge have a common ancestor (the revision from which they derive). If that's the case, we can ease the merge.

Given A' and A* two revisions with common ancestor A, for each hunk:

- If the hunk is common for all three revisions we keep it unchanged (nobody touched it)
- If the hunk is common for two of the three revisions
  - Common between A' and A* : we keep one of the two (they are equal anyway)
    - Example: there was a bug in A and A' and A* fixed it in the exact same way
  - Common between A and A' : we keep the change introduced by A*, since A' didn't change from ancestor
  - Common between A and A* : we keep the change introduced by A', since A* didn't change from ancestor
- If all three hunks are different the merge must be done manually

Of course there could be some problems during the merge: for example both revisions alone works perfectly but merging them introduce a new bug because of some incompatibilities. However, this is a semantic code problem and there's not much we can do as a SCM.



