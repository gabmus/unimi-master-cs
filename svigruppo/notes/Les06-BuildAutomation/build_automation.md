# Build Automation

Developing software needs a collective effort, large work groups with objectives in a rapid evolution and a lot of concerns weaved make a lot difficult the division of work.

The software production consists mainly in changing files: systems of *configuration management* allows us to keep track of the evolution of the various versions of our code.

Ordinated cooperation requires often a lot of added work, even a good programmer must go through a lot of troubles and make a lot of effort to put his contribute in a collective project.

Company policies (or kibbutz one) nowadays became an essential part of the everyday work of a developer.

A software product needs to be assembled (compiled), because it has a lot of component and that is not a simple task:

- Dependency from uncontrolled components (*dependency hell*);
- Dependency from other developed components

The more a project is big, the more complex it is to keep track of all its dependencies, this is why we need tools that let us automate this process (build automation).

### Semantic Versioning

Given a version number `MAJOR.MINOR.PATCH`, increment the:

1. `MAJOR` version when you make incompatible API changes;
2. `MINOR` version when you add functionality in a backwards compatible manner;
3. `PATCH` version when you make backwards compatible bug fixes.

Additional labels for pre-release and build metadata are available as extensions to the `MAJOR.MINOR.PATCH` format.

## Make

One of the most used build automation tools is **make**, it allows us to specify dependencies when we generate the compiled and running code.

Dependency: if we change a pre-requirement, then the process generation must be repeated.

```c
helloworld.o: helloworld.c
		cc -c -o helloworld.o helloworld.c

helloworld: helloworld.o
		cc -o $@ $<

.PHONY: clean
clean:
		rm helloworld.o helloworld`

```

How does make work?

- All dependencies end up in an acyclic graph that allows a unique *topological sort* (because we can go though each target only once);
- The generation process (receipt) are executed following the topological sort;
- In modern *make* buildings, it is possible to run different independent generation process at the same time in a parallel way.

## Standard Makefile Targets

- `make all` - Build programs, libraries, documentation, etc. (Same as make.);
- `make install` -  Install what needs to be installed;
- `make install-strip` -  Same as `make install`, then strip debugging symbols;
- `make uninstall` -  The opposite of `make install`;
- `make clean` -  Erase what has been built (the opposite of `make all`);
- `make distclean` - Additionally erase anything `./configure` created.
- `make check` - Run the test suite, if any.
- `make installcheck` - Check the installed programs or libraries, if supported.
- `make dist` - Create `PACKAGE-VERSION.tar.gz`.

The `make` model assumes a fixed build environment, that be carried to a new system and still work.

## The C Portability

The C language has some portability problems because of the various libraries version installed in the various systems. They:

- Don't exist everywhere (ex. `strtod()`);
- Have different names (ex. `strchr()` vs `index()`);
- Have different prototypes (ex. `int setpgrp(void)` vs `int setpgrp(int, int)`);
- Have different behaviors (ex. ` malloc(0)`);
- Require different dependencies to work (ex. `pow()` is in `libm.so` or `libc.so`?);
- Require different treatments (ex. `string.h` vs `strings.h` vs `memory,h`). 

To solve this problems, the fist solutions were:

- Adding `#if/#else` macros;
- Substitution macros;
- Substitution functions;

*Example: code cluttered with `#if/#else`*

```c
#if !defined(CODE_EXECUTABLE)
	static long pagesize = 0;
#if defined(EXECUTABLE_VIA_MMAP_DEVZERO)
	static int zero_fd;
#endif
	if (!pagesize) {
#if defined(HAVE_MACH_VM)
 		pagesize = vm_page_size;
#else
		pagesize = getpagesize();
#endif
#if defined(EXECUTABLE_VIA_MMAP_DEVZERO)
		zero_fd = open("/dev/zero", O_RDONLY,0644);
		if (zero_fd < 0) {
 			fprintf(stderr, "trampoline: Cannot open /dev/zero!\n");
 			abort();
 		}
#endif
	}
#endif
```

## Configure

![](img/img1.png)



**Configure** is a tool that verifies the characteristics of the building environment and:

- Generates a `config.h` file with correct `#define`;
- Generates a `Makefile`.

![](img/img2.png)

All the `*.in` files are *configuration templates* from which `configure` generates the script used to verify the environment.

## Ant

Ant is a build automation formed by:

​			` dependencies + generation process + build environment reconfiguration`

It is written in Java and XML, and is a plugin for Java.

```xml
<?xml version="1.0"?>
<project name="Hello" default="compile">
	<target name="clean" description="remove intermediate files">
		<delete dir="classes"/>
	</target>
	<target name="clobber" depends="clean" description="remove all artifacts">				<delete file="hello.jar"/>
    </target>
	<target name="compile" description="compile the Java source code to clMake">
		<mkdir dir="classes"/>
		<javac srcdir="." destdir="classes"/>
	</target>
	<target name="jar" depends="compile" description="create a Jar file fo<jar">
        <jar destfile="hello.jar">
 			<fileset dir="classes" includes="**/*.class"/>
 			<manifest>
				<attribute name="Main-Class" value="HelloProgram"/>
			</manifest>
		</jar>
	</target>
</project>
```

## Perspective

Tools support in process development:

- Configuration management artifact;
- Configurations;
- History of configurations.

Build automation dependencies between artifacts:

- **Make** - The environment is implicit: we have dependencies only between those artifacts that we have control of;
- **Ant/Maven** - Thanks to centralized catalogs, dependencies between each component, the software environment is still implicit (but embedded);
- **Gradle** - Even the software environment is described in the "build automation", at least part of it.

## Gradle

- Domain specific language based on Groovy;
- Build-by-convention (configurable);
- Supports catalogs of its components;
- Test support;
- Repositories available.

*Example: Gradle*

```c
plugins {
	id "java"
	id "jacoco"
}

repositories {
	jcenter()
}

dependencies {
	testCompile "junit:junit:4.11"
	testCompile 'org.assertj:assertj-core:3.5.2'
	compile 'commons-io:commons-io:2.5'
}

jacoco {
	jacocoTestReport.reports.xml.enabled = true
}
```

*Example: Gradle DSL*

```c
task hello {
	group 'svigruppo'
	description 'Saluta lo sviluppatore'
}

doLast {
	println 'Hello user!'
}

task anotherHello {
	doFirst {
		println 'Salutoni!'
	}
}

anotherHello.dependsOn hello
```

```c
task copia(type: Copy) {
	from 'source'
	into 'destination'
}

task ciao(type: Exec) {
	workingDir '.'
	commandLine '/usr/bin/echo', 'ciao mamma'
}
```

## Continuous Integration

Traditionally, the software integration is the longest and riskier part of software projects.

The **continuous integration** (incrementally) is used to minimize the risk of failures:

- Reduced deployment risk;
- Believable progress;
- User feedback.

Continuous integration follows this steps:

1. Work on a local copy on the develop machine;
2. *Build* vs *Compile*, other than test construction/execution;
3. Working build on the develop machine;
4. Loading on the integration machine;
5. Working build on the integration machine.

At least once per day.

Fowler: "The current open source repository of choice is Subversion. (The older open-source tool CVS is still widely used, and is much better than nothing, but Subversion is the modern choice.)”

“One of the features of version control systems is that they allow you to create multiple branches, to handle different streams of development. This is a useful, nay essential, feature — but it’s frequently overused and gets people into trouble. Keep your use of branches to a minimum.”

![image-20200116115700875](img/img3.png)

A *build* should not take more than 10 minutes, otherwise we lose the immediate feedback. But what if more time is required?

- *Deployment pipeline* - The build is split in phases: commit build, slower test build, ...
- The commit build takes less than 10 minutes, the rest goes on later.

### Continuous Delivery

The system where integration happens should be as similar as possible to the production one.

The deployment to the production environment can me automatized, granting a major control over the effective configuration we are using.

## Git Submodules

They let us keep control of a dependency:

- A *subdirectory* contains another *repository*;
- `git submodule add repo dirname`;
- The file `.gitmodules` gets a new version;
- `git submodule init` or `git submodule update` to obtain the actual module version;
- In the *frozen* project we have a specific version `git add submoduledirname`.

To delete a submodule there is no direct way, we have to do some operations:

1. Manually delete in `.gitmodules`;
2. Manually delete in `.git/config`;
3. Run command `git rm --cached submoduledirname`;
4. Commit changes (deleting submodule).





















