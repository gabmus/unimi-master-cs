# Docker for DevOps

Uno dei principali problemi principali nello sviluppo di software complessi è la Dependency Hell, perchè devo tener conto delle numerose:

- Librerie (spesso poliglotte aka di linguaggi diversi)
- Servizi (dbms, cache, log ...)
- Persone

Pertanto converrebbe far fare allo sviluppatore parte delle operations (ops) oltre al development (dev) : approccio devops!

## Qualità desiderate

Dal punto di vista del **dev** vorrei:

- Predicibilità
- Replicabilità
- Versioning, vorrei, durante il processo di sviluppo, tenere traccia delle varie configurazioni
- Leggerezza, vorrei che la macchina su cui devo lavorare sia configurabile con l'ambiente minimo di sviluppo nel minor tempo possibile e col minor costo possibile (non voglio perdere una mattinata per configurare tutto)

Dal punto di vista dell'**ops** vorrei:

- Tolleranza ai fault
- Portabilità
- Scalabilità
- Componibilità
- Scheduling & Orchestration

### Soluzioni possibili

Per poter ottenere queste qualità esistono diversi tipi di soluzione:

- Sistemi di configuration management (Ansible, Chef, Puppet, Saltstack, Terraform)
   - Poco lightweight
- Sistemi di virtualizzazione (LXC, KVM, Vagrant...)
   - Poco lightweight
- Sistemi a container (Docker, Singularity)
   - ...

### Immagine (filesystem)

Un'immagine nel contesto di docker è una copia del file-system read-only (concretizzata in un grafo aciclico DAG di copy-on-write). 

Per ottenere una copia di un'immagine è sufficiente usare il comando pull

```sh
$docker pull hello-world
```

### Container (filesystem + process)

Un container è una immagine insieme a un copy-on-write layer scrivibile (concretamente è un filesystem con un binario eseguibile)