# Agile Methods

Develop software is a hard task, as we saw for the *Tar Pit* case and all the previous solution.

To overcome those problems, in the '90 a lot of new approaches were born from the annoyance of the extreme emphasis due to the documentation in the development process. They studied the crucial points of the software and it life cycles, aiming to reduce the weight of the documentation.

Martin Fowler in 2001 published the new **agile values** in a **manifest**, whose purpose were to define priorities within projects:

- People and interaction more than process and instrumentation;
- Working software more than exhaustive documentation;
- Collaboration with client more than contact negotiation;
- Respond to changes more than follow strictly a plan.

## The Agile Principles

This manifest presents **12 principles** that define what it means to be an *agile developer*:

1. Release valuable software, often and consistently;
2. Changing the requirement, even on further stages of the project;
3. Deliver frequently working software;
4. Customers and developers must frequently work together;
5. Keep motivated and well supported people;
6. Face to face conversations;
7. Working software is the main measure for progress;
8. *Sustainable coding* - Being able to maintain constant rhythm;
9. Technical excellence;
10. *Simplicity*  is essential - The art of maximizing quantity of undone work;
11. Have an auto-organized team;
12. On regular ranges, the team must reflect on how to improve efficiency.

The manifest does not give rules, just advices, speaking to the hearth of developers instead of their brain. It comes from the frustration of wasting time doing something no one would use like some unneeded documentations.

The most problematic part in the agile methods is the participation of the customer, which is interpreted differently from the various agile approaches.

## Simplicity and Minimalism

Agile aims to be simple and minimalist, and it does that by the *"You aren't gonna need it" (YAGNI)* motto. which means **not to implement or even think about something until it is not really necessary**, and if it is, do it in the simplest possible way.

This is in explicit contrast with the software engineering principle of *"Design for change"*: if a new feature/adaption is not accurately designed it will cost too much.

Agile propose the new use of **user stories** and **test cases**:

- *User stories* are features that helps shift the focus from writing about requirements to talking about them. All agile user stories include a written sentence or two and, more importantly, a series of conversations about the desired functionality.
- *Test cases* are the set of conditions or variables used to determine whether a system under test satisfies requirements. They are created manually from specifications, which are later used to created tests used by the QA team.

## The Scrum Team

**Scrum Team** is the name given to the **self organized agile team**, formed by people who has all the required skills for completing all the needed stuff, without the help of external groups.

A scrum team has three main roles: the *Product Owner*, the *Scrum Master* and the *Development Team*.

More specifically, we have:

- Made by 7 ± 2 people from the development team, a product owner and a scrum master;
- Periodic reunions with different purposes, like daily stand-ups;
- A **product owner**, tasked to communicate with the customer, define priorities based on opportunities and business risks, and track the backlog;
- A **scrum master**, who supports the general work of the group, removing all the obstacles and controls that the rules are followed by the group.
- The **development team**: estimates the complexity of the job, identify risks and shows the progress of the product they are working on.

In a Scrum Team, equal importance is given to the implementation as the organization of the work:

- The work is split in epics, made of stories, released with sprints of 1-3 weeks;
- *Closed window rule*: while a sprint is in progress, no functionalities has to be added. If it is really necessary, the sprint is restarted;

- In the planning reunions, the members estimates the complexity of a user story with **planning poker**, using as unit of measure days as: 1, 2, 3, 4, 8, 13, 20, 40, 100;
- In the reunions *pigs* (those directly involved) are identified and the *chickens* (those only interested) who only gives opinions only if requested from the *pigs*.

## Reunions

The reunions can have different purposes, and are divided as:

- **Daily stand-up** (15 minutes)
  - What did we do yesterday?
  -  What do we do today?
  -  Are there any problem?
- **Planning** (1-5 days)
  - Sprint scheduling;
  - Defining the sprint backlog with the esteem for each epic/story.
- **Retrospective** (30 minutes)
  -  At the end of a sprint, to find improvements.
- **Review** (1 hour) 
  - At the end of a sprint, showing the work to stakeholders.

# Agile Programming

Each methodology uses different programming techniques, the most famous are:

- Pair programming;
- Collective code ownership; 
- Refactoring;
- Test Driven Development;
- Velocity tracking;

## Pair Programming

Pair programming consist in **developing software in groups of two people**, where one has the role of the co-pilot, thinking and dictating the code, and the other one is the pilot, the one who actually writes the suggested code from the keyboard.

Having two peoples instead of one that writes the same code can look dumb, but it has been proven very efficient, because explaining and discuss your code with the pilot produces higher quality software, and allows those people to know that entirely, so that if one ore those has to leave the company, the other one can replace him, without having a third guy who has to learn the code from the very begin (a big waste of time).  

## Collective Code Ownership

The basic idea is that **everyone can modify everyone's code**, in the interest of the greater good to have a correctly working code.

For example, if a system doesn't work and to make it work again i have to directly touch the code written by someone else, i do that for the greater good.

Everyone has responsibility on the whole code: it's not important to know who introduced a bug, instead we have to work as quick as possible to fix it together.

-  Single code base;
- Continuous integration:
- Code like a form of broadcast communication;

The fact that the whole code can be modified at will would be hellish if we wouldn't have a feedback system that allows us to understand what changed between each alteration.

The most used tools that helps us managing that is **Git** and continuous integration techniques. As soon as i apply new changes, those tools helps us testing it that creates problem with the other modules of the application, allowing us to react consequently.

<div allign=center>
    <img src="img/integ.gif" /><br>
    <i>Integration tests: two modules may work great alone, but together they fail the system</i>
</div>

**N.B.** Even if the code base is public, it is still very important to implement *information hiding*! 

## Refactoring

The refactoring, according to Fowler, **is a disciplined technique that can restructure the code**, by altering his internal structure, but maintaining the same semantics. 

Refactoring functions are:

- Rename variables or fields, like the Eclipse's refactoring that let us change a name of a variable in out application without changing the code behavior;
- Factorization of a repetitive code in a method/function;
- Attribute fields in getter and setter methods;
- Removing conditions, replacing them with opportune dynamic links or polymorphism (subclasses)
- Factorization of complex behaviors in superclasses (maybe abstract);

The **"Code-smell"** is a peculiar function that analyze ("sniffs") the code, finding bad coded stuff ("smelly things") like duplicated code, redundancies, and so on. 

**N.B.** More refactoring examples are available [here](https://refactoring.com/catalog/).

## Test-Driven Development (TDD)

This is **NOT a verify technique**, it's the most common mistake, the test-driven development **specifies what we want to obtain** at the end of the software development.

In the TDD, **tests are written before the unit itself**, and serves as a requirement to guide the implementation:

1. Add a test;
2. Run all tests, making sure that the new test will fail;
3. Write an implementation;
4. Run all tests, making sure that they all pass;
5. Do some refactoring (if needed) without breaking the code;
6. Repeat.

The goal is to write code in the simplest possible way in order to pass the written test. Once done, we can do some refactoring expanding the code, increasing it's complexity, like by making it more general.

After the refactoring phase:

- If the test stays **green**, the phase ends, and we can proceed and implement new code.
- If the test becomes **red**, it means something has gone wrong: the changes got reverted and reimplemented until we reach the *green* spot.

Each bug must should be carefully examined and should also become a new test case.

There are some support framework for testing that allow us to implement this technique, for example *JUnit* for Java

```java
public static void main(String[] args)
{
    Calculator calc = new Calculator();
    int sum = calc.evaluate("1+2+3");

    assertEquals(6,sum);    // <-- unit test
}
```

There are also some libraries that let us do some **behavior verification**, with the help of **mock objects**: particular objects that emulate another the behavior of another one.

They are pretty handy for testing, because sometimes can be really inconvenient use the real ones (for example, if we have a software that sends mails, we can't send a mail every time we run a test, so we emulate that behavior with a mock object).

```java
public class OrderInteractionTester extends MockObjectTestCase
{
    public void testFillingRemovesInventoryIfInStock()
    {
        Order order = new Order("Car", 50);

        // Creo oggetto di classe Mock che simula il 
        // funzionamento di un oggetto di classe Warehouse
        Mock warehouseMock = new Mock(Warehouse.class); 

        // Mi aspetto che venga chiamato il metodo hasInventory con
        // parametri "Car" e 50 e che ritorni true
        warehouseMock.expects(once()).method("hasInventory")
            .with(eq("Car"), eq(50))
            .will(returnValue(true));

        // Mi aspetto che venga chiamato il metodo remove con
        // parametri "Car" e 50 e che ritorni true
        warehouseMock.expects(once()).method("remove")
            .with(eq("Car"), eq(50))
            .will(returnValue(true));

        // Uso il mock come proxy (sostituto) per fillare l'ordine
        order.fill((Warehouse) warehouseMock.proxy());

        warehouseMock.verify();
        assertTrue(order.isFilled()); // unit test
    }
}
```

## Velocity Tracking

The velocity tracking is a primary analysis element that can measure the effective progress during the developing process. It was born to answer effort-progress ratio problem.

### The Task Board

The velocity tracking mechanism is often use to trace progress on a **task board**:

![](img/taskboard.png)

Each day some post-its are taken, based on the story we are working on, and they are moved along it:

- When we begin to work on a new story, we move the post-it from **not started** to **in progress**;
- When a story is completed, it is moved to the **done** section;

To measure the effective progress that has been done, we count all the post-its and, from that, we draw a **burndown chart**, which is the ratio between the **ideal tasks remaining**, designed in the planning phase, and the **actual tasks remaining** in our project.

![](img/burndown.png)

After that, we can plan our next sprint to recover if we were not fast enough in the previous one, trying to make up for that delay, in order to keep consistency with the ideal planned line.

The number of days for each burndown iteration is computed as follows: 

​												`num_programmers * week_days * num_sprint_weeks * 1/3`

1/3 means that on each work day, the actual time spent working is 1/3 of the available work time given, because the rest is used in meetings and other activities.

*Example: With six programmers and a sprint of two weeks*

6 * 5 * 2 * 1/3 = 20 days

## More Planning: Agile Shared Time Estimates

Agile methods endorse self-organized team vs a manager that imposes deadlines: it would be terrible if a manager, not part of the team and unaware of its capabilities and difficulties, fixes a deadline (usually super early) to deliver a software, without even giving insight on it. This is why self-organization is key in time estimates for agile development.

To sum up, planning is an important but critical step and it must be optimized as much as possible.

### Key Problems In Time Estimates

- Losing time
  - It's important to minimize as possible the time spent for the discussion, coordination and calculation of the estimates: the stakeholders doesn't pay the team to organize itself but for the software it produces!
- Anchoring effect
  - The Anchoring is a cognitive bias where an individual depends too much on an initial piece of information offered by others when making decisions. 
  - For example, when deciding time estimates, if I say that for the development of a feature it would take little (1 month), the estimates of all the others will be influenced by mine so that they'll be more or less similar ($\pm$ 5-10%). To avoid it's important to avoid talking about concrete times and to express the estimates NOT publicly and subsequently (one after another). 

Under the light of these problems, different methods were born, we'll see the most successful ones.

### Planning Poker

The planning poker is a card-based estimate method that avoids the anchoring problem entirely:

1. Cards are presented
2. The team can ask for questions and clarifications, discuss about assumptions and risks (never talking or mentioning about concrete times)
3. Each member chooses a card representing its estimate without letting it see to the others
4. Everyone contemporary turn their card and show it to the others
5. The ones who have expressed the higher and lower estimates are asked why they chose that estimate and have a minute to answer and convince the others (this is because these are critical and border values, which makes the motivation interesting)
6. Replay until unanimity is achieved (with the help of a moderator if needed)

#### First Step: Comparative Evaluation

In this step, the team is going to compare and estimate how much the user stories are going to be easier or harder.

1. A pile of user stories cards is made 
2. Take the first card and put it in the middle of the table
3. The first member draws a card, reads it loud and puts it 
   - Left to the first card: it is simpler 
   - Under the first card: it is equivalent
   - Right to the first card: it is harder
4. Each next developer can do one of the following per turn
   - Draw another card and position it left, middle, right (like last step)
   - Move a positioned card to another position, explaining the reason (for ex. move it from left to middle)
   - Pass the turn (if there are no cards on the pile and doesn't want to move positioned cards)

This evaluation ends when no more cards are on the pile and nobody wants to move positioned cards. This way we have estimated the relative complexity of each user story.

#### Second step: Quantify Distance

In this step, we are going more in detail to quantify in relative numbers the user stories

1. A scale of values is chosen (Fibonacci, powers of 2, ...)
2. Basing off that scale, we start evaluating from the second value (that's because I could find a simpler job in the next iteration that I didn't consider)
3. Is the next column of cards twice as hard, half as hard or similar to this column? Basing off this answer, assign the appropriate value from the scale

#### Third Step: Absolute Scale

In this last step, we try to set an absolute base value for the simplest card: from that we can deduce the absolute value of all the others basing off the proportion between its relative value (from the scale) and the absolute base value.

Is this step useful? It is, but only in the first iteration, since the next ones will be based off the velocity of the development team.

### Velocity

The velocity is the observed capacity of the development team to complete work. It substitutes the need to remap in absolute values the relative estimates, allowing to compensate for wrong estimates internally to the team.

This must NOT be used as a measure to compare teams or team efficiency in time as this measure fluctuates depending on the complexity of the work the team is facing.

### #NoEstimate

What about not even making estimates? Why bother and lose time for it? Is it really necessary?

An agile movement is proposing to skip time estimates completely and start working immediately in order to avoid losing time on them. Will it work? It really depends on the team. If the team is made of experienced and close-knit members it could work, because they already have an implicit (but raw) time estimate to check if the development is going as planned; otherwise, it's difficult to coordinate a team without user stories.