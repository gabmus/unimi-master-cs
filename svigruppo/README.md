# Git exercises

## Remove `segreti.txt` from all of git repo's history

```bash
git filter-branch --force --index-filter "git rm --cached --ignore-unmatch segreti.txt" --prune-empty --tag-name-filter cat -- --all
git for-each-ref --format="delete %(refname)" refs/original | git update-ref --stdin
git reflog expire --expire=now --all
git gc --prune=now

# to avoid committing the same file in the future
echo "segreti.txt" >> .gitignore
git add .gitignore
git commit -m "added segreti.txt to gitignore"

# in caso ci sia un remote
# git push origin --force --all
# git push origin --force --tags
```

## Do an urgent bugfix with a dirty working directory and index

## Create the repo

```bash
# 2 commits
echo "i am file A" > A
git add A
git commit -m "added A"
echo "i am file B" > B
git add B
git commit -m "added B"

# index != HEAD
echo "i am file WIP" > WIP
git add WIP

# working directory != index
echo "this line hasn't been tracked yet" >> A

# at least 1 untracked file in the working directory
echo "i am not tracked" > NOT_TRACKED
```

## Add an urgent bugfix

**Task**: create a file BUGFIX and commit it, final situation must be indentical to starting except for the new file

### Solution with stash

```bash
git stash
echo "urgent" > BUGFIX
git add BUGFIX
git commit -m "added urgent BUGFIX"
git stash pop
```

### Solution without stash

```bash
git checkout -b mystash
git commit -m "my stashed index stuff"
git commit -am "my stashed working directory stuff"
git checkout master
echo "urgent" > BUGFIX
git add BUGFIX
git commit -m "added urgent BUGFIX"
git checkout mystash
git rebase master
git reset --soft HEAD^
for i in $(git ls-files); do git restore --staged "$i"; done
git reset --soft HEAD^
git checkout master
git branch -D mystash
```
