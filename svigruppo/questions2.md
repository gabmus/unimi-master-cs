- Spiegare la legge di Brooks
 - la legge brooks dice che se un progetto è in ritardo, aggiungere personale lo ritarderà ulteriormente.
   questo accade per 3 motivi principali:
   - il nuovo personale ci metterà del tempo ad essere produttivo
   - i membri già produttivi saranno costretti a rallentare per seguire l'integrazione nel lavoro dei nuovi membri
   - il numero di canali di comunicazione aumenta in modo piu  meno quadratico al numero di lavoratori, questo genererà confusione e rallenterà il lavoro

- sviluppo a cattedrale
 - lo sviluppo a cattedrale è un metodo organizzativo proposto da brooks per risolvere il problema del Tar Pit, consiste in una struttura gerarchica molto rigida in cui i processi decisionali vengono lasciati in mano a pochi membri, mentre il lavoro di implementazione viene affidato a diversi team sottostanti che non dovranno necessariamente comunicare fra loro, comunicheranno infatti solamente con il proprio responsabile.
   In questo modo si riduce lo sforzo comunicativo necessario al team, è inoltre meno costoso intregrare dei nuovi implementatori, di contro se un membro del team decisionale deve abbandonare il lavoro può esser difficile e costoso rimpiazzarlo.
   
- Come funzionano i pacchetti dpkg di DEBIAN
  - i pacchetti dpkg sono utilizzati in alcune distro linux per:
    - contengono un file di configurazione molto completo che racchiude:
     - le dipendenze da installare 
     - i permessi necessari
     - gli script da eseguire per mantenere l'integrità del sistema
     - le priorità
    - rispettano delle policy DEBIAN che garantiscono la compatibilità quasi totale con il parco software linux

- Discutere i possibili usi di Docker utili a facilitare lo sviluppo in gruppi di lavoro complessi.
  - Docker è un software che permette di creare dei container, cioè uno strumento molto più leggere e performanti di una VM, per questo possono esser considerate lo stato dell'arte della Build Automation, quando si deve compilare un software versionato è necessario salvarsi le dipendenze, le varie versioni delle dipendenze, la versione del compilatore ed eventuali plugin, etc... 
   - docker può esser utilizzato per salvare queste informazioni in dei container, in modo che sia veloce ed automatico ricreare lo stato della macchina al momento della compilazione ed esequzione della versione x del software in sviluppo, 
   - inoltre può esser utile per testare la compilazione e esecuzione in determinate configurazioni della macchina
   
- Ruoli team scrum
 - lo scrum è un metodo agile che favorisce lo sviluppo veloce e contiuativo ed una comunicazione costante con i clienti ad una documentazione esaustiva ed una gerarchia rigida.
   un team scrum è diviso in 3 ruoli:
    - **product owner**: è un membro del team che si occupa di comunicare con il cliente, e di creare e proporre le storie da sviluppare nello sprint successivo, si occuma amche di definire il concetto di Done, cioè quando una storia può dirsi completa.
    - **scrum master** : è un membro del team che si occupa anche facilitare il lavoro del team per evitare che si creino atriti interni, inoltre tiene traccia delle performance del team sprint dopo sprint.
    - **team**: il team di sviluppo, di piccole dimensioni stima il costo delle storie per poi svilupparle.
    tendenzialmente non esiste un leader nel team ma alcune implementazioni dello scrum possono avere un team leader che gestisce il team, anche in queato caso il team leader non è al di sopra del team.
    
- Se git incarna l'idea di modello ottimistico o pessimistico
  - git incarna un modello ottimistico poichè permette di lavorare contemporaneamente a un singolo artifact, fornendo supporto per gestire le incompatibilità.
    inoltre garantisce a chiunque ha la repo pieni poteri sulla stessa, contando sul buon senso degli sviluppatori.

- Come funzionano le pull request
  - le pull request sono un meccanismo che permette di regolare gli interventi esterni al team proprietario del progetto, permettono infatti ad uno sviluppatore che non ha i diritti di scrittura sulla repo github di richiedere che la modifica fatta sia integrata nella repo ufficiale, permette inoltre di commentare le modifiche e discuterne in un'area dedicata.

- Cos'è il versionamento semantico
  - il versionamento semantico è un metodo per tenere traccia della versione del software tramite 3 numeri version: MAJOR.MINOR.PATCH :
    - le patch sono piccole correzioni che garantiscono la totale compatibilità tra le versioni
    - le minor version sono piccoli combiamenti ed aggiunte al software che garantiscono comunque la compatibilità
    - le major version sono grandi cambiamenti al software in cui non può esser garantita la compatibilità

- Cosa afferma la legge di Liskov.
  - la legge di liskov garantisce che una sottoclasse di una classe nella programmazione per contratti rispetti la relazione is_a se:
    - le pre-condition dei metodi overridati devono essere uguali o meno stringenti a quelli del metodo padre
    - le post-condition dei metodi overridati devono essere uguali o più stringenti a quelli del metodo padre

- Spiegare il concetto di Velocity.
  - è in metro di valutazione delle performance di un team scrum, utilizza valutazioni e misurazioni interne al team quindi non può esser utilizzato per confrontare i team ma solo da un team per valutare se stessi in uno sprint e tra uno sprint e l'altro

- design by contract
  - è un paradigma di programmazione che vuole fondere design e sviluppo in un solo processo introducendo i contratti come parte integrante del codice:
    - un contratto è un insieme di condizioni da rispettare per definire fatta quella parte del software, si dividono in 3 categorie principali:
      - pre-condizioni : condizioni da rispettare prima di entrare in una metodo, la loro veridicità è a carico dei clienti.
      - post-condizioni: condizioni da rispettare all'uscita da un metodo, la loro verifica è a carico degli sviluppatori.
      - invarianti: condizioni che devono sempre esser valide in un qualunque punto della classe, fanno parte delle pre e post condizioni di tutte le metodo della classe e la loro veridicità è a carico degli sviluppatori
  
- continuos integration ( collegato al configuration management e come viene utilizzato per la CI)
  - è una pratica che vuole rendere l'integrazione del lavoro dei singoli membri del team nel progetto completo, una pratica costante e sfumata nel lavoro gornaliero.
    è stata proposta per combattere la grande difficoltà generata dall'itegrazione di grandi porzioni di codice sviluppate in solitaria dai programmatori, inoltre rende più facile individuare i BUG vista la piccola parte di codice integrata dall'ultima versione stabile. è una pratica resa possibile dalla nascita di software di versionamento come git che permettodo con pochi comandi di caricare il lavoro fatto senza interrompere il flusso di lavoro

- asserzioni
  - le asserzioni sono il primo tentativo di integrare il design nella programmazione, permettono di includere in una porzione di codice delle condizioni che devono essere rispettate nel momento in cui si arriva all'asserzione.

- software configuration management
  - sono una serie di pratiche che vogliono tenere traccia dell'evoluzione del software in sviluppo.
  l'obbiettivo è quello di tenere traccia di tutti gli elememti di cui è rilevante il cambiamento nel software, detti artifact,
  esistono diverse lineen di pensiero su cosa va versionato:
  - sorgente
  - eseguibile
  - entrambi
  tutte le modalità hanno pregi e difetti, ma la più utilizzata è la prima, quindi si tende a versionare tutto ciò che non può essere ottenuto da altri elementi.

- sistemi di build automation
  - durante lo sviluppo software è necessario compilare il prodotto diverse volte, in diverse versioni, da un lato esistono infatti software per monitorare l'evoluzione del software versione dopo versione, questo permette di avere il sorgente vi qualunque versione velocemente.
  Ma per compilare un software non basta il sorgente, ma abbiamo bisogno di diverse dipendenze e software di cui non abbiamo il controllo dell'evoluzione. per risolvere questo prblema sono stati creati software che tendtano di esplicitare le dipendenze sempre più nel dettaglio:
  - make: software che da un makefile in cui è scritta la ricetta permette di compilare velocemente il software, il problema è che le dipendenze in questo caso sono statiche e in molti casi, soprattutto in un progetto di grandi dimensioni non è sufficiente
  - ant: software più avanzato che permette di esplicitare anche le dipendenze esterne di cui non si ha il controllo.
  - gradle: gradle permette di riprodurre l'ambiente di sviluppo in modo molto dettagliato, è in grado di riprodurre anche se stesso a partire da un file di configurazione molto semplice
  - Docker: software che permette di creare e avviare container, costrutti simili a macchine virtuali che non emulano però l'hardware ma tutto il file system di una macchina permettendo di riprodurre fin nei minimi dettagli l'ambiante di build delle diverse versioni.
