# Use After Free Vulnerabilities

Le vulnerabilità Use After Free (UAF) avvengono quando un puntatore che punta a un oggetto deallocato (su cui è stata chiamata la `free()`) viene dereferenziato. In questo modo, un attaccante potrebbe ottenere informazioni riservate da queste locazioni oppure utilizzarle nel tentativo di eseguire del codice malevolo.

Le UAF sono molto difficili da scovare con una revisione del codice manuale, poiché richiedono notevole attenzione e conoscenza dei momenti di allocazione e deallocazione delle variabili durante l'esecuzione del programma. Inoltre la vulnerabilità è **temporanea** poiché esiste solo in un determinato punto del programma in cui esiste il dangling pointer.

2 tipi: sullo heap e sullo stack
Funziona solo su programmi che usano linguaggi che lasciano la gestione della memoria al programmatore: per ragioni di efficienza C e C++ non svolgono controlli sui puntatori deallocati e quindi son vulnerabili a questo tipo di attacco.

Linguaggi invece tipo Java si accorgono del tentativo di utilizzo di un puntatore deallocato (con garbage collector ad esempio) pertanto lanciano una eccezione.

```c
char* retptr() {
    char p ,*q ;
    q = &p ;
    return q ; // sto tentando di ritornare q, ovvero l'indirizzo 
               // della variabile p sullo stack che però verrà
               // deallocata al ritorno della funzione: sto
               // ritornando un indirizzo non valido ed exploitabile
}

int main( ){
    char *a , *b;
    int i ;
    a = malloc(16) ;
    b = a + 5;
    
    free(a) ;
    b[2] = 'c' ; // qui tento di utilizzare un indirizzo non 
                 // derivato da una malloc a sè stante ma basato su `a`(freed): UAF su heap
    b = retptr();
    *b = 'c' ; // qui tento di utilizzare l'indirizzo della ex-variabile 
               // di stack ritornata dalla funzione retptr: UAF su stack
}
```