# Sicurezza Informatica

Il corso tratta dello studio di tipi di attacchi ed exploitation di errori e di vulnerabilità nei programmi.

### Modalità d'esame

- Parte teorica a quiz (1 ora e mezza max)
- Esercizio pratico su computer (3 ore max)

Entrambi vengono svolti nello stesso giorno.

### Esempio : Buffer overflow

Un buffer overflow è un bug che colpisce il codice di basso livello, tipicamente C e C++ perchè questi linguaggi non hanno dei controlli su questi buffer di memoria.

Solitamente il programma crasherebbe con questo bug, tuttavia è possibile sfruttare questa vulnerabilità alterando la situazione eseguendo del codice per: 

- Rubare informazioni private (heartbleed)
- Corrompere informazioni importanti
- Eseguire del codice arbitrario dell'attaccante

Il buffer overflow, nonostante sia un problema vecchio, è ancora un tipo di attacco rilevante perchè è un bug che avviene con regolarità (anche se il programmatore è bravo). 

Inoltre C/C++ sono ancora molto popolari perchè molto efficienti dal punto di vista della gestione della memoria, soprattutto in dispositivi in cui questa qualità è critica, come sistemi embedded e real-time, server web e kernel di sistemi operativi.

Bisognerà trovare soluzioni che proteggano da questo bug ma che mantengano comunque l'efficienza del linguaggio.

### Storia

Il buffer overflow viene dall'attacco nel 1988 del worm  di Morris, codice malevolo autoreplicante e autopropagante che sfruttava appunto questa vulnerabilità per diffondersi.
Causò circa $10-100M di danni al network DARPANET in cui si è diffuso.

Esempi moderni sono rappresentati da 

- Code Red nel 2001, 300k macchine server infettati in 14h
- SQL Slammer nel 2003

### Conclusioni

Quello che faremo è capire come funzionano questi attacchi e come difendersi da essi.

---

## Memory Layout 
Come il programma e i suoi dati sono posti in memoria? Come appare lo stack? Che effetto hanno chiamare e ritornare da funzioni sulla memoria?

### Composizione

Come sappiamo, ogni programma è contenuto in memoria e il suo processo possiede il proprio spazio riservato in essa. Tuttavia il processo vede questa porzione come se fosse tutta la memoria e pertanto crea una serie di indirizzi virtuali ( `0x00000000` a `0xffffffff` ad esempio) che poi verranno tradotti in indirizzi fisici (quelli veri della memoria) dal sistema operativo o dalla CPU.

![StackLayout](StackLayout.png)

Le parti più vulnerabili sono quelle dinamiche (run-time) ovvero heap e stack e pertanto ci concentreremo su di esse.

![HeapStackGrowth](HeapStackGrowth.png)

L'heap e lo stack crescono in direzioni opposte

- Lo stack decresce dagli indirizzi alti verso il basso 
- L'heap cresce dagli indirizzi bassi verso l'alto

### Stack

Quando un programma ha delle chiamate a funzioni, esso pone le informazioni necessarie (come l'activation frame) nello stack.

Lo **stack pointer** punta sempre all'item correntemente **in cima** allo stack. Una operazione di push pre-decrementa (perchè cresce verso il basso) lo stack pointer prima di mettere l'item sullo stack. 

Ogni volta che avviene una chiamata a funzione devo salvare diverse informazioni; per fare ciò le salvo nello stack:

- I dati del chiamante
- Lo stack frame della funzione, ovvero
    - I **parametri** della funzione (in ordine inverso)
    
    - Il valore del registro **`%eip` (instruction pointer)**, in modo da salvare l'indirizzo dell'istruzione (della funzione chiamante) che ha invocato la funzione per poterci ritornare quando la funzione termina 
    
    - Il valore del registro `%esp` (stack pointer) in **`%ebp` (frame pointer)**, in modo da salvare il vecchio stack pointer della funzione chiamante per poterlo recuperare quando la funzione chiamata termina
    
      
    
    - Le **variabili locali** utilizzate al suo interno

*Esempio*: **Disposizione dello stack in seguito alla chiamata a `func`**

![StackExample](StackExample.png)

```c
void func(char* arg1, int arg2, int arg3)
{
    char loc1[4];
    int loc2;
    ...
}
```

### Recap

#### Funzione chiamante (caller):

1. **Pusha i parametri** della funzione chiamata nello stack
   - `push funcArgs`
2. **Pusha l'indirizzo di ritorno** per quando la funzione chiamata termina (`%eip`)
   - `push (old %eip)`
3. **Jump all'indirizzo della funzione chiamata**
   - `%eip = funcAddress`

#### Funzione chiamata (callee):

4. Pusha il frame pointer del chiamante (old `%ebp`) nello stack per poterlo recuperare quando la funzione chiamata termina
   - `push (old %ebp)`
5. Setta il frame pointer di questa funzione (`%ebp`) allo stack pointer corrente (`%esp`), il quale mi permette di localizzare a runtime gli indirizzi delle variabili locali, dei parametri della funzione e l' `%eip` a cui tornare a fine funzione basandosi su degli offset di `%ebp`
   - `%ebp = %esp`
6. Pusha le variabili locali sullo stack
   - `push localVars`
   - N.B. Le variabili locali vengono pushate in questo ordine
      1. Puntatori alle aree di memoria contigua (char\*, int\*, ...)
      2. Variabili "primitive" locali (int, float, ...)
      3. Aree di memoria contigua (char[], int[], ...)

#### Ritorno da callee a caller:

7. Pop delle variabili locali
   - `pop localVars`
8. Leggo il vecchio frame pointer che avevo salvato sullo stack e lo ri-setto
   - `%ebp = (old %ebp)`
9. Jumpa all'indirizzo di ritorno `%eip` salvato sullo stack
   - `%eip = (old %eip)`
10. Pop dei parametri
    - `pop funcArgs`

---

## Buffer overflow

Il buffer è una memoria continua associata a una variabile o un campo. Molto comune con le stringhe in C (che sono array di char terminati da \0 aka NUL)

L'overflow è uno stato del buffer che si verifica quando viene inserito più di quanto il buffer può supportare (quindi più della dimensione supportata e allocata).

*esempio*

```c
void func(char *arg1)
{
    int authenticated = 0;
    char buffer[4];
    strcpy(buffer, arg1);
    if (authenticated)
        ...
}

int main()
{
    char *mystr = "AuthMe!";
    func(mystr);
}
```

E' evidente che sto tentando di copiare la stringa "AuthMe!" di 8 caratteri in un buffer da 4 (overflow).
In questo modo, invece che crashare il programma, si riesce a sovrascrivere il resto del buffer nella variabile locale `authenthicated`.

![AuthBOF](AuthBOF.png)

Questo può non sembrare un problema, ma in realtà è gravissimo perchè:

- Espone contenuti sensibili
- Permette di sovrascrivere tutto il contenuto dello stack (ANCHE IL RETURN ADDRESS `%eip`  = posso saltare dove voglio e controllare il flusso del programma)

Infatti come nell'esempio, sovrascrivendo la variabile `authenticathed` essa diventa diversa da zero e quindi sono autenticato anche se non dovrei!

Questo tipo di attacco basato sul buffer overflow viene anche definito "stack smashing". Come vedremo in futuro l'user input è intrinsicamente causa di problemi, volontari (attaccanti) o meno: è necessario quindi adottare precauzioni e validare l'input prima di utilizzarlo (soprattutto in funzioni vulnerabili come `strcpy` e `gets`)

Un tipo di protezione implementabile, detta *"canarino"*, inserisce un valore tra return address e le variabili locali: se il valore *"canarino"* cambia viene detectato lo "stack smashing". Sarà sufficiente?

## Code injection

Il fatto che in Intel tutto il codice leggibile nello stack è anche eseguibile ha portato alla creazione di un nuovo attacco: la code injection.

Essa consiste nell'inserire del codice malevolo all'interno del buffer in qualche modo e farlo puntare ad `%eip` (l'instruction pointer) in modo da farlo eseguire.

Il problema è conoscere effettivamente l'indirizzo del buffer nello stack in modo da sapere da dove iniziare a copiare il codice all'interno. Può essere calcolato un range ma è comunque difficile ottenerlo in modo preciso.

Una strategia è la nop-sled, ovvero scrivo una serie di nop (no operation 0x90), ovvero una istruzione da 1 byte, fino a quando non sono nel buffer.

Inoltre non so come far fare eseguire il mio codice perchè non so dove viene caricato.

1. Distanza da sovrascrivere (grandezza del debuffer)
   - Si ottiene sovrascrivendo a tentativi byte a byte o con ricerca dicotomica fino a che crasha: allora so la sua capienza

2. Codice
3. Indirizzo del buffer













































