# Z3 : an efficient SAT solver

## Il SAT problem

Un problema SAT è traducibile in una formula proposizionale composta da variabili booleane connesse da operatori booleani (and, or, not ...)

*Esempio*:

$` (a \lor b) \land (b \land c) \land (d \land c) `$

I problemi SAT sono quelli che dobbiamo risolvere in seguito a una analisi della symbolic execution di un programma per risolvere le path conditions, ad esempio.

Se la formula è soddisfacibile, allora c'è un particolare assegnamento delle variabili che la rende vera (SATisfied).

La soluzione della formula è basata su truth tables: purtroppo trovare la soluzione su una truth table è di complessità esponenziale rispetto al numero delle variabili O(2^n); pertanto SAT è un problema NP-Completo.

### SMT Theories

SMT sta per **Satisfiability Modulo Theories** ed è un estenzione di SAT con altre teorie. SMT aggiunge dei livelli astratti per gestire formule più complicate.
Le teorie aggiunte a SMT sono ad esempio la teoria degli interi, dei reali, degli array, dei tipi di dato, dei vettori di bit e dei puntatori. 
L'SMT può essere vista come una logica del primo ordine. 

Lo Z3 è un Theorem Prover.

### Z3 SAT/SMT Solver (Microsoft)

![](z3satsolv.png)

...

Esempio:

poniamo di voler sommare 13 = 01101 e 7 = 00111

possiamo definire la funzione di somma binaria come segue

$` cos_1 = d_i \iff ( a_i \iff (b_i \iff c_i)) `$

possiamo poi efinire l'operazione di riporto come segue

$` cos_2 = c_{i - 1} \iff ((a_i \land b_i) \lor (a_i \land c_i) \lor (b_i \land c_i)) `$

In modo da ottenere la formula finale con gli addendi

$` cos_1 \land \cos_2 \land (\lnot a_1 \land a_2 \land a_3 \land \lnot a_4 \land a_5) \land (\lnot b_1 \land \lnot b_2 \land b_3 \land b_4 \land b_5) `$

con $` a_i `$ = cifre binarie del numero 13, $` b_i `$ = cifre binarie del numero 7

## Esercizi bof + sat solver

Quando tenterò di fare l'esercizio, devo usare la soluzione del sat solver per arrivare alla funzione vulnerabile e poi exploitarla come al solito
