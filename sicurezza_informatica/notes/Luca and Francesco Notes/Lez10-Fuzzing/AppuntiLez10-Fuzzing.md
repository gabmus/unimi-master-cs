## Cos'è il Fuzzing

Il Fuzz testing è praticamente una sorta di **random testing** i cui input per i casi di test sono generati randomicamente o semi randomicamente. 

L'obiettivo del fuzz testing è essere sicuro che non accadano *bad things*  (ovvero crash del programma, lanci di eccezioni o non terminazione). Come abbiamo visto spesso gli errori di memoria corrispondono a vulnerabilità sfruttabili da attaccanti.
Anche la non terminazione è un problema di sicurezza poichè può creare *Denial of Service (DOS) Attack*. 

Il fuzz testing può essere uno strumento utile ma limitato (e.g. Non può sapere quando il nostro programma produce un output sbagliato, che è anch'esso una *bad thing*, perchè non può sapere quale dovrebbe essere l'output del nostro test ma solo che non devono crashare, o non terminare). E' quindi un'attività complementare ma non rimpiazza del tutto i tradizionali test funzionali del codice.

## Tipi di Fuzzing

Ci sono tre tipologie principali di fuzz testing:

- **Black box fuzzing**: il testing tool non sa nulla del programma o del formato degli input e genera input random da dare in pasto al programma. E' semplice da usare ma esplora solitamente pochi stati del programma tranne se è molto fortunato.
- **Grammar base fuzzing**: Il tool genera input avendo una grammatica di supporto che descrive il formato dell'input atteso dal programma target. Ovviamente è più difficile da usare (bisogna anche produrre la grammatica), ma può andare più a fondo nello spazio degli stati del programma andando quindi a coprire più parti del programma.
- **White box**: Il tool genera nuovi input almeno parzialmente controllati dal codice del programma target.  Questi tool sono solitamente semplici da usare (il tool guarda il codice del programma e decide da solo quali input generare per andare in parti differenti del codice del programma target) ma possono essere costosi computazionalmente (perchè utilizzano spesso cose come **dimostratori di teoremi**). 

## Fuzzing inputs

Ci sono modi differenti per cui un tool di fuzzing genera input da passare al programma target. 

- **Mutation**: Il modo più comune per passare input al programma. Prende un input legale e lo muta, usandolo come nuovo input. (l'input legale può essere prodotto dall'uomo o da un test funzionale scritto dal programmatore o automatico da una grammatica). La mutazione può essere forzata per aderire a una grammatica.
- **Generational**: Generare l'input da zero, randomicamente o avendo una grammatica.
- **Combinations**: Combinazioni delle due precedenti (applicazioni successive).


Ci sono due modi fondamentali di usare i fuzzers. La prima è basandosi su file, la seconda su networks.

## File - based Fuzzing

Il fuzzer tool crea nuovo input (mutandolo o generandolo), quindi esegue il programma con quell'input e vede cosa succede. 

*Visualizzazione del Processo basato su file:*

![](filebased.jpg)

Esempi:

- **Radamsa**: di tipo black box basato su mutazioni (come input diamo la stringa insieme a un seme random e al numero di mutazioni da generare)

  ![](radamsa.jpg)

- **Blab**: fuzzer basato su grammatiche, che viene specificata dall'utente come una regex nelle grammatiche context-free.

  ![](blab.jpg)

- **American Fuzzy Lab**: E' un fuzzer di tipo white-box basato su mutazioni. Esegue i seguenti passi:

  - Viene strumentato il programma target per ottenere informazioni run-time (memorizzando delle tuple di inizio e fine dei blocchi che indicano dove l'esecuzione del programma avviene).

  - Viene eseguito il test e mutiamo l'input del test per generare un nuovo test solo se viene generata una tupla ancora non vista da quel test, altrimenti lo scartiamo (perchè non è utile a raggiungere nuove parti del codice)

  - Chiama periodicamente i test raccolti per non bloccarsi sui minimi locali (ciò non fare cambiamenti piccoli locali, ma cambiamenti grandi globali per andare da altre parti dello spazio degli stati del codice)

    ![](aflab.jpg)

- **SAGE**: Utilizza la symbolic execution come la sua tecnologia per generare test. E' anch'esso white-box.

  ## Network - based Fuzzing

Sono quei fuzzer che si basano sulla comunicazione tramite rete. Ne esistono di due tipi:

- 1° Tipo: **Agisce come un'agente della coppia di comunicazione**, impostando messaggi da e per un programma target cercando di farlo crashare. Gli input possono essere prodotti mutando una precedente interazione o producendo una interazione da zero (con l'uso di una grammatica)

  ![](networkbased.jpg)

- 2° Tipo: **Agisce come man in the middle**, mutando gli input che si scambiano le due entità (magari utilizzando una grammatica) con l'obiettivo di far crashare il programma.

  ![](maninthemiddle.jpg)

Vediamo alcuni esempi di programmi concreti:

- Esempio 1: **SPIKE**.  E' un *fuzzer creation kit* e fornisce delle API in C per programmare fuzzers che interagiscono con server remoti utilizzando protocolli di rete.
  Il programmatore utilizza SPIKE creando una serie di blocchi che formano parte dei messaggi del protocollo e lasciando dei "buchi" in questi blocchi che verranno successivamente riempiti dal fuzzer per generare l'input.
- Esempio 2: **Burp Intruder**. E' un'elemento della suite di programmi Burp. Esso permette di customizzare attacchi in maniera automatica per le applicazioni web. In maniera simile a SPIKE permette all'utente di craftare il template dei messaggi e lasciare dei buchi (chiamati payloads) per le operazioni di fuzzing. Inoltre, a differenza di SPIKE che è una API, Burp Intruder ci da una GUI da poter utilizzare, oltre che tutti gli altri programmi della suite che forniscono proxy, scanner, spider e altre robe fighe. 

## Dealing with crashes

Una volta utilizzato il fuzzer, quindi, avviene un crash. Quindi ci poniamo alcune domande, ovvero:

- Qual'è la **causa scatenante**? (in modo da poterla fixare)

  - (Sotto domanda) C'è un modo di **rendere l'input più corto** in modo da essere più comprensibile?

    Alcuni tool ci danno un'aiuto a trovare alcune risposte a queste domande, ad esempio cercando di rendere l'input più corto automaticamente. 

  - (Sotto domanda) Ci sono **due o più crash** che stanno segnalando lo stesso bug

- Questi crash sono rilevanti per la sicurezza? Cioè segnalano una **vulnerabilità sfruttabile**?

  - Avere un NULL pointer è raramente exploitabile a confronto con il buffer overflow.

## Finding memory errors with fuzzing

Gli errori di memoria sono particolarmente difficili da trovare dopo aver fatto fuzzing perchè gli effetti di un memory error potrebbero verificarsi molto dopo dove originariamente è avvenuto l'errore. (E.g. se vado oltre le dimensioni del buffer, il programma non crasha subito ma solo nel momento in cui chiamo il buffer con un indice più grande). 

- Un modo per far sì che i crash accadano immediatamente dopo aver sovrascritto un buffers è utilizzando un **address sanitizer** (ASAN). Questo è un tool (per compilare) che instrumenta il programma in modo che l'accesso ad un array controlla overflow e user after free errors. 
  Quindi possiamo prima compilarlo con questo tool, poi fare fuzzing e quindi controllare se il programma è crashato con un errore segnalato dall'ASAN.

Inoltre possiamo utilizzare questo giochetto per altri tipi di errori.

## Altri fuzzer noti

- **CERT Basic Fuzzing Framework (BFF)** ha trovato bugs in Adobe Reader, Flash Player, Apple Preview e Quick Time. 
- **Sulley (Google)**. Fa tante cose fighe utilizzando la rete, tipo facendo fuzzing in parallelo, categorizzando ogni tipo di fault trovato, ecc. 

## Summary

Il penetration testing simula attacchi reali cercando di trovare vulnerabilità exploitabili in sistemi completi. 

Il penetration testing segnala **problemi reali** (ma ovviamente il fatto che il penetration testing non porti a nessun risultato non significa che non ci potrebbero essere problemi di sicurezza).

Il penetration testing usa una varietà di tools (**Scanners, proxies, exploit injectors, fuzzers**) ma questo non deve comunque rimpiazzare il testing funzionale normale dei programmatori. 

