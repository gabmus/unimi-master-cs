# Low Level Defense

Abbiamo visto come nei low level attack si sfruttano vulnerabilità di funzioni e di codice per poter iniettare, controllare e sovrascrivere determinate zone di memoria (su heap e stack) per vari scopi (eseguire codice custom cambiando il flusso del programma, spoofing, data stealing ecc...)

## Channel problem

Il ***channel problem*** è un problema di vulnerabilità per cui metadati di controllo e dati del programma sono presenti nello stesso canale (heap o stack): nel canale heap ho i metadati subito prima dei dati (in un chunk ho `prev_size` e `size` prima dello spazio utente), mentre nel canale stack ho il return address(`%eip`) insieme ai dati. Questo rende i metadati aperti a problemi gravi di vulnerabilità perchè come abbiamo visto è possibile sovrascriverli con degli overflow.

Pertanto per poterci difendere da questi attacchi possiamo applicare procedure di **memory safety** e **type safety** oltrechè **difese automatiche** come i canarini degli stack e la randomizzazione degli indirizzi.

Questi ultimi metodi non sono mai efficaci al 100% ma rendono solo molto più difficile sfruttare queste vulnerabilità per attaccanti poco esperti riducendo la memory surface attaccabile. 

La miglior difesa da questi problemi rimane una programmazione responsabile e sicura da parte dei programmatori (**secure coding**)

## Memory Safety

I low-level attacks sono permessi da una mancanza di **memory safety**, ovvero una qualità per cui un programma:

1. Permette la creazione di puntatori **solo** tramite metodi standard
   - es. Usando `p = malloc(...)`, `p = &x` ...
2. Permette a un puntatore di accedere **solamente** alla memoria che appartiene a esso
   - es. Non posso usare un puntatore ottenuto da `malloc()` per andare a puntare qualcosa nello stack o altri indirizzi arbitrari: può solo accedere alla sua area di memoria dedicata dalla `malloc()`

Per raggiungere questo scopo, ho combinato due ideali di safety

1. **Temporal safety**

2. **Spatial safety**

Se un linguaggio è memory safe allora tutti i possibili programmi compilati di quel linguaggio saranno memory safe. Molti linguaggi sono memory safe (Java, Python, Ruby ...) ma anche type-safe (ancora più sicuro).

Nonostante tutti i problemi di safety siano risolti da questi linguaggi, il C/C++ sono comunque dei linguaggi fondamentali utilizzati a livello globale per la loro estrema performance. Sono stati ideati diversi framework per renderli sicuri tentando di mantenere le performance il più possibile inalterate (CCured, Softbound/CETS, Intel MPX).

### Temporal safety

Una violazione di temporal safety avviene quando si cerca di ***accedere una zona di memoria indefinita*** (undefined), ovvero non inizializzate, non allocate o deallocate in precedenza.

*Esempio*: Accesso a un "freed" pointer (Use After Free)

```c
int* p = malloc(sizeof(int));
*p = 5;
free(p);
printf("%d\n",*p)        //UAF violation: p was freed so that memory no longer belong to p
```

*Esempio*: Accesso a un "uninitialized" pointer

```c
int* p;
*p = 5;    //uninitialized pointer violation
```

Come abbiamo osservato, una delle questioni che la temporal safety cerca di addressare è la Use-After-Free, la quale sfrutta i dangling pointer, ovvero puntatori su cui è stata svolta la `free()` , e cerca di accedere a quella memoria.

Una tecnica di secure coding utile in questi casi è **settare il puntatore** su cui è stata svolta la free **a zero** (o NULL). In questo modo è impossibile riutilizzarlo, anche in caso di non memory safety.

```c
int x = 5;
int* px = &x;
...
free(p);
p = 0;    //azzero il puntatore in modo da evitare UAFs
```

### Spatial safety

Una violazione di spatial safety, invece, avviene quando si tenta di accedere tramite un puntatore a un area di memoria che non appartiene a esso o a cui non è stato assegnato (ad esempio quando si tenta di accedere a della memoria successiva a quella accessibile a un puntatore compiendo degli overflow)

Per implementare la **spatial safety** possiamo pensare ai puntatori come triple (p, b, e) in cui

- **p** è il puntatore attuale all'indirizzo in memoria (**pointer**)

- **b** è la base della regione di memoria a cui può accedere p (**base**)

- **e** è il limite della regione di memoria a cui può accedere p (**extent**)

L'accesso alla memoria è possibile solo se   
$$
b \le p \le e - sizeof(typeof( p ))
$$
Pertanto `p` deve essere compreso fra il primo indirizzo `b` e l'ultimo indirizzo prima di `e`.

Le operazioni aritmetiche dei puntatori alterano solamente `p`, lasciando ovviamente `b` ed `e` inviariati. Inoltre `e` dipende sempre dal tipo di `p`.

Queste triple sono definite come **fat pointer**, perchè usano `b` ed `e` come metadati per la safety. 

*esempio*

```c
int x;            // assume sizeof(int) == 4
int* y = &x;    // p = &x, b = &x, e = &x + 4
int* z = y+1;    // p = &x + 4 , b = &x, e = &x + 4
*y = 3;            // OK:  &x <= &x      <= (&x + 4) - 4
*z = 3;            // BAD: &x <= &x + 4 </= (&x + 4) - 4 | p è maggiore di e: op. non valida 
```

Il controllo in questo caso viene svolto quando tento di dereferenziare il puntatore `z`: la condizione non è verificata perchè 

$$
p \nleq e-sizeof(int)
$$

infatti

$$
(\&x + 4) \nleq (\&x + 4) - 4
$$

Un buffer overflow violerebbe la regola base della spatial safety perchè tenterebbe di andare oltre il bound prestabilito da `e` per il puntatore p, pertanto diventa facile da rilevare e da interrompere. Tuttavia gestire triple di fat pointers pesa molto in termini di performance.

### Summary

Ricapitolando, possiamo vedere come

- La **spatial safety** controlla che l'accesso alla memoria sia in una regione legale
  - Contrasta gli overflow
- La **temporal safety** controlla che quella regione sia ancora in gioco (ovvero allocata e attiva)
  - Contrasta i double `free()`, use after `free()` ...

## Type Safety

La filosofia alla base della type safety è che ogni oggetto in un programma dovrebbe essere inscritto in un tipo (int, pointer to int, pointer to function ... ). In questo modo, le operazioni su un oggetto sono sempre compatibili con il loro tipo, evitando comportamenti non definiti (**undefined behaviour**). Vengono definiti undefined proprio perchè potrebbe succedere qualsiasi cosa (spesso in vantaggio di un attaccante) quando si verificano.

Ad esempio, quando in C exploitiamo un buffer overflow, stiamo di fatto sfruttando un undefined behaviour che si verifica quando tento di scrivere in un buffer più di quanto esso possa contenere; partendo dall'overflow posso far comportare il programma in diversi modi (change code flow, code injection, ...) 

La type safety è più forte della memory safety perchè evita ai programmi di svolgere codice che porta a crash di programmi.

*Esempio*: Un casting di un puntatore a intero -> puntatore a funzione

```c
int (*cmp)(char*, char*);
int *p = (int*)malloc(sizeof(int));
*p = 1;
cmp = (int (*)(char*, char*))p;
cmp("hello","bye"); // crash! memory safe but not type safe
```

Il casting è perfettamente lecito dal punto di vista della memory safety, infatti rispetterebbe la formula e i parametri dei fat pointer. Tuttavia usa la memoria nel modo sbagliato, interpretando un tipo come se fosse un altro: potrebbe dare origine a un undefined behaviour.

### Enforce invariants

La type safety viene spesso usata per garantire le invarianti, ovvero variabili che mi permettono di controllare alcune proprietà sul flusso di dati in un programma.

Il flusso dati (data flow) di una variabile è il percorso che svolge lungo tutto il programma (quali strutture di controllo passa, come cambia, quali operazioni sono svolte su di essa ... ). Con l'analisi del flusso di dati possiamo quindi controllare come essa varia.

La type safety è più forte e sicura della memory safety, ma è anche molto costosa dal punto di vista computazionale. 

La type safety può essere garantita a livello statico o a livello dinamico.

### Type safety statica

Nei linguaggi con tipizzazione statica viene stabilito direttamente nel codice sorgente dove viene assegnato esplicitamente per mezzo di parole chiave apposite, come ad esempio `int, long, float, char`.

In questo modo è possibile verificare direttamente in compilazione la type safety, controllando che tutti i valori inseriti nelle variabili siano del tipo corrispondente.

### Type safety dinamica

Nei linguaggi a tipizzazione dinamica, invece, la dichiarazione del tipo non è richiesta alla creazione della variabile: questo perchè ogni oggetto del linguaggio ha un unico tipo "Dinamico", pertanto ogni operazione è permessa ma potrebbe non essere implementata. 

Essenzialmente verrebbe svolta una coercizione di tipo e in caso non fosse compatibile verrebbe lanciata un eccezione; un esito sfortunato ma preferibile a un comportamento indefinito.

### Type confusion

Sono dei tipi di attacchi che cercano di eludere la type safety facendo cambiare dei bit in modo da far puntare a un puntatore un tipo diverso da quello compatibile (es. far puntare un int* a un struct*). Molti di questi attacchi sono svolti con attacchi rowhammer, ovvero attacchi elettromagnetici, di calore o di programmi appositi che sovraccaricano la memoria con l'obiettivo di alterare la memoria RAM fisica per indurre errori di lettura/scrittura e confondere i tipi in maniera deterministica.

### Type safety su C/C++ fattibile?

Generalmente C/C++ sono scelti per ragioni di performance e di controllo come gestire la memoria e gli oggetti del programma manualmente e interagire con hardware di basso livello. 

Applicare a questi linguaggi concetti di type safety sarebbe overkill per la loro funzione in quanto aggiungerebbero troppo overhead, basti pensare a quanto i type enforcement siano computazionalmente dispendiosi:

- **Garbage collection**
  - ***Pro***: evita violazioni temporali
  - ***Contro***: anche se veloce come `malloc()`/`free()` occuperebbe molta più memoria
- **Fat pointers e null-pointer checks**
  - ***Pro***: evitano violazioni spaziali
  - ***Contro***: Aggiungono overhead su memoria e tempi
- **Nascondere come i tipi sono rappresentati** (in memoria) 
  - ***Pro***: permette di forzare invarianti utili alla privacy e sicurezza dei dati
  - ***Contro***: potrebbe inibire ottimizzazioni cruciali

---

## Difese automatiche e debugging

Fino a quando la memoria in C sarà safe cosa fare per difenderci dai low level attacks?

- **Rendere il bug difficile da sfruttare**
  
  - Studiare i passi necessari per riprodurre il bug dell'exploit e rendere ognuno di essi il più possibile difficile o impossibile da portare a termine

- **Evitare il bug** 
  
  - Con pratiche di secure coding
  
  - Con revisione del codice estesa e avanzata con tools di analisi

Queste strategie sono **complementari**: è sempre bene evitare bugs, ma abilitare alcune di queste protezioni è sempre saggio.

### Avoiding stack smashing

Ricordiamo gli step di un attacco BOF su stack:

1. Putting attacker code into the memory (no special chars like \0, \n, CR, LF)
2. Getting %eip to point and run attacker code
3. Finding the return address

Vogliamo rendere questi step più difficili da svolgere: complicare l'exploit cambiando le librerie, il compilatore o l'OS può essere utile.

#### Difesa 1 : Canarino

Questa tecnica veniva usata nelle miniere per vedere quanto fossero sicure (ovvero prive di gas nocivi) per i minatori: essi portavano un canarino nella miniera e, se moriva, significava che l'area non era sicura.

Allo stesso modo, è possibile piazzare un "canarino", ovvero un valore arbitrario, a una determinata posizione fra i metadati dello stack.

Se leggendo la sua posizione in memoria, a fine funzione, trovo che il valore del canarino è cambiato e non è quello che ci aspettavamo, allora è in corso un probabile stack overflow e il programma viene terminato per stack smashing.

In ogni funzione del programma viene aggiunto del codice per aggiungere il canarino all'inizio dello stack e per controllarlo a fine funzione.

```pseudocode
f1(args)
    x = create_random_canary()
    push x on stack
    save_canary_in_memory(x)

    ... 
    <rest_of_function f1>
    ...

    compare(actual_canary, x)
    ret
```

Ma quale valore utilizzare per il canarino? Ci sono diverse implementazioni:

- **Terminator canaries** (CR, LF, NUL ...)
  - Usa valori che non sono possibili da inserire con funzioni tipo scanf, gets, ... perchè una volta letti interrompono il flusso di input (e quindi di overflow)
- **Random canaries**
  - Scrive un nuovo valore random quando il processo inizia
  - Salva il valore reale da qualche parte in memoria
  - Protegge il valore reale da scritture (rendendolo read-only)
- **Random XOR canaries**
  - Come i canarini random normali ma salvano maschere di XOR sulle info di controllo

Questa tecnica protegge dallo step 1. dello stack smashing, ovvero "Putting attacker code into the memory".

#### Difesa 2 : Rendere il codice in stack e heap non eseguibili

Definire delle policy per stabilire che tutto ciò che c'è in stack e heap sono dati e non codice eseguibili rende lo stack smashing (come quello per lo shellcode) inutile, anche se fossero bypassati i canarini: se si tentasse di eseguire del codice si avrebbe una eccezione e il processo terminerebbe.

Questa tecnica viene chiamata Data Execution Prevention (DEP).

#### Difesa 3 : Randomizzazione del layout dello spazio di indirizzamento

Purtroppo la possibilità di non eseguire più lo shellcode dai dati non basta a fermare questi attacchi, infatti potrei sostituire al return address una funzione di libc che chiama `exec()` con /bin/sh come args e aggirare la non eseguibilità (tecnica di Return Oriented Programming ROP che vedremo in seguito).

Pertanto per risolvere questo nuovo problema vogliamo rendere molto più difficile indovinare dove si trovi il return address all'interno dello stack.

Per farlo si utilizza la Address-space Layout Randomization (ASLR), la quale **randomizza la posizione dei segmenti di memoria** (Stack, Heap, Text, BSS ...) eseguendo una nuova permutazione a ogni esecuzione del programma.

Per trovare l'indirizzo del buffer, del ret address o delle funzioni system di libc diventa quindi molto più difficile.

Anche qui, non ho un meccanismo di difesa perfetto, soprattutto se utilizzo macchine a 32 bit: purtroppo è stato dimostrato che non potrei avere una randomness sufficiente a contrastare attacchi di tipo bruteforce.

Se non fosse abilitato il DEP, potrei svolgere uno **spray attack**, ovvero potrei riempire lo shellcode su tutta la memoria del processo, cercare di saltare a un indirizzo casuale e sperare di trovare uno shellcode.
