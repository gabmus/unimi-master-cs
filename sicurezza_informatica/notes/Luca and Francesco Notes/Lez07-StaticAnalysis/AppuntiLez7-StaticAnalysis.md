# Static Analysis

## Pratiche correnti di QA e code analysis

La pratica corrente di controllo e di quality assurance del software prevede due standard: il testing automatico e il code auditing.

### Testing

Il testing mi permette di assicurarmi che il programma funzioni correttamente dato un set di input. 

![](testing.png)

Dati gli input al programma e ottenuti gli output, possiamo verificare la loro correttezza tramite un "oracolo" costruito dal tester (ad esempio una test suite).

**Pro**: I possibili fallimenti trovati dimostrano falle e problemi nel sistema e aiutano a fixarli

**Contro**: Testare un programma molto complesso e con molti input aumenta esponenzialmente i tempi di verifica (è una operazione combinatoria, sostanzialmente), rendendo difficile coprire tutti i possibili code paths e, di conseguenza, non dando garanzie nette.

Il problema di non poter testare tutti i code path possibili e verificarli è grave, poichè basterebbe anche uno solo di quei code path non verificati a permettere a un attaccante di exploitare un determinato bug e causare danni. 

Possiamo concludere come il testing (come inteso qui) non sia sufficiente.

### Code auditing

Il code auditing invece è un controllo manuale svolto da un altra persona, la quale tenta di verificare se il tuo codice è corretto.

![](codeaudit.png)

**Pro**: Il fatto che un'altra persona stia controllando il tuo codice rende molto più semplice scovare bugs rispetto al solo testing in quanto gli esseri umani sono in grado di ***astrarre dal codice*** e generalizzare al di sopra delle singole esecuzioni.

**Contro**: Utilizzare persone per controllare il codice non è sempre la scelta migliore, in quanto alcuni bug possono rimanere nascosti (errore umano / disattenzione): assoldare un umano, inoltre, è costoso; anche in questo caso non ho garanzie.

Possiamo vedere che anche combinando **code auditing** e **testing**, non ho comunque garanzie di avere un programma che funzioni il 100% delle volte e sappiamo che un attaccante cerca di sfruttare tutti i possibili bug del programma per exploitarli.

Come possiamo correre ai ripari?

## Cos'è la static analysis

Questa tecnica permette di **analizzare il codice di un programma senza eseguirlo** (quindi senza necessitare di input esterni, ambiente di esecuzione ecc...). Svolge circa lo stesso lavoro di un umano durante una code review.

**Pro**:

- Riesce a individuare numerose possibili esecuzioni del programma (a volte tutte, fornendo una **garanzia**)
- Riesce ad accorgersi di programmi incompleti (es. Librerie)

**Contro**:

- Può analizzare solo proprietà limitate
- Può non riuscire a individuare alcuni errori o avere falsi allarmi (dipende dal livello di significatività per true negatives o false positives)
- Può essere time consuming su programmi più grandi

Nonostante questi drawbacks, l'analisi statica aiuta molto gli sviluppatori perchè, occupandosi di ***controllare queste proprietà limitate ma fondamentali***, permette loro di **concentrarsi su problemi più complessi** aiutandoli, al tempo stesso, a non compiere più queste tipologie di errori e **incoraggiando migliori pratiche di sviluppo** (es. annotazioni).

### Rember the halting problem

Come sappiamo dall'Halting Problem, un analizzatore non è in grado di capire se, dato un programma P e un input I, P riesca a terminare poichè è un **problema indecidibile**. Pertanto dobbiamo concentrare l'analisi statica su proprietà fattibili e NON riducibili all'halting problem (se riesco a ridurre un problema all'halting problem, allora anch'esso è indecidibile).

L'analisi statica perfetta quindi non è possibile, ma anche da imperfetta è molto utile. Alcune delle imperfezioni sono:

- Non-terminazione (l'analizzatore non termina con problemi indecidibili)
- Falsi allarmi (false positives, ovvero errori trovati dall'analizzatore che NON sono veri errori)
- Errori mancati (true negatives, ovvero che l'analizzatore non riporta errori anche se ce ne sono)

## Proprietà degli analizzatori

Escludendo la non-terminazione, possiamo classificare gli analizzatori in base alle proprietà dei **falsi allarmi** e degli **errori mancati**:

- **Soundness** *"Se l'analisi dice che X è vero, allora X è vero"*
  - Se il programma è definito dall'analizzatore error-free, allora lo è veramente (gli allarmi non implicano l'erroneità). 
  - *Dico solo cose vere, ma non le dico tutte*
- **Completeness** *"Se X è vero, allora l'analisi dirà che X è vero"*
  - Se il programma è definito dall'analizzatore errato, allora lo è veramente (il silenzio non implica l'assenza di errori)
  - *Dico molte cose, alcune di queste sono vere*

![](soundcomplete.png)

Gli analizzatori di oggi sono più "soundy" che complete anche perchè è più difficile da implementare la completezza (anche perchè si scontra con l'halting problem!)

## Arte dell'analisi statica

Nel tentativo di creare tool di analisi utili, bisogna considerare i diversi tradeoff nel design dell'analizzatore:

- **Precisione**: un tool preciso cerca di modellare e imitare il più possibile il comportamento del programma in modo da minimizzare i falsi allarmi
- **Scalabilità**: un tool scalabile riesce ad analizzare efficacemente ed efficientemente anche programmi molto grandi e complessi
- **Comprensibilità**: un tool comprensibile è in grado di fornire report degli errori facili da interpretare per gli sviluppatori in modo che possano agire subito e nel modo corretto

Il code style è importante in questo campo: del codice difficile da comprendere per un umano, sarà difficile da comprendere anche per un analizzatore. Pertanto, per evitare rallentamenti o falsi allarmi nelle analisi, conviene scrivere codice non troppo convoluto per la sanità mentale propria e dell'analizzatore.
