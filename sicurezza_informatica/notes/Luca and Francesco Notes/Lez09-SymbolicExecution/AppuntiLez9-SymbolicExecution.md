# Symbolic execution

Come sappiamo, il software è estremamente prono a bugs, e per trovarli è necessario un estensivo testing e code review. Tuttavia questo non basta, infatti alcuni bugs non vengono trovati perchè 

- in feature raramente usate
- in circostanze particolari
- a causa del non-determinismo

A questo proposito ci viene incontro la **static analysis**, la quale permette di analizzare tutti le possibili esecuzioni di un programma. Questa tecnica di analisi ha dato origine a innumerevoli software e tool di aiuto allo sviluppo allo scopo di migliorare la qualità del software: ma quindi è la panacea universale?

No, purtroppo. Come in tutte le cose, non esiste un silver bullet (MongaDB docet) e bisogna bilanciare la sensitività (trovando bug difficili) e la probabilità di trovare falsi positivi.

L'astrazione ci permette di modellare tutte le possibili esecuzioni, ma con essa si introduce il conservatorismo. Aggiungere sensitività (flow, context, path, ecc) aggiunge precisione riducendo il conservatorismo, ma una astrazione più precisa è più costosa e non ha scalabilità su codici molto grandi e comunque non elimina del tutto i falsi positivi.
Bisogna sempre tenere conto che l'astrazione dell'analisi è diversa dall'astrazione dello sviluppatore e quindi lo sviluppatore potrebbe non capire se l'errore che il tool sta generando è vero o è un falso positivo.

## Symbolic execution : a middle ground

Seguiamo ora una strada alternativa: sfruttiamo le asserzioni.

*Remember* : ***Proprietà analizzatori***

- **Sound** : implica che l'analisi non produce falsi negativi, ovvero 
  - se l'analisi dice che c'è un bug, non è detto che sia vero perchè potrei avere dei falsi positivi
  - se l'analisi dice che non ci sono bug allora è sicuramente vero
- **Complete** : implica che l'analisi non produce falsi positivi, ovvero 
  - se l'analisi dice che c'è un bug allora è sicuramente vero
  - se l'analisi dice che non ci sono bugs, non è detto che sia vero perchè potrei avere dei falsi negativi

Non esistono analizzatori sia sound che complete (perfetti), ma cercano di bilanciare fra le due qualità.

Usando le asserzioni sono sicuro che i bug che trovo sono bug reali (quindi sono più complete che sound) poichè ogni test esplora solo una possibile soluzione 

```c
assert(f(3) == 5)
```

Generalmente speriamo che i test case siano il più generalizzabili possibili: a questo proposito la symbolic execution ci viene incontro assegnando dei simboli a ogni variabile responsabile di un cambiamento di flusso e simulando tutti i path dell'albero delle esecuzioni possibili (constraint solver).

*Esempio*:

```c
int a = α, b = β, c = γ;
// symbolic
int x = 0, y = 0, z = 0;
if (a) 
   x = -2;

if (b < 5) 
{
    if (!a && c) 
       y = 1; 

    z = 2;
}
assert(x+y+z != 3)
```

![SymExecTree](SymExecTree.png)

*Il costraint solver, insieme ai path di esecuzione, assomigliano molto al Prolog* (duh prolog è un constraint solver)

Ciò è possibile perchè l'esecutore simbolico può far girare il programma fino al punto in cui esso dipende da un valore ignoto, e successivamente può concettualmente forkare se stesso per tenere in considerazione tutti i possibili valori di quel valore ignoto che agiscono sul *control flow* del programma.

Ogni symbolic execution path è comune a molte esecuzioni di un programma (questo perchè c'è esattamente un set di esecuzione che soddisfa quella path condition). La symbolic execution quindi copre l'execution space del programma MOLTO più del testing tradizionale. 

La symbolic execution è, rispetto alla static analysis:

- Complete, ma non sound (usualmente non termina per i troppi path)
- Path, flow and context sensitive

L'idea della symbolic execution esiste da molto tempo (~1975) ma il fatto che sia molto compute-intensive l'ha lasciata nel dimenticatoio fino ad ora. Fortunatamente ora i pc sono più potenti, a prezzi affordable e con migliori algoritmi di costrain solving (SMT/SAT). Si riescono quindi a risolvere istanze velocemente, checkando le asserzioni e tagliando (prune) i path infeasible.

La Symbolic execution ha avuto una impennata di interesse negli ultimi 15 anni (a partire dal 2005). La motivazione principale riguarda il **bug finding** più profondo rispetto ai testing normali.

## Forking Execution

Come detto un symbolic executor può forkare dove c'è un branch e ciò accade quando vi sono soluzioni (ovvero esistono dei valori che ci portano) per entrambi i path del branch.
*N.B. Si possono avere dei branch anche nell'accesso a un array, controllando implicitamente se l'indice è compreso nel range consentito oppure no.* 

Ma come scegliamo sistematicamente quali direzioni ispezionare e in che ordine?

- **Chiamare il contraint solver per controllare la praticabilità del percorso** (controllandone la *path condition*) e quindi mettere tutti questi path in una coda ed esplorarli a uno a uno. 
  Purtroppo chiamare il contraint solver per ogni condizione può essere troppo costoso.

  Ad un certo punto, inoltre, il nostro symbolic executor raggiungerà il "bordo" della nostra applicazione (e.g. Librerie o chiamate di sistema). In alcuni casi possiamo ispezionare anche quel codice oppure dobbiamo creare un "modello" del codice da eseguire in quei casi senza dover analizzare l'intera libreria (poichè è facile che il symbolic executor si blocchi lì dentro).

- **Concolic Execution** (chiamata anche *dynamic symbolic execution*) : eseguo il programma in maniera concreta con input reali ma considerandoli anche come input simbolici. Gli input concreti determineranno la strada da prendere. L'idea è quindi quella di instrumentare il programma avendo uno shadow concrete state del programma (con input generati randomicamente) e tenendo traccia delle path condition che eseguiamo. Così facendo esploreremo un path alla volta, dall'inizio alla fine e il prossimo path può essere determinato semplicemente negando alcuni elementi dell'ultima path condition.
  Con la **Concretization** rimpiazziamo le variabili simboliche con valori reali che soddisfano le path condition (perdendo simbolicità e rischiando di perdere qualche path ma semplificando i vincoli simbolici).  Questo ci permette, in poche parole, di eseguire una system call con valori reali quando necessario, perdendo in "symbolicness", ma rendendoci capaci di continuare l'esecuzione simbolica del nostro programma senza bloccarci dentro la system call.

---

## Search and SMT

La symbolic execution è semplice e utile ma è computazionalmente costosa. Vedremo come l'uso effettivo della symbolic execution si riduce a un problema di ricerca. L'obbiettivo è quello di ricercare tra un numero elevato di possibilità per trovare eventi interessanti (bugs).

## Path Explosion Problem

E' un problema comune nella symbolic execution. Per considerare l'interezza dello spazio di esecuzione di un programma un symbolic executor deve considerare **ogni** path, ma visto che ogni programma (di grandi dimensioni) ha un numero elevatissimo di paths non possiamo eseguire la symbolic execution in maniera esaustiva. Infatti i programmi possono essere esponenziali nel numero di branch. 

*Esempio*:

`int a=alpha, b=beta, c=gamma;
if (a) ... else ...
if (b) ... else ...
if (c) ... else ...`

3 variabili, 8 program paths ( 2^3).

Inoltre i cicli sulle variabili simboliche sono ancora peggio.
*Esempio*

`int a=alpha; while(a) do ...`
Potenzialmente 2^31 paths ovvero tutti i possibili valori.

Se confrontiamo la symbolic execution con la static analysis sotto questo aspetto, quest'ultima ha un chiaro beneficio: essa termina pur considerando tutte le possibili esecuzioni del programma e lo fa utilizzando l'astrazione dell'esecuzione multipla dei loop o branch condition. Ma, di contro, usando questa astrazione possiamo avere falsi positivi. 

## Search Strategies

Per massimizzare i benefici della symbolic execution dobbiamo quindi vederla come un algormitmo di ricerca. 

L'idea più semplice di eseguire la symbolic execution è quella di eseguire una *Depth-first search (DFS)* o una *Breadth-first search (BFS)*. Di contro, se usiamo questi algoritmi non stiamo sfruttando le nostre **high-level knowledge**  sul problema (ovvero come poter cercare bug specifici o come raggiungere parti note del programma). Inoltre *DFS* potrebbe facilmente bloccarsi su una sola parte del programma cercando di andare sempre più in profondità. 

Dobbiamo quindi cercare di prioritizzare la ricerca, cercando di indirizzarla verso path che fanno fallire le asserzioni (controllo delle dimensioni degli array, controllo dei null pointer, ecc). Inoltre facciamo eseguire questa ricerca solo per un periodo fissato di tempo, quindi prioritizzare la ricerca ci aiuta a utilizzare il tempo in maniera più conveniente per trovare i bug.

Pensiamo all'esecuzione del programma come un DAG (nodi = stati del programma, archi = transazioni dagli stati del programma). Quindi possiamo pensare alla nostra ricerca simbolica come una sorta di **algoritmo di esplorazione di un grafo**.

Alcune di queste strategie sono descritte qui sotto.

## Randomness

Non sappiamo a priori quale path scegliere, quindi introdurre randomicità sembra una buona idea. Questo viene fatto da molti SAT solver moderni e funziona bene. 

- Idea 1: scegliere il prossimo path da eseguire in modo **uniformemente randomico** (*Random Path strategy*)
- Idea 2: **far ripartire la ricerca in modo random** se non abbiamo trovato qualcosa di interessante negli ultimi X path esplorati o X secondi. 
- Idea 3: **Scegliere tra path con la stessa priorità in maniera randomica** (tipo se vogliamo avere più coverage possiamo dare differenti priorità a varie parti del codice e scegliere tra queste parti in base alla loro priorità)

Un aspetto negativo della randomness è la riproducibilità ovvero poichè abbiamo effettuato scelte random per scovare un bug particolare, se facciamo eseguire il symbolic executor sullo stesso programma due volte non è garantito che ritroviamo lo stesso bug trovato precedentemente e potrebbe addirittura trovare nuovi bugs. 
Può non sembrare un aspetto negativo, ma dal punto di vista dell'ingegneria del software, una volta fixato il primo bug come faccio a capire se è ancora presente se non posso più riprodurlo (?).
Quindi è probabilmente consigliabile usare una funzione pseudo random, basata su un seed e memorizzare il seed quando viene trovato un bug.

## Coverage-guided Heuristics

Idea: Visitare parti del codice che non abbiamo visitato in precedenza quando scegliamo quale path dobbiamo seguire. 
Diamo un punteggio ad ogni statement del programma, in base a quante volte è stato attraversato. Il prossimo statement da esplorare viene scelto tra quelli che hanno il punteggio più basso.

Questo funziona perchè spesso gli errori sono in parti del programma difficili da raggiungere e questa strategia cerca di raggiungere tutto.

Questo approccio potrebbe anche non funzionare perchè potremmo non essere mai in grado di raggiungere alcuni statement se le dovute precondizioni non sono rispettate. 

## Generational search

E' un ibrido fra la BFS e la coverage-guided scritta prima. 

Generation 0: scegli un programma e un random input ed eseguilo fino al termine.
Generation 1: prendiamo i path dalla generation 0, neghiamo una condizione in un path e prendiamo quel path. 
...
Generation n: stesso approccio di generation 1, ma con la generation n-1

Inoltre utilizziamo un'euristica di tipo coverage per scegliere la priorità dei path che scegliamo. 
Questa ricerca è spesso usata con la concolic execution. (sembra MOLTO simile infatti)

## Combined search

E' ovvio quindi pensare che nessuna strategia di quelle descritte fin ora è quella vincente tutte le volte per tutti i possibili programmi. Infatti alcune strategie di ricerca possono trovare alcuni bug che altre strategie non trovano e viceversa.
L'idea è quindi quella di applicare più strategie di ricerca allo stesso tempo. (Processi parallelli)

## SMT Solver Performance

SAT (Satisfiability) solvers sono il cuore degli SMT (SAT Modulo Theories) solvers. In pratica gli SMT solver implementano delle ottimizzazioni (per delle identità semplici, per tutti i concetti relativi agli array, per avere una cache per tutte le queries già risolte o per rimuovere variabili inutili). Tutte queste ottimizzazioni possono fare una enorme differenza di performance. 

Gli SMT solver più popolari sono:

- Z3 - sviluppato alla Microsoft Research
- Yices - sviluppato alla SRI
- STP - sviluppato da Ganesh che adesso lavora alla Waterloo 
- CVC3 - sviluppato alla NYU

Bisogna però sempre ricordare che, indipendentemente dalla strategia di ricerca utilizzata, potrebbero capitare bug particolarmente difficili e tediosi da trovare (perchè consumano molto tempo) e questa rimane una limitazione fondamentale del problema.