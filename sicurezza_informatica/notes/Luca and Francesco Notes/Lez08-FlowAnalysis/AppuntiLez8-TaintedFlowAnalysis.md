# Static analysis application : Flow analysis

In seguito, andremo a vedere una delle implementazioni della static analysis, ovvero la **tainted flow analysis**.

Generalmente, una **flow analysis** si occupa di osservare come i possibili valori in un programma fluiscono fra le locazioni di memoria (variabili ad esempio). Vediamo cosa intendiamo per *tainted*.

## Tainted Flow analysis

La causa del successo di numerosi attacchi di basso livello (BOF, HOF, UAF ...) è la fiducia (mal riposta) dello sviluppatore nei confronti dell'input utente, sul quale non viene svolto alcun tipo di controllo:

- Consideriamo quindi tutto l'user input come **tainted** (sospetto, contaminato)
- Consideriamo tutti gli altri valori del programma (variabili, valori hardcoded non accessibili all'utente) come **untainted** 

Se la flow analysis si accorge che dei dati tainted vengono utilizzati in posti dove ci si aspettavano dei dati untainted, allora vuol dire che c'è una potenziale vulnerabilità nel programma.

*Esempio*:

```c
char *input = fgets(...);
printf("%x", input); 
//il primo argomento ("%x") è hardwired nel codice dallo sviluppatore quindi esso è considerato untainted
//il secondo argomento ("input") è un user input ottenuto da una fgets() e pertanto potrebbe essere normale o malevolo: viene quindi considerato tainted
```

Per questo, conviene assegnare a ogni variabile il tipo tainted se sono ottenute dall'utente in input a runtime (possibilmente controllati dagli avversari) oppure il tipo untainted se NON DEVONO poter essere controllate dall'avversario.

Ricordiamo l'**attacco sul format-specifier** del `printf()`

```c
char* name = fgets(...);
printf(name);    // la stringa name potrebbe essere sfruttata dall'attaccante e portare a heartbleed o
               // peggio se contiene dei format specifier non validi
```

L'attaccante potrebbe settare

- `name = "%s%s%s"`, per crashare il programma e stampare informazioni dallo stack (potenzialmente sensibili)
- `name = "..%n.."`, per scrivere potenzialmente in memoria

Come possiamo notare, specificando dei format specifier nella format string ma non passando i valori da sostituirvi, ho un undefined behaviour sfruttabile dall'attaccante. Pertanto possiamo concludere che la funzione `printf()` necessita di una **format string untainted**.

*Esempio*2:

```c
//ridefinisco la funzione printf con la notazione tainted/untainted
printf(untainted char* format, tainted T toPrint){...}

tainted char* name = fgets(...);
tainted char* input = fgets(...);
printf(name, input); //fallisce perchè name, usata come format string, dovrebbe essere untainted
                   //input invece non ha alcun problema perchè printf si aspettava un valore tainted
```

Voglio quindi che il mio analizzatore studi il flusso del mio programma e quindi noti se c'è un flusso legale o illegale

### Flusso legale

Un flusso legale si verifica quando utilizzo una variabile untainted come se fosse tainted: lecito e non conduce a problemi. 
$` (untainted ≤ tainted) `$

```c
void f(tainted int x) {...} // mi aspetto una var. tainted quindi se mi arriva untainted va bene comunque
untainted int a = ...;
f(a); // ok! legal flow
```

### Flusso illegale

Un flusso illegale si verifica quando utilizzo una variabile tainted come se fosse untainted: non lecito perchè la variabile tainted potrebbe appunto essere contaminata da un utente malevolo. 

$` (tainted \nleq untainted) `$

```c
void g(untainted int x) {...} // mi aspetto una var. untainted quindi se mi arriva tainted non la posso accettare perchè potrebbe provocare un exploit
tainted int b = ...;
g(b); // crash! illegal flow
```

Possiamo quindi dedurre che tainted < untainted secondo una relazione di ordinamento e vedere il flusso permesso come un reticolo. Questa relazione di ordinamento ci servirà per determinare quali flussi siano consentiti o meno.

## Come avviene l'analisi

La flow analysis avviene come un inferenza di tipo: se non è presente nessun qualifier, lo deduciamo.

Steps:

1. Creo un nome per ogni qualifier mancante (**α**, **ß**, ...)
2. Per ogni statement del programma, genero dei vincoli o constraint (in forma `q1‎`≤`q2`) su possibili soluzioni
   - Ad esempio, uno statement `x = y` genera il vincolo qy ‎≤ qx, ovvero i rispettivi qualifier
3. Risolvo i vincoli per trovare le soluzioni per **α**, **ß**, ..
   - Una soluzione è una sostituzione di qualifiers (tainted / untainted) per i nomi assegnati (**α**, **ß**, ..) tale per cui tutti i flow sono legali
   - Se **non esiste** la soluzione, ***potremmo*** avere un **flow illegale**

### Esempio di analisi

Date le seguenti funzioni

```c
int printf(untainted char* format, ...){...}
tainted char* fgets(...);
```

Analizziamo il seguente codice

```c
char* name = fgets(...);
char* x = name;
printf(x);
```

Ho un flusso legale?

1. Assegno i qualifier a chi non li possiede
   
   - **α** per la variabile `name`
   - **ß** per la variabile `x`

2. Genero i vincoli per ogni statement del programma
   
   - **tainted** ≤ **α**, perchè `fgets()` ritorna qualcosa di tainted e viene assegnato ad **α**
   - **α** ≤ **ß**, perchè **ß** viene assegnato ad **α**
   - **ß** ≤ **untainted**, perchè **ß** viene usato nella `printf()` come untainted

3. Risolvo i vincoli
   
   - Possiamo vedere una netta catena di vincoli da soddisfare
   
   - I tre vincoli si fondono in uno solo:  **tainted** ≤ **α** ≤ **ß** ≤ **untainted**
   
   - Proviamo a risolvere questo vincolo sostituendo i qualifier
     
     1. Il primo vincolo richiede che **α** = **tainted** per essere valido pertanto **tainted** ≤ **tainted** ≤ **ß** ≤ **untainted**
     
     2. Il secondo vincolo richiede che **ß** = **tainted** per essere valido quindi **tainted** ≤ **tainted** ≤ **tainted** ≤ **untainted**
     
     3. Il terzo vincolo non può essere soddisfatto perchè viola la legge 
        
        $$
        (tainted \nleq untainted)
        $$

![](analysisex.png)

Pertanto ho scoperto un **illegal flow** perchè non ho alcuna soluzione possibile per **α** e **ß**!

## Flussi particolari

E' possibile che alcuni flussi che dovrebbero essere legali sono considerati illegali (falso positivo) oppure abbiamo delle situazioni di indecisione.

#### Riassegnamento (falso positivo)

```c
char* name = fgets(...);
char* x;
x = name;
x = 'ok';
printf(x);    //x in realtà è untainted perchè ho sovrascritto il valore tainted con un valore hardwired, tuttavia ricevo comunque un allarme: è un falso positivo
```

#### Indecisione condizionale

```c
char* name = fgets(...);
if ...
    x = name;
else
    x = 'ok';
printf(x);    //qui ricevo un allarme perchè c'è la possibilità di usare la variabile tainted, ma non è sempre vero: dipende dall'if
```

### Risoluzione dei flussi particolari : Add sensitivity

Il problema principale della nostra analisi è che è *flow insensitive*, in quanto ogni variabile ha **solo un qualifier** che astrae la taintedness dei valori che conterrà.

#### Flow Sensitivity

Una ***flow sensitive analysis*** terrebbe conto di questi cambiamenti, assegnando più qualifier alle variabili in base al flusso del programma. 

Per fare ciò, applico la SSA (single static assignment): se nel mio programma ho una variabile che viene assegnata più volte, **creo diversi alias** per essa ogni volta che viene assegnata; questo mi permette di ottenere una maggiore granularità e  precisione di etichettamento tainted/untainted allo scopo di evitare falsi positivi.

Vediamo l'esempio del *riassegnamento* che dava un falso allarme applicando questa tecnica di flow sensitivity:

```c
char* name = fgets(...);
char* x1, x2; // ho due riassegnamenti di x nel mio programma quindi devo generare due qualifier diversi

x1 = name; //etichetto x con x1 perchè gli ho assegnato un valore (tainted)
x2 = 'ok'; //etichetto x con x2 perchè l'ho riassegnato (untainted)

printf(x2); //non ho errore perchè l'ultima etichetta di x ha un valore untainted
```

#### Path Sensitivity

Per risolvere i casi di *indecisione condizionale* invece conviene svolgere una ***path sensitive analysis***, ovvero un tipo di analisi che controlla la fattibilità del percorso seguito dal programma durante l'esecuzione.

Vediamo il seguente esempio:

![](pathsens.png)

Abbiamo posto una label su ogni statement del programma in modo da poter tracciare i vari path percorribili:

- Quando x ≠ 0, il programma segue il percorso 1-2-3-5-6
- Quando x = 0, il programma segue il percorso 1-3-4-6

Come possiamo vedere in entrambi i casi ottengo dei risultati che NON portano a illegal flows: l'unico caso di illegal flow possibile è quello del path 1-3-4-5-6 ma questo path non è raggiungibile poichè

- Se x = 0, y diventa tainted per la `fgets()` ma non può essere stampato
- Se x ≠ 0, y diventa untainted quindi non ho problemi a stamparlo con `printf()`

### Summary

Le aggiunte flow e path sensitivity aggiungono precisione, che è un buon risultato; tuttavia aggiungono un pesante overhead nella analisi statica e rendono più difficile trovare una soluzione infatti:

- La *flow sensitivity* aumenta il numero di nodi nel grafo dei vincoli (poichè aumenta i qualifier per ogni variabile riassegnata)
- La *path sensitivity* richiede più risorse e procedure per risolvere le path conditions

Quindi abbiamo un tradeoff fra precisione (++) e scalabilità (--), limitando la grandezza dei programmi che possiamo analizzare.

## Altri metodi di flow analysis

Vediamo come possiamo scalare la flow analysis a problemi più complessi tipici di un linguaggio completo e i relativi problem set.

### Pointer aliasing

Tornando ai problemi precedenti, vediamo come riassegnamenti di puntatori particolari non vengono individuati come tainted e misso un illegal flow.

```c
char* a = "hi"; //'a' è una stringa  (puntatore ad array di char)

(char*)* p = &a; //puntatore 'p' che punta ad 'a' (la stringa 'hi')
(char*)* q = p;  //puntatore 'q' che punta a ciò che punta 'p': è un suo alias

char* b = fgets(...); //stringa da user input (tainted)

*q = b; //'q' punta a una stringa tainted ora -> ma 'q' è un alias di 'p' quindi anche 'p' diventa tainted dato che ora puntano entrambi allo stesso indirizzo (di 'b') 

printf(*p); //il contenuto di 'p' è tainted ma l'analisi non se ne accorge: ho missato un illegal flow!
```

Come possiamo vedere l'analisi usata fin'ora non si accorge di questo trucchetto coi puntatori per arrivare a un illegal flow. Vediamo effettivamente cosa non funziona:

1. Assegno i qualifier a chi non li possiede
   
   - **α** per la variabile `a`
   
   - **ß** per la variabile `p` 
   
   - **γ** per la variabile `q`
   
   - **ω** per la variabile `b`

2. Genero i vincoli per ogni statement del programma
   
   - **untainted** ≤ **α**, perchè assegno ad `a` una stringa hardcoded
   
   - **α** ≤ **ß**, perchè assegno a `p` la reference di `a`
   
   - **ß** ≤ **γ**, perchè assegno a `q` lo stesso valore indicato da `p`
   
   - **tainted** ≤ **ω**, perchè assegno a `b` un valore ottenuto da una `fgets()`, ovvero intrinsecamente tainted
   
   - **ω** ≤ **γ**, perchè assegno al valore dereferenziato da `q` il valore contenuto in `b`
   
   - **ß** ≤ **untainted**, perchè chiamo la `printf()` su un valore (presumibilmente) untainted 

3. Risolvo i vincoli
   
   - Osserviamo la catena di vincoli
   
   - **untainted** ≤ **α** ≤ **ß** ≤ **γ** and **tainted** ≤ **ω** ≤ **γ** and **ß** ≤ **untainted**
   
   - Questa catena di vincoli ha soluzione per 
     
     - **α** = **ß** = **untainted**
     
     - **ω** = **γ** = **tainted**

E' evidente che l'analisi perde l'illegal flow perchè non si accorge che `p` e `q` sono puntatori alias, quindi scrivere dati tainted su `q` rende anche il contenuto di `p` tainted.

Abbiamo dunque un problema di **aliasing** di puntatori, difficile da individuare; una strategia risolutiva possibile prevede che ogni assegnamento via puntatore dovrebbe "fluire" verso entrambi i lati

- Nell'esempio di prima avevamo **ß** ≤ **γ**: oltre a questo vincolo aggiungiamo anche quello inverso ovvero  **γ** ≥ **ß**

- In questo modo se ho due alias e uno diventa tainted, anche l'altro deve diventare tainted 
  
  - Infatti se nell'esempio avevo **ß** ≤ **untainted**, ora col doppio flusso so che **ß** diventa **tainted** dal nuovo vincolo **tainted** ≤ **ω** ≤ **γ** ≤ **ß** ≤ **untainted**: otterrei quindi **tainted** ≤ **untainted**, accorgendomi finalmente del flusso illegale!

- Con questo metodo garantisco che i vincoli di alias siano "sound"

- Tuttavia potrebbe portare a falsi allarmi

Per ridurre eventuali aggiunti falsi allarmi posso osservare che, se i puntatori sono assegnati a delle variabili const, non è necessario fare il flusso ambo i lati (tanto non può cambiare la variabile)

### Implicit flows

Vediamo in seguito alcuni flussi illeciti nascosti dalla complessità del codice:

```c
void copy(tainted char* src, untainted char* dst, int len) 
{
   untainted int i;
   for (i = 0; i < len; i++) 
   {
      dst[i] = src[i]; //illegal flow: assegno un pezzo di tainted a un untainted
     }
}
```

Possiamo osservare come catturo facilmente qui sopra l'illegal flow, poichè sto trasferendo direttamente caratteri da un array tainted a uno untainted.

Vediamo invece il prossimo esempio, più subdolo:

```c
void copy(tainted char *src, untainted char *dst, int len) 
{
   untainted int i, j;
   for (i = 0; i<len; i++) 
   {
      for (j = 0; j<sizeof(char)*256; j++) 
      {
         if (src[i] == (char)j)
            dst[i] = (char)j; //legal? missed flow
      }
   }
}
```

Possiamo vedere come, anche se `j` e `dst[i]` sono perfettamente untainted secondo l'analisi, il valore di `j` in realtà dipende in modo implicito dal valore tainted di `src[i]`: l'analizzatore non se ne accorge e ho quindi missato un illegal flow.

Il flow precedente è un **flow implicito**, in quanto l'informazione in un valore influenza un altro implicitamente: ***i dati non sono fluiti direttamente, ma l'informazione sì.***

Lo strumento che ci aiuta a individuare questi flow impliciti è la **information flow analysis**, il quale metodo che ci permette di scoprire questi flow impliciti è mantenere una ***scoped label del program counter*** (pc) la quale rappresenta la massima taintedness della istruzione correntemente sul pc.

Quando svolgo assegnamenti andrò a creare una label che involve il pc:

- L'assegnamento `x = y` produce due vincoli:
  
  - **label(y)** ≤ **label(x)**, come al solito
  
  - **pc** ≤ **label(x)**, con questo vincolo invece setto la label(x) come upper bound di taintedness in modo che il flusso non subisca leak durante l'assegnamento di `x`

In questo modo posso individuare gli implicit flow osservando le variazioni di taintedness del program counter.

*Esempio*: Uso dell'information flow analysis

![](infoflow.png)

Questa analisi è molto sensibile a falsi positivi, senza considerare che tutte queste pc label da tracciare aggiungono numerosi vincoli da verificare: tutto ciò significa un calo delle performance dell'analizzatore a causa del pesante overhead.

Inoltre generalmente è raro spezzettare un assegnamento in tante piccole istruzioni, generalmente lo facciamo in una sola linea: possiamo quindi considerarli come casi di confine e di poca influenza, quindi generalmente i tool di tainting analysis ignorano gli implicit flows per ragioni di performance.

## Altre sfide di taint analysis

Abbiamo visto l'analisi per molti elementi di un linguaggio, ma bisogna considerare anche i rimanenti se vogliamo che il nostro analizzatore statico sia robusto. Vedremo che esistono diverse soluzioni con diversi tradeoff in base alle nostre esigenze.

### Taint through operations

E' possibile indurre taintedness attraverso gli operatori? Probabilmente sì, dipende molto anche dall'operatore considerato

```c
tainted int a;
untainted int b; 
int c = a + b; //is c tainted? probably yes
```

### Function pointers

Quando abbiamo dei puntatori a funzione in un programma, spesso essi sono uninitialized a compile-time poichè vengono inizializzati a run-time. Risulta quindi difficile se non impossibile, svolgere l'analisi statica su di essi: cosa fare per renderli meno vulnerabili?

Potremmo considerare un set di tutti i possibili indirizzi a cui potrebbe saltare quel puntatore a funzione (++ preciso) oppure una base e un offset di indirizzi possibili (++ scalabile): in questo modo, facendo i vincoli, diremmo che quella funzione salta solo a tutti quei possibili indirizzi. 

### Struct fields

Come analizzare la taintedness di uno struct dato che è composto da campi multipli? Considero la taintedness di ogni campo (++ preciso) o dello struct intero (++ scalabile)?

Inoltre potremmo applicare lo stesso discorso agli oggetti e classi OOP dato che `objects = structs + function pointers`

### Arrays

Quando analizzo gli array, devo tener traccia della taintedness di ogni elemento o generalizzo tutto l'array con un solo valore (array tainted se anche uno degli elementi è tainted)?

## Raffinamento della taint analysis

- Estendere la taint analysis per gestire sanitizzatori di input
  - Quindi implementare funzioni che convertano dati tainted di input in dati untainted affidabili (ad esempio le funzioni che fanno escape di caratteri in modo da evitare SQL injections / HTML injections)
- Utilizzare labels per sources e sinks
  - Source = Punto di ingresso dei dati, ad esempio user input (tainted)
  - Sink = Punto di arrivo dei dati, ad esempio una funzione vulnerabile a cui non dovrebbero essere passati input tainted (sensitive sink)
- Evitare leak di dati confidenziali
  - Non voglio che secret sources vadano in sink pubblici (evitando implicit flows)

## Altri tipi di analisi

### Pointer analysis

Basa la sua analisi sul controllare se i puntatori puntano alla stessa locazione e se vengono sfruttati in questo modo. Condivide molti elementi con la flow analysis.

### Data flow analysis

Analisi molto antica (già dal 1970), flow sensitive, traccia il data flow delle variabili nei programmi, ovvero la loro liveness: quando sono "vive" (il loro valore esiste e può essere letto in futuro) o quando sono morte (il loro valore può essere sovrascritto).

### Abstract interpretation

Fondamenta della data flow analysis e della static analysis in generale.
