# Heap Overflow on Metadata

The heap is a pool of memory used for dynamic allocation at runtime with the operations:

- **malloc()**, to grab a chunk of heap memory;
- **free()**, to release a chunk of heap memory;

In a heap we can store objects, big buffers, structs, persistence, large things. It is slow and manual, in the sense that a programmer is responsible for the memory allocations and the liberation.

*Example: A heap usage*

```c
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
	char *buf1 = malloc(128);
	char *buf2 = malloc(256);
	
    read(fileno(stdin), buf1, 200);
	
    free(buf2);
	free(buf1);
} 
```

## Heap Chunks

The heap is divided in **chunks** of different size that are used for the `malloc()` allocations. The various fields of a chunk have different meaning for allocated and free chunks.

*Example: A heap chunk*

```C
struct malloc_chunk {
	INTERNAL_SIZE_T prev_size;  // Size of previous chunk (if free)
    INTERNAL_SIZE_T size;		// Size in bytes, including overhead
	
    // double links -- used only if free
    struct malloc_chunk* fd;	// forward chunk pointer
	struct malloc_chunk* bk;	// backward chunk pointer
    
	// Only used for large blocks: pointer to next larger size
	// double links -- used only if free
	struct malloc_chunk* fd_nextsize; 
	struct malloc_chunk* bk_nextsize;
};

```

![image-20200121151554990](img/img.png)

If a program were to call `malloc(256)`, `malloc(512)`, and finally `malloc(1024)`, the
memory layout of the heap is as follows:

```
Meta-data of chunk created by malloc(256)
The 256 bytes of memory return by malloc
-----------------------------------------
Meta-data of chunk created by malloc(512)
The 512 bytes of memory return by malloc
-----------------------------------------
Meta-data of chunk created by malloc(1024)
The 1024 bytes of memory return by malloc
-----------------------------------------
Meta-data of the top chunk
```

## Allocations and Deallocations

In the memory heap allocation, the **top chunk** represents the remaining available memory on the heap.

When a new memory request is made (malloc), the top chunk gets split in two:

- The first part becomes the requested memory chunk;
- Te second part becomes the new top chunk (so the top chunk shrunk in size).

When a chunk is freed, some operations must be done on the structure:

- The least significant bit of the `size` field in the meta data of the next chunk must be cleared;
- The `prev_size` field of this next chunk will be set to the size of the chunk we are freeing.

There are actually **multiple lists of free chunks**. Each list contains free chunks of a specific size.

When a chunk is freed it checks whether the chunk before it has already been freed. In case the
**previous chunk is not in use, it's coalesced** with the chunk being freed.

When a memory allocation request is made, it **first searches for a free chunk that has the same
size** (or a bit larger), and will reuse that memory. Only if no appropriate free chunk was found will the top chunk be used.

## Chunk Deallocation: Unlink()

When a chunk deallocation request is make, the function `unlink()` takes care of it, that does not only work on the specified chunk, but also makes changes on the forward and backward chunks.

```C
void unlink(malloc_chunk *P, malloc_chunk *BK, malloc_chunk *FD) {
	FD = P->fd;
	BK = P->bk;
	FD->bk = BK;
	BK->fd = FD;
}
```

![image-20200121153212085](img/img2.png)



**EXPLOIT.** Those pointers can be used to write arbitrary values to arbitrary locations.

```C
FD->bk = return address;
FD = P->fd;
BK = P->bk = address of the buffer (injection vector);
FD->bk = BK;
```

It is possible to use the `unlink()` vulnerability of freeing adjacent chunks (linked list). That way it is possible to overflow part of the metadata and overwrite the pointers, making them point to some injected malevolent code data.

The overwriting must be done before the `free()` execution (that will call the unlink).

This `glibc` vulnerability has been patched, so this attack on the `unlink()` function no longer works.

## Most Recent Techniques

New techniques were born to attack the heap with different methodologies that take advantages of different program vulnerabilities: 

- The House of Prime;
- The House of Mind;
- The House of Force;
- The House of Lore;
- The House of Spirit;
- The House of Chaos;

We will focus on the **House of Force attack**.

# House of Force Attack

The conditions required for an **House of Force** attack are:

- The possibility of overwriting the *top chunk* (metadata overwriting);
- The existence of a `malloc()` with a user controllable size (control over input);
- The possibility to call another `malloc()`.

*Example: A vulnerable code for an House of Force attack*

```c
int main(int argc, char *argv[]) {
	char *buf1, *buf2, *buf3;
	if (argc != 4) return;
    
	buf1 = malloc(256);
	strcpy(buf1, argv[1]);
	buf2 = malloc(strtoul(argv[2], NULL, 16));
	buf3 = malloc(256);
	strcpy(buf3, argv[3]);
	free(buf3);
	free(buf2);
	free(buf1);
	return 0;
}
```

## Exploitation Technique

The av->top variable always points to the top chunk. **The goal is to overwrite av->top with a user
controllable value**. 

During a call to `malloc()` this variable is used to get a reference to the top chunk (in case no other chunks could fulfill the request). This means that if we control the value of av->top, and we can force a call to malloc which uses the top chunk, **we control where the next chunk will be allocated**. Consequently we can **write arbitrary bytes to any address**.

We want to assure that any request (of arbitrary large size) will use the top chunk. To accomplish
this we abuse the overflow in the program to overwrite the metadata of the top chunk. First we write
256 bytes to fill up the allocated space, and we finally overwrite the size with the largest possible
(unsigned) integer.

```C
static void* _int_malloc(mstate av, size_t bytes)
//....
top = av->top;
size = chunksize(top);
if ((unsigned long)(size) >= (unsigned long)(bytes + MINSIZE)) {
	remainder_size = size - nb;
	remainder = chunk_at_offset(victim, nb);
	av->top = remainder;
	// ...

```

**define chunk_at_offset(p, s)** ((mchunkptr)(((char*)(p)) + (s)))
Av_top + bytes_to_allocate = Address in memory
Bytes_to_allocate = Address in memory - Av_top ;

By writing in memory at any arbitrary address we can execute any arbitrary code.
















