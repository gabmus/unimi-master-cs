# Static Analysis: Part 2

Let's see how to scale the flow analysis on more complex problems typical of a complete language and the relative problem sets.

## Pointer Aliasing

If we reassign specific pointers, creating an **alias**, they could not be seen as tainted and this could lead to a miss of an illegal flow:

```c
char* a = "hi"; 		// 'a' is a string (pointer to a char array)
(char*)* p = &a; 		// 'p' is a pointer that points to 'a'
(char*)* q = p;  		// 'q' is a pointer that points to what 'p' is pointing to: 
						//  it's an alias
char* b = fgets(...); 	// user input string (tainted)

*q = b; 		// 'q' points to a tainted string now, but 'q' is an alias of 'p' so
				// 'p' is going tainted as well because they point to the same
				// address, the 'b' one

printf(*p); 	// 'p' content is tainted, but the flow analysis does not notice it:
				// it misses an illegal flow!
```

Why the illegal flow is not found? Let's see what happens during the analysis:

1. When defining qualifiers:
   
   - **α** for variable `a`
   
   - **ß** for variable `p` 
   
   - **γ** for variable `q`
   
   - **ω** for variable `b`

2. During constraint generation:
   
   - **untainted** ≤ **α**, because `a` is assigned to an hard-coded string;
   
   - **α** ≤ **ß**, because `p` has assigned an `a` reference;
   
   - **ß** ≤ **γ**, because `q` has the same value pointed by  `p`;
   
   - **tainted** ≤ **ω**, because `b` has assigned a value from a `fgets()` function, which is tainted;
   
   - **ω** ≤ **γ**, because i'm putting on the dereferenced value of `p` the value of  `b`;
   
   - **ß** ≤ **untainted**, because the `printf()` call is made on a value that is supposed to be untainted. 

3. Solving constraints:
   
   - We observe the chain of constraints;
   
   - **untainted** ≤ **α** ≤ **ß** ≤ **γ** and **tainted** ≤ **ω** ≤ **γ** and **ß** ≤ **untainted**
   
   - The only possible solution is: 
     
     - **α** = **ß** = **untainted**
     
     - **ω** = **γ** = **tainted**

The analysis loses an illegal flow because it does not notices that `p` and `q` are alias pointers, so  writing tainted data on `q` makes the content of `p` tainted too. 

This is the **aliasing pointer problem**, a pretty hard vulnerability to spot. A possible solver strategy could be to foresee that each assignment done by pointers should flow on both sides.

In the previous example we had  **ß** ≤ **γ**: other that this constraint, we add that the inverse one  **γ** ≥ **ß**. This way even if i have two alias and one of them becomes tainted, the other one  becomes tainted too. By doing so i can have:

- Sound aliasing constraint;
- Possible false alarms.

In order to reduce the possibles false alarms, if pointers are never assigned to (constants) then backward flow is not needed (sound). Drop backward flow edge anywat (trades false alarms for missed errors).

## Implicit Flows

Here we have some illegal flows hidden in code complexity.

*Example: Easy illegal flow*

```c
void copy(tainted char* src, untainted char* dst, int len) {
   untainted int i;
   for (i = 0; i < len; i++) 
   {
      dst[i] = src[i];	// illegal flow: assigning tainted to untainted
     }
}
```

On this example, it's easy to capture the illegal flow, because it's transferring chars directly from a tainted array to an untainted one. 

*Example: Tricky illegal flow*

```c
void copy(tainted char *src, untainted char *dst, int len) {
   untainted int i, j;
   for (i = 0; i<len; i++) 
   {
      for (j = 0; j<sizeof(char)*256; j++) 
      {
         if (src[i] == (char)j)
            dst[i] = (char)j; 		// missed illegal flow
      }
   }
}
```

On this example we can see that, even if `j` and `dst[i]` are perfectly untainted from the analysis, the value of `j` actually depends implicitly to the tainted value of `src[i]`: the analyzer doesn't notice that, so we have a missed illegal flow. 

The prior flow is an **implicit flow**, since information in one value *implicitly* influences another: **data does not flow directly, but information does**.

### The Information Flow

One way to discover the implicit flows problem is the **information flow analysis**, whose methods allows us to discover the implicit flows and maintain a scoped **program counter label**, which represents the maximum taint affecting the current program counter (pc).

Generalized analysis tracks information flow.

On each assignment, the analyzer will try to find a label that involves the pc. For example, an assignment `x = y` produces two constraints:

- **label(y)** ≤ **label(x)**, as usual;
- **pc** ≤ **label(x)**, a constraint that sets the label `x` as a taintedness upper bound, so that the flow won't leak during the assignment.

By doing so, the analyzer can spot implicit flows by observing taintedness variation on the program counter.

*Example: Information flow*

![](img/infoflow.png)

This analysis is **false positive** sensitive, and all those tracked pc label adds many constraint that has to be verified. This will drop the analyzer's performance, causing a **big overhead**.

Also it's typically hard to break an assignment in smaller instructions, generally it's a one-line statement, so they are treated like border line cases and mostly not much relevant. 

Because of all of those problems and the small benefits, **tainting analyses tend to ignore implicit flows**.

## More Deep Analysis

We analyzed many element of a language, but to have a strong analyzer we have to consider as many elements as possible. Here are a bunch of different solutions according to our needs.

- **Taint through operations** - Introducing taintedness through operation.

  ```c
  tainted int a;
  untainted int b;
  int c = a + b; 		// Is c tainted? probably
  ```

- **Function pointers** - When we have function pointers in a program, they're often uninitialized at compile-time. This makes them hard to analyze. Considering a set of possible addresses we can limit where our function call will jump.

- **Struct fields** - Analyzing a struct taintedness by field or his whole? Is that the same for objects?

- **Arrays** - Keep track of taintedness of each array element or one element representing the whole array?

Refining taint analysis is possible, we can:

- Extend taint analysis to manage **input sanitizers**, by adding functions that converts tainted input data to untainted and trusted ones.
- Label additional sources and sinks:
  - Source: where the data comes from (input streams);
  - Sink: Where the data arrives, like a function variable that should not have tainted values.

- Avoid leaking confidential data
  - Don't want secret sources to co to public sinks (implicit flows more relevant in this setting);
  - Dual of tainting.

Other kinds of analysis:

- **Pointer analysis** ("points-to" analysis) - Determine whether pointers point to the same locations. Shares many elements of flow analysis.
- **Data flow analysis** - Invented in the early 1970's, it's flow sensitive, tracks "data flow facts" about variables in the program, their "liveness" (when their value exists and can be read).
- **Abstract interpretation** - Invented in the late 1970's as a theoretical foundation for data flow analysis and static analysis generally. Associated with certain algorithms.