# Z3: An Efficient SAT/SMT Solver

Z3 is a solver developed by Microsoft, useful for solving constraint problems, like path conditions.

## The SAT Problem

A SAT problem can be translated in a formula formed by boolean variables connected only with boolean operators.

*Example:* ` (a or b) and (b and c) and (d and c) `

SAT problems goal is to find the solving formula by finding a way that solves the given boolean expression.

SAT problems are NP-Complete.

Solvers are mainly used for computing path conditions, related to the source code of a program. If the formula is satisfiable, then it means that there is a specific variable assignment that makes it true.

When solving SAT problems for path condition, we must express all conditions in boolean formulas. Problem is that those conditions depend on many variables (arithmetic operators, variables, and other stuff). We need a map of all program conditions with the relative boolean formula, a "translation". This **translation is made by SMT Theories**.

SMT Theories are an extension of SAT module that let more complex expressions to be fused, including theories on integers, real numbers, arrays, pointers and more. SMT module contains a translation of those theories in SAT problem.

The theory reasoner finds the conflict, the solver solves it. 