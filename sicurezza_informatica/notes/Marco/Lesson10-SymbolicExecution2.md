# Symbolic Execution - Part 2

Symbolic execution is appealingly **simple and useful**, but **computationally expensive**.

We will see how the effective use of symbolic execution boils down to a kind of search and also take a moment to see how its feasibility at all has been aided by the rise of SMT solvers,

## Path Explosion

Consider the whole execution of a program with the symbolic execution means to **explore every path**, but because every (big) program has an enormous number of path it's not possible to do an exhaustive analysis.

- Programs can be exponentials in branching structures;
- Loops on symbolic variables are even worse.

By comparing the symbolic execution with the static analysis on that, we can see a clear benefit on that one: the static analysis will actually **terminate**, even when considering all possible program runs.

However, the **termination is possible by approximating** (with abstractions) multiple loop executions or branch conditions. Essentially assumes all branches, and any number of loop iterations, are feasible.

But the static analysis approach can **lead to false alarms**.

## Search Strategies

To maximize symbolic execution's benefits, we must see that as a search algorithm.

Simplest idea:

- Depth-first search (DFS): **worklist = stack**;
- Breadth-first search (BFS): **worklist = queue**;

Potential drawbacks:

- **Not guided** by any higher level knowledge (how to find specific bugs or reach specific known parts of the program);
- **DFS could easily get stuck** in one part of the program. It could keep going around a loop over and over again;
- **BFS is more intrusive to implement**. Can't easily be concolic.

Our next idea, then, will be to **prioritize search**, trying to steer search towards paths more likely to contain assertion failures (checking array's size, null pointers, ...). Only run for a certain length of time, so if we don't find a bug/vulnerability within time budget, stop, preventing non-termination.

Think of **program execution as a DAG**:

- Nodes = program states;
- Edges(`n1`, `n2`) = can transition from state `n1` to state `n2`.

We need a kind of **graph exploration algorithm** that at each step picks among all possible paths.

## Randomness

We don't know a priori which paths to take, so adding some randomness seems like a good idea. Many modern SAT solvers do that, and it works:

- Idea 1: Pick next path to explore **uniformly at random** (*Random path strategy*, each path with the same possibility of being picked);
- Idea 2: **Randomly restart search** if haven't hit anything interesting in a while;
- Idea 3: **Choose among equal priority paths at random**. If we want to have more coverage, we can give different priorities to pieces of code and then choose among them according to the given priority).

One drawback of randomness is the low reproducibility, because of random searches, if we run more times the same executor on the same code it may give the same result, it can give more or less bugs, and if i fixed a bug can i really be sure it is not present anymore?

Probably good to use **pseudo-randomness** based on seeds, and then record which seed is picked, to prevent bugs prom disappearing (or reappearing) on later runs.

## Coverage-Guided Heuristics

The main idea of a coverage-guided heuristic is to **visit statements we haven't seen before** when choosing which path to follow next.

Approach:

- Give a score to each statement, based on how many times it has been seen;
- Pick next statement to explore that has the lowest score.

This idea might work because errors are often in hard-to-reach parts of the program and this strategy tries to reach everywhere.

Also this idea could fail because i could never be able to get to a statement if proper precondition are not set up.

## Generational Search

Generational search is an **hybrid of BFS and coverage-guided**.

- *Generation 0*: Pick one program at random, run to completion;
- *Generation 1*: 
  - Take paths from *generation 0*;
  - Negate one branch condition on a path to yield a new path prefix;
  - Find a solution for that prefix;
  - Take the resulting path.
- *Generation n*: Similar to *generation 1*, but branching off *generation n-1*.

Also uses a coverage heuristic to pick priority. This search is often used with concolic execution.

## Combined Search

We established that no strategy can overcome all the others, so the combined search idea is to **run multiple searches** at the same time.

- Depends on conditions needed to exhibit bug;
- So will be as good as "best" solution, within a constant factor for wasting time with other algorithms;
- Could potentially use different algorithms to reach different parts of the program.

## SMT Solver Performance

SAT (satisfiability) solvers are at core of SMT (SAT module theories) solvers. In theory, could reduce all SMT queries to SAT queries, but in practice SMT-level optimizations are critical.

All those improvements can make an important difference in performance.

Popular SMT solvers are:

- **z3** - developed at Microsoft Research;
- **Yices** - developed at SRI;
- **STP** - Developed by Vijay Ganesh, now working at Waterloo;
- **CVC3** - Developed primarily at NYU.

Independently from the technique adopted, there may happens bugs difficult to spot (mainly because too much time consuming) and therefore this remains a basic limit.

## Symbolic Execution Systems

- **Resurgence** - Two key systems that triggered revival of this topic: *Dart* and *Exe*;
- **Sage** - Concolic executor developed at Microsoft Research. Uses generational search. Primarily targets bugs in file parsers. Used on production software at Microsoft since 2007.
- **KLEE** - Symbolically executes LLVM bitcode. Works in the style of our basic symbolic executor: uses `fork()` to manage multiple states, employs a variety of search strategies, mocks up environment to deal with system calls, file accesses, etc. Free available with LLVM distribution.
- **Mayhem** - Runs on binaries. Uses BFS-style search and native execution. Combines best of symbolic and concolic strategies. Automatically generates exploits when bugs found.
- **Mergepoint** - Extends Mayhem with a technique called *veritesting*: combines symbolic execution with static analysis; uses static analysis for complete code blocks; use symbolic execution for hard-to-analyze parts. Better balance of time between solver and executor.

## Summary

**Symbolic execution generalizes testing**, it uses static analysis to direct generation of tests that cover different program paths.

Used in practice to **find security-critical bugs** in production code (SAGE at Microsoft, Mergepoint for Linux).

Many tools are freely available.