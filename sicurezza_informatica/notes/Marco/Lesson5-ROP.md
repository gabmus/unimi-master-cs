# Return Oriented Programming (ROP)

Defenders and Attackers are chasing each other like cat and mouse:

- **Attack**: Break stack/heap to inject code.

- **Defense:** Make stack/heap non-executable to prevent code injection (DEP).

- **Attack**: Jump/return to libc.

- **Defense**: Hide the address of desired libc code or return address using ASLR.

- **Attack**: Brute force search (for 32-bit systems) or information leak (format string vulnerability).

- **Defence**: Avoid using libc code entirely and use code in the program text instead.
- **Attack**: Construct needed functionality using **return oriented programming** (ROP).

## The ROP Approach

The return oriented programming was introduced by Hovav Shacham in 2007 in his text *The Geometry of Innocent Flesh on the Bone: Return-into-libc without Function Calls (on the x86), CCS'07*.

The main idea is, rather than use a single (libc) function to run your shellcode, **string together pieces of existing code, called gadgets**, to do it instead.

This brings new challenges:

- Finding the gadgets you need;
- String them together.

**Gadgets** are instruction groups that end with a `ret` command.

The Stack serves as the code pool to find gadgets:

- `%esp` is the program counter;
- Gadgets invoked via `ret` instruction;
- Gadgets get their argument via `pop` instructions and similar.

*Example:*

![image-20200122145348583](img/img5.png)

## Gadgets Are Cool

Running a ROP attack, as we saw, requires pieces of code named **gadgets** glued together to make out malicious code. The problem now is how to find them.

![image-20200122145649022](img/img6.png)

First approach: **Automate a search of the target binary for gadgets** by looking for `ret` instructions, and then work backwards.

Are there sufficient gadgets to do anything interesting? Yes. Shacham found that for significant codebases, like libc, **gadgets are Turing complete**, therefore can do pretty much everything, especially on x86's dense instruction set.

Schwartz et al (USENIX Security '11) have automated gadget shellcode creation, through not needing/requiring Turing completeness.

## Blind ROP

Randomizing the location of the code, by compiling for position independence, on a 64-bit machine makes attacks very difficult. Recent, published attacks are often for 32-bit versions of executables.

To overcome this defense, **Blind ROP** was born. If server restarts on a crash, but does not re-randomize:

1. Read the stack to **leak canaries and a return address**;
2. Find gadgets (at run-time) to **effect call to `write`**;
3. **Dump binary to find gadgets for shellcode**.

This way the "ROP team" was able to **completely automatically**, only through remote interactions, **develop a remote code exploit for nginx**, a popular web server.

In conclusion, give an inch and they take a mile. Memory safety is really useful.















