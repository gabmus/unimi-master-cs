# Use-After-Free Vulnerabilities

The use-after-free vulnerabilities (UAF) comes in two different types:

- Vulnerabilities on the heap;
- Vulnerabilities on the stack.

It works only on programs that uses **languages that leaves the memory handling to the programmer** (like C/C++). For efficiency reasons, those languages don't make controls over deallocated pointers and so they are vulnerable to this kind of attacks.

Languages like Java, that gets noticed when we try to use a deallocated pointer (garbage collector) won't let us do that, because an exception is trowed.

Use-after-free vulnerabilities are **very hard to spot during manual code review** as they require knowing the pattern of allocation and deallocation that occurs during a program's execution.

The vulnerability is a **temporary** one, that only exists at particular points in time, when the stale pointer presents itself.

UAF vulnerabilities happens when **a pointer to an object that has been freed is deferenced**. This can result in information leakage, but the attacker could also modify an unintended memory location that **potentially can lead to code execution**.

*Example: Leaving a dangling pointer*

```C
int* ptr = new int;
*ptr = 8;
int* ptr2 = new int;
*ptr = -5;

ptr = ptr2;
delete ptr2;	// ptr is left dangling
ptr2 = null;
```

*Example: a UAF vulnerability*

```c
int main( ){
    char *a , *b;
    int i;
    a = malloc(16);
    b = a + 5;
    
    free(a);
    b[2] = 'c';		// Here i'm trying to use an address that does not come from a 						// malloc or a free: UAF on heap
    b = retptr();
    *b = 'c'; 		// Here i'm trying to use the address of the ex-variable from 	 				 	 // the stack returned from function retptr: UAF on stack
}

char* retptr() {
    char p,*q;
    q = &p;
    return q;		// Here i'm trying to return q, the address of variable p on the 					 // stack that will be deallocated when returning. I'm returning    					// and unvalid exploitable address 
}
```