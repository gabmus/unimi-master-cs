# Meltdown and Spectre

Vulnerabilities in modern computers leak passwords and sensitive data.

Meltdown and Spectre **exploit critical vulnerabilities in modern processors**. These hardware vulnerabilities allow programs to steal data which is currently processed on the computer.

While programs are typically not permitted to read data from other programs, a malicious program can exploit Meltdown and Spectre to get hold of secrets stored in the memory of other running programs. This might include your passwords stored in a password manager or bowser, your personal photos, emails, instant messages and even business-critical documents.

Meltdown and Spectre work on personal computers, mobile devices, and in the cloud. Depending on the cloud provider's infrastructure, it might be possible to steal data from other customers.

## Meltdown 

Meltdown breaks the most fundamental isolation between user applications and the operating system. 

This attack allows a program to access the memory, and thus also the secrets of other programs and the operating system.

If your computer has a vulnerable processor and runs an unpatched operating system, it is not safe to work with sensitive information without the chance of leaking the information.

This applies both to personal computers as well as cloud infrastructure. Luckily, there are software patches against Meltdown.

## Spectre

Spectre breaks the isolation between different applications. It allows an attacker to trick error-free programs, which follow best practices, into leaking their secrets. In facts, the safety checks of said best practice actually increase the attack surface and may make applications more susceptible to Spectre.

Spectre is harder to exploit than Meltdown, but it is also harder to mitigate. However, it is possible to prevent specific known exploits based on Spectre through software patches.

## Notes

- Most computer are affected by those vulnerabilities;
- It is not possible to detect if someone has exploited Meltdown or Spectre on your own computer, the exploitation does not leave any traces in traditional log files;
- Antiviruses can in theory detect and block these attacks, but this is unlikely in practice. Unlike usual malware, Meltdown and Spectre are hard to distinguish from regular benign applications. However, your antivirus may detect malware which uses the attacks by comparing binaries after they become known.
- If a system is affected by those vulnerabilities, the proof-of-concept exploit can read the memory content of your computer. This may include passwords and sensitive data stored on the system.
- There are patches against those vulnerabilities for every operating system. There is also work to harden software against future exploitation of Spectre, respectively to patch software after exploitation through Spectre.
- Meltdown is named as such because it melts security boundaries which are normally enforced by the hardware.
- Spectre is named as such because of its root cause, speculative execution. As it is not easy to fix, it will haunt us for quite some time.