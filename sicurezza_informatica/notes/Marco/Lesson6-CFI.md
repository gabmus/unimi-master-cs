# Control Flow Integrity

## Behavior-Based Detection

Stack canaries, non-executable data and ASLR aim to complicate various steps in a standard attack, but they still may not stop it.

We need to **observe the program's behavior** to understand if it is doing what we expect it to do. If not, it might be compromised.

Our new challenges are:

- Define what is the "expected behavior".
  - **Control flow graph (CFG).**
- Detect deviations from expectation in an efficient way.
  - **In-line reference monitor (IRM).**
- Avoid to compromise the detector.
  - **Sufficient randomness, immutability.**

It's important to protect the detector and in general all protections themselves, because many viruses as first thing they try to turn off or alter the protection systems (antivirus, antimalware, ...).

Tools for controls and analysis:

- **Call graph**: A representation of all function calls of a program (global simplification of a control flow graph);
- **Control flow graph**: A representation like a call graph, that also defines the return patter, by doing a map on where the control flow goes.
- **In-line reference monitor**: A tool to handle the *indirect calls* by using tags for avoiding cycles in the control flow that can be used to trick the security system.

## Efficiency

The **classic CFI** (2005) imposes a **16%** overhead on average, and **45%** in the worst case.

- Works on arbitrary executables;
- Not modular (no dynamically linked libraries).

The **modular CFI** (2014) imposes **5%** overhead on average, and **12%** in the worst case.

- Only uses C (part of LLVM);
- Modular, with separate compilation.

## Safety

Modular CFI can eliminate **95.75%** of ROP gadgets on x86-64 versions of SPEC2006 benchmark suite by ruling their use non-compliant with the CFG.

Average Indirect-target Reduction (AIR) > 99%. AIR is, in essence, the percentage of possible targets of indirect jumps that CFI rules out. For CFI: nearly all of them.

Jumps can be:

- Indirect: `call %eax`;
- Direct: `call 0x462fa23d`.

In general, most of the security systems force programs to follow their data flow but there are attacks that aim the program behavior itself.

## Call Graph vs Control Flow Graph

With a code like this

```c
sort2(int a[], int b[], int len) {
	sort(a, len, lt);
	sort(b, len, gt);
}
```

```c
bool lt(int x, int y) {
    return x<y;
}

bool gt(int x, int y) {
    return x>y;
}
```

The **call graph** associated with that code only values the function calls:

![image-20200122153441497](img/img7.png)

The **control flow graph**, does not only track function calls, but also tracks returns:

![image-20200122153720727](img/img8.png)

The computation of all the call/return CFG is made in advance during compilation or from the binary file.

CFG let us monitor the control flow of the program and ensure that it only follows paths allowed.

**Direct calls need not be monitored **, because they always call the same target. Assuming the code is immutable, the target address cannot be changed.

The CFG **monitors only indirect calls**, which are made via register or `¶et`

- `jmp`, `call`, `ret` with non-constant targets.

## In-line Monitor

The monitor in-line is implemented as a **program transformation**:

- Insert a **label just before the target address** of an indirect transfer;
- Insert **code to check the label of the target** at each indirect transfer and abort if the label does not match.

Those labels are determined by the CFG.

### Simplest labeling

![image-20200122154844227](img/img9.png) 

- Use the same label at all targets;

- Blocks return to the start of direct-only calls targets but not incorrect ones.

### Detailed labeling

![image-20200122155424244](img/img10.png)

Use of *constraints*:

- Return sites from calls to `sort` must share a label *L*;
- Call targets `gt` and `lt` must share a label *M*;
- Remaining label unconstrained *N*;

Still permits call from site A to return to site B.

# Defeating CFI

Let's try some strategies to pass the CFI controls:

- **Inject code** that has a legal label? 
  - Won't work because we assume **non-executable data**;
- **Modify code labels** to allow the desired control flow?
  - Won't work because the **code is immutable**;
- **Modify stack during a check** to make it seem to succeed? 
  - Won't work, because **adversary cannot change registers** into which we load relevant data.

CFI can defeat control flow modifying attacks (remote code injection, ROP/return-to-libc, ...), but **not manipulation of control-flow that is allowed by the labels/graph**. This is known as **mimicry attacks**, and the simple single-label CFG is susceptible to these.

CFI also does **not control data leaks or corruptions**. Hearthbleed would not be prevented, nor the `authenticated` buffer overflow, because control modification is allowed by graph.













