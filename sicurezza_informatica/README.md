# Sicurezza Informatica

Il corso tratta dello studio di tipi di attacchi ed exploitation di errori e di vulnerabilità nei programmi.

### Modalità d'esame

- Parte teorica a quiz (1 ora e mezza max)
- Esercizio pratico su computer (3 ore max)

Entrambi vengono svolti nello stesso giorno.
