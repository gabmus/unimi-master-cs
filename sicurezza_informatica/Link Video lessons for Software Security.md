## Link Videolezioni corso Sicurezza 

Il corso di software security di Lanzi è stato copiato quasi interamente da YouTube. Ha copiato pure le slide utilizzate quindi di seguito trovate i link alle videolezioni sulle stesse slide utilizzate da Lanzi. (Sono in inglese, rip Filippo)



- Lezione 0: **Overflow Introduction**
  [Software security - Low Level Security Introduction](https://www.youtube.com/watch?v=_7EhoAsWyQM&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=4)

- Lezione 1: **Memory Layout**
  [Software security - Memory Layout](https://www.youtube.com/watch?v=uouBwjDe17M&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=5)

- Lezione 2: **Buffer Overflow**

  [Software security - Buffer Overflow](https://www.youtube.com/watch?v=JqoDzNzvRu4&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=6)

- Lezione 3:  **Code Injection**

  [Software security - Code Injection](https://www.youtube.com/watch?v=H0FEr_-udj8&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=7)

- Lezione 4: **Heap Overflow**

  Il prof non ha usato le slide di questo sito, ma in questo video parla un pochino dell'heap overflow.

  [Software security - Other Memory Exploits](https://www.youtube.com/watch?v=mK1MX-ATrX8&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=8)

- Lezione 5: **User After Free**

  Il prof non ha usato le slide di questo sito

- Lezione 6: **Memory Safety & Type Safety**

  Il prof ha unito le slide di questi 4 video

  [Software security - Defenses Against Low Level Attacks Introduction](https://www.youtube.com/watch?v=PVW_JdrbpOY&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=10)

  [Software security - Memory Safety](https://www.youtube.com/watch?v=CqaEDK9HjRM&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=11)

  [Software security - Type Safety](https://www.youtube.com/watch?v=maqsnRRfgiE&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=12)

  [Software security - Avoiding Exploitation](https://www.youtube.com/watch?v=QDjnmLPnE7Y&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=13)

- Lezione 7: **ROP**

  [Software security - Return Oriented Programming - ROP](https://www.youtube.com/watch?v=XZa0Yu6i_ew&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=14)

- Lezione 8: **CFI**

  [Software security - Control Flow Integrity](https://www.youtube.com/watch?v=LKK7U_gF744&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=15)

- Lezione 9-10 : **Static Analysis**

  Il prof ha unito le slide di questi 7 video

  [Software security - Static Analysis Introduction part 1](https://www.youtube.com/watch?v=HDPoRF7cqeE&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=35)

  [Software security - Static Analysis Introduction part 2](https://www.youtube.com/watch?v=7GAx7vhbLxY&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=36)

  [Software security - Flow Analysis](https://www.youtube.com/watch?v=jn1ouGTNonI&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=37)

  [Software security - Flow Analysis Adding Sensitivity](https://www.youtube.com/watch?v=UhY8_Wb40B0&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=38)

  [Software security - Context Sensitive Analysis](https://www.youtube.com/watch?v=ICj4iuJqElo&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=39)

  [Software security - Flow Analysis Scaling it up to a Complete Language and Problem Set](https://www.youtube.com/watch?v=wxD036vmGBw&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=40)

  [Software security - Challenges and Variations](https://www.youtube.com/watch?v=-ySWQVFQBgw&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=41)

- Lezione 11-12: **Symbolic Execution**

  Il prof ha unito le slide di questi 5 video

  [Software security - Introducing Symbolic Execution](https://www.youtube.com/watch?v=FlzroEd4pnw&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=42)

  [Software security - Symbolic Execution A Little History](https://www.youtube.com/watch?v=8p8SpTeXsvY&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=43)

  [Software security - Basic Symbolic Execution](https://www.youtube.com/watch?v=k6GbaJq-aFo&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=44)

  [Software security - Symbolic Execution as Search, and the Rise of Solvers](https://www.youtube.com/watch?v=YAr10OtZx2w&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=45)

  [Software security - Symbolic Execution Systems](https://www.youtube.com/watch?v=T46n8rQapm8&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=46)

- Lezione 13: **Fuzzing**

  [Software security - Fuzzing](https://www.youtube.com/watch?v=XbCRpogu_Iw&list=PL2jykFOD1AWY1E3MB38_uOfpvEf4gnW80&index=49)

- Lezione 14: **Z3**

  Il prof non ha usato le slide di questo sito

- Lezione 15: **Meltdown & Spectre**

  Il prof non ha usato le slide di questo sito