# Search algorithms for planning

In questa lezione andremo a trattare algoritmi di ricerca su alberi che, applicati poi alla robotica, forniscono soluzioni a vari problemi del settore (e.g. pathfinding).

### Search problem

Per problema di ricerca intendiamo cercare un insieme di azioni che svolte ci faranno raggiungere un traguardo che è impossibile da raggiungere usando una sola di quelle azioni.

Un esempio di problema di ricerca potrebbe essere dato dalla ricerca di un percorso su un grafo da una sorgente S a un goal G.

![](img/graph.png)

Il setting del nostro problema si riduce quindi ad avere:

- Un **agente**
- Un **goal**
- Una **formulazione** del problema, ovvero
  - Un insieme di **azioni** svolgibili dall'agente
  - Un insieme di **stati** percorribili dall'agente

In sostanza, vogliamo che l'agente raggiunga il goal attraversando gli stati specificati tramite le azioni specificate.

### Planning problem

Per problemi di planning intendiamo problemi più intricati di quelli di ricerca, che possono essere risolti sfruttando la struttura del problema stesso per costruire piani di azione complessi.

Un esempio di problema di planning è cercare di riconfigurare una pila di scatole, ognuna con suo colore e posizione, e ottenere una pila ordinata in un modo diverso da quello originale.

![](img/stat.png)

Il setting del nostro problema si riduce quindi ad avere:

- Un **agente**
- Un **goal**
- Una **formulazione** del problema, ovvero
  - Un insieme complesso di **azioni**, dotate di
    - Precondizioni
    - Effetti
  - Un insieme complesso di **stati**, composto da statement proposizionali

Quello che vogliamo fare effettivamente è trovare un modo per arrivare nello stato finale: non sempre è possibile calcolare tutte le possibili azioni che ci hanno condotto a questo come nei problemi di ricerca!

### Search algorithms for planning

I problemi di search e quelli di planning non sono due mondi a sé e disgiunti, ma sono molto interlacciati poiché tentano di risolvere problemi **simili**, con l'unica differenza che quelli di planning sono più complessi.

Per questo motivo, i problemi di planning vengono spesso **scomposti** in problemi più semplici, risolvibili come problemi di search: uno di questi che vedremo è il path-planning, che sfrutta algoritmi di ricerca come il BFS e il DFS per trovare il path ottimale da seguire.

#### Esempio: 8-puzzle

L' 8-puzzle è un problema di ricerca discreto, in cui vogliamo riconfigurare una griglia di tile numerate a partire da posizioni casuali a posizioni in ordine crescente.

![](img/8puz.png)

Vediamo il **setting** del problema:

- Set di stati: posizione di ogni tile numerata e di quella blank
- Stato iniziale
- Stato goal
- Azioni: spostamento di una tile a Sinistra, Destra, Su, Giù
- Transizione: dato uno stato di partenza e una azione si ottiene una nuova griglia in uno stato differente
- Test di raggiungimento goal: test che verifica se la griglia dopo una transizione è uguale allo stato di goal
- Costo: ogni movimento di una tile costa 1, vogliamo minimizzare il numero di movimenti per raggiungere lo stato di goal

![](img/8pt.png)

Vedendo come si possono espandere i possibili stati, otteniamo un (enorme) albero di possibili azioni che possiamo svolgere per arrivare alla soluzione, pertanto ci serve un algoritmo di esplorazione di questo albero che minimizzi il numero di movimenti, ovvero il costo, ovvero la profondità della soluzione trovata nell'albero.

Ora, **riformuliamo** il problema secondo un approccio **state-based**:

- Definiamo un **set di nodi**, ognuno dei quali rappresenta uno **stato**: assumiamo che lo **spazio** degli stati sia **finito** e **discreto**.
- Per ogni stato, abbiamo un **set di azioni** che possono essere compiute dall'agente da quello stato (non sempre sono quattro le azioni possibili!)
- Definiamo un **modello di transizione** tale per cui dato uno stato iniziale e una azione otteniamo uno stato di arrivo.
- Ogni transizione ha un **costo**: assumiamo che sia maggiore o uguale di una costante positiva (aka 1)
- Definiamo uno **stato iniziale** e uno **stato goal**

Possiamo quindi vedere il nostro problema come un grafo $G = (V, E) $ dove 

- I **vertici** $v \in V$ rappresentano gli **stati** (di cui uno di essi è iniziale e un altro è goal)
- Gli **archi** $e \in E$ rappresentano le **azioni** svolgibili da ogni stato con il costo associato

![](img/gve.png)

Detto questo, vogliamo descrivere effettivamente qual è la soluzione che vogliamo: ci basta trovare una soluzione qualsiasi o vogliamo una particolare soluzione che minimizza il costo?

![](img/feas.png)

Se vogliamo solo conoscere se **esiste una soluzione** e quale sia, allora stiamo trattando un problema di **feasibility** in cui vogliamo trovare una sequenza di azioni (path) che dallo stato iniziale ci porti allo stato finale specificato.

![](img/opt.png)

Se invece vogliamo non solo conoscere una qualsiasi **soluzione** ma vogliamo che sia la **migliore**, quindi con minor costo, stiamo trattando di un problema di **optimality**.

Non è sempre possibile trovare la soluzione ottima come vedremo, pertanto esistono **soluzioni approssimate**, più semplici da calcolare ma comunque vicine a quella ottima.

### One problem, many representations

La qualità della soluzione e della scelta degli algoritmi dipende molto da una **attenta e accurata formulazione del problema**.

Per esempio, se volessi programmare un robot che dal dipartimento di Informatica raggiunga una delle due stazioni metro più vicina potrei formulare e quindi rappresentare il problema in più modi!

![](img/robm1.png) 

Una rappresentazione a grafo è molto semplice, ma può bastare? E' abbastanza granulare per poterla applicare nella realtà?

![](img/robm2.png)

Una rappresentazione a griglia è più fitta, ma quanto deve essere grande la griglia? Quali altri elementi devo considerare (traffico, passaggi pedonali fra edifici, mezzi pubblici...)? 

Come vediamo non è semplice formulare un problema ma è importantissimo per trovare una soluzione fattibile e possibilmente ottimizzata.

### Problem specification

Proviamo ora a capire come specificare al meglio un planning problem.

Un primo approccio possibile che potrebbe venirci in mente è definire il grafo completo $G= (V,E)$ con tutti i possibili stati e transizioni. 

Tuttavia questa è una soluzione **naive**, dato che la maggior parte delle volte si ha una **esplosione combinatoria** di stati per cui elencarli tutti è completamente infattibile: basti pensare a tutti i possibili stati che posso avere su una board di scacchi (~10^47^ stati).

Ci serve quindi una procedura efficiente per il goal checking.

### Caratteristiche di un search algorithm

Un algoritmo di ricerca esplora il grafo di stati e transizioni $G = (V, E)$ fino a quando non scopre la soluzione desiderata:

- Per un problema di **feasibility**, quando si arriva al goal node l'algoritmo restituisce il percorso che ci ha portato a quel nodo
- Per un problema di **optimality**, quando si arriva al goal node, l'algoritmo verifica che il percorso appena effettuato sia quello col costo minore prima di ritornarlo come soluzione

Risulta quindi necessario **salvare il percorso svolto** fino al goal node, passo per passo, costruendo il cosiddetto sottografo di ricerca.

Quello che ora dobbiamo definire è la **strategia di esplorazione**, ovvero come svolgere la scelta di quale nodo esplorare a partire dal nodo in cui l'agente si trova.

### Valutazione di un search algorithm

Un algoritmo di ricerca viene valutato sotto diverse dimensione, a partire dalla strategia esplorativa fino alla soluzione ottenuta (ottima/feasible) e gli eventuali costi spaziotemporali:

- **Completezza**
  - Se c'è una soluzione, allora l'algoritmo è in grado di trovarla.
  - ***Sistematicità*** (sottoproprietà)
    - Se lo spazio degli stati è finito, l'algoritmo visiterà tutti i possibili stati
    - Se lo spazio degli stati è infinito, allora dobbiamo chiederci se la ricerca in sé è sistematica
      - Se c'è una soluzione, l'algoritmo deve tornarla in tempo finito
      - Se non c'è una soluzione, l'algoritmo non termina ma tutti gli stati raggiungibili devono essere stati visitati: se il tempo tende all'infinito, allora tutti gli stati devono essere stati visitati dall'algoritmo
- **Soundness**
  - Se l'algoritmo trova una soluzione, essa è in linea con le caratteristiche desiderate nella formulazione del problema. e.g.
    - Feasibility: La soluzione trovata porta a un goal?
    - Optimality: La soluzione trovata porta a un goal con costo minimo?
- **Ottimalità**
  - Se c'è una soluzione, allora l'algoritmo è in grado di trovare quella ottimale.
- **Complessità spaziale**
  - Quantità di memoria necessaria per trovare la soluzione.
- **Complessità temporale**
  - Quantità di tempo necessaria per trovare la soluzione.

Le complessità spaziotemporali sono espresse in "Big O" notation, indicando l'upper bound stimato per quell'algoritmo (es. $O(n^k)$ = polinomiale, $O(2^n)$ = esponenziale ...).

Vediamo degli esempi di algoritmi di ricerca in un labirinto:

![](img/cpsy.png)

Un algoritmo di ricerca completo e sistematico proverebbe tutti i possibili cammini (colorandoli tutti), prima di trovare finalmente l'uscita. Ovviamente dal punto di vista di complessità spaziale e/o temporale sarebbe estremamente più gravoso.

![](img/ncns.png)

Un algoritmo di ricerca non completo e non sistematico non garantisce di trovare una soluzione sempre poiché una volta tracciata una linea (e quindi un percorso) rimarrebbe bloccato in un vicolo cieco senza poter tornare indietro, tuttavia è poco gravoso dal punto di vista di complessità spaziotemporale.

## Algoritmi di ricerca feasible

Vediamo un quindi un esempio di istanza per un problema di search:

![](img/ex.png)

A partire da questo dobbiamo definire una strategia di search per il nostro algoritmo che ci dica quale nodo visitare a partire da quello in cui sono correntemente.

Vediamo i principali algoritmi di ricerca su grafi: DFS e BFS

### Depth-First Search (DFS) 

L'algoritmo di DFS espande la sua ricerca in profondità, andando sempre più "giù" nel grafo fino a quando, non trovando un vicolo cieco, svolge del **back-tracking** per poter considerare anche gli altri rami che non aveva considerato.

![](img/dfs.gif)

In questo caso, la preferenza del DFS è quella di esplorare il left-most child dell'albero in figura ma funzionerebbe comunque usando altre regole di ordine.

Seguendo queste regole, la soluzione del grafo precedente, in cui da A dovevamo raggiungere E sarebbe 

![](img/dfs2.png)

$(A \rightarrow B \rightarrow D \rightarrow F \rightarrow G \rightarrow E)$ con profondità 5 e larghezza 2

#### Valutazione DFS

Il DFS è sistematico è completo quando il nostro grafo è privo di loop: quando invece sono presenti dobbiamo svolgere del loop removal per poter procedere ed evitare di rimanere bloccati all'interno del loop.

Il DFS con loop removal è considerato sound e completo per spazi finiti: per calcolarne le complessità spaziotemporali usiamo due variabili

- $d$ è il maximum depth, ovvero il massimo numero di passi nella soluzione (nell'esempio 5)
- $b$ è il maximum branching, ovvero il massimo numero di azioni fattibili in uno stato (nell'esempio 2)

Pertanto per trovare una soluzione avrò

- Complessità spaziale $O(d)$
- Complessità temporale $1+b+b^2+...+b^d = O(b^d)$

### Breadth-first search (BFS)

L'algoritmo BFS anzichè andare in profondità procede in larghezza, esplorando prima tutti i nodi a una certa distanza prima di procedere più avanti in profondità.






<img src="img/bfs.gif" style="zoom:150%;" />

Anche in questo caso si può partire dal left-most child (ma anche in ordine lessicografico) per esplorare il grafo.

Seguendo queste regole, la soluzione del grafo precedente, in cui da A dovevamo raggiungere E sarebbe 

![](img/bfs2.png)

La soluzione raggiunta, come possiamo vedere, è anche quella **meno profonda**, infatti conta solo 3 azioni di profondità (non è per forza detto che sia la meno costosa!): questa è una proprietà caratteristica dell'algoritmo BFS (**shallowest solution**).

#### Valutazione BFS

Il BFS come possiamo osservare è completo e sistematico e non ha problemi di loop. Inoltre è più semplice calcolare la complessità spaziotemporale poiché è esattamente di $O(b^q)$, con $q$ profondità della soluzione meno profonda. 

### Redundancy in DFS/BFS

Sia DFS che BFS visitano i nodi multiple volte come possiamo vedere negli scorsi esempi

![](img/bds.png)

Per evitare che questa ridondanza accada è sufficiente utilizzare una enqueued list (come usato nell'algoritmo) dove mettere i nodi che dobbiamo ancora esplorare così da evitare di aggiungerli una seconda volta e ripetere lo stesso path di esplorazione

**Algoritmo DFS(G)** 

![](img/dfsq.png)

Dato un grafo $G = (V, E)$ e nodo sorgente $s \in V$, l'algoritmo cerca di esplorare il grafo il più possibile in profondità, partendo dall’ultimo vertice scoperto $v$ che abbia ancora vertici inesplorati uscenti da esso. Terminata l’esplorazione torna indietro per esplorare il vertice con cui $v$ era stato scoperto e così via fino a quando tutti i vertici sono esplorati. 

Non parto da un singolo nodo sorgente ma svolgo l’esplorazione per tutti i nodi del grafo fino a quando non vengono scoperti tutti. Avrò anche un timer globale che serve per indicare il tempo di scoperta e di fine esplorazione per ogni nodo.

Come nella BFS, quando scopro un vertice $v$, memorizzo il parent e lo coloro da bianco a grigio, ma in questo caso salvo anche il tempo di scoperta, esploro in profondità tutti i nodi uscenti da esso e solo alla fine lo coloro di nero e salvo il tempo di fine esplorazione.

```pseudocode
foreach u in V		
	col[u] = white; //inizializzazione: pongo tutti i vertici a bianco,		
	p[u] = null;	 //e i parent a null		

time = 0; 		//inizializzo timer globale	
foreach u in V		
	if col[u] == white			
		DFS_visit(u);		//faccio partire DFS per tutti i nodi white

DFS_visit(u):	
    time++; d[u] = time; color[u] = gray;	//set timer e colore scoperta	
    
    foreach a in Adj[u]		 
    	if col[a] == white			// il prune avviene tramite colorazione
    		DFS_visit(a);			//faccio partire DFS per i nodi 		
    		p[a] = v;			//adiacenti e setto il loro parent		
    
    time++; f[u] = time; col[u] = black;		//set timer e colore fine esploraz. 
```



La visita in profondità fornisce molte informazioni sulla struttura di un grafo: una delle più importanti è che il sottografo dei parent forma effettivamente una foresta d’alberi.

**Algoritmo BFS(G, s)**  

![](img/bfsq.png)

Dato un grafo $G = (V, E)$ e nodo sorgente $s \in V$, l'algoritmo esplora i nodi raggiungibili e calcola la loro distanza da esso, creando un albero BFS avente radice $s$, per cui ogni nodo $v$ raggiungibile da $s$ sarà collegato a esso da un cammino minimo.

Viene definita visita in ampiezza poichè esplora tutti i vertici a distanza $k$ prima di esplorare quelli a distanza (o profondità) $k+1$.

Per tenere traccia del lavoro in corso coloro i nodi man mano che li esploro per capire se

- non sono ancora stati scoperti → bianco
- sono stati scoperti ma non esplorati → grigio
- sono stati esplorati completamente → nero

Assegno a ogni nodo *v* scoperto il parent, ovvero il nodo *u* da cui *v* è stato scoperto e la distanza dalla sorgente, che sarà la distanza del parent *u* più uno.

```pseudocode
foreach u in V		
	col[u] = white;   //inizializzazione: pongo tutti i vertici a bianco,		
	d[u] = inf;		 //le distanze a infinito e i parent a null		
	p[u] = null;		

col[s] = gray;		//inizializzo il nodo sorgente: scoperto, diventa grigio 	
d[s] = 0;			//avrà distanza 0 da se stesso e non avrà alcun parent 
p[s] = null;		//perchè è la radice
	
queue = empty;	//uso una coda perchè ho una esplorazione FIFO	
enqueue(Q,s);	//metto in coda il nodo sorgente
	
while notempty(Q)		
	u = dequeue(Q);				//estraggo il nodo u dalla coda		
	foreach v in adj[u]			//per ogni nodo adiacente a u			
    	if col[v] = white		//se il nodo v è bianco				
			col[v] = gray		//allora lo scopro e diventa grigio				
             d[v] = d[u] + 1;     //la distanza è quella di u più 1				
			p[v] = u;			//il parent è u				
			enqueue(Q,v);	//metto in coda esplorazione v
    col[u] = black;			//ho finito di esplorare la lista di adiacenza di u, lo pongo a nero
```


In questo modo posso ridurre di molto la complessità temporale richiesta per l'esplorazione.

### Depth-limited Search

Variante del DFS, è stata ideata per risolvere problemi di search quando i grafi sono molto densi/profondi o quando lo spazio degli stati è infinito.

E' sufficiente svolgere un DFS e limitarlo fino a una certa profondità di esplorazione $l$: potrebbe dare un risultato non-ottimale però almeno posso evitare di fare controlli troppo estesi quando non è necessario.

Complessità

- Spaziale $O(b^l)$
- Temporale $O(bl)$

### Iterative-deepening DFS

Variante della Depth-limited search, invece di fermarsi a un certo limite, si svolgono diverse iterazioni di profondità crescente, partendo da $l=0$ e incrementando fino a quando la soluzione non è trovata.

Questo algoritmo è ovviamente completo in spazi finiti.

Complessità

- Spaziale $O(b^q)$
- Temporale $O(bq)$

## Algoritmi di ricerca optimal

Assumiamo ora di voler ottenere non solo una soluzione che mi porti allo stato goal ma anche quella dal costo minore, dove con costo minore intendiamo quella dove la somma del costo delle transizioni lungo il path sia minima.

Possiamo partire dall'idea del BFS, ovvero quella di scandagliare per livelli di profondità, all'idea dell'UCS (Uniform Cost Search), ovvero quella di scandagliare per livelli di costo accumulati delle transizioni (se i costi sono tutti uguali equivale al BFS).

![](img/ucs.png)

Questo algoritmo è completo ma anche ottimo poiché mi permette di trovare il percorso col minor costo: dimostriamolo

![](img/optucs.png)

Ipotizziamo che

- L'algoritmo UCS ha selezionato che il percorso ottimale per raggiungere il goal $V$ dal nodo $A$ è il percorso $p$
- $p$ non è, in realtà, il percorso ottimale per raggiungere $V$

Questo vuol dire che esiste un nodo $X$ nella stessa frontiera di $V$ che 

- dà origine a un percorso $p'_1$ da $A$ a $X$ 
- dà origine a un percorso $p'_2$ da $X$ a $V$

E che quindi il percorso $p' = p'_1+p'_2$ sia quello ottimo, ovvero $c(p') < c(p)$ e che quindi $c(p'_1) < c(p'_1)+(p'_2) < c(p)$ 

Staremmo quindi dicendo che $c(p'_1) < c(p)$ ma se questo fosse vero, allora l'UCS avrebbe selezionato $p'_1$ come percorso visto che avrebbe dovuto avere un costo minore.

Pertanto questa ipotesi è rigettata ed è stata confermata l'ottimalità dell'UCS.

### UCS with extended list

Possiamo ottimizzare e ridurre i tempi di esecuzione anche per l'UCS prunando i rami dell'albero come abbiamo fatto coi DFS e BFS. Tuttavia non utilizzeremo una enqueued list, poichè rischierebbe di rendere la ricerca non sound, ma una extended list: ogni volta che selezioniamo un nodo:

- Se il nodo è già nella extended list lo discardiamo
- Altrimenti lo estendiamo e lo aggiungiamo alla lista

![](img/ucsex.png)

### Informed vs non-informed search

Questi algoritmi che abbiamo visto sono tutti di ricerca non-informata in quanto la strategia di esplorazione non utilizza semantiche particolari per prevedere dove trovare il goal.

Esistono euristiche di approssimazione che migliorano e ottimizzano questi algoritmi:

- Hill climbing
- Beam (of width $w$)

## Summing up

![](img/sump.png)

![](img/recap.png)