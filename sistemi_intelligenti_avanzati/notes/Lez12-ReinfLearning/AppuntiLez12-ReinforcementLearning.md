# Reinforcement learning

## Agenti intelligenti

Per agenti intelligenti, intendiamo entità che agiscono e che operano scelte, controllano, pianificano e predicono strategie di controllo seguendo un particolare comportamento.

### Stimoli e riflesso pavloviano

Ogni agente interagisce con l'ambiente, trae informazioni da esso e agisce di conseguenza, in seguito a una interna ponderazione. L'ambiente esterno fornisce quindi degli **stimoli** per il nostro agente.

Consideriamo un cane come nostro agente: chiaramente alla vista di una ciotola di cibo esso inizierà a salivare, in attesa di mettere qualcosa sotto i denti. La vista del cibo è quindi uno **stimolo naturale (incondizionato)** .

Ivan Pavlov, in un curioso esperimento, ha provato a creare uno **stimolo artificiale** collegato a quello naturale del cibo: ogni volta che dava da mangiare al suo cane, faceva partire poco prima un metronomo. Dopo una serie di iterazioni ripetute, notò che il cane iniziava a salivare appena udiva il suono del metronomo.

E' evidente come il suono del metronomo sia stato collegato a quello del cibo, diventando quindi uno **stimolo condizionato** per cui il cane, a quel suono, si aspetta la ciotola di cibo anche se non la ha ancora vista.

La chiave di questo condizionamento è il **reward** del cibo, ovviamente, in quanto il cane è attratto dalla scorpacciata imminente.

Pavlov, in seguito a questo esperimento, capì una cosa importante dell'intelligenza negli esseri viventi: la mente agisce in modo **predittivo** anziché puramente reattivo. Nell'esperimento infatti possiamo vedere come il cane pregusta il cibo anche se non ha ancora visto la ciotola.

Questo è giustificato in quanto la relativa lentezza della risposta del sistema nervoso agli stimoli è ben nota: per esempio, se non avessimo una mentalità predittiva, cadremmo sempre ogni volta che ci sbilanciamo poiché non abbiamo abbastanza tempo per reagire coscientemente.

## Apprendimento con rinforzo (reward)

Abbiamo quindi visto come un agente può essere condizionato nelle proprie azioni tramite gli stimoli che riceve e l'esperienza vissuta dalle precedenti azioni. Andiamo a definire ciò in modo più esaustivo.

Un agente interagisce con l'ambiente tramite **azioni** su di esso: il cane può stare fermo, correre, mangiare e così via. Questo insieme di azioni può essere continuo o discreto.

Un agente monitora l'ambiente ricevendo **stimoli** come **input**: il cane può usare i suoi sensi per trarre informazioni da esso (annusare il cibo, vedere la ciotola, udire il metronomo...)

La **scelta dell'azione** da compiere in quel momento non è banale e richiede un certo grado di intelligenza: il cane sa che mangiare dà più "reward" in confronto al continuare a correre o rimanere accucciato.

L'agente è in grado di **modificare il comportamento** in base agli stimoli che riceve e al **feedback** (reward) che ottiene in seguito dall'ambiente (il cane saliva al suono del metronomo per prepararsi al cibo). Ovviamente il reward ricevuto deve essere **associabile** all'azione **immediatamente** svolta, quindi non può essere troppo lontano nel tempo.

![](img/appr.png)

Il concetto quindi di **apprendimento con rinforzo**, detto anche reinforcement learning (RL), si pone su queste basi

### Concetto di reward

Solitamente il "reward" è una informazione di tipo qualitativo, spesso binaria, del tipo "giusto/sbagliato" o "successo/fallimento". 

E' importante notare come questa sia una **mera informazione**, di per sé neutrale, e che non ha alcun accenno a come aggiornare il comportamento dell'agente: deve essere l'agente stesso che deve **interpretare** questo segnale di rinforzo come positivo o negativo e di quanto, in modo da poter **adattare** meglio il proprio comportamento e quindi le sue successive azioni.

Questo è il nostro obiettivo quindi: creare degli agenti intelligenti che abbiano sufficiente reasoning per apprendere dalla loro esperienza e migliorare la scelta di azioni in base ai reward, e quindi al feedback, ricevuti.

#### Esempio applicativo

Un esempio di RL applicato ad algoritmi pubblicitari potrebbe essere il seguente.

![](img/ad.png)

L'algoritmo può mostrare a diversi utenti diversi posizionamenti delle pubblicità all'interno di un sito:

- la sua **azione** è la scelta nel posizionamento dell'ad
- il **rinforzo** che riceve è la quantità di click che riceve quell'ad in quella posizione

In questo modo, l'agente tenderà a prediligere i posizionamenti che rendono più click in quanto ogni click è un rinforzo positivo e quindi un reward maggiore.

### Ottimizzare la scelta dell'azione

Abbiamo detto che il nostro agente deve saper scegliere al meglio la prossima azione da svolgere in base alla sua esperienza: ma come gli vogliamo far compiere questa scelta? Qual è il ragionamento che deve compiere l'agente?

#### Exploration vs exploitation

Nel dilemma della scelta della azione migliore, abbiamo un ulteriore problema da considerare: preferire l'esplorazione di nuove soluzioni migliori o sfruttare quelle già conosciute al massimo?

Questo è il noto problema della scelta fra exploration ed exploitation:

- Nell'exploration l'agente preferisce esplorare lo spazio delle azioni per scoprire le azioni migliori: è importante che l'agente esplori lo spazio delle soluzioni, provando anche azioni apparentemente non ottimali.
- Nell'exploitation l'agente preferisce sfruttare le azioni migliori che già conosce poichè danno un reward assicurato

E' evidente come non sia possibile preferire strettamente l'una all'altra, ma sarebbe meglio trovare un sano bilanciamento fra le due: troppa exploration porterebbe a sprechi e sforzi inutili per trovare una azione migliore (che magari non esiste) e troppa exploitation potrebbe far perdere azioni migliori all'agente perchè troppo limitato nel suo circoletto, venendo surclassato da altri agenti più esplorativi.

Quindi quale sarebbe il comportamento ottimale? Ovvero come bilancio questi due frangenti?

#### n-Armed bandit problem

Abbiamo un agente che vuole giocare alle slot machine e ha di fronte a sé $n$ macchinette. A ogni turno può giocare a una macchinetta fra le tante (azione) e da essa otterrà un reward stocastico (ovviamente, è una slot machine!).

Il problema è stazionario, quindi le slot machine avranno le stesse probabilità durante tutta la durata del trial (non si adattano a farti perdere come quelle reali!)

L'obiettivo è massimizzare la ricompensa a lungo termine, non il reward istantane o.

Come facciamo a selezionare l'azione che fornisce la massima ricompensa a lungo termine se non conosciamo la statistica delle slot machine?

Supponiamo che 

- il reward della slot machine sia definito dalla densità di probabilità associata alla macchina
- la densità di probabilità del reward sia costante nel tempo e non nota
- la densità di probabilità sia descrivibile da una funzione analitica come una gaussiana, definita da media e stdev

Detto questo, per cercare di massimizzare la ricompensa, consento all'agente di avere memoria e di memorizzare il valore associato alle diverse azioni (altrimenti non potrei sfruttare l'esperienza derivata dalle azioni passate).

A un certo punto, potrei notare che una delle azioni fornisce più reward e sfruttare solo quella: starei facendo una scelta greedy, pertanto starei sfruttando la conoscenza ottenuta finora e sfruttarla per ottenere ricompense (exploitation). 

A questo punto perchè dovremmo scegliere una azione che non appare migliore e quindi esplorare altre azioni?

Siccome abbiamo detto che a noi interessa l'obiettivo a lungo termine e siccome la ricompensa **non è deterministica** non è detto che svolgendo sempre le stesse azioni, quindi scegliendo sempre la stessa macchinetta, posso raggiungere un più alto obiettivo a fine trial: in altre parole non mi interessa massimizzare la ricompensa istantanea, ma quella finale!

Per questo, è necessario preservare un minimo istinto di exploration per trovare soluzioni migliori alternative ed exploitare anche quelle, bilanciando i due aspetti.

#### Value function

L'idea principale è quella di dare un **valore** numerico a ogni azione.

Nel caso delle slot machine il reward istantaneo non può rappresentare questo valore associato all'azione perchè è solamente un campione estratto da una più larga distribuzione statistica: un valore più appropriato da associare ad ogni azione (quindi a ogni macchinetta) è il reward medio ottenuto da essa.

La value function quindi non è altro che una media campionaria costruita nel tempo dall'agente a partire dai reward istantanei forniti dall'ambiente in seguito a ogni azione.

Formalizzando, possiamo vedere il nostro problema modellato come segue:

- l'agente può selezionare una fra $n$ azioni dallo spazio $a = \{a_1, ... a_n\}$
- In funzione dell'azione scelta $a_k$, la slot machine mi darà un reward $r(a_k)$ 
- al tempo $t$, la value function per l'azione $a_k$ viene calcolata come media campionaria di tutti i reward precedenti $Q_t(a_k) = \frac{r_1(a_k) + ... + r_N(a_k)}{N(a_k)}$
- per il teorema del limite centrale, il valore medio campionario del reward medio tende al valore medio della distribuzione dei reward per $N \rightarrow \infin$, pertanto $\lim_{N(a_k) \to \infin} Q_t(a_k) = Q^*(a_k) = \mu_k$

Pertanto detto questo, la scelta ottimale all'istante t è quella che massimizza la Value Function in t fra tutte le possibili azioni $a_k$:
$$
a^* : Q_t(a^*) = \max_{a_k}{Q_t(a_k)}
$$
L'interazione fra agente e ambiente è aggiornata come segue

![](img/grd.png)

Questa è la scelta greedy ed exploitative che abbiamo menzionato prima: come introduciamo l'esplorazione?

Formalizziamo un attimo il problema:

![](img/bnd.png)

Come possiamo facilmente vedere ho un problema: supponiamo all'inizio che tutte le value function siano pari a zero, poichè non abbiamo ancora ottenuto alcun reward, quindi avremo $Q(a_1) =\ ...\ = Q(a_{10})$  

Supponiamo di scegliere l'azione $a_4$ e di ricevere da essa un reward $r(a_4) = 0.1$. Dopo questo, l'agente aggiorna la Value Function di $a_4$ per cui $Q(a_4) = 0.1$

Se dovessimo applicare la scelta greedy di prima sceglieremmo sempre l'azione $a_4$ poichè è quella con più alta value function ma lo è solamente perchè la abbiamo scelta per prima! Rimarrei bloccato in questo circolo vizioso (ed exploitative) senza trovare per forza la azione migliore.

Pertanto dobbiamo necessariamente dare spazio alla esplorazione.

#### Politiche $\epsilon$-greedy

Invece che scegliere il massimo fra le azioni a mia disposizione, utilizzo un modello basato su probabilità: l'azione greedy (maggior reward) ha una probabilità di essere scelta di $ 1-\epsilon$, altrimenti viene scelta una qualsiasi azione diversa, dove $\epsilon$ è una probabilità arbitrariamente piccola.

Riassumendo
$$
a^* : Q_t(a^*) = \max_{a_k}{Q_t(a_k)} \ \ \ P=1-\epsilon \\
a \ne a^* \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ P=\epsilon
$$
In questo modo mantengo una certa capacità di esplorazione rimanendo comunque vicino alle scelte greedy (more exploitation, less exploration). Per questo motivo, essa viene definita anche una **near-greedy policy**

#### Pursuit methods

Una policy di pursuit cerca di inseguire, quindi dare più importanza, alle azioni che danno più reward ma in modo meno greedy rispetto al metodo $\epsilon$-greedy.

Inizialmente tutte le azioni hanno probabilità equivalente di essere scelte: per esempio, con 10 azioni disponibili, ognuna di esse avrà il 10% di probabilità di essere scelta.

Dopo ogni scelta, basandosi sui reward ottenuti, la probabilità di scegliere un'azione viene aggiornata:

- **Aumenta** la probabilità di scelta per l'azione con **value function migliore** 
- **Diminuisce** la probabilità di scelta per le **altre azioni**

Formalizzando, data $\pi(a_k)$ la probabilità di scegliere l'azione $a_k$
$$
\pi_{t+1}(a^*_{t+1}) = \pi_t(a^*_{t+1})+\beta[1-\pi_t(a^*_{t+1})]
$$
A differenza del $\epsilon$-greedy, che ha appunto un coefficiente greedy statico, qui il coefficiente di scelta greedy è dinamico nel tempo e varia man mano che si va avanti con le iterazioni: viene definito quindi pursuit poichè insegue quella con probabilità maggiore, quindi reward maggiore, (exploit) ma senza disdegnare altre opzioni (explore).

##### Roulette genetica

Questa politica di pursuit viene adottata anche nella "roulette" degli algoritmi genetici, dove tutte le azioni si ritrovano in un pool e ognuna di esse ha una certa probabilità di essere selezionata in base alla sua misura di fitness (value function).

![](img/roul.png)

In questo esempio vediamo come ci siano 4 azioni, in cui quella blu ha più probabilità di essere estratta poichè con maggiore fitness.

#### Comparazione finale

Se dovessimo comparare gli algoritmi di scelta visti finora nell'esempio dell' "n-armed bandit", potremmo vedere come gli algoritmi più esplorativi riescano a trovare e scegliere soluzioni sempre migliori, anche dopo un gran numero di trial.

![](img/gr.png)

Vediamo qui come 

- la scelta greedy costante rimane fissa sulla sua posizione iniziale e non migliorando nel tempo (nessuna esplorazione)
- la scelta greedy con $\epsilon$ pari a 0.1 è più efficace di quella a 0.01 nell'esplorare lo spazio delle azioni, riuscendo a trovarne sempre di nuove e migliori

Introducendo anche il pursuit, possiamo vedere come nel tempo, la percentuale di azioni ottime sale drasticamente rispetto ad una $\epsilon$ statica e non adattiva:

![](img/pr.png)

Concludiamo quindi che avere un buon bilanciamento fra esplorazione ed exploitation consente di avere risultati migliori.

### Implementazione ricorsiva di $Q(a)$

L'algoritmo che calcola $Q(a_k)$ viene svolto a ogni turno, pertanto vogliamo che sia il più semplice e veloce possibile. Inoltre non abbiamo una memoria infinita, quindi non possiamo tenere in memoria tutti i reward ottenuti ad ogni iterazione $t$.

Pertanto vogliamo aggregare questi risultati man mano che li ottengo: notiamo che
$$
Q_N(a_k) = \frac{r_1(a_k) +\ ...\ + r_N(a_k)}{N(a_k)}
$$
e subito dopo avremmo che
$$
Q_{N+1}(a_k) = \frac{r_1(a_k) +\ ...\ + r_N(a_k) + r_{N+1}(a_k)}{(N+1)(a_k)}
$$
![](img/qn.png)

Possiamo vedere come il passo N+1 sia il passo N più il reward appena ottenuto (circa), pertanto se vogliamo aggregarlo possiamo riscrivere l'equazione seguendo questi passaggi
$$
Q_{N+1} = \frac{r_1 +\ ...\ + r_n}{N+1} + \frac{r_{N+1}}{N+1} = \\ 
\frac{Q_N N}{N+1} + \frac{r_{N+1}}{N+1} = \\ 
\frac{Q_N (N+1-1)}{N+1} + \frac{r_{N+1}}{N+1} = \\ 
\frac{Q_N (N+1) - Q_N}{N+1} + \frac{r_{N+1}}{N+1} = \\
Q_N - \frac{Q_N}{N+1} + \frac{r_{N+1}}{N+1} 
$$
Concludendo, possiamo affermare che il calcolo di $Q$ nella prossima iterazione $N+1$ equivale a:

![](img/qn1.png)

Raccogliamo il denominatore come coefficiente $\alpha = \frac{1}{N+1}$ e riscriviamo il tutto come:

![](img/fq.png) 

Pertanto, le uniche cose che l'agente deve tenere in memoria per ogni azione sono due

- $Q_N$, ovvero il valore di quella azione calcolato nella scorsa iterazione 
- $N$, ovvero il numero di volte per cui è stata scelta quella azione

Questa che abbiamo ottenuto è la forma generale della RL, ovvero
$$
New Estimate = OldEstimate +StepSize[Target-OldEstimate]
$$
$\alpha$, detta quindi $StepSize$ o **peso**, è l'elemento che bilancia l'innovazione(esplorazione) e tradizione(exploitation)

Da notare che:

- Se $\alpha = 0$, la value function non viene modificata nel tempo (rimane fissa)
- Se $\alpha = 1$, la value function assume il valore del reward istantaneo (dimentica tutto il passato e tiene conto solo del reward corrente)

### Caso non stazionario

Tutto questo però funziona in un caso stazionario, per cui nel nostro esempio le probabilità della slot rimanevano fisse durante tutto il task. Oppure potrei voler tenere in considerazione di più i reward recenti anziché quelli vecchi.

![](img/nst.png)

Finora abbiamo visto come, mantenendo $\alpha$ per ogni interazione pari a $\frac{1}{N+1}$, abbiamo mantenuto il peso per tutti i campioni allo stesso modo.

Cosa succederebbe se ponessimo $\alpha$ costante, anzichè lasciarlo variare secondo $N$?

![](img/csta.png)

Espandendo i termini dell'equazione ricorrente otteniamo una sommatoria di termini che rappresenta l'insieme delle pesature delle varie value function nel tempo.

![](img/wgt.png)

E' evidente come i reward non vengono pesati tutti allo stesso modo: il peso di ogni campione decresce esponenzialmente a partire da $I = N$ (tempo presente) tornando indietro fino a $I = 1$  (tempo iniziale).

I reward recenti hanno quindi un peso maggiore rispetto a quelli remoti: questo comportamento è definito **recency-weighted average**.

E' dimostrabile come la somma di tutti questi pesi è unitaria (così come dovrebbe essere in una classica weighted average).

![](img/smp1.png)

Aggiungo e sottraggo il termine $(1-\alpha)^N$ dentro la sommatoria:

![](img/smp2.png)

Sostituita la sommatoria con regole analitiche, riscriviamo:

![](img/smp3.png)

Possiamo quindi considerare questo nuovo modo di scrivere $Q_{N+1}$ come generalizzato

![](img/fn.png)

#### Considerazioni su condizioni iniziali

Nei metodi con $\alpha = \frac{1}{N+1}$, il parametro $Q_0$ viene utilizzato solo come inizializzazione e al primo passo, per poi venire sostituito da $Q_1$.

Nei metodi con $\alpha$ costante, invece, $Q_0$ conta ancora meno, dato che i vecchi valori vengono pesati sempre meno man mano che si va avanti. Tuttavia, la sua polarizzazione è permanente, con $Q_0 = 0$.

$Q_0$ può essere utilizzato per fornire della conoscenza a-priori (transfer learning) o per favorire l'esplorazione (ponendo valori iniziali a positivo/negativo ad esempio)

## Schema riassuntivo finale

![](img/rc.png)