# Sistemi fuzzy : logica non binaria

## Logica classica e limiti

Nella **logica classica** abbiamo effettivamente **due valori** $A = \{T, F\}$ che rappresentano rispettivamente la verità o la falsità. Qualsiasi funzione di verità T, data una o più proposizioni in input, deve restituire un valore compreso fra questi due ovvero $T : \{p1, p2, ..., pN\} \rightarrow \{0,1\}$

Questa funzione ha una descrizione esaustiva del calcolo su tutti i possibili valori discreti in ingresso tramite la tabella di verità. Inoltre valgono i principi di

- **Terzo escluso** $A = T \iff A^C = F$
  - Se A è vera, allora il suo complemento è sicuramente falso (non esiste un terzo valore)
- **Non-contraddizione** $A \cap A^c = \empty$
  - L'intersezione fra A e il suo complemento è l'insieme vuoto

<img src="img/cls.png" style="zoom:80%;" />

In questo esempio ho due casi:

- Se Michela c'è (Michela = true) allora "Vado a sciare" = true
- Se Michela non c'è (Michela = false) allora "Vado a sciare" = false

In questo caso copro tutti i possibili casi e non ho alcuna incertezza; posso dire che la mia logica è **crisp** (ben definita e distinta)

![](img/fzz.png)

In questo esempio invece è difficile stabilire effettivamente e in modo crisp cosa si intenda per nuvoloso: siamo in una situazione ambigua poichè noi sappiamo benissimo cosa intendiamo per nuvoloso tuttavia ci è difficile renderlo chiaro. Quello che ci interessa, in questo caso è il grado di "nuvolosità": quanto più ci sono nuvole nel cielo (e grigie magari), tanto più lo consideriamo nuvoloso.

![](img/fzz2.png)

Ma qual è il grado di nuvolosità che ci consente di dire "ok vado al cinema"?

## Logica fuzzy

La logica fuzzy pone le sue basi proprio su questo concetto di **gradazione** (più o meno nuvoloso). 

Dal punto di vista matematico, una funzione fuzzy è equivalente a una proposizione logica che può assumere un numero infinito di valori tra zero e falso: ovvero invece di avere in output un valore vero o falso ($\{T, F\} = \{1,0\}$) avrò un valore compreso fra vero e falso, ovvero nell'intervallo continuo $[0,1]$

 La funzione fuzzy sarà quindi definita così $T : \{p1, p2, ..., pN\} \rightarrow [0,1]$

Conseguentemente a ciò, i principi che abbiamo enunciato prima di logica classica non valgono più:

- Viola la legge del terzo escluso $A = T \nLeftrightarrow A^C = F$
  - Non è più vero che esistono solo due valori: infatti esistono tutti i possibili valori intermedi
  - Nell'esempio: non esiste più solo cielo "non nuvoloso" o "nuvoloso" ma tutte le gradazioni intermedie
- Viola la legge di non contraddizione $A \cap A^c = \empty$
  - Non esistendo più due soli valori, allora non esiste una esclusività fra di essi
  - Nell'esempio: il cielo può essere contemporaneamente nuvoloso e non nuvoloso, ovviamente in gradazioni diverse

Ma vediamo un altro esempio di fuzzy set che evidenzi i problemi della logica fuzzy e le conseguenti soluzioni adottabili

## Il castello di sabbia

Costruiamo una montagnetta di granelli di sabbia: la proposizione S~d~ è l'affermazione "*x* è un castello di sabbia" la quale è funzione del numero di granelli *d* di cui la montagnetta *x* è composta.

### Logica classica

Come possiamo definire il numero esatto di granelli oltre il quale quella montagnetta è definibile un castello? Sarebbe effettivamente come dire che con *d* granelli di sabbia, x sia un castello ma se ne levassimo uno allora non sarebbe più tale.

![](img/cas1.png)

Esisterebbe quindi un "granello magico" *d~k~* che stabilisce una certa soglia per cui

- prima del quale, la montagnetta non è un castello
- oltre il quale, la montagnetta è magicamente diventata un castello

Possiamo vedere come forzare la logica classica a un concetto fuzzy è molto limitante e per certi versi scorretto poiché, non tenendo conto delle varie sfumature, non è possibile individuare un esatto punto critico tale per cui la proposizione è sicuramente vera o falsa.

### Logica fuzzy

Supponendo di avere una montagnetta di *n* granelli di sabbia, se abbiamo 0 granelli avremo che la nostra funzione valga T(0) = 0 = false, ovvero la montagnetta non esiste.

Partendo da questo caso base, possiamo definire un certo numero di granelli di sabbia *d~n~* tale per cui ho sicuramente un castello di sabbia per cui T(*d~n~*) = 1 = true.

Ora, per qualunque valore di granelli compreso nell'intervallo [0, *d~n~*] avrò un certo **grado di appartenenza** che, come sappiamo, è compreso nell'intervallo [0,1]. 

![](img/fzz3.png)

Siamo di fronte quindi a una funzione T che dato un numero X di granelli di sabbia, mi stabilisce quanto quella montagnetta è simile a un castello di sabbia (quindi il grado di appartenenza della montagnetta alla classe dei castelli di sabbia).

Allo stesso modo possiamo definire una funzione totalmente complementare a T che ci dice quando una montagnetta NON è un castello di sabbia dato il numero di granelli X.

![](img/fzz3b.png)

Questo grado di appartenenza viene anche chiamato **grado di membership** o **fitness** nella classe di appartenenza corrispondente.

In questo caso abbiamo visto delle funzioni di membership lineari, tuttavia è possibilissimo definire qualsiasi tipo di funzione continua tra 0 e 1.

![](img/fzz4.png)

Ma con questo metodo è possibile modellare più classi di appartenenza con la stessa variabile di entrata? Proviamo a pensare a un esempio: l'analisi della velocità di una macchina.

Un sensore riesce a rilevare la velocità di una macchina che è appena passata e voglio classificare questo valore in una delle mie 3 categorie: alta, media e bassa. Ovviamente dobbiamo metterci d'accordo su cosa intendiamo per alta, media e bassa pertanto definiamo delle funzioni di membership apposite, trapezoidali o triangolari che siano. In questo modo sappiamo quando "termina" una classe e "inizia" un'altra.

![](img/vel1.png)

Possiamo vedere come secondo la logica classica ogni classe è separata da un singolo valore "magico", mentre secondo la logica fuzzy è possibile una intersezione fra classi.

Come possiamo classificare quindi un'auto che viaggia a 35 km/h in logica fuzzy? In logica classica sarebbe semplicemente in classe media, ma vediamo che ragionamento possiamo fare quando consideriamo le sfumature:

<img src="img/vel2.png" style="zoom:125%;" />

Possiamo vedere come a 35km/h

- Possiamo classificarla come velocità media con grado di membership a 0.75
- Possiamo classificarla come velocità bassa con grado di membership a 0.25

Abbiamo quindi più informazioni rispetto alla logica classica! Inoltre evitiamo bruschi cambi di membership da bassa a media a alta, a favore di cambiamenti di visione più graduali. 

Una volta appreso quali classi sono state "attivate" in base alla membership, possiamo poi farci dei ragionamenti su, che a breve vedremo (es. semaforo autoregolante che misura le velocità delle auto).

## Fuzzyness vs probability

Nella ambiguità del linguaggio spesso siamo portati a dire quando vediamo una montagnetta di sabbia "E' probabile che sia un castello". La frase però più giusta in questo caso sarebbe "Sembra un castello".

Il motivo di questa distinzione è cercare di evitare di confondere la fuzzyness di un evento con la probabilità che un evento accada, perchè sono due cose fondamentalmente diverse:

- La **fuzzyness** descrive ***l'ambiguità*** insita di un evento
  - Descrive quindi la dimensione strutturale del problema
  - es. Piove in modo leggero (non so quantificare "leggero", poichè è intrinsecamente ambiguo)
- La **probabilità** descrive ***l'incertezza*** che l'evento avvenga
  - Descrive quindi la dimensione temporale del problema
  - es. E' probabile che domani piova (qui intendo che sono incerto sul fatto che "domani piova", ma non è una incertezza strutturale ma temporale)

## Operatori logici nella logica fuzzy

Così come gli operatori logici funzionano nella logica classica, allo stesso modo posso estenderli alla logica fuzzy con un funzionamento totalmente equivalente: 

- **AND** 
  - $T(A\ and\ B) = min(T(A),\ T(B))$
  - es. if $A = 0.3, B = 0.5$ then $(A\ and\ B) = 0.3$
- **OR**
  - $T(A\ or\ B) = max(T(A),\ T(B))$
  - es.  if $A = 0.3, B = 0.5$ then $(A\ or\ B) = 0.5$
- **NOT**
  - $T(notA) = 1-T(A)$
  - es. $A = 0.3$ then $(notA) = 0.7$

con $T(A),\ T(B) \in [0,1]$

## Rappresentazione geometrica di classi fuzzy

E' possibile considerare più classi fuzzy contemporaneamente strutturandolo come un ipercubo di n dimensioni, con lato ovviamente di lunghezza 1 (poiché l'intervallo è sempre [0,1]).

Possiamo considerare come esempio due classi fuzzy che rappresentano un temporale: "vento forte" e "molti fulmini": in questo caso possiamo modellare un ipercubo di 2 dimensioni (un quadrato) in cui ogni punto rappresenta il grado di appartenenza rispetto a una o l'altra classe.

![](img/geo1.png)

In questo caso possiamo vedere come l'evento A si colloca all'interno di questo quadrato: 

- ha un valore di fitness (o grado di membership) di 1/3 per quanto riguarda la classe "molti fulmini"
- ha un valore di fitness (o grado di membership) di 3/4 per quanto riguarda la classe "vento forte"

Possiamo identificare dunque questo evento alle coordinate A = (1/3, 3/4).

Ovviamente agli angoli (estremi dell'ipercubo) avrò dei valori crisp: infatti su (0,0) sono sicuro che il temporale non sussiste in quanto non ho né "molti fulmini" né "vento forte", mentre su (1,1) sicuramente sta avvenendo un temporale in piene forze. 

Il massimo caso di indecisione lo ritroviamo proprio nel centro dell'ipercubo, ovvero in questo caso in (0.5, 0.5) poichè risulta che $A \cup A^c = A \cap A^c$

### Misurare la fuzzyness

Ci verrà comodo utilizzare le coordinate che abbiamo individuato in precedenza per valutare il grado di fuzzyness di un evento dall'origine (0,0).

![](img/geo3.png)

Per farlo potremmo semplicemente calcolare la lunghezza o norma del vettore $A-O$, usando la cosiddetta distanza euclidea. Tuttavia per questioni di comodità ci basta calcolare la distanza fuzzy, che non è altro che la somma fra le due coordinate

![](img/geo2.png)

Questa metrica, detta *l~1~*, rispetta tutte le operazioni citate in precedenza, la disuguaglianza triangolare e in generale tutte le operazioni che ci serviranno. 

Questa metrica ci servirà a determinare quindi il livello di **entropia** di un evento:

![](img/ent.png)

Quando l'evento A si posiziona lungo i **vertici** allora la mia entropia sarà zero $E(A) = 0$ poiché sono sicuro, in modo crisp, della verità della proposizione.

In caso contrario, quando l'evento A si posiziona sul **centro**, ovvero nel caso di maggior indecisione, l'entropia relativa sarà pari a uno $E(A) = 1$ 

Ma come calcolo il livello di entropia date queste premesse?
$$
E(A) = \frac{a}{b} = \frac{l_1(A, A_{vicino})}{l_1(A, A_{lontano})}
$$
dove $a$ e $b$ sono i vettori che congiungono l'evento A con i vertici rispettivamente più vicino e più lontano

Nell'esempio pertanto avremo 
$$
E(A) = \frac{a}{b} = \frac{\frac13+\frac14}{\frac23+\frac34} = \frac{7}{17} \approx 0.412
$$

## Fuzzy systems

I fuzzy system sono dei sistemi che sfruttano il motore della logica classica per risolvere i predicati, quindi analizzando la veridicità delle proposizioni in ingresso, ma ammettendo la vaghezza tipica dei sistemi fuzzy.

Questi sistemi vengono anche definiti **sistemi esperti** e si basano come abbiamo detto su regole classiche inferenziali legate da AND e OR come conoscenza di base (appartenenti quindi all'ipotesi debole sull'AI). Pertanto il motore di reasoning applicherà su delle proposizioni il classico approccio IF ... THEN ... ELSE ...

La risposta finale deve comunque essere T (true) o F (false) nonostante l'ammissione di valori fuzzy, pertanto dovremo definire delle modalità di finalizzazione del risultato in base ai gradi di membership rilevati (come vedremo poi)

### Base di conoscenza

Come abbiamo detto, la base di conoscenza viene definita enunciando un insieme di regole IF-THEN-ELSE: pertanto è possibile rappresentare questa conoscenza tramite semplici tabelle di verità. Questa semplicità permette di memorizzare ciò in ROM o FPGA.

Tuttavia a differenza della logica classica, in cui una e una sola regola può essere "attivata" alla volta data la combinazione di input, in logica fuzzy è possibile che più regole valgano dato che abbiamo tutti i valori intermedi invece che solo 0 o 1.

![](img/tab.png)

In sostanza, è necessario poter

- Accedere a più di una parola di memoria e quindi a più regole per una trasformazione Input -> Output
- E' necessario memorizzare da qualche parte l'informazione sul grado di fit generato dall'input per decidere l'output da generare

Questo lavoro sarà svolto da una Fuzzy Associative Memory (FAM), la quale lavora su insiemi fuzzy A appartenenti all'ipercubo di $n$ dimensioni $I^n$ e definisce degli insiemi B che gli corrispondono su un ipercubo di $p$ dimensioni $I^p$. 

La trasformazione dagli insiemi in $I^n$ a quelli in $I^p$ avviene tramite un insieme finito di regole/funzioni. 

### Esempio: semaforo intelligente

Proviamo ad applicare il concetto di FAM per chiarificarlo: supponiamo di voler creare un semaforo intelligente, il quale rileva il numero di macchine che arrivano per capire quanto traffico è presente e dunque regolare la durata del verde (più traffico, più durata verde).

Pertanto definiamo gli insiemi A e B

- A rappresenta il traffico rilevato quindi $A \in I^n$ = {Scarso, Medio, Alto}
- B rappresenta la durata del verde quindi $B \in I^p$ = {Breve, Lunga}

Un esempio di regola che trasforma A in B potrebbe essere:

​	(Traffico: Scarso) $\rightarrow$ (Durata_Verde: Breve), quindi (A~2~, B~3~) for short

ovvero se il traffico è leggero, non è necessario tenere il verde per tanto tempo.

Ora, per ogni classe dell'insieme di ingresso A (3), deve essere data una regola e quindi definita la corrispondente uscita fra le classi di B (2), similarmente alla tabella di verità. Avremo quindi 3 regole nella nostra FAM

| Regola # | Traffico (IF ... ) | Durata Verde (THEN ...) |
| -------- | ------------------ | ----------------------- |
| Regola 1 | Scarso             | Breve                   |
| Regola 2 | Medio              | Lunga                   |
| Regola 3 | Alto               | Lunga                   |

Queste regole generalmente sono stabilite da un esperto di dominio, quindi in questo caso un vigile ad esempio. Vediamo quindi il processo completo che compie il nostro sistema per trasformare gli ingressi in uscita.

#### 1. Da input numerico a classi fuzzy

Inizialmente a mia disposizione ho solamente il dato numerico di quante macchine sono presenti di fronte al semaforo, pertanto devo trasformare questo valore in una (o più) classi fuzzy di riferimento in base al grado di appartenenza a esse.

Proviamo a definire queste tre classi di appartenenza come due trapezoidi (BASSO, ALTO) e un dente di sega (MEDIO) e supponiamo che il semaforo rilevi una densità di traffico pari a 55 auto ogni 10 min.

![](img/fam1.png)

Possiamo vedere che, per come abbiamo definito le classi, abbiamo un overlap: sia la classe BASSO che la classe MEDIO sono attivate, ovviamente con diversi livelli di fit (0.5 per medio e 0.2 per basso).

Possiamo dire quindi che il traffico è sia scarso che medio, tuttavia più tendente al medio: questo è quello che arriva in ingresso al nostro sistema di ragionamento, ovvero la FAM.

#### 2. Da classi fuzzy di input a classi fuzzy di output

Siccome abbiamo ricevuto che il traffico è sia BASSO che MEDIO, allora la nostra FAM avrà attive sicuramente due regole, ovvero la 1 e la 2 infatti

	IF (BASSO) THEN (BREVE) --- attiva
	IF (MEDIO) THEN (LUNGA) --- attiva
	IF (ALTO) THEN (LUNGA)  --- non attiva

 Ma in questo caso avrei una visione schizofrenica, poiché dovrei settare la durata sia a BREVE che a LUNGA, cosa non possibile.

Pertanto ci può risultare utile sfruttare il valore di fitness delle classi attivate per utilizzarle come fitness delle regole attivate: pertanto diremo che

- La regola semaforo BREVE ha fitness pari a 0.2
- La regola semaforo LUNGO ha fitness pari a 0.5

Ma ora, uscendo dalla FAM, come determino la durata effettiva del semaforo, ovvero l'output finale?

#### 3. Da classi fuzzy di output a output numerico

Dobbiamo senza dubbio convertire l'attivazione di queste regole in un valore di durata per il semaforo verde. Supponiamo di avere il vigile che ci fornisce i valori di BREVE e LUNGO e costruiamo un insieme fuzzy.

![](img/fam2.png)

Dobbiamo quindi defuzzyficare ciò che abbiamo per ottenere un valore numerico: esistono due metodi

- Defuzzyficazione mediante massimo $\max m_b(y_j)$
  - Viene scelta l'uscita proveniente da una sola delle regole attivate, ovvero quella con il maggior grado di fitness fra di esse.
  - Non rappresenta al meglio la situazione in quanto scarta il possibile peso delle regole con attivazione più bassa, provocando sbalzi meno graduali da una regola all'altra (e quindi da una durata a un'altra)
- Defuzzyficazione tramite media pesata (detto anche "Metodo del centroide") $\frac{\sum{F_i* y_i}}{\sum{F_i}}$
  - Invece di scegliere una sola regola, si prendono i valori associati a ogni regola e si svolge una media pesata sul grado di fitness di ogni regola.
  - Questo vuol dire che maggiore è il grado di fitness di una regola e tanto più peserà il valore corrispondente di uscita nel risultato finale. Questo favorisce variazioni graduali dell'output, coerenti con variazioni graduali dell'input.

Nel nostro esempio, usando la media pesata, avremmo che
$$
Durata = \frac{\sum{F_i* y_i}}{\sum{F_i}} = \frac{(0.2 * 80s)+(0.5*260s)}{0.2+0.5} = 208.57s
$$
infatti 

- la durata BREVE di 80s ha peso equivalente a 0.2, così come lo è il relativo grado di fitness della regola
- la durata LUNGO di 260s ha peso equivalente a 0.5 per lo stesso motivo

Questa gradualità consente di cogliere le sfumature dei valori in ingresso e replicarle 1:1 in uscita!

### Progettazione di un sistema fuzzy

Osserviamo a questo punto gli step necessari per progettare un nostro sistema fuzzy

1. Per prima cosa dobbiamo identificare le **variabili di ingresso e uscita** del sistema e il loro **range** (A, B)
2. Poi dobbiamo identificare delle **classi fuzzy** in cui le variabili possono ricadere e i loro **boundaries**
3. In seguito dobbiamo definire una trasformazione dei valori di ingresso a valori di uscita come insieme di **regole fuzzy**, ovvero il funzionamento e il contenuto della FAM : per ogni combinazione di classi fuzzy di input (con OR, AND) è possibile definire una classe di output.
4. Infine dobbiamo poter **de-fuzzyficare l'output** (e in che modalità) per poterlo rendere come valore tangibile in uscita dal nostro sistema fuzzy

### Schema riassuntivo di un sistema fuzzy

![](img/fam0.png)

<hr>

## Esercitazione sistemi fuzzy

Proviamo a considerare un altro esempio, questa volta però su un tipo di FAM leggermente diverso. 

Nel mondo di tutti giorni è possibile dover fare riferimento a più variabili per classificare un evento e reagire di conseguenza: per esempio, potrei decidere di portare il cappotto solo se piove e fa freddo: questo presuppone di aver osservato una temperatura di classe "fredda" e allo stesso tempo una umidità di classe "umido". 

Come faccio a stilare un giudizio fuzzy a partire dall'analisi di variabili multiple?

### FAM multi-variabile

Considerando l'esempio precedente, più classi composte vengono attivate contemporaneamente (ad esempio "freddo" e "umido"). Per ragionare su queste, ho applicato una composizione di proposizione del tipo: *IF (fredda AND umida) THEN ...*

Pertanto possiamo generalizzare l'analisi di variabili multiple nelle nostre regole della FAM come una serie di proposizioni unite da AND e OR del tipo *IF (... AND ... OR ... AND ...) THEN*

![](img/famlt.png)

Pertanto

- Ogni dato di input avrà una certa fit rispetto alle classi fuzzy a cui appartiene ogni variabile
- Le fit vengono combinate tramite AND (minimo) e OR (massimo) utilizzando le loro T-norm
- La combinazione finale avrà una sua fit

Come prima, ogni combinazione di variabili può attivare una o più regole della FAM.

### Esempio : Cart-pole

![](img/cart.png)

Supponiamo di avere un carrello con un palo in cima: lo scopo del sistema di controllo è muovere il carrello, rimanendo sulla rotaia, in modo da non far cadere il palo, mantenendolo quindi in equilibrio.

Il sistema di per sé è altamente non lineare ed è sicuramente instabile a causa dell'equilibrio precario, anch'esso instabile, del palo in quella posizione.

Trovare una soluzione analitica è pressoché impossibile, pertanto ci rimane da provare la soluzione numerica e approssimata e per farlo conviene trattare il problema come una **black-box** : in questo modo faccio finta di non sapere nulla della soluzione analitica, pertanto se il palo si muove perché non più in equilibro, ALLORA faccio qualcosa. L'input della black-box quindi in questo caso sarà il solo movimento del palo. 

Questo approccio è quello su cui si basa il machine learning.

#### Semplificazione del problema

Iniziamo quindi a semplificare il problema, trattandolo come una black-box, e seguiamo i passi della costruzione di un sistema fuzzy enunciato in precedenza.

![](img/cart2.png)

#### 1a) Identificazione variabili del sistema I/O 

L'input dipende dal movimento del palo, quindi dovremo considerare due fattori: 

- l'orientamento del palo $\theta$ 
- la velocità angolare del palo $\theta'$

come output invece avremo solamente

- la forza da applicare al carrello $F$

Tutte e tre le variabili vanno considerate al momento di tempo $t$ in quanto il sistema di controllo lavora in tempo discreto: pertanto alla fine avremo che le variabili sono

**Input**: $A = \{\theta(t), \theta'(t)\}$ - **Output**: B = $\{F(t)\}$

#### 1b) Identificazione dei range delle variabili trovate

Per le variabili trovate dobbiamo trovare un possibile range di valori per la suddivisione in classi successiva:

- $\theta(t) \in [-90°,90°]$ poiché il palo può solo ruotare sul suo perno, definendo un intervallo "circolare"
- $\theta'(t) \in [-\infin,+\infin]$ poiché il palo può ruotare a una qualunque velocità angolare di gradi al secondo
- $F(t) \in [-25N,25N]$ poiché, conoscendo il motore del carrello, sappiamo anche la forza massima che può applicare (supponiamo qui 25N)

### 2) Definizione delle classi fuzzy

Possiamo definire ora le classi fuzzy in cui classificheremo i valori di input/output che riceviamo dal nostro carrello: per esempio, per misurare gli input che abbiamo possiamo considerare 7 classi:

- Negative Large, Medium e Small (NL, NM, NS rispettivamente)
- Zero (ZE)
- Positive Large, Medium e Small (PL, PM, PS rispettivamente)

#### Funzioni di appartenenza per orientamento $\theta$

![](img/cartor.png)

Qui possiamo vedere come abbiamo definito le varie classi di equivalenza relative all'orientamento $\theta$ del palo tramite la funzioni di fitness per ognuna di esse. Una cosa interessante da notare è che le classi sono più "strette" e concentrate vicino allo zero: questa scelta è motivata dal fatto che vogliamo una precisione più alta quando sono vicino allo zero.  

Consideriamo in figura il valore di l'input $\theta$ = 15, per esempio: possiamo osservare che con tale ingresso, le classi che si attiverebbero sono la PS e la ZE, con rispettivi gradi di fitness (0.8, 0.2).

#### Funzioni di appartenenza per velocità angolare $\theta'$

![](img/cartvl.png)

Anche qui abbiamo una maggiore densità delle classi vicino allo zero per lo stesso motivo durante la modellazione per la velocità angolare $\theta'$.

Supponiamo di avere in ingresso $\theta'$ = -10 : con tale ingresso, abbiamo attive NS e ZE, con rispettivi gradi di fitness (0.5, 0.5).

#### Funzioni di appartenenza per forza $F$

![](img/cartfz.png)

Allo stesso modo, definiamo le classi di output per la de-fuzzyficazione.

### Costruzione delle regole di trasformazione I/O

Una volta ottenuti i valori di fitness e le relative classi attivate dobbiamo andare a vedere quali regole della FAM vengono attivate: per farlo è necessario definire delle variabili che rappresentano i valori fuzzyficati.

Nel nostro esempio entrano in gioco 3 variabili, due di input e una in output: $A_1^\theta, A_2^{\theta'};B^F$

Pertanto il lavoro della FAM sarà trasformare $A_1^\theta$ e $A_2^{\theta'}$ in $B^F$ ovvero seguire una funzione $(\theta, \theta') \rightarrow F$ che svolge questa trasformazione tramite regole linguistiche fornite dall'esperto di dominio.

Un possibile esempio di regola della FAM potrebbe essere (NM, ZE; PM) ovvero se in ingresso ho un'orientamento medio negativo e la velocità è circa nulla allora applico una forza medio positiva. Questa regola è ovviamente traducibile nel formato IF-AND-OR-THEN accennato in precedenza

*IF (**NM** AND **ZE**) THEN **PM***

Infatti non posso analizzare le variabili singolarmente per dare un output, ma serve una loro combinazione.

In questo caso con due variabili (bidimensionale) è facile visualizzarlo: basta una tabella di combinazioni

![](img/carttb.png)

In questo caso rappresentiamo tutta la possibile superficie di combinazioni, di cui ne sono state codificate solamente 15 (in base alla decisione dell'esperto di dominio).

Torniamo all'esempio di input precedente: $(\theta = 15, \theta' = -10)$: avremmo attivato

- Per rotazione le regole PS e ZE con (0.8, 0.2)
- Per velocità le regole NS e ZE con (0.5, 0.5)

Pertanto, combinando i vari input ottenuti, accoppiamo in modo combinatorio le quattro classi attivate e osserviamo le regole attivate

1. (PS, ZE; NS)
2. (ZE, ZE; ZE)
3. (PS, NS; NS)
4. (ZE, NS; PS)

Se consideriamo la Tnorm come il minimo fra le fitness otterremmo che

1. Con fit di PS = 0.8 per rotazione e fit di ZE = 0.5 per velocità otteniamo che la regola (PS, ZE; NS) ha in uscita NS con fit pari al minimo fra le fit degli ingressi quindi $min\{0.8, 0.5\} = 0.5$

![](img/carttn1.png)

2. Altrimenti, con fit di ZE = 0.2 per rotazione e fit di ZE = 0.5 per velocità otteniamo che la regola (ZE, ZE; ZE) ha in uscita ZE con fit pari al minimo fra le fit degli ingressi quindi $min\{0.2, 0.5\} = 0.2$

![](img/carttn2.png)

3. Allo stesso modo, con PS = 0.8 e NS = 0.5 avremo in uscita NS con 0.5 di fit
4. Infine, con ZE = 0.2 e NS = 0.5 avremo PS in uscita con 0.2 di fit

### Defuzzyficazione delle regole attivate

Dobbiamo ora trasformare queste classi di output in valori effettivi di forza da applicare al carrello: come determinarla, dato che abbiamo quattro regole attive?

Abbiamo detto che abbiamo due metodi a nostra disposizione, ovvero il metodo del massimo e il metodo del centroide (il secondo è meglio del primo).

#### Metodo del massimo

Date le regole attive

1. (PS, ZE; NS)   fit = 0.5
2. (ZE, ZE; ZE)    fit = 0.2
3. (PS, NS; NS)   fit = 0.5
4. (ZE, NS; PS)    fit = 0.5

Scelgo come output la classe NS, dato che ha attive due classi con livelli di fitness maggiori. Se avessi due classi con due output diversi e livello di fitness medesimo dovrei fare una scelta arbitraria (ugh!).

Scegliendo NS per la forza in uscita avremo Y = -10N, operando una scelta crisp ma sensibile alle discontinuità variabili.

#### Metodo del centroide

Date le regole attive

1. (PS, ZE; NS)   fit = 0.5
2. (ZE, ZE; ZE)    fit = 0.2
3. (PS, NS; NS)   fit = 0.5
4. (ZE, NS; PS)    fit = 0.5

svolgo una media pesata per ottenere la forza in uscita:

![](img/cartmd.png)

In questo modo la forza viene bilanciata sulle fitness, operando una scelta più graduale e fine, riducendo gli sbalzi improvvisi di forza.

## Riflessioni finali

Avendo osservato i sistemi fuzzy come sistemi intelligenti c'è da chiedersi: da dove viene la conoscenza?

Sicuramente i sistemi fuzzy sono sistemi IA deboli, ovvero sono meccanici e ragionano come se fossero intelligenti: in questo caso, ragionano come se fossero degli esperti di dominio pertanto la conoscenza deriva proprio da essi (infatti sono loro che definiscono le regole della FAM!)

Il punto difficile dei sistemi fuzzy è capire come tarare le membership functions per suddividere in classi nel modo più corretto possibile: a proposito dei sistemi neuro-fuzzy sono stati ideati per capire quanto meglio può funzionare il sistema fuzzy date certe membership functions e fare fine-tuning su di esse in base alle performance del sistema.