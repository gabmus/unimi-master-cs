# Modelli e sistemi lineari

## Introduzione ai modelli

In questa lezione vedremo cosa si intende per modello e come calcolare delle variabili o parametri in modo che sia massimizzata la loro probabilità.

![](img/predmdl.png)

Possiamo vedere un **modello predittivo** come una scatola nera avente input, outputs e regole interne che trasformano l'input nel corrispettivo output secondo delle funzioni probabilistiche.

Come possiamo vedere nell'immagine in entrata abbiamo una causa $u$ e in uscita un effetto $z$, ottenuto applicando delle funzioni secondo dei parametri interni $w$. L'uscita tuttavia va edulcorata con del $v-noise$ per essere utilizzata poiché va misurata con errore per essere reale.

Alcuni esempi di utilizzo dei modelli predittivi sono

- **Regressione predittiva**

  - Analizzo dati di eventi precedenti (regressione sui dati) per prevedere cosa succederà nel prossimo evento e con quale probabilità

    ![](img/rp.png)

  - Per esempio, se volessi prevedere quanto varranno i titoli della borsa cinese domani, potrei fare un analisi dell'andamento di essi negli scorsi anni nello stesso periodo e basarsi su quelle informazioni per fare una previsione

- **Classificazione**

  - Analizzo l'ingresso e tento di classificarlo in una delle classi che conosco. L'uscita sarà quindi in quale classe ho posto il valore d'ingresso

    ![](img/cl.png)

  - Per esempio, se volessi riconoscere quale numero è stato scritto a mano libera in una foto, potrei creare una rete neurale che, svolte i calcoli nei layer, mi dica a quale classe di numero appartenga il disegno: nella foto abbiamo il disegno di un 4 che viene in uscita posto nella classe corretta.

Quando si vuole definire un sistema predittivo bisogna definire un insieme di step

![](img/steps.png)

- **Realizzazione del modello**, suddivisa in
  - ***Definizione del progetto***, identificando nello scope di progetto le variabili e il loro range (come nei sistemi fuzzy)
  - ***Preparazione dati***, preparazione della conoscenza che dobbiamo trasmettere al nostro sistema intelligente: un modello predittivo dovrà avere una base di conoscenza su cui fare inferenze.
  - ***Costruzione***, dove definiamo le funzioni causa-effetto, i parametri $u,v,z$ e così via
  - ***Validazione del modello***, in cui verifichiamo la qualità delle predizioni e svolgendo tuning dei parametri opportunamente
- **Utilizzo del modello** e fine-tuning postumo in base alle necessità

### Problemi associati ai modelli

Ogni modello viene creato per risolvere uno specifico problema in uno specifico modo: i principali tre problemi collegati al modello che abbiamo osservato sono risolti da

![](img/predmdl.png)

- **Utilizzo forward**
  - Utilizzato per risolvere i così definiti problemi di controllo, classificazione o predizione: vogliamo determinare l'effetto $\{z\}$ a partire dalla causa $\{u\}$ e dai parametri $\{w\}$ 
  - (Es. andamento borsa cinese di prima, sistemi di micro-controllo)
- **Utilizzo backward**
  - Utilizzato per risolvere il problema inverso rispetto a quello forward: vogliamo determinare la causa $\{u\}$ partendo dall'effetto misurato (con errore) $\{z_m\}$ e dai parametri $\{w\}$ 
- **Utilizzo learning**
  - Utilizzato per i problemi di identificazione del modello: data una causa $\{u\}$ e un effetto misurato (con errore) $\{z_m\}$ vogliamo determinare il procedimento da attuare per passare da causa a effetto

## Sistemi lineari

I sistemi lineari di equazioni sono definiti tali in quanto non possiedono termini esponenziali. I sistemi lineari vedremo che si dimostreranno molto comodi poiché

- facilmente manipolabili tramiti semplici operazioni (algebra delle matrici)
- controllabili e prevedibili (non rischiamo di n-uplicare le cose inaspettatamente non avendo degli esponenziali)
- compatti da rappresentare

$$
a_{11}x_1+a_{12}x_2+...a_{1N}x_N+ = b_1 \\
a_{21}x_1+a_{22}x_2+...a_{2N}x_N+ = b_2 \\
... \\
a_{M1}x_1+a_{M2}x_2+...a_{MN}x_N+ = b_M
$$



Essi sono rappresentabili tramite l'equazione $Ax = b$, dove 

- $A$ rappresenta la matrice $n \cdot m$ dei coefficienti delle incognite
- $x$ rappresenta il vettore delle incognite
- $b$ rappresenta il vettore dei termini noti

Seguendo questa forma, possiamo rappresentare un sistema lineare come un prodotto matriciale:
$$
\begin{cases} 3x_1 + 5x_2 + x_3 = 1 \\ 7x_1\ –\ 2x_2 + 4x_3 = 6 \\ -6x_1 + 3x_2 + 2x_3 = 7 \end{cases} \rightarrow \begin{bmatrix} 3 & 5 & 1 \\ 7 & -2 & 4 \\ -6 & 3 & 2 \end{bmatrix} \begin{bmatrix} x_1 \\ x_2 \\ x_3 \end{bmatrix} = \begin{bmatrix} 1 \\ 6 \\ 7 \end{bmatrix}
$$

Per esempio, possiamo adattare un sistema lineare al problema di learning, dove:

- Le incognite $x_j$ rappresentano i parametri $\{w\}$ del modello da individuare
- I coefficienti $a_{ij}$ i valori di ingresso al modello, ovvero le cause $\{u\}$
- I termini noti $b_{i}$ i valori misurati in uscita, ovvero l'effetto (misurato con errore) $\{z_m\}$

Fra le comuni operazioni e proprietà sulle matrici che si serviranno ci sono:

- Trasposizione (scambia righe con colonne)
- Somma e prodotto per un coefficiente
- Somma di matrici
- Prodotto di matrici
- Matrice identità
- Determinante e rango di una matrice
- Inversa di una matrice
- Ortogonalità e ortonormalità

Esercizio a 3 rette (fascio di rette....)

Tuttavia i termini noti che abbiamo considerato sinora non sono precisi ma soffrono di un errore di precisione, come abbiamo affermato in precedenza. E' conveniente separare questo errore dalla misura di $\{z_m\}$ : avremo quindi due componenti $b_i + v_i$, dove $b_i$ rappresenta l'uscita pura dal modello e $v_i$ rappresenta l'errore aggiunto.

Pertanto, riformulando il nostro sistema diventerebbe $Ax=b+N$ ovvero
$$
a_{11}x_1+a_{12}x_2+...a_{1N}x_N+ = b_1+v_1 \\
a_{21}x_1+a_{22}x_2+...a_{2N}x_N+ = b_2+v_2 \\
... \\
a_{m1}x_1+a_{m2}x_2+...a_{mN}x_N+ = b_m+v_m
$$
Risulta impossibile però riuscire a risolvere tutte le equazioni del sistema con delle incognite appropriate: quello che ci conviene fare è invece tentare di ridurre il più possibile l'errore che ci separa dalla soluzione reale in tutte le equazioni. Vogliamo quindi "spargere" l'errore che commettiamo fra un po' tutte le $x$ per cui riusciamo a minimizzare l'errore in tutti, cercando di far contente un po' tutte le equazioni.

Pertanto dalla ricerca di una soluzione analitica passiamo a una soluzione approssimata, risolta come problema di **ottimizzazione**. Ma cosa vogliamo ottimizzare?

Definiamo una funzione costo, tale per cui $(Ax - b)^2 = \sum_k{(v_k)^2} = ||Ax-b||^2$

Come possiamo vedere il costo dipende dalla somma degli errori $v_k$ al quadrato, pertanto vogliamo minimizzare questo valore nel nostro problema di ottimizzazione. 
$$
\min_x{\sum_k{(v_k)^2}} = \min_x{(Ax-b)^2}
$$
Geometricamente parlando, cercheremmo di trovare il punto a distanza quadratica minima da tutte le rette: per questo motivo viene definita soluzione ai minimi quadrati.

Vedremo tuttavia nel prossimo argomento come passare da una soluzione algebrica a una soluzione statistica di un sistema lineare.

## Distribuzioni di probabilità

### Probabilità continua

Per trovare una soluzione statistica dobbiamo andare a trattare le probabilità non più in modo discreto come abbiamo visto la scorsa volta, ma in modo continuo.

![](img/pc.png)

La probabilità di un evento nel continuo lavora non più su probabilità puntuali (su punti, valori singoli) ma su intervalli di probabilità. Parliamo quindi ora di densità di probabilità, poichè abbiamo aree più "elevate" e quindi che hanno più probabilità rispetto ad altre. Ovviamente l'intera area sottesa dalla curva di probabilità deve essere 1, ovvero
$$
\int_{-\infin}^{+\infin}{p(x)dx} = 1
$$
così come lo era per la somma di tutte le probabilità nel discreto.

Pertanto la probabilità $P(x)$ di u n evento su un suo intervallo $[\overline{x}, \overline{x} + \Delta x]$ è pari a un area di probabilità pari all'integrale 
$$
0 \le \int_\overline{x}^{\overline{x}+\Delta x}{p(x)dx} \le 1
$$

### Distribuzione normale / gaussiana

Il teorema del limite centrale afferma che la distribuzione della somma di un numero elevato di variabili casuali indipendenti e identicamente distribuite tende distribuirsi normalmente, indipendentemente dalla distribuzione delle singole variabili. Questa distribuzione normale viene visualizzata tramite una curva gaussiana del tipo

![](img/gra.png)

rappresentabile tramite la seguente equazione
$$
p(x | \mu,\sigma) = \frac{1}{(\sqrt{2\pi})\sigma^D}*e^{-\frac12(\frac{x-\mu}{\sigma})^2}
$$
dove 

- $\mu$ corrisponde alla media
- $\sigma$ corrisponde alla deviazione standard
- $D$ corrisponde alla dimensione del problema, genericamente (come in questo caso) pari a 1 (monodimensionale)

In una distribuzione normale il valore con probabilità più elevato è il valore medio $\mu$, come possiamo vedere dai grafici, mentre la deviazione standard $\sigma$ definisce la campanatura della curva: più è bassa e più i valori di alta probabilità si concentreranno sul valore medio $\mu$, più è alta e più i valori di probabilità saranno distribuiti su tutto l'intervallo (curva più piatta).

Ogni valore $x$ che cade nella distribuzione viene localizzato tramite la sua distanza dal valor medio $\mu$, ovvero da quanti $\sigma$ lo allontanano da esso.

![](img/gsig.png)

Seppure la gaussiana abbia un supporto infinito di range (va da $-\infin$ a $+\infin$), qualsiasi valore $x$ cada nella sua distribuzione ha il 99.73% di probabilità di ricadere nell'intervallo $[-3\sigma, 3\sigma]$: questo vale per il fatto che la maggior parte della densità di probabilità si trova attorno al valore medio $\mu$ ed è minima ai suoi antipodi (definiti "code" della gaussiana).

#### Momenti di una variabile

Ogni variabile statistica ha un momento centrale $a$ di riferimento: nel caso della gaussiana, questo punto coincide col valor medio $\mu$. 

A partire da questo posso calcolare **momenti alternativi**: data una variabile statistica $X$, il momento $k$-esimo è individuato rispetto al momento centrale $a$ secondo questa formula
$$
\mu^k(X) =\int_{-\infin}^{+\infin}{(x-a)^kp(x)dx}
$$
Il **momento primo** ($\mu^k, k=1$) di una variabile statistica $X$ viene definito **valore atteso** $E[X]$ e rappresenta la media totale della distribuzione di probabilità:
$$
E[X] = \mu^1(X) = \int_{-\infin}^{+\infin}{(x-\mu)p(x)dx}
$$
Il **momento secondo** ($\mu^k, k=2$) di una variabile statistica $X$ rappresenta la **varianza** $\sigma^2$ dal valore atteso $E[X]$ :
$$
E[(X-\mu)^2] = \mu^2(X) = \int_{-\infin}^{+\infin}{(x-\mu)^2p(x)dx}
$$
Il **momento terzo** ($\mu^k, k=3$) di una variabile statistica $X$ rappresenta la asimmetria (detta anche **skewness**) dal valore atteso $E[X]$ :
$$
E[(X-\mu)^3] = \mu^3(X) = \int_{-\infin}^{+\infin}{(x-\mu)^3p(x)dx}
$$
Il **momento quarto** ($\mu^k, k=4$) di una variabile statistica $X$ rappresenta la **kurtosi**, ovvero il peso delle code di p(x) (utile specialmente nelle distribuzioni "piatte", molto distribuite) :
$$
E[(X-\mu)^4] = \mu^4(X) = \int_{-\infin}^{+\infin}{(x-\mu)^4p(x)dx}
$$

### Dipendenza delle misure dal modello

![](img/predmdl.png)

Tornando al nostro sistema, abbiamo che detto che le nostre uscite sono date da $z_i$, ovvero il risultato di una funzione $f(u_i, w)$ a cui si aggiunge del rumore $v_i$. 

Questo rumore aggiunto (dovuto a fluttuazioni, errori di misura ecc...) potrebbe essere modellato statisticamente da una distribuzione normale, pertanto posso determinare la probabilità di ottenere $z_m$ da $z$. Ma come abbiamo detto, $z$ è dato da $f(u,w)$. 

Pertanto possiamo dire che la probabilità di misurare $z_m$ è **condizionata** dai valori di ingresso $u$ e dai parametri $w$:

![](img/condz.png)

In conclusione, possiamo affermare che tanto più i parametri saranno **corretti** (e quindi tanto più corretto è il nostro modello), tanto **maggiore** sarà la **probabilità** di avere $z$ in uscita dal modello dato un certo ingresso $u$.

#### Esempio : fitting di una retta

Per chiarire i concetti appena citati, facciamo un esempio: supponiamo di voler stimare i parametri di una retta avendo a disposizione N misure rumorose effettuate.

![](img/fr.png)

La retta è un modello lineare ed è rappresentabile come $z = f(u,w) = m\cdot u+q$, con $u$ ingresso e $W= {m,q}$ parametri del nostro modello da stimare. 

Supponiamo che queste uscite $z_{im}$ siano affette da errore gaussiano a media nulla, ovvero che $z_{im} = z_i+G(\mu,\sigma^2)$ con $\mu = 0$. 

Il nostro errore di misura $v_i$ sarà esattamente modellato da questo errore gaussiano pertanto $v_i = G(0, \sigma^2)$.

Infine, avremo che $z_{im} = z_i+v_i = (m \cdot u_i + q) + v_i$: sappiamo bene che è impossibile trovare una retta che passi per tutti questi punti affetti da rumore, pertanto vogliamo trovare i parametri $m$ e $q$ che rendano la retta il più verosimile a quella corretta. Ma cosa intendiamo per verosimile? 

Definiamo una funzione di verosimiglianza: date N variabili casuali indipendenti qual è la probabilità di misurare un vettore composto da certi valori $[z_{1m},\ ...,\ z_{Nm}]$?
$$
p(z_{1m},\ ...,\ z_{Nm}) = p(z_{1m}) \cdot p(z_{2m}) \cdot\ ...\ \cdot p(z_{Nm}) = L(z_{1m},\ ...,\ z_{Nm})
$$
Siccome la probabilità congiunta di eventi indipendenti è il prodotto delle loro probabilità, allora la nostra funzione L di likelihood (verosimiglianza) userà questo valore per determinare quanto più sia verosimile la retta risultante.

Il nostro problema di ottimizzazione si sposta quindi sulla massimizzazione del risultato della funzione di likelihood $L=L(z| u,w)$ rispetto ai parametri $w$. 

Pertanto dovremo tweakare i parametri $w$ affinchè si trovi una combinazione che massimizzi $L$.

Nel nostro esempio della retta, dovremo cercare di rendere la retta il più possibile equidistante e centrata da tutti i punti, per rappresentare in media un po' tutti.