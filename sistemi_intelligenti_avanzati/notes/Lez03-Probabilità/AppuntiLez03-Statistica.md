# Cenni di statistica

## Introduzione al calcolo delle probabilità

Le azioni "intelligenti" vengono fatte verso un ambiente che presenta una certa dose di **incertezza**. 

Per esempio, se volessimo andare a Malpensa per prendere un aereo quanto tempo prima dobbiamo partire da casa nostra? Dobbiamo fare una stima sul percorso basandoci su molteplici eventi e imprevisti casuali che potrebbero accadere: c'è traffico? l'autobus fa ritardo? non riusciamo a prenderlo in tempo? e così via...

Pertanto, anche se la nostra esperienza ci dice che in 60 minuti arriveremmo, dobbiamo prendere in conto questi imprevisti e tenerci un margine maggiore di tempo nel caso accadessero: per esempio, potremmo decidere di uscire 120 minuti prima anziché 60.

Possiamo concludere che il decision-making che compiamo prende in carico questi elementi di incertezza legati al futuro e per soppesare questi elementi può aiutarci la statistica.

## Probabilità 

Quando vogliamo calcolare se un evento è possibile e con quale frequenza si verifichi ci affidiamo al concetto di probabilità.

### Probabilità classica (oggettivista)

Se si è in grado di **conoscere a priori** il numero $n_f$ di eventi favorevoli ed il numero $n_{tot}$ di eventi totali verificabili per un evento $\omega$, si può introdurre il concetto di probabilità classica, secondo cui la probabilità che si verifichi un evento favorevole è data da:

$P=\frac{n_f}{n_{tot}}$ con $\small 0\leq P \leq 1$

Il limite di questa definizione di probabilità è che implica che tutti gli eventi sono equiprobabili, ovvero hanno la stessa probabilità di verificarsi. Inoltre bisogna conoscere a priori il numero di casi favorevoli e quello di casi totali, fatto non sempre possibile. 

Un esempio di applicazione di questa definizione lo si ha con l'estrazione di una carta da un mazzo da 40 carte totali. Supponiamo infatti di voler calcolare che esca il cavallo di bastoni. Chiaramente il caso favorevole è uno e i casi possibili sono 40. La probabilità cercata è pertanto 1/40.

### Probabilità frequentista

Si tratta di una **definizione a posteriori** della probabilità, che segue cioè l'avvenimento dell'evento. Supponiamo infatti di compiere un certo numero $N$ di prove e di verificare che una data modalità si presenti $n$ volte. Introduciamo allora il concetto di frequenza $f$:

$f=\frac{n}{N}$

Di per sé già questo basterebbe per fornire una stima sulla probabilità cercata. In realtà bisognerebbe compiere un numero molto grande di prove (idealmente infinite prove) e si avrebbe che la probabilità che l'evento si verifichi è:

$P=\lim_{N\rightarrow \infty }\frac{n}{N}$

Questo vale per il teorema del limite centrale, il quale stabilisce che la frequenza di un evento su infinite realizzazioni è uguale alla sua probabilità.

Il problema principale della visione frequentista si basa sui campioni, ovvero le classi di riferimento:

- **Omogeneità del campione** : come faccio ad effettuare la media di eventi in modo "sicuro"?
- **Limitatezza del campione** : come faccio a eseguire "infinite" realizzazioni per misurare la frequenza?

### Probabilità totale

Supponiamo di avere $A = \{a_1, a_2\}$ con $a_1, a_2$ eventi mutualmente esclusivi, per esempio un incontro di boxe

- $a_1$ probabilità che vinca lo scontro il pugile 1
- $a_2$ probabilità che vinca lo scontro il pugile 2

La probabilità che si verifichi uno tra i due casi possibili è sempre 1, infatti c'è sempre un vincitore in un incontro. Pertanto la probabilità totale di A è sempre pari a 1, pertanto la somma delle probabilità di tutti gli eventi deve essere 1:

$P(A) = P(a_1)+P(a_2) = 1$

### Implicazioni logiche

Le proposizioni in logica sono dette eventi nella terminologia dei sistemi intelligenti: quando vogliamo collegarle e trarre conclusioni possiamo svolgere delle implicazioni logiche.

Ad esempio " Carie $\rightarrow$ Mal di denti " è una implicazione che suppone che avere le carie sia condizione sufficiente per avere il mal di denti. In realtà non è sempre detto: il mal di denti può essere dovuto in seguito a diverse cause, ad esempio

" Carie OR Problemi gengive OR ascessi OR .... $\rightarrow$ Mal di denti"

Come abbiamo visto, quando svolgiamo delle implicazioni logiche (le stesse che potrebbe fare il nostro sistema intelligente) abbiamo diversi problemi:

- Lazyness : non si riescono ad elencare tutte le situazioni
- Ignoranza teorica : non abbiamo una conoscenza sufficiente a spiegare tutto nel dominio di interesse
- Ignoranza pratica : anche se avessimo una conoscenza completa, non riusciamo a conoscere le condizioni esatte in cui si verifica

Pertanto, quando svolgiamo delle implicazioni logiche dobbiamo corredarle di un certo **grado di credenza (belief)**, ovvero quanto siamo confidenti nella implicazione che stiamo affermando. 

Questa infatti potrà rivelarsi vera o falsa con una certa probabilità, la quale è basata sulla conoscenza che abbiamo a priori e non sull'evento che si è già verificato (infatti la conoscenza a priori si ottiene dall'analisi dei pazienti precedenti, ovvero delle esperienze precedenti) 

In sostanza lo possiamo considerare come un grado di confidenza con cui possiamo fare quella affermazione.

### Combinazione di probabilità con connettivi logici (probabilità congiunta)

Quando consideriamo due eventi indipendenti e proviamo a cercare una loro combinazione svolgiamo il prodotto fra le probabilità degli eventi presi singolarmente. Tuttavia, questo vuole anche dire che cercando la loro combinazione, ho un rapporto logico fra quei due eventi.

Ad esempio, qual è la probabilità per cui lanciando due dadi a sei facce contemporaneamente esca 12? In altri termini potrei dire qual è la probabilità che esca 6 per il dado A AND che esca 6 per il dado B? Come possiamo notare, abbiamo una congiunzione in termini di AND fra questi due eventi indipendenti.

Banalmente basta svolgere 
$$
P(N=12) = P(dado_A = 6)\ AND\ P(dado_b=6) = P(dado_A = 6) \cdot P(dado_b=6) = \frac16 \cdot \frac16 = \frac{1}{36}
$$
#### Probabilità di 2 variabili conjgiunte

Riassumendo dall'esempio precedente, dati due eventi A e B ***indipendenti*** possiamo dire che

![](img/indip.png)

- La loro **intersezione (AND)** è data dal **prodotto** delle probabilità.
  - $ P(X = A\ \ AND\ \ Y = B) = P(X=A) \cdot P(Y=B) $
- La loro **unione (OR)** è data dalla **somma** delle probabilità
  - $ P(X = A\ \ AND\ \ Y = B) = P(X=A) + P(Y=B) $

Altrimenti, se A e B sono ***dipendenti*** fra loro, allora 

<img src="img/dip.png" style="zoom: 67%;" />

- La loro **intersezione (AND)** è data dalla **somma** delle probabilità meno la loro unione (questo perchè l'intersezione viene contata due volte nella somma e quindi è possibile isolarla rimuovendo tutto il resto, ovvero l'unione)
  - $ P(X = A\ \ OR\ \ Y = B) = P(X=A) + P(Y=B) - P(X = A\ \ OR\ \ Y = B) $
- La loro **intersezione (AND)** è data dalla **somma** delle probabilità meno la loro intersezione (questo perchè l'intersezione viene contata due volte nella somma e quindi bisogna rimuoverla per ottenere l'unione pura)
  - $ P(X = A\ \ OR\ \ Y = B) = P(X=A) + P(Y=B) - P(X = A\ \ AND\ \ Y = B) $

### Probabilità condizionata

Cosa succede se l'evento A dipende dall'evento B e quest'ultimo è indipendente? Facciamo un esempio:

"Supponiamo di aver tirato un dado e aver ottenuto 5: qual è la probabilità per cui tirando il secondo dado esca come somma 11?"

Possiamo dedurre che, avendo 5, è necessario che esca un 6 per avere somma 11 pertanto la probabilità è 1/6.
$$
P(N = 11\ |\ Dado_A = 5) = 1/6
$$
Come possiamo vedere abbiamo una incertezza molto minore rispetto a quando dobbiamo tirare i due dadi contemporaneamente ($ P(N = 11) = 2/36 = 1/18$), risultato ovviamente dato dal fatto che il valore di uno dei due dadi è già conosciuto.

Questa viene definita probabilità condizionata, per cui un evento A dipende dal risultato noto dell'evento B e si scrive come $ P(Y = A\ |\ X = B)$, ovvero "probabilità di A dato B"

Possiamo riformulare le probabilità congiunte definite in precedenza usando le probabilità condizionate:

- $ P(A\ \ AND\ \ B) = P(A\ |\ B) \cdot P(B) $
  - La probabilità intersezione è data dalla probabilità di B di verificarsi, moltiplicata con la probabilità di A di verificarsi dato B.

## Inferenza statistica

Una inferenza statistica è il calcolo della probabilità di un evento, a partire dall'informazione collezionata sperimentalmente. Non partiamo quindi a calcolare probabilità di eventi dall'ignoto ma abbiamo una base di dati a cui fare riferimento.

Per esempio, consideriamo di essere in uno studio dentistico e di avere nel tempo misurato la frequenza di persone con mal di denti, cavità e carie e quindi calcolato una tabella di probabilità congiunte:

![](img/dent1.png)

Questa tabella ci può aiutare a predire quanto è probabile che

- Un cliente abbia mal di denti quando entra in studio 

  ![](img/dent2.png)

  - $ P(mal\ di\ denti) = 0.108+0.012+0.016+0.064 = 0.2 = 20\% $ 

- Un cliente abbia la carie OPPURE il mal di denti

  ![](img/dent3.png)

  - Ci basta calcolare le probabilità congiunte come definito in precedenza, tenendo conto che carie e mal di denti sono eventi dipendenti

  - $$
    P(carie\ OR\ mal\ di\ denti) = \\ P(carie) + P(mal\ di\ denti) - P(carie\ AND\ mal\ di\ denti) = \\ (0.108+0.012+0.072+0.008) + (0.108+0.012+0.016+0.064) - (0.108+0.012) = \\ 0.2+0.2-0.12 = 0.28 = 28\%
    $$

- e così via ...

Quello che abbiamo svolto in alcuni di questi calcoli è stata la **marginalizzazione**: 

![](img/marg.png)

Quando abbiamo calcolato la probabilità di carie abbiamo contato solo i valori di probabilità della rispettiva riga, tenendo fissa la variabile carie e facendo variare le altre due, ovvero i casi in cui ci siano o non ci siano cavità e in cui ci sia o non ci sia il mal di denti

Queste che abbiamo avuto di fronte sono tutte **probabilità a priori**, ovvero calcolate PRIMA di conoscere qualsiasi informazione sul nostro paziente.

Se il nostro paziente che arriva ci dice che ha il mal di denti, allora queste probabilità devono essere condizionate dalla conoscenza di questo fatto: parliamo quindi di **condizionamento statistico delle variabili**.

Ricordando che una probabilità condizionata si calcola svolgendo $ P(A\ |\ B) = \frac{P(A\ AND\ B)}{P(B)}$, modifichiamo la nostra tabella di conseguenza seguendo questa formula $ P(carie\ |\ maldidenti) = \frac{P(carie\ AND\ maldidienti)}{P(maldidenti)}$

- Nel caso mal di denti sia vero, siccome ha una probabilità a-priori del 20% di accadere, divido le probabilità di quella colonna per 0.2
- Nel caso mal di denti sia falso, siccome ha una probabilità a-priori del 80% di accadere, divido le probabilità di quella colonna per 0.8

Otteniamo quindi questa tabella, al netto dei calcoli dovuti al condizionamento dato dalla nuova conoscenza

![](img/conds.png)

## Teorema di Bayes

Quando dobbiamo modellare delle relazioni strettamente di **causa-effetto**, quindi di totale dipendenza di un evento rispetto a un altro ci viene incontro il teorema di Bayes, che ponendo le basi sulla nozione di probabilità condizionata afferma che
$$
P(causa\ |\ effetto) = \frac{P(effetto\ |\ causa) \cdot P(causa)}{P(effetto)}
$$
Solitamente non conosciamo le statistiche della causa, ma possiamo misurare la frequenza dell'effetto e calcolarne una statistica per determinare quanto è probabile che quell'effetto sia dato da quella causa.

Per esempio, un dottore durante una diagnosi conosce bene che una determinata causa genera determinati sintomi in un paziente $P(sintomi\ |\ causa)$, tuttavia in certi casi osserva i sintomi ma non riesce a determinarne la causa e quindi deve fare affidamento su un calcolo di probabilità $P(causa\ |\ sintomi)$. 

Grazie al teorema di Bayes è possibile fare queste inferenze in modo accurato.

Ma proviamo a fare un paio di esempi per chiarire le idee

### Taxi blu o verde?

![](img/extax.png)

Saremmo portati a pensare che se l'affidabilità del testimone è del 80% allora tale è la probabilità che ci abbia visto giusto: purtroppo questa è solo una porzione della realtà che dobbiamo considerare infatti dobbiamo considerare la probabilità a-priori che quello sia stato effettivamente un taxi blu!

![](img/extaxtree.png)

Come vediamo in questo albero decisionale, la probabilità che il taxi nell'incidente fosse blu è solo del 15%, combinandolo poi con l'affidabilità dell'80% avremo che (applicando Bayes)
$$
P(Taxi = blu\ |\ Colore = blu ) = \frac{P(Colore = blu\ |\ Taxi = blu) \cdot P(Taxi=blu)}{P(Colore = blu)}
$$
Quello che vogliamo calcolare quindi è: qual è la probabilità che il taxi sia davvero blu, se il colore osservato era blu?

Troviamo i componenti del secondo membro di questa equazione:

- $P(Colore = blu\ |\ Taxi = blu)$ è pari a 0.8 poichè è l'affidabilità data del testimone (così come l'opposto nel caso fosse verde ovvero $P(Colore = verde\ |\ Taxi = verde)$)

- $P(Taxi = blu)$ è pari a 0.15 poiché nella città la probabilità di incontrare un taxi blu è il 15% 

- $P(Colore = blu)$ è data dalla probabilità marginale di aver visto il colore blu (incluso anche quando non era tale commettendo un errore)

  - $$
    P(Colore = blu) = P(Colore = blu\ |\ Taxi = blu) \cdot P(Taxi = blu) + \\ P(Colore = blu\ |\ Taxi = verde) \cdot P(Taxi = verde) = \\ 0.8*0.15+0.2*0.85 = 0.29
    $$

Pertanto la probabilità finale che il testimone abbia visto blu e sia il colore giusto è
$$
P(Taxi = blu\ |\ Colore = blu ) = \frac{0.8*0.15}{0.29} = 0.41
$$
Il nostro testimone ha in realtà solamente il 41% di probabilità di essere nel giusto, non 80% come avremmo potuto pensare!

### Estensione a più variabili (naive Bayes)

Applicare il teorema di Bayes a eventi più complessi (congiunti, ad esempio) rende i calcoli più difficili. Per esempio in $P(X\ |\ Y_1\ AND\ Y_2)$ dovremmo considerare il calcolo delle probabilità congiunte prima di poter applicare Bayes.

Fortunatamente, in molti casi è sufficiente una applicazione **naive** di Bayes, tale per cui possiamo considerare gli effetti indipendenti tra loro ed entrambi dipendono da una stessa causa.

Nell'esempio di prima avremmo
$$
P(X\ |\ Y_1\ AND\ Y_2) = P(Y_1\ AND\ Y_2\ |\ X) \cdot P(X) = \\ P(Y_1\ |\ X) \cdot P(Y_2\ |\ X)
$$
e quindi in generale avremmo che

![](img/nvb.png)

